﻿using System;
using System.Web.Http.Filters;
using System.Web.Http.Controllers;
using System.Web.Http.Tracing;
using System.Web.Http;
using DealerWizard.Service.Helpers;
using DealerWizard.Service.Contracts;

namespace DealerWizard.Service.ActionFilters
{
    public class LoggingFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext filterContext)
        {
            //var userSessionManager = GlobalConfiguration.Configuration.DependencyResolver.GetService(typeof(IUserSessionManager)) as IUserSessionManager;

            //GlobalConfiguration.Configuration.Services.Replace(typeof(ITraceWriter), new NLogger(null));
            var trace = GlobalConfiguration.Configuration.Services.GetTraceWriter();
            trace.Info(filterContext.Request, "Controller : " + filterContext.ControllerContext.ControllerDescriptor.ControllerType.FullName + Environment.NewLine + "Action : " + filterContext.ActionDescriptor.ActionName, "JSON", filterContext.ActionArguments);
        }
    }
}