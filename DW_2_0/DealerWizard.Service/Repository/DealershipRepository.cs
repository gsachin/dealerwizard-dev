﻿using System.Collections.Generic;
using DealerWizard.Service.Models;
using DealerWizard.Data.Shared;
using System.Data;
using DealerWizard.Service.Contracts;
using System;

namespace DealerWizard.Service.Repository
{
    public class DealershipRepository : IDealershipRepository
    {
        public IEnumerable<DealershipAttribute> GetDealershipDetails(int DealerId)
        {
            var dealerDetails = new List<DealershipAttribute>();

            AdoFramework.ReadRecords(
                connectionString: ConnectionString.Get(),
                storedProcedureName: "GetActualServiceData",
                parameters: new List<AdoFramework.Parameter>() {
                    new AdoFramework.Parameter("Dealer_key", SqlDbType.Int, DealerId) 
                },
                 processors: (reader =>
                 {
                     dealerDetails.Add(
                         new DealershipAttribute
                         {
                             DealerId = AdoFramework.ReadInt("Dealer_key", reader),
                             Dated = AdoFramework.ReadDateTime("Actual_Date", reader),
                             NoOfTechnician = AdoFramework.ReadDouble("Tech_Distinct_Count", reader),
                             WorkingDays = AdoFramework.ReadDouble("Working_Days", reader),
                             ServiceActualHours = AdoFramework.ReadDouble("Svc_Actual_Hours", reader)
                         }
                         );
                 }));

            return dealerDetails;
        }

        public List<VehicleMake> GetVehicleMakeByDealer(int DealerId)
        {
            var _ls = new List<VehicleMake>();

            _ls.AddRange(new VehicleMake[] {new VehicleMake()
            {
                MakeId = 1,
                MakeName = "Model 1"
            },
            new VehicleMake()
            {
                MakeId = 2,
                MakeName = "Model 2"
            },
            new VehicleMake()
            {
                MakeId = 3,
                MakeName = "Model 3"
            }
            }
            );
            return _ls;
        }

        public List<VehicleModel> GetVehicleMakeModelByDealer(int DealerId)
        {

            var _ls = new List<VehicleModel>();
            AdoFramework.ReadRecords(
                connectionString: ConnectionString.Get(),
                storedProcedureName: "spGetMakeModel",
                parameters: new List<AdoFramework.Parameter>() {
                    new AdoFramework.Parameter("Dealer_key", SqlDbType.Int, DealerId) 
                },
                 processors: (reader =>
                 {
                     _ls.Add(
                         new VehicleModel
                         {
                             MakeId = AdoFramework.ReadInt("Make_key", reader),
                             MakeName = AdoFramework.ReadString("Make_Desc", reader),
                             ModelId = AdoFramework.ReadInt("Model_Key", reader),
                             ModelName = AdoFramework.ReadString("Model_Desc", reader),
                             VehicleType= AdoFramework.ReadString("Vehicle_Type",reader)
                         }
                         );
                 }));
            return _ls;
        }
        
        public DealerSetting GetDealerSetting(int dealerkey)
        {
            DealerSetting dealerSetting = null;
            AdoFramework.ReadRecords(ConnectionString.Get(), "spDefaultEquityForDealer",
                new List<AdoFramework.Parameter>() { new AdoFramework.Parameter("@V_Dealer_Key", SqlDbType.VarChar, dealerkey) }, (reader) => {
                    dealerSetting = new DealerSetting
                    {
                        DefaultBookColumn = AdoFramework.ReadString("DLR_Default_UC_Book_Column", reader),
                        DefaultPercentValue = AdoFramework.ReadIntNullable("DLR_Default_UC_Percent_Of_Book", reader)

                    };
                });

            return dealerSetting;
        }

        public IEnumerable<BookValueItem> GetDealerWiseBookValueList(int dealerkey)
        {
            List<BookValueItem> BookValueItems = new List<BookValueItem>();
            AdoFramework.ReadRecords(ConnectionString.Get(), "spGetBlackBookItemsForDealer",
                new List<AdoFramework.Parameter>() { new AdoFramework.Parameter("@Dealer_Key", SqlDbType.VarChar, dealerkey) }, (reader) => {
                    BookValueItems.Add(
                        new BookValueItem
                        {                         
                            BookValueText = AdoFramework.ReadString("BV_Text", reader),
                            BookValueName = AdoFramework.ReadString("BV_Value", reader),
                            SortOrder = AdoFramework.ReadInt("BV_Sort_Order", reader)
                        });
                });

            return BookValueItems;
        }
    }
}