﻿using System;
using DealerWizard.Service.Contracts;
using System.Runtime.Caching;

namespace DealerWizard.Service.Repository
{
    public class InMemoryCacheManager<T> : ICacheManager<T>
    {
        public void DeleteCacheItem(string key)
        {
            var cache = MemoryCache.Default;
            if (cache.Contains(key))
            {
                cache.Remove(key);
            }
        }

        public T GetCacheItemBy(string key)
        {
            var cache = MemoryCache.Default;
            if (!cache.Contains(key))
            {
                return default(T);
            }
            return (T)cache.GetCacheItem(key).Value;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <param name="key"></param>
        /// <param name="expiration">In Absolute expiration type cache expire in 4 hours; whereas Sliding expiration defaults to 2 hours</param>
        public void InsertCacheItem(T item, string key, CacheExpirationType expiration = CacheExpirationType.Absolute)
        {
            var cache = MemoryCache.Default;
            if (!cache.Contains(key))
            {
                CacheItemPolicy expirationPolicy = new CacheItemPolicy();
                if (expiration.Equals(CacheExpirationType.Absolute))
                {
                    expirationPolicy.AbsoluteExpiration = new DateTimeOffset(DateTime.Now.AddHours(4));
                }

                if (expiration.Equals(CacheExpirationType.Sliding))
                {
                    expirationPolicy.SlidingExpiration = new TimeSpan(2, 0, 0);
                }

                cache.Add(new CacheItem(key, item), expirationPolicy);
            }
        }
        
    }
    
}