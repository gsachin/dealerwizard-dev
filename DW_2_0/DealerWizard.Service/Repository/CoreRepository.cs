﻿using DealerWizard.Data.Shared;
using DealerWizard.Service.Contracts;
using DealerWizard.Service.Models;
using System.Collections.Generic;
using System;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using System.Web.Security;

namespace DealerWizard.Service.Repository
{
    public class CoreRepository :ICoreRepository
    {
        public IEnumerable<UserAssociatedDealership> GetAllDealers()
        {
            var allDealers = new List<UserAssociatedDealership>();

            AdoFramework.ReadRecords(ConnectionString.Get(), "spGetDealers", new List<AdoFramework.Parameter>(), (reader) => {
                allDealers.Add(
                    new UserAssociatedDealership
                    {
                        Id = AdoFramework.ReadInt("DEALER_KEY", reader),
                        Name = AdoFramework.ReadString("DLR_NAME", reader),
                        ReferenceDealerId = AdoFramework.ReadGuid("DEALER_ID", reader)
                    });
            });

            //var associatedLegacyDealerships = GetAllDwDealers();

            //foreach (var legacyDealer in associatedLegacyDealerships)
            //{
            //    var dealer = allDealers.FirstOrDefault(i => i.ReferenceDealerId.Equals(legacyDealer.Id));
            //    if (dealer != null)
            //    {
            //        dealer.IsDefault = legacyDealer.IsDefault;
            //    }
            //}

            return allDealers;
        }

        public IEnumerable<LegacyDealership> GetAllDwDealers()
        {
            var associatedDealers = new List<LegacyDealership>();

            AdoFramework.ReadRecords(ConnectionString.GetDWConnectionString(), "usp_tblDealerships_GET_All_Dealers", new List<AdoFramework.Parameter>(), (reader) => {
                associatedDealers.Add(
                    new LegacyDealership
                    {
                        Id = AdoFramework.ReadGuid("DLR_ID", reader),
                        Name = reader["DLR_NAME"].ToString()
                    });
            });

            return associatedDealers;
        }

        public IEnumerable<UserAssociatedDealership> GetAssociatedDealersByUser(Guid user)
        {
            var associatedDealers = new List<UserAssociatedDealership>();

            var associatedLegacyDealerships = GetAllActiveDwDealersByUser(user) as IList<LegacyDealership>;
            var newDealerships = GetAllDealers();

            if (!associatedLegacyDealerships.Any())
            {
                var userPref = GetUserPreferences(user);
                associatedLegacyDealerships.Add(new LegacyDealership { Id = userPref.DLR_ID, IsDefault = true, Name = userPref.DLR_Name });
            }

            foreach (var legacyDealer in associatedLegacyDealerships)
            {
                var dealer = newDealerships.FirstOrDefault(i => i.ReferenceDealerId.Equals(legacyDealer.Id));
                if (dealer != null)
                {
                    dealer.IsDefaultDealer = legacyDealer.IsDefault;
                    associatedDealers.Add(dealer);
                }
            }

            return associatedDealers;
        }

        public IEnumerable<LegacyDealership> GetAllActiveDwDealersByUser(Guid user)
        {
            var associatedDealers = new List<LegacyDealership>();

            AdoFramework.ReadRecords(ConnectionString.GetDWConnectionString(), "usp_tblDealerships_GET_Active_Dealers",
                new List<AdoFramework.Parameter>() {
                    new AdoFramework.Parameter("@lp_bit_Sort_IND", SqlDbType.Bit, 1),
                    new AdoFramework.Parameter("@lp_ui_UserID", SqlDbType.UniqueIdentifier, user)
                },
                (reader) =>
                {
                    associatedDealers.Add(
                        new LegacyDealership
                        {
                            Id = AdoFramework.ReadGuid("DLR_ID", reader),
                            Name = AdoFramework.ReadString("DLR_NAME", reader),
                            IsDefault = AdoFramework.ReadBool("Access_Default", false, reader)
                        });
                });

            return associatedDealers;
        }

        public IEnumerable<LegacyDealership> GetAllActiveDwDealers()
        {
            var associatedDealers = new List<LegacyDealership>();

            AdoFramework.ReadRecords(ConnectionString.GetDWConnectionString(), "usp_tblDealerships_GET_Active_Dealers",
                new List<AdoFramework.Parameter>(),
                (reader) =>
                {
                    associatedDealers.Add(
                        new LegacyDealership
                        {
                            Id = AdoFramework.ReadGuid("DLR_ID", reader),
                            Name = AdoFramework.ReadString("DLR_NAME", reader)
                        });
                });

            return associatedDealers;
        }

        public IEnumerable<UserAssociatedDealership> GetAllActiveDealers()
        {
            var associatedDealers = new List<UserAssociatedDealership>();

            var associatedLegacyDealerships = GetAllActiveDwDealers();
            var newDealerships = GetAllDealers();

            foreach (var legacyDealer in associatedLegacyDealerships)
            {
                var dealer = newDealerships.FirstOrDefault(i => i.ReferenceDealerId.Equals(legacyDealer.Id));
                if (dealer != null)
                {
                    //dealer.IsDefault = legacyDealer.IsDefault;
                    associatedDealers.Add(dealer);
                }
            }

            return associatedDealers;
        }

        public UserPreference GetUserPreferences(Guid user)
        {
            UserPreference userPreferences = null;
            AdoFramework.ReadRecords(ConnectionString.GetDWConnectionString(), "usp_tblUserAttributes_GET_By_USR_ID", 
                new List<AdoFramework.Parameter>() { new AdoFramework.Parameter("@lp_ui_USR_ID", SqlDbType.UniqueIdentifier, user) }, (reader) => {
                    userPreferences = new UserPreference
                    {
                        DLR_ID = AdoFramework.ReadGuid("DLR_ID", reader),
                        DLR_Name = AdoFramework.ReadString("DLR_Name", reader),
                        Landing_Page = AdoFramework.ReadString("Landing_Page", reader),
                        Title = AdoFramework.ReadString("Title", reader),
                    };
            });

            return userPreferences;
        }

        public IEnumerable<MenuItems> MenuItemsByUserId(Guid UserId)
        {
            var menuItems = new List<MenuItems>();
            AdoFramework.ReadRecords(
                connectionString: ConnectionString.GetDWConnectionString(),
                storedProcedureName: "spGetDWMenuByUserID",
                parameters: new List<AdoFramework.Parameter>() {
                    new AdoFramework.Parameter("@userid", SqlDbType.UniqueIdentifier, UserId)

                },     
                 processors: (reader =>
                 {
                     menuItems.Add(
                         new MenuItems
                         {
                             MenuId = AdoFramework.ReadInt("MenuId", reader),
                             Name = AdoFramework.ReadString("Name", reader),
                             URL = AdoFramework.ReadString("URL", reader),
                             Active = AdoFramework.ReadBool("Active", reader),
                             ParentMenuId = AdoFramework.ReadIntNullable("ParentMenuId", reader),
                             Order = AdoFramework.ReadIntNullable("MenuOrder", reader),
                             IconClassName = AdoFramework.ReadString("IconClassName", reader)
                         }
                         );
                 }));
            return menuItems;
        }

        public string GetUserNameByEmailTrackingCode(string etc)
        {
            DataSet ds = new DataSet();
            var username = "";
            var connectionString = ConnectionString.GetDWConnectionString();
            Guid gidKey = Guid.Empty;

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmdGetTracking = new SqlCommand("usp_GET_UPDATE_EmailTrackingCode", con);
                cmdGetTracking.CommandType = CommandType.StoredProcedure;
                cmdGetTracking.Parameters.Add("@lp_ui_ETC_ID", SqlDbType.UniqueIdentifier).Value = new Guid(etc);

                using (SqlDataAdapter da = new SqlDataAdapter(cmdGetTracking))
                {
                    con.Open();
                    da.Fill(ds);
                    con.Close();
                }
            }

            if (ds.Tables.Count > 0)
            {
                DataTable dt = ds.Tables.Count > 1 ? ds.Tables[1] : ds.Tables[0];

                if (dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];

                    if ((byte)dr["ReturnCode"] == 1 && !Convert.IsDBNull(dr["LastUserId"]))
                    {
                        gidKey = (Guid)dr["LastUserId"];
                        MembershipUser myUser = Membership.GetUser(gidKey, true);
                        username=myUser.UserName;
                        //validUser = true;
                    }
                }
            }
            return username;
        }

        public UserLoginTrack LogUserLoginTrack(Guid userId, string remoteIpAddress, string userAgent)
        {
            UserLoginTrack loginTrack = null;
            AdoFramework.ReadRecords(
                connectionString: ConnectionString.GetDWConnectionString(),
                storedProcedureName: "spTrackUserLogin",
                parameters: new List<AdoFramework.Parameter>() {
                    new AdoFramework.Parameter("@userid", SqlDbType.UniqueIdentifier, userId),
                    new AdoFramework.Parameter("@remoteIdAddress", SqlDbType.VarChar, remoteIpAddress),
                    new AdoFramework.Parameter("@userAgent", SqlDbType.VarChar, userAgent),
                    new AdoFramework.Parameter("@loginTrackingId", SqlDbType.UniqueIdentifier, DBNull.Value)
                },
                 processors: (reader =>
                 {
                     loginTrack = new UserLoginTrack {
                         Id = AdoFramework.ReadLong("Id",reader),
                         LoginTrackingId = AdoFramework.ReadGuid("LoginTrackingId", reader),
                         UserId = AdoFramework.ReadGuid("UserId", reader)
                     };
                    
                 }));
            return loginTrack;
        }
    }
}