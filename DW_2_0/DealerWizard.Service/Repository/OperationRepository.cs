﻿using DealerWizard.Service.Models;
using DealerWizard.Data.Shared;
using System.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using DealerWizard.Service.Contracts;

namespace DealerWizard.Service.Repository
{
    public class OperationRepository : IOperationRepository
    {
        #region Configurations

        public string GetKPISPNameByType(string type)
        {
            if (type.ToLower() == "newcar") return "spGetNewCarKPIByDealerandDate";
            if (type.ToLower() == "usedcar") return "spGetUsedCarKPIByDealerandDate";
            if (type.ToLower() == "variable") return "spGetVariableKPIByDealerandDate";
            if (type.ToLower() == "fi") return "spGetFIKPIByDealerandDate";
            if (type.ToLower() == "inventory") return "spGetInventoryKPIByDealerandDate";
            if (type.ToLower() == "service") return "spGetServiceKPIByDealerandDate";
            if (type.ToLower() == "dealership") return "spGetDlrOverviewKPIByDealerandDate";
            return string.Empty;
        }

        public string GetSMSPNameByType(string type)
        {
            if (type.ToLower() == "newcar" || type.ToLower() == "usedcar") return "spGetSalesMeasuresByDealerandDate";
            if (type.ToLower() == "variable") return "spGetVariableSalesMeasuresByDealerandDate";
            if (type.ToLower() == "fi") return "spGetFIMeasuresByDealerandDate";
            if (type.ToLower() == "inventory") return "spGetHInventoryMeasuresByDealerandDate";
            if (type.ToLower() == "service") return "spGetServiceMeasuresByDealerandDate";
            if (type.ToLower() == "dealership") return "spGetDlrOverviewMeasuresByDealerandDate";
            return string.Empty;
        }

        private int GetVehicleTypeByType(string type)
        {
            if (type.ToLower() == "newcar") return 1;
            if (type.ToLower() == "usedcar") return 0;
            return -1;
        }
        #endregion


        public IEnumerable<KpiItems> GetKpiData(string type, int dealerKey, string startDate, string endDate)
        {
            var kpiItems = new List<KpiItems>();
            AdoFramework.ReadRecords(
                connectionString: ConnectionString.Get(),
                storedProcedureName: GetKPISPNameByType(type),
                parameters: new List<AdoFramework.Parameter>() {
                    new AdoFramework.Parameter("Dealer_key", SqlDbType.VarChar, dealerKey) ,
                    new AdoFramework.Parameter("StartDate", SqlDbType.VarChar,DateTime.ParseExact(startDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy-MM-dd")),//
                    new AdoFramework.Parameter("SelectedDate", SqlDbType.VarChar,DateTime.ParseExact(endDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy-MM-dd"))//
                },
                 processors: (reader =>
                 {
                     kpiItems.Add(
                         new KpiItems
                         {
                             measurename = AdoFramework.ReadString("MeasureName", reader),
                             ActualValue = AdoFramework.ReadDouble("ActualValue", reader),
                             GY = AdoFramework.ReadDouble("GY", reader),
                             YR = AdoFramework.ReadDouble("YR", reader)
                             //DataFormat = AdoFramework.ReadChar("DataFormat", reader)
                         }
                         );
                 }));
            return kpiItems;
        }

        public object[] GetSalesMeasures(string type, int dealerKey, string startDate, string endDate, string periodType, char previousPeriod)
        {
            var salesMeasureItems = new List<SmallMultipleItem>();
            var salesAggrgatedSum = new List<SmallMultipleItem>();
            object[] obj = new object[2];
            AdoFramework.ReadRecords(
                 ConnectionString.Get(),
                 GetSMSPNameByType(type),GetParameters(type, dealerKey, startDate, endDate, periodType, previousPeriod),
                 (reader => { salesMeasureItems.Add(new SmallMultipleItem { Columns = GetColumnsDataFromReader(reader) }); }),
                 (reader => { salesAggrgatedSum.Add(new SmallMultipleItem { Columns = GetColumnsDataFromReader(reader) }); })
                );

            obj[0] = salesMeasureItems;
            obj[1] = salesAggrgatedSum;
            return obj;
        }

        private IEnumerable<AdoFramework.Parameter> GetParameters(string type, int dealerKey, string startDate, string endDate, string periodType, char previousPeriod)
        {
            List<AdoFramework.Parameter> para = new List<AdoFramework.Parameter>();
            para.Add(new AdoFramework.Parameter("@Dealer_Key", SqlDbType.VarChar, dealerKey));
            para.Add(new AdoFramework.Parameter("@StartDate", SqlDbType.VarChar, DateTime.ParseExact(startDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy-MM-dd")));
            para.Add(new AdoFramework.Parameter("@SelectedDate", SqlDbType.VarChar, DateTime.ParseExact(endDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy-MM-dd")));
            para.Add(new AdoFramework.Parameter("@PeriodType", SqlDbType.VarChar, periodType));
            para.Add(new AdoFramework.Parameter("@PreviousPeriod", SqlDbType.Char, previousPeriod));//previousPeriod ? "Y":"N")
            if (type.ToLower() == "newcar" || type.ToLower() == "usedcar")
                para.Add(new AdoFramework.Parameter("@VehicleType", SqlDbType.Char, GetVehicleTypeByType(type)));
            return para;
        }

        private List<Item> GetColumnsDataFromReader(SqlDataReader reader)
        {
            List<Item> Columns = new List<Item>();
            for(int i=0;i<reader.FieldCount;i++)
            {
                Item Column = new Item();
                Column.ColumnName = reader.GetName(i);
                Column.ColumnValue = reader.GetValue(i);
                Column.ColumnType = reader.GetValue(i).GetType();
                Columns.Add(Column);
            }
            return Columns;
        }

        public List<object> FilterResultSales(IEnumerable<SmallMultipleItem> result, string type)
        {
            var resultArr = new List<object>();
            var measureCount = 1;

            List<Item> AllColumns = new List<Item>();

            if (result.Count() > 0)
            {
                //Generates AllColumn Collection for Query Column Based
                foreach (var SMItems in result)
                {
                    DateTime ReportDate = (DateTime)SMItems.Columns.Where(c => c.ColumnName == "ReportDate").ToList()[0].ColumnValue;
                    foreach (var itm in SMItems.Columns) itm.ReportDate = ReportDate;
                    AllColumns.AddRange(SMItems.Columns);
                }

                //Customize Format Of Collection
                for (int i = 0; i < result.ElementAt(0).Columns.Count; i++)
                {
                    var ColName = result.ElementAt(0).Columns[i].ColumnName;
                    if (ColName.ToLower() != "year" && ColName.ToLower() != "month" && ColName.ToLower() != "reportdate")
                    {
                        //For Each COlumn , Create Measure
                        resultArr.AddRange(AllColumns
                            .Where(c => c.ColumnName == result.ElementAt(0).Columns[i].ColumnName)
                            .Select(val => new
                            {
                                Key = "Measure" + measureCount.ToString(),
                                Name = val.ColumnName,
                                Date = (val.ReportDate.HasValue) ? val.ReportDate.Value.ToString("yyyy-MM-dd") : "",
                                Value = val.ColumnValue
                            }));
                        measureCount++;
                    }
                }
            }
            
            //var measure4 = result.Select(i => new { Key = "Measure4", Name = "FI Gross", Date = (i.Dated.HasValue) ? i.Dated.Value.ToString("yyyy-MM-dd") : "", Value = i.FIGross });
            //var measure2 = result.Select(i => new { Key = "Measure2", Name = "Sales Gross", Date = (i.Dated.HasValue) ? i.Dated.Value.ToString("yyyy-MM-dd") : "", Value = i.SalesGross });
            //var measure1 = result.Select(i => new { Key = "Measure1", Name = "Units Del", Date = (i.Dated.HasValue) ? i.Dated.Value.ToString("yyyy-MM-dd") : "", Value = i.UnitsDelivered });
            //var measure7 = result.Select(i => new { Key = "Measure7", Name = "Sales PRU", Date = (i.Dated.HasValue) ? i.Dated.Value.ToString("yyyy-MM-dd") : "", Value = i.SalesPRU });
            //var measure8 = result.Select(i => new { Key = "Measure8", Name = "FI PRU", Date = (i.Dated.HasValue) ? i.Dated.Value.ToString("yyyy-MM-dd") : "", Value = i.FIPRU });

            //var measure6 = result.Select(i => new { Key = "Measure6", Name = "Avg Days To Sale", Date = (i.Dated.HasValue) ? i.Dated.Value.ToString("yyyy-MM-dd") : "", Value = i.AverageDayToSale });
            //var measure3 = result.Select(i => new {Key ="Measure3", Name = "Avg Sales Gross", Date = (i.Dated.HasValue) ? i.Dated.Value.ToString("yyyy-MM-dd") : "", Value = (i.AverageSalesGross) });
            //var measure5 = result.Select(i => new { Key = "Measure5", Name = "Avg FI Gross", Date = (i.Dated.HasValue) ? i.Dated.Value.ToString("yyyy-MM-dd") : "", Value = (i.AverageFIGross) });

            
            //resultArr.AddRange(measure1);
            //resultArr.AddRange(measure2);
            //resultArr.AddRange(measure7);
            //resultArr.AddRange(measure4);
            //resultArr.AddRange(measure8);
            //resultArr.AddRange(measure6);

            return resultArr;

        }

        public List<object> FilterSalesAggregatedResult(IEnumerable<SmallMultipleItem> result, string type)
        {
            var resultArr = new List<object>();
            var measureCount = 1;

            List<Item> AllColumns = new List<Item>();

            if (result.Count() > 0)
            {
                //Generates AllColumn Collection for Query Column Based
                foreach (var SMItems in result)
                {
                    AllColumns.AddRange(SMItems.Columns);
                }

                //Customize Format Of Collection
                for (int i = 0; i < result.ElementAt(0).Columns.Count; i++)
                {
                    var ColName = result.ElementAt(0).Columns[i].ColumnName;
                    if (ColName.ToLower() != "year" && ColName.ToLower() != "month" && ColName.ToLower() != "reportdate")
                    {
                        //For Each COlumn , Create Measure
                        resultArr.AddRange(AllColumns
                        .Where(c => c.ColumnName == result.ElementAt(0).Columns[i].ColumnName)
                        .Select(val => new
                        {
                            Name = val.ColumnName,
                            Value = val.ColumnValue
                        }));
                        measureCount++;
                    }
                }
            }

            //foreach (var SMItems in result)
            //{
            //    foreach (var SMItem in SMItems.Columns)
            //    {
            //        resultArr.AddRange(result.Select(i => new { Name = SMItem.ColumnName, Value = SMItem.ColumnValue }));
            //        measureCount++;
            //    }
            //}

            //var measure4 = result.Select(i => new { Name = "FI Gross", Value = i.FIGross });
            //var measure2 = result.Select(i => new { Name = "Sales Gross", Value = i.SalesGross });
            //var measure1 = result.Select(i => new { Name = "Units Del", Value = i.UnitsDelivered });
            //var measure7 = result.Select(i => new { Key = "Measure7", Name = "Sales PRU", Date = (i.Dated.HasValue) ? i.Dated.Value.ToString("yyyy-MM-dd") : "", Value = i.SalesPRU });
            //var measure6 = result.Select(i => new { Name = "Avg Days To Sale", Value = i.AverageDayToSale });
            //var measure8 = result.Select(i => new { Key = "Measure8", Name = "FI PRU", Date = (i.Dated.HasValue) ? i.Dated.Value.ToString("yyyy-MM-dd") : "", Value = i.FIPRU });
            //// var measure3 = result.Select(i => new {Name = "Avg Sales Gross", Value = (i.AverageSalesGross) });
            ////var measure5 = result.Select(i => new {Name = "Avg FI Gross",  Value = (i.AverageFIGross) });

            //var resultArr = new List<object>();
            //resultArr.AddRange(measure1);
            //resultArr.AddRange(measure2);
            //resultArr.AddRange(measure7);
            //resultArr.AddRange(measure4);
            //resultArr.AddRange(measure8);
            //resultArr.AddRange(measure6);

            return resultArr;

        }
    }
}