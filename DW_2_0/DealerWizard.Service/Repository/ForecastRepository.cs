﻿using System;
using System.Collections.Generic;
using DealerWizard.Service.Models;
using DealerWizard.Data.Shared;
using System.Data;
using DealerWizard.Service.Contracts;

namespace DealerWizard.Service.Repository
{
    public class ForecastRepository : IForecastRepository
    {
        public List<ForecastedSale> GetForecastedSale(int dealerId, string startDate, string endDate)
        {
            var _ls = new List<ForecastedSale>();
         
            AdoFramework.ReadRecords(
                connectionString: ConnectionString.Get(),
                storedProcedureName: "GetForecastData",
                parameters: new List<AdoFramework.Parameter>() {
                    new AdoFramework.Parameter("Dealer_key", SqlDbType.BigInt, dealerId) ,
                    new AdoFramework.Parameter("StartDate", SqlDbType.VarChar,DateTime.ParseExact(startDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy-MM-dd")),
                    new AdoFramework.Parameter("EndDate", SqlDbType.VarChar,DateTime.ParseExact(endDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy-MM-dd"))
                },
                 processors: (reader =>
                 {
                     _ls.Add(
                         new ForecastedSale
                         {
                             Dealer_Key = AdoFramework.ReadInt("Dealer_Key", reader),
                             Dlr_Name = AdoFramework.ReadString("Dlr_Name", reader),
                             Make_Key = AdoFramework.ReadInt("Make_Key", reader),
                             Make_Desc = AdoFramework.ReadString("Make_Desc", reader),
                             Model_Key = AdoFramework.ReadInt("Model_Key", reader),
                             Model_Desc = AdoFramework.ReadString("Model_Desc", reader),
                             New_Used = AdoFramework.ReadInt("New_Used", reader),
                             Date_Key = AdoFramework.ReadInt("Date_Key", reader),
                             FullDate= AdoFramework.ReadString("FullDate", reader),
                             Forecast_Units_Delivered = AdoFramework.ReadDouble("Forecast_Units_Delivered", 0d, reader),
                             Forecast_Total_Gross = AdoFramework.ReadDouble("Forecast_Total_Gross",0d, reader),
                             Forecast_Sales_Gross = AdoFramework.ReadDouble("Forecast_Sales_Gross", 0d, reader),
                             Forecast_FI_Gross = AdoFramework.ReadDouble("Forecast_FI_Gross", 0d, reader),
                             Forecast_Financed = AdoFramework.ReadDouble("Forecast_Financed", 0d, reader),
                             Forecast_GAP = AdoFramework.ReadDouble("Forecast_GAP", 0d, reader),
                             Forecast_Maint = AdoFramework.ReadDouble("Forecast_Maint", 0d, reader),
                             Forecast_SVC = AdoFramework.ReadDouble("Forecast_SVC", 0d, reader),
                             Lower80_Units_Delivered = AdoFramework.ReadDouble("Lower80_Units_Delivered", 0d, reader),
                             Lower80_Total_Gross = AdoFramework.ReadDouble("Lower80_Total_Gross", 0d, reader),
                             Lower80_Sales_Gross = AdoFramework.ReadDouble("Lower80_Sales_Gross", 0d, reader),
                             Lower80_FI_Gross = AdoFramework.ReadDouble("Lower80_FI_Gross", 0d, reader),
                             Lower80_Financed = AdoFramework.ReadDouble("Lower80_Financed", 0d, reader),
                             Lower80_GAP = AdoFramework.ReadDouble("Lower80_GAP", 0d, reader),
                             Lower80_Maint = AdoFramework.ReadDouble("Lower80_Maint", 0d, reader),
                             Lower80_SVC = AdoFramework.ReadDouble("Lower80_SVC", 0d, reader),
                             Upper80_Units_Delivered = AdoFramework.ReadDouble("Upper80_Units_Delivered", 0d, reader),
                             Upper80_Total_Gross = AdoFramework.ReadDouble("Upper80_Total_Gross", 0d, reader),
                             Upper80_Sales_Gross = AdoFramework.ReadDouble("Upper80_Sales_Gross", 0d, reader),
                             Upper80_FI_Gross = AdoFramework.ReadDouble("Upper80_FI_Gross", 0d, reader),
                             Upper80_Financed = AdoFramework.ReadDouble("Upper80_Financed", 0d, reader),
                             Upper80_GAP = AdoFramework.ReadDouble("Upper80_GAP", 0d, reader),
                             Upper80_Maint = AdoFramework.ReadDouble("Upper80_Maint", 0d, reader),
                             Upper80_SVC = AdoFramework.ReadDouble("Upper80_SVC", 0d, reader)
                         }
                         );
                 }));
            return _ls;

        }

        public List<ActualSale> GetActualSale(int dealerId, int makeId, int modelId)
        {
            var actualSales = new List<ActualSale>();

            AdoFramework.ReadRecords(
                connectionString: ConnectionString.Get(),
                storedProcedureName: "GetActualData",
                parameters: new List<AdoFramework.Parameter>() {
                    new AdoFramework.Parameter("Dealer_key", SqlDbType.Int, dealerId) ,
                    new AdoFramework.Parameter("Make_Key", SqlDbType.Int, makeId),
                    new AdoFramework.Parameter("Model_Key", SqlDbType.Int, modelId)
                },
                 processors: (reader =>
                 {
                     actualSales.Add(
                         new ActualSale
                         {
                             Dealer_Key = AdoFramework.ReadInt("Dealer_Key", reader),
                             Dealer_Name = AdoFramework.ReadString("Dealer_Name", reader),
                             Make_Key = AdoFramework.ReadInt("Make_Key", reader),
                             Model_Key = AdoFramework.ReadInt("Model_Key", reader),
                             Make_Desc = AdoFramework.ReadString("Make_Desc", reader),
                             Model_Desc = AdoFramework.ReadString("Model_Desc", reader),
                             New_Used = AdoFramework.ReadInt("New_Used", reader),
                             Actual_Date = AdoFramework.ReadString("Actual_Date", reader),
                             Units_Delivered = AdoFramework.ReadInt("Units_Delivered", 0, reader),
                             Total_Gross = AdoFramework.ReadDouble("Total_Gross", 0d, reader),
                             Sales_Gross = AdoFramework.ReadDouble("Sales_Gross", 0d, reader),
                             FI_Gross = AdoFramework.ReadDouble("FI_Gross", 0d, reader),
                             Financed = AdoFramework.ReadInt("Financed", 0, reader),
                             GAP = AdoFramework.ReadInt("GAP", 0, reader),
                             Maint = AdoFramework.ReadInt("Maint", 0, reader),
                             SVC = AdoFramework.ReadInt("SVC", 0, reader)
                         }
                         );

                 }));
            return actualSales;
        }

        public IEnumerable<VehicleInventory> GetVehicleInventory(int dealerId)
        {
            var vehicleInventoryList = new List<VehicleInventory>();

            AdoFramework.ReadRecords(
                connectionString: ConnectionString.Get(),
                storedProcedureName: "GetMonthlyInventory",
                parameters: new List<AdoFramework.Parameter>() {
                    new AdoFramework.Parameter("Dealer_key", SqlDbType.Int, dealerId)
                },
                 processors: (reader =>
                 {
                     vehicleInventoryList.Add(
                         new VehicleInventory
                         {
                             DealerKey = AdoFramework.ReadInt("Dealer_Key", reader),
                             MakeKey = AdoFramework.ReadInt("Make_Key", reader),
                             ModelKey = AdoFramework.ReadInt("Model_Key", reader),
                             Make = AdoFramework.ReadString("Make", reader),
                             Model = AdoFramework.ReadString("Model", reader),
                             NewOrUsed = AdoFramework.ReadInt("New", reader),
                             Count = AdoFramework.ReadInt("Inventory_Count", reader),
                             Value = AdoFramework.ReadDouble("Inventory_Value", reader),
                             AsOnDate = AdoFramework.ReadString("InventoryDate", reader)
                         });
                 }));
            return vehicleInventoryList;
        }

        public List<ServiceActual> GetServiceActual(int dealerId, int makeId, int modelId)
        {
            var actualSales = new List<ServiceActual>();

            AdoFramework.ReadRecords(
                connectionString: ConnectionString.Get(),
                storedProcedureName: "GetActualServiceDataByLType",
                parameters: new List<AdoFramework.Parameter>() {
                    new AdoFramework.Parameter("Dealer_key", SqlDbType.Int, dealerId)
                },
                 processors: (reader =>
                 {
                     actualSales.Add(
                         new ServiceActual
                         {
                             DealerKey = AdoFramework.ReadInt("Dealer_Key", reader),
                             DealerName = AdoFramework.ReadString("Dealer_Name", reader),
                             LaborType = AdoFramework.ReadString("Labor_Type", reader),
                             ActualDate = AdoFramework.ReadString("Actual_Date", reader),
                             ServiceELR = AdoFramework.ReadDouble("Service_ELR", reader),
                             SvcLaborSalesPerRO = AdoFramework.ReadDouble("Svc_Labor_Sales_PerRO", reader),
                             SvcHoursPerRO = AdoFramework.ReadDouble("Svc_Hours_PerRO", reader),
                             SvcLaborSales = AdoFramework.ReadDouble("Svc_Labor_Sales", reader),
                             SvcLaborGross = AdoFramework.ReadDouble("Svc_Labor_Gross", reader),
                             SvcHoursSold = AdoFramework.ReadDouble("Svc_Hours_Sold",  reader),
                             SvcROCount = AdoFramework.ReadInt("Svc_RO_Count", 0, reader)                             

                         }
                         );
                 }));
            return actualSales;
        }

        public List<ServiceForecast> GetServiceForecast(int dealerId, string startDate, string endDate)
        {
            var listOfForecastedServiceMeasures = new List<ServiceForecast>();

            AdoFramework.ReadRecords(
                connectionString: ConnectionString.Get(),
                storedProcedureName: "GetServiceForecastData",
                parameters: new List<AdoFramework.Parameter>() {
                    new AdoFramework.Parameter("Dealer_key", SqlDbType.BigInt, dealerId) ,
                    new AdoFramework.Parameter("StartDate", SqlDbType.VarChar,DateTime.ParseExact(startDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy-MM-dd")),
                    new AdoFramework.Parameter("EndDate", SqlDbType.VarChar,DateTime.ParseExact(endDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy-MM-dd"))
                },
                 processors: (reader =>
                 {
                     listOfForecastedServiceMeasures.Add(
                         new ServiceForecast
                         {
                             DealerKey = AdoFramework.ReadInt("Dealer_Key", reader),
                             DealerName = AdoFramework.ReadString("Dlr_Name", reader),
                             LaborType = AdoFramework.ReadString("Labor_Type", reader),
                             DateKey = AdoFramework.ReadInt("Date_Key", reader),
                             FullDate = AdoFramework.ReadDateTime("FullDate", reader),

                             ForecastServiceELR = AdoFramework.ReadDouble("Forecast_Service_ELR", reader),
                             ForecastSvcLaborSalesPerRO = AdoFramework.ReadDouble("Forecast_Svc_Labor_Sales_PerRO", reader),
                             ForecastSvcHoursPerRO = AdoFramework.ReadDouble("Forecast_Svc_Hours_PerRO", reader),
                             ForecastSvcLaborSales = AdoFramework.ReadDouble("Forecast_Svc_Labor_Sales", reader),
                             ForecastSvcLaborGross = AdoFramework.ReadDouble("Forecast_Svc_Labor_Gross", reader),
                             ForecastSvcHoursSold = AdoFramework.ReadDouble("Forecast_Svc_Hours_Sold", reader),
                             ForecastSvcROCount = AdoFramework.ReadDouble("Forecast_Svc_RO_Count", reader),

                             Lower80ServiceELR = AdoFramework.ReadDouble("Lower80_Service_ELR", reader),
                             Lower80SvcLaborSalesPerRO = AdoFramework.ReadDouble("Lower80_Svc_Labor_Sales_PerRO", reader),
                             Lower80SvcHoursPerRO = AdoFramework.ReadDouble("Lower80_Svc_Hours_PerRO", reader),
                             Lower80SvcLaborSales = AdoFramework.ReadDouble("Lower80_Svc_Labor_Sales", reader),
                             Lower80SvcLaborGross = AdoFramework.ReadDouble("Lower80_Svc_Labor_Gross", reader),
                             Lower80SvcHoursSold = AdoFramework.ReadDouble("Lower80_Svc_Hours_Sold", reader),
                             Lower80SvcROCount = AdoFramework.ReadDouble("Lower80_Svc_RO_Count", reader),

                             Upper80ServiceELR = AdoFramework.ReadDouble("Upper80_Service_ELR", reader),
                             Upper80SvcLaborSalesPerRO = AdoFramework.ReadDouble("Upper80_Svc_Labor_Sales_PerRO", reader),
                             Upper80SvcHoursPerRO = AdoFramework.ReadDouble("Upper80_Svc_Hours_PerRO", reader),
                             Upper80SvcLaborSales = AdoFramework.ReadDouble("Upper80_Svc_Labor_Sales", reader),
                             Upper80SvcLaborGross = AdoFramework.ReadDouble("Upper80_Svc_Labor_Gross", reader),
                             Upper80SvcHoursSold = AdoFramework.ReadDouble("Upper80_Svc_Hours_Sold", reader),
                             Upper80SvcROCount = AdoFramework.ReadDouble("Upper80_Svc_RO_Count", reader)
                         }              
                         );
                 }));
            return listOfForecastedServiceMeasures;
        }
    }
}