﻿using DealerWizard.Data.Shared;
using DealerWizard.Service.Contracts;
using DealerWizard.Service.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;

namespace DealerWizard.Service.Repository
{
    public class ReportRepository : IReportRepository
    {
        public IEnumerable<Columns> ReportColumnsMetadata(int reportkey)
        {
            var column = new List<Columns>();
            AdoFramework.ReadRecords(
                connectionString: ConnectionString.GetAdminConString(),
                storedProcedureName: "spGetReportMetadata",
                parameters: new List<AdoFramework.Parameter>() {
                    new AdoFramework.Parameter("@Report_Key", SqlDbType.VarChar, reportkey)

                },                   //new [(),()]
                 processors: (reader =>
                  {
                      column.Add(
                          new Columns
                          {
                              ColumnDisplayName = AdoFramework.ReadString("Column_Display_Name", reader),
                              reportname = AdoFramework.ReadString("Report_Name", reader),
                              ColumnName = AdoFramework.ReadString("Column_Name", reader),
                              UserFriendlyName = AdoFramework.ReadString("UserFriendly_Display_Name", reader),
                              ColumnDataType = AdoFramework.ReadString("Column_Data_Type", reader),
                              Ismeasure = AdoFramework.ReadBool("IsMeasure", false, reader),
                              IsLink = AdoFramework.ReadBool("IsReportLink", false, reader),
                              IsLinkOnHeader = AdoFramework.ReadBool("IsLinkOnHeader", false, reader),
                              IsHidden = AdoFramework.ReadBool("IsHidden", false, reader),
                              ReportRefKey = AdoFramework.ReadInt("Report_Ref_Key", reader),
                              NextReportName = AdoFramework.ReadString("LinkReportName", reader),
                              ReportFilters = AdoFramework.ReadString("ReportFilters", reader),
                              ReportType = AdoFramework.ReadString("Report_Type", reader),
                              DataType = AdoFramework.ReadString("TypeOfData", reader),
                              UrlName = AdoFramework.ReadString("URL_Name", reader),
                              UrlPath = AdoFramework.ReadString("URL_Path", reader),
                              UrlRefKey = AdoFramework.ReadInt("URL_REF_KEY", 0, reader)
                          }
                          );
                  }));
            return column;
        }

        public JObject ReportData(int dealerKey, string selectedDate, string endDate, int reportkey, string filterType, string filterValue, string param, IEnumerable<Columns> metaDataCollection)
        {

            int sqlLongRunningQueryTimeOutInSeconds = 60;
            var confiVal = ConfigurationManager.AppSettings.Get("sqlLongRunningQueryTimeOutInSeconds");

            if (confiVal != null)
            {
                sqlLongRunningQueryTimeOutInSeconds = int.Parse(confiVal);
            }

            JObject reportDataObj = new JObject();
            JArray array = new JArray();
            AdoFramework.ReadRecordsWithLongTimeout(
                 ConnectionString.GetAdminConString(),
                "spGetReportTable", sqlLongRunningQueryTimeOutInSeconds,
                new List<AdoFramework.Parameter>() {
                    new AdoFramework.Parameter("Dealer_key", SqlDbType.VarChar, dealerKey) ,
                    new AdoFramework.Parameter("StartDate", SqlDbType.VarChar,DateTime.ParseExact(selectedDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy-MM-dd")),//
                    new AdoFramework.Parameter("SelectedDate", SqlDbType.VarChar,DateTime.ParseExact(endDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy-MM-dd")),//
            new AdoFramework.Parameter("@Report_Key", SqlDbType.VarChar, reportkey),
            new AdoFramework.Parameter("@FilterType", SqlDbType.VarChar, filterType),
            new AdoFramework.Parameter("@Params", SqlDbType.VarChar, param),
            new AdoFramework.Parameter("@FilterValue", SqlDbType.VarChar, filterValue)

                },
                 (reader =>
                 {

                     JObject JObj1 = new JObject();

                     foreach (var item in metaDataCollection)
                     {
                         JObj1[item.ColumnName] = AdoFramework.ReadString(item.ColumnName, reader);
                     }



                     array.Add(JObj1);
                 }), (reader =>
                 {

                     JObject measureAggregates = new JObject();

                     foreach (var item in metaDataCollection)
                     {
                         if (item.Ismeasure)
                         {
                             measureAggregates[item.ColumnName] = AdoFramework.ReadString(item.ColumnName, reader);
                         }
                     }



                     reportDataObj["aggregates"] = measureAggregates;
                 }));
            reportDataObj["data"] = array;

            return reportDataObj;
        }

        public IEnumerable<Department> DepartmentData()
        {

            var department = new List<Department>();

            AdoFramework.ReadRecords(ConnectionString.GetAdminConString(), "spGetDepartmentsForReport", new List<AdoFramework.Parameter>(), (reader) =>
            {
                department.Add(
                    new Department
                    {
                        Department_Key = Convert.ToInt32(reader[0]),
                        Department_Name = reader[1].ToString()
                    });
            });

            return department;
        }

        public IEnumerable<Report> AllReports()
        {

            var report = new List<Report>();

            AdoFramework.ReadRecords(ConnectionString.GetAdminConString(), "Sp_GetAllReport", new List<AdoFramework.Parameter>(), (reader) =>
            {
                report.Add(
                    new Report
                    {
                        ReportKey = AdoFramework.ReadInt("Report_Key", reader),
                        ReportName = AdoFramework.ReadString("Report_Name", reader),
                        Dealer_Key = AdoFramework.ReadInt("Dealer_Key", reader),
                        DepartmentKey = AdoFramework.ReadInt("Department_Key", reader),
                        Status = AdoFramework.ReadString("Status", reader),
                        Created_By = AdoFramework.ReadString("Created_By", reader),
                        ReportScope = AdoFramework.ReadString("Report_Scope", reader),
                        ReportType = AdoFramework.ReadString("Report_Type", reader),
                        Department_Name = AdoFramework.ReadString("Department_Name", reader)
                    });
            });

            return report;
        }

        public Report ReportDataByReortKey(int reportkey)
        {
            var report = new Report();
            report.Columnsdata = new List<ColumnsReportModel>();
            report.Filterdata = new List<FilterModel>();
            //var col = new List<ColumnsReportModel>();
            // var filter = new List<FilterModel>();
            AdoFramework.ReadRecords(
                 ConnectionString.GetAdminConString(),
                "Sp_GetReportsById",
                new List<AdoFramework.Parameter>() {
                    new AdoFramework.Parameter("@Report_Key", SqlDbType.VarChar, reportkey)

                },
                 (reader =>
                 {

                     report.ReportKey = AdoFramework.ReadInt("Report_Key", reader);
                     report.ReportName = AdoFramework.ReadString("Report_Name", reader);
                     report.Dealer_Key = AdoFramework.ReadInt("Dealer_Key", reader);
                     report.DepartmentKey = AdoFramework.ReadInt("Department_Key", reader);
                     report.Status = AdoFramework.ReadString("Status", reader);
                     report.Created_By = AdoFramework.ReadString("Created_By", reader);
                     report.ReportScope = AdoFramework.ReadString("Report_Scope", reader);
                     report.ReportType = AdoFramework.ReadString("Report_Type", reader);
                     report.Department_Name = AdoFramework.ReadString("Department_Name", reader);
                 }), (reader =>
                 {

                     report.Columnsdata.Add(
                         new ColumnsReportModel
                         {
                             Report_Param_Key = AdoFramework.ReadInt("Report_Param_Key", reader),
                             Report_Key = AdoFramework.ReadInt("Report_Key", reader),
                             Column_Display_Name = AdoFramework.ReadString("Column_Display_Name", reader),
                             Column_Key = AdoFramework.ReadInt("Column_Key", reader),
                             Link_Enable = AdoFramework.ReadBool("Link_Enable", reader),
                             Report_Ref_Key = AdoFramework.ReadInt("Report_Ref_Key", reader),
                             URL_Ref_Key = AdoFramework.ReadIntNullable("URL_Ref_Key", reader),
                             Order_By = AdoFramework.ReadInt("Order_By", 0, reader),
                             IsHidden = AdoFramework.ReadBoolNullable("IsHidden", reader),
                             Sequence = AdoFramework.ReadInt("Sequence", reader),
                             Ismeasure = AdoFramework.ReadBool("Ismeasure", reader)
                         });

                 }), (reader =>
                 {

                     report.Filterdata.Add(
                         new FilterModel
                         {
                             Report_Filter_Key = AdoFramework.ReadInt("Report_Filter_Key", reader),
                             Report_Key = AdoFramework.ReadInt("Report_Key", reader),
                             Column_Key = AdoFramework.ReadInt("Column_Key", reader)

                         });
                 }
                 ));



            return report;
        }
        public ReportDataByDepartmentKey ReportDataByDepartmentKey(int DepartmentKey)
        {
            var report = new ReportDataByDepartmentKey();
            var rep = new List<ReportsData>();
            var col = new List<ColumnsData>();

            // var report = new List<ReportData>();
            AdoFramework.ReadRecords(
                 ConnectionString.GetAdminConString(),
                 "spGetDataForReport",
                new List<AdoFramework.Parameter>() {
                    new AdoFramework.Parameter("@Department_Key", SqlDbType.VarChar, DepartmentKey)

                }, (reader =>
                 {

                     col.Add(
                         new ColumnsData
                         {
                             Column_key = AdoFramework.ReadInt("Column_key", reader),
                             Column_Display_Name = AdoFramework.ReadString("Column_Display_Name", reader),
                             IsMeasure = AdoFramework.ReadBool("IsMeasure", reader)
                         }
                         );
                     report.ColumnsData = col;

                 }), (reader =>
                 {

                     rep.Add(
                         new ReportsData
                         {
                             Report_Key = AdoFramework.ReadInt("Report_Key", reader),
                             Report_Name = AdoFramework.ReadString("Report_Name", reader),
                             Status = AdoFramework.ReadString("Status", reader)

                         }
                         );
                     report.ReportsData = rep;
                 })
                );
            return report;
        }
        public int InsertReportData(ReportConfiguratorModel data)
        {
            AdoFramework.ExecuteNonQuery(
                 connectionString: ConnectionString.GetAdminConString(),
                 storedProcedureName: "spCreateReport",
                 parameters: new List<AdoFramework.Parameter>() {
                    new AdoFramework.Parameter("@ReportName", SqlDbType.VarChar,data.ReportName),
                    new AdoFramework.Parameter("@ReportType", SqlDbType.VarChar,data.ReportType),
                    new AdoFramework.Parameter("@DepartmentKey", SqlDbType.BigInt,data.DepartmentKey),
                    new AdoFramework.Parameter("@Status", SqlDbType.VarChar,data.Status),
                    new AdoFramework.Parameter("@DealerKey", SqlDbType.BigInt,-1),
                    new AdoFramework.Parameter("@CreatedBy", SqlDbType.VarChar,data.CreatedBy),
                    new AdoFramework.Parameter("@Report_Scope", SqlDbType.VarChar,data.ReportScope),
                    new AdoFramework.Parameter("@Filters", SqlDbType.VarChar,data.Filter),

                    new AdoFramework.Parameter("@ReportColumns", SqlDbType.Structured,GetTableData(data)),
                    new AdoFramework.Parameter("@Report_Key", SqlDbType.BigInt,data.ReportKey)

                 }
                 );
            return 1;

        }
        private DataTable GetTableData(ReportConfiguratorModel data)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Report_Key");
            dt.Columns.Add("Column_Display_Name");
            dt.Columns.Add("Column_Key");
            dt.Columns.Add("Link_Enable");
            dt.Columns.Add("Report_Ref_Key");
            dt.Columns.Add("URL_Ref_Key");
            dt.Columns.Add("Order_By");
            dt.Columns.Add("IsHidden");
            dt.Columns.Add("Sequence");

            foreach (var item in data.Dimensions)
            {
                dt.Rows.Add(null, item.Column_Display_Name, item.Column_Key, item.Link_Enable, item.Report_Ref_Key, null, item.Order_By, null, item.Sequence);
            }
            foreach (var item in data.Measures)
            {
                dt.Rows.Add(null, item.Column_Display_Name, item.Column_Key, item.Link_Enable, item.Report_Ref_Key, null, item.Order_By, null, item.Sequence);
                //dt.Rows.Add(null, item.Column_Display_Name, item.Column_Key, item.Link_Enable, item.Report_Ref_Key, null, item.Order_By, null, item.Sequence);

            }
            return dt;

        }

        public IEnumerable<ReportObjectModel> GetReportObjectByDepartmentDealerKey(int DealerKey, string DepartmentCode)
        {
            var column = new List<ReportObjectModel>();
            AdoFramework.ReadRecords(
                connectionString: ConnectionString.GetAdminConString(),
                storedProcedureName: "spGetReportObjectForDemo",
                parameters: new List<AdoFramework.Parameter>() {
                    new AdoFramework.Parameter("@Dealer_Key", SqlDbType.BigInt, DealerKey),
                    new AdoFramework.Parameter("@Department_Code", SqlDbType.VarChar, DepartmentCode)
                },                   //new [(),()]
                 processors: (reader =>
                 {
                     column.Add(
                         new ReportObjectModel
                         {
                             key = AdoFramework.ReadInt("Report_Key", reader),
                             reportId = AdoFramework.ReadInt("Report_Key", reader),
                             reportName = AdoFramework.ReadString("Report_Name", reader),
                             reportType = AdoFramework.ReadString("Report_Type", reader),
                             departmentName = AdoFramework.ReadString("Department_Name", reader),
                             filterType = AdoFramework.ReadString("Filter_Type", reader),
                             filterValues = AdoFramework.ReadString("Filter_Values", reader),
                             isActive = AdoFramework.ReadBool("isactive", reader),
                             defaultFilter = AdoFramework.ReadString("Default_Filter", reader)
                         }
                         );
                 }));
            return column;
        }

        public JObject ReportDataDealershipOverviewDefault(int dealerKey, string selectedDate, string endDate, string filterType, string filterValue, string param, IEnumerable<Columns> metaDataCollection, int reportkey = 64)
        {
            JObject reportDataObj = new JObject();
            JArray array = new JArray();
            AdoFramework.ReadRecords(
                 ConnectionString.GetAdminConString(),
                "spGetDlrOverviewReportData",
                new List<AdoFramework.Parameter>() {
                    new AdoFramework.Parameter("Dealer_key", SqlDbType.VarChar, dealerKey) ,
                    new AdoFramework.Parameter("StartDate", SqlDbType.VarChar,DateTime.ParseExact(selectedDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy-MM-dd")),
                    new AdoFramework.Parameter("SelectedDate", SqlDbType.VarChar,DateTime.ParseExact(endDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy-MM-dd"))


                },
                 (reader =>
                 {

                     JObject JObj1 = new JObject();

                     foreach (var item in metaDataCollection)
                     {
                         JObj1[item.ColumnName] = AdoFramework.ReadString(item.ColumnName, reader);
                     }



                     array.Add(JObj1);
                 }), (reader =>
                 {

                     JObject measureAggregates = new JObject();

                     foreach (var item in metaDataCollection)
                     {
                         if (item.Ismeasure)
                         {
                             measureAggregates[item.ColumnName] = AdoFramework.ReadString(item.ColumnName, reader);
                         }
                     }



                     reportDataObj["aggregates"] = measureAggregates;
                 }));
            reportDataObj["data"] = array;

            return reportDataObj;
        }

        public JObject ReportDataDealTypeSummary(int dealerKey, string selectedDate, string endDate, string filterType, string filterValue, string param, IEnumerable<Columns> metaDataCollection, int reportkey = 49)
        {
            JObject reportDataObj = new JObject();
            JArray array = new JArray();
            AdoFramework.ReadRecords(
                 ConnectionString.GetAdminConString(),
                "spGetDealTypeSummaryReportData",
                new List<AdoFramework.Parameter>() {
                    new AdoFramework.Parameter("Dealer_key", SqlDbType.VarChar, dealerKey) ,
                    new AdoFramework.Parameter("StartDate", SqlDbType.VarChar,DateTime.ParseExact(selectedDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy-MM-dd")),
                    new AdoFramework.Parameter("SelectedDate", SqlDbType.VarChar,DateTime.ParseExact(endDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy-MM-dd"))


                },
                 (reader =>
                 {

                     JObject JObj1 = new JObject();

                     foreach (var item in metaDataCollection)
                     {
                         JObj1[item.ColumnName] = AdoFramework.ReadString(item.ColumnName, reader);
                     }



                     array.Add(JObj1);
                 }), (reader =>
                 {

                     JObject measureAggregates = new JObject();

                     foreach (var item in metaDataCollection)
                     {
                         if (item.Ismeasure)
                         {
                             measureAggregates[item.ColumnName] = AdoFramework.ReadString(item.ColumnName, reader);
                         }
                     }



                     reportDataObj["aggregates"] = measureAggregates;
                 }));
            reportDataObj["data"] = array;

            return reportDataObj;
        }

        public JObject ReportDataDealTypeSummaryDownload(int dealerKey, string selectedDate, string endDate, string filterType, string filterValue, string param, IEnumerable<Columns> metaDataCollection, int reportkey = 64)
        {
            JObject reportDataObj = new JObject();
            JArray arrayDownload = new JArray();
            AdoFramework.ReadRecords(
                 ConnectionString.GetAdminConString(),
                "spGetDealTypeSummaryReportData",
                new List<AdoFramework.Parameter>() {
                    new AdoFramework.Parameter("Dealer_key", SqlDbType.VarChar, dealerKey) ,
                    new AdoFramework.Parameter("StartDate", SqlDbType.VarChar,DateTime.ParseExact(selectedDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy-MM-dd")),
                    new AdoFramework.Parameter("SelectedDate", SqlDbType.VarChar,DateTime.ParseExact(endDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy-MM-dd"))


                },
                 (reader =>
                 {

                     JObject JObj1 = new JObject();
                     JObject JObjDowonload = new JObject();

                     foreach (var item in metaDataCollection)
                     {

                         JObjDowonload[item.ColumnDisplayName] = AdoFramework.ReadString(item.ColumnName, reader);
                     }


                     arrayDownload.Add(JObjDowonload);

                 }), (reader =>
                 {

                     JObject measureAggregates = new JObject();

                     foreach (var item in metaDataCollection)
                     {
                         if (item.Ismeasure)
                         {
                             measureAggregates[item.ColumnName] = AdoFramework.ReadString(item.ColumnName, reader);
                         }
                     }



                     reportDataObj["aggregates"] = measureAggregates;
                 }));

            reportDataObj["download"] = arrayDownload;

            return reportDataObj;
        }

        public JObject DealershipOverviewDefaultDownload(int dealerKey, string selectedDate, string endDate, string filterType, string filterValue, string param, IEnumerable<Columns> metaDataCollection, int reportkey = 64)
        {
            JObject reportDataObj = new JObject();
            JArray arrayDownload = new JArray();
            AdoFramework.ReadRecords(
                 ConnectionString.GetAdminConString(),
                "spGetDlrOverviewReportData",
                new List<AdoFramework.Parameter>() {
                    new AdoFramework.Parameter("Dealer_key", SqlDbType.VarChar, dealerKey) ,
                    new AdoFramework.Parameter("StartDate", SqlDbType.VarChar,DateTime.ParseExact(selectedDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy-MM-dd")),
                    new AdoFramework.Parameter("SelectedDate", SqlDbType.VarChar,DateTime.ParseExact(endDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy-MM-dd"))


                },
                 (reader =>
                 {

                     JObject JObj1 = new JObject();
                     JObject JObjDowonload = new JObject();

                     foreach (var item in metaDataCollection)
                     {

                         JObjDowonload[item.ColumnDisplayName] = AdoFramework.ReadString(item.ColumnName, reader);
                     }


                     arrayDownload.Add(JObjDowonload);

                 }), (reader =>
                 {

                     JObject measureAggregates = new JObject();

                     foreach (var item in metaDataCollection)
                     {
                         if (item.Ismeasure)
                         {
                             measureAggregates[item.ColumnName] = AdoFramework.ReadString(item.ColumnName, reader);
                         }
                     }



                     reportDataObj["aggregates"] = measureAggregates;
                 }));

            reportDataObj["download"] = arrayDownload;

            return reportDataObj;
        }

        public JObject ReportDataDownload(int dealerKey, string selectedDate, string endDate, int reportkey, string filterType, string filterValue, string param, IEnumerable<Columns> metaDataCollection)
        {

            int sqlLongRunningQueryTimeOutInSeconds = 60;
            var confiVal = ConfigurationManager.AppSettings.Get("sqlLongRunningQueryTimeOutInSeconds");

            if (confiVal != null)
            {
                sqlLongRunningQueryTimeOutInSeconds = int.Parse(confiVal);
            }

            JObject reportDataObj = new JObject();

            JArray arrayDownload = new JArray();
            AdoFramework.ReadRecordsWithLongTimeout(
                 ConnectionString.GetAdminConString(),
                "spGetReportTable", sqlLongRunningQueryTimeOutInSeconds,
                new List<AdoFramework.Parameter>() {
                    new AdoFramework.Parameter("Dealer_key", SqlDbType.VarChar, dealerKey) ,
                    new AdoFramework.Parameter("StartDate", SqlDbType.VarChar,DateTime.ParseExact(selectedDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy-MM-dd")),//
                    new AdoFramework.Parameter("SelectedDate", SqlDbType.VarChar,DateTime.ParseExact(endDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy-MM-dd")),//
            new AdoFramework.Parameter("@Report_Key", SqlDbType.VarChar, reportkey),
            new AdoFramework.Parameter("@FilterType", SqlDbType.VarChar, filterType),
            new AdoFramework.Parameter("@Params", SqlDbType.VarChar, param),
            new AdoFramework.Parameter("@FilterValue", SqlDbType.VarChar, filterValue)

                },
                 (reader =>
                 {

                     JObject JObjDowonload = new JObject();

                     foreach (var item in metaDataCollection)
                     {
                         JObjDowonload[item.ColumnDisplayName] = AdoFramework.ReadString(item.ColumnName, reader);
                     }


                     arrayDownload.Add(JObjDowonload);

                 }), (reader =>
                 {

                     JObject measureAggregates = new JObject();

                     foreach (var item in metaDataCollection)
                     {
                         if (item.Ismeasure)
                         {
                             measureAggregates[item.ColumnName] = AdoFramework.ReadString(item.ColumnName, reader);
                         }
                     }



                     reportDataObj["aggregates"] = measureAggregates;
                 }));
            reportDataObj["download"] = arrayDownload;

            return reportDataObj;
        }

        public JObject GetUsedCarEquityDetail(int dealerKey, string startDate, string selectedDate, IEnumerable<BookValueItem> columnlist)
        {

            JArray arr = new JArray();
            JObject OuterObj = new JObject();
            var detail = new List<UsedCarEquity>();
            AdoFramework.ReadRecords(
                connectionString: ConnectionString.GetAdminConString(),
                storedProcedureName: "spGetUsedCarEquityDetail",
                parameters: new List<AdoFramework.Parameter>() {
                    new AdoFramework.Parameter("@Dealer_Key", SqlDbType.VarChar, dealerKey)                     
                },
                 processors: (reader =>
                 {
                     JObject obj = new JObject();
                     foreach (var item in columnlist)
                     {
                         if (item.ColumnType == "String")
                         {
                             obj[item.BookValueText] = AdoFramework.ReadString(item.BookValueText, reader);
                         }
                         else if (item.ColumnType == "Double")
                         {
                             obj[item.BookValueText] = AdoFramework.ReadDoubleNullable(item.BookValueText, reader);
                         }                          

                     }
                     obj["Diff Inv vs Book"] = 0d;
                     arr.Add(obj);
                 }));
            OuterObj["data"] = arr;
            return OuterObj;
        }


        public IEnumerable<UserFriendlyName> UserFriendlyName()
        {

            var userfriendlyname = new List<UserFriendlyName>();

            AdoFramework.ReadRecords(ConnectionString.GetAdminConString(), "spCreateUserfriendlyName", new List<AdoFramework.Parameter>(), (reader) =>
            {
                userfriendlyname.Add(
                    new UserFriendlyName
                    {
                        Display_Name = reader[0].ToString(),
                        User_Friendly_Name = reader[1].ToString()

                    });
            });

            return userfriendlyname;
        }
    }
}



