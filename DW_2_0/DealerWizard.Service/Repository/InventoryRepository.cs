﻿using DealerWizard.Service.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DealerWizard.Service.Models;
using DealerWizard.Data.Shared;
using System.Data;

namespace DealerWizard.Service.Repository
{
    public class InventoryRepository : IInventoryRepository
    {
        public IEnumerable<VehicleInventoryItem> GetCurrentInventory(int dealerId)
        {
            var vehicleInventoryList = new List<VehicleInventoryItem>();

            AdoFramework.ReadRecords(
                connectionString: ConnectionString.Get(),
                storedProcedureName: "GetCurrentInventory",
                parameters: new List<AdoFramework.Parameter>() {
                    new AdoFramework.Parameter("Dealer_key", SqlDbType.BigInt, dealerId)
                },
                 processors: (reader =>
                 {
                     vehicleInventoryList.Add(
                         new VehicleInventoryItem
                         {
                             DealerKey = AdoFramework.ReadInt("Dealer_Key", reader),
                             MakeKey = AdoFramework.ReadInt("Make_Key", reader),
                             ModelKey = AdoFramework.ReadInt("Model_Key", reader),
                             Make = AdoFramework.ReadString("Make", reader),
                             Model = AdoFramework.ReadString("Model", reader),
                             NewOrUsed = AdoFramework.ReadInt("New", reader),
                             VehicleKey = AdoFramework.ReadLong("Vehicle_Key", reader),
                             Value = AdoFramework.ReadDouble("Inventory_Value", reader)
                         });
                 }));
            return vehicleInventoryList;
        }
    }
}