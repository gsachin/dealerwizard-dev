﻿using DealerWizard.Service.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DealerWizard.Service.Models;
using DealerWizard.Data.Shared;
using System.Data;

namespace DealerWizard.Service.Repository
{
    public class UserActivityRepository : IUserActivityRepository
    {
        public bool SaveUserActivityLogs(IEnumerable<UserActivity> UserActivity)
        {
            AdoFramework.ExecuteNonQuery(
                connectionString: ConnectionString.Get(),
                storedProcedureName: "[dw].[spSaveUserActivity]",
                parameters: new List<AdoFramework.Parameter>() {
                    new AdoFramework.Parameter("@UserActivityColumns", SqlDbType.Structured,GetTableData(UserActivity))
                }
                );
            return true;
        }
        private DataTable GetTableData(IEnumerable<UserActivity> data)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Type");
            dt.Columns.Add("DealerId");
            dt.Columns.Add("ActivityDetail");
            dt.Columns.Add("Route");
            dt.Columns.Add("ActivtyTime");
            dt.Columns.Add("LoggingTrackingId");

            foreach (var item in data)
            {
                dt.Rows.Add(item.Type, item.DealerId, item.ActivityDetail, item.Route, item.ActivityTime, item.LoggingTrackingId);
            }

            return dt;
        }
    }
}