﻿using DealerWizard.Service.Contracts;
using System.Web.Security;
using System;
using System.Security.Claims;
using System.Linq;
using DealerWizard.Service.Models;

namespace DealerWizard.Service.Repository
{
    public class UserSessionManager : IUserSessionManager
    {
        ICacheManager<UserAttributes> userCacheManager;
        public UserSessionManager( ICacheManager<UserAttributes> userCacheManager)
        {
            this.userCacheManager = userCacheManager;
        }

        public ClaimsIdentity UserClaimsIdentity
        {
            get
            {
                return ClaimsPrincipal.Current.Identity as ClaimsIdentity;
            }
        }

        public string UserName
        {
            get
            {
                return UserClaimsIdentity.Claims.First(i => i.Type.Equals(ClaimTypes.Name)).Value;
            }
        }

        public string[] UserRoles
        {
            get
            {
                return UserClaimsIdentity.Claims.Where(i => i.Type.Equals(ClaimTypes.Role)).Select(i => i.Value).ToArray();
            }
        }

        public Guid UserLoginTrackingId
        {
            get
            {
                Guid trackingId = Guid.Empty;
                Guid.TryParse(UserClaimsIdentity.Claims.Where(i => i.Type.Equals(ClaimTypes.UserData)).Select(i => i.Value).First(), out trackingId);
                return trackingId;
            }
        }


        public UserAttributes GetCurrentUser()
        {   
            UserAttributes currentUser = userCacheManager.GetCacheItemBy(UserName);
            if (currentUser == null)
            {
                var membershipUser = Membership.GetUser(UserName);
                currentUser = new UserAttributes
                {
                    Email = membershipUser.Email,
                    Id = Guid.Parse(membershipUser.ProviderUserKey.ToString()),
                    Name = membershipUser.UserName
                };
                userCacheManager.InsertCacheItem(currentUser, UserName, CacheExpirationType.Sliding);
            }
            return currentUser;
        }

        public Guid GetCurrentUserId()
        {
            return GetCurrentUser().Id;
        }

        public bool ReValidateSession()
        {
            if (UserClaimsIdentity == null)
            {
                return false;
            }

            var userNameClaim = UserClaimsIdentity.Claims.First(i => i.Type.Equals(ClaimTypes.Name));

            if (userNameClaim == null || userNameClaim.Value.Equals(string.Empty))
            {
                return false;
            }

            return true;

        }
    }
}