﻿using Autofac;
using Autofac.Integration.WebApi;
using DealerWizard.Service.Contracts;
using DealerWizard.Service.Helpers;
using DealerWizard.Service.Repository;
using DealerWizard.Service.Serializer;
using System;
using System.Reflection;
using System.Web.Http;
using System.Web.Http.Tracing;

namespace DealerWizard.Service
{
    public class IocConfig
    {
        public static void Configure()
        {
            var builder = new ContainerBuilder();
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            
            builder.RegisterHttpRequestMessage(GlobalConfiguration.Configuration);
            builder.RegisterType<OperationRepository>().As<IOperationRepository>().InstancePerRequest();
            //builder.RegisterType<NewCarRepo>().As<INewCarRepo>().InstancePerRequest();
            builder.RegisterType<ReportRepository>().As<IReportRepository>().InstancePerRequest();
            builder.RegisterType<CoreRepository>().As<ICoreRepository>().InstancePerRequest();
            builder.RegisterType<ForecastRepository>().As<IForecastRepository>().InstancePerRequest();
            builder.RegisterType<InventoryRepository>().As<IInventoryRepository>().InstancePerRequest();
            builder.RegisterType<DealershipRepository>().As<IDealershipRepository>().InstancePerRequest();
            builder.RegisterType<UserActivityRepository>().As<IUserActivityRepository>().InstancePerRequest();
            // builder.RegisterType<UsedCarRepository>().As<IusedCarRepository>().InstancePerRequest();
            builder.RegisterType<UserSessionManager>().As<IUserSessionManager>().InstancePerRequest().InstancePerLifetimeScope();

            builder.RegisterType<CsvSerializer>().As<ICsvSerializer>().InstancePerRequest();
            
            builder.RegisterGeneric(typeof(InMemoryCacheManager<>)).As(typeof(ICacheManager<>)).InstancePerRequest().InstancePerLifetimeScope();

            builder.RegisterType<NLogger>().As<ITraceWriter>().InstancePerLifetimeScope();

            
            var container = builder.Build();

            var resolver = new AutofacWebApiDependencyResolver(container);

            GlobalConfiguration.Configuration.DependencyResolver = resolver;
        }

    }
}