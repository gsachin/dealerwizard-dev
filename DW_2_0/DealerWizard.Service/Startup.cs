﻿using System;
using Microsoft.Owin;
using Owin;
using Microsoft.Owin.Security.OAuth;
using System.Web.Http;
using System.Configuration;

[assembly: OwinStartup(typeof(DealerWizard.Service.Startup))]
namespace DealerWizard.Service
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)  
        {
            //here we will configure OAuthAuthorizationServerProvider

            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll); // to enable cors options

            // adding reference to MyAuthorizationServerProvider
            var myProvider = new MyAuthorizationServerProvider();
            var authSessionTimeOutstring = ConfigurationManager.AppSettings.Get("AuthSessionTimeOutInMinutes");
            double authSessionTimeOutInMinutes = (authSessionTimeOutstring == null) ? 100 : double.Parse(authSessionTimeOutstring);
            

           OAuthAuthorizationServerOptions options = new OAuthAuthorizationServerOptions
            {
               
                AllowInsecureHttp = true, //as demo project we uses insecure http true but in live we use secure version
                TokenEndpointPath = new PathString("/token"), // this is the path from where the user gets the token after providing valid credentials
                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(authSessionTimeOutInMinutes), // to set time span till which the accessed token remains valid after its aquired else a new token need to be aquired or get it refresh
                Provider = myProvider // define the provider
            };

            app.UseOAuthAuthorizationServer(options);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());

            // register webapiconfig
            HttpConfiguration config = new HttpConfiguration();
            WebApiConfig.Register(config);
        }
    }
}
