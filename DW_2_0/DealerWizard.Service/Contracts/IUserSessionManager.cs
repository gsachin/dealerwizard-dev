﻿using DealerWizard.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DealerWizard.Service.Contracts
{
    public interface IUserSessionManager
    {
        string UserName { get; }

        string[] UserRoles { get; }
        Guid GetCurrentUserId();

        UserAttributes GetCurrentUser();

        bool ReValidateSession();

        Guid UserLoginTrackingId { get; }
        
    }
}
