﻿using DealerWizard.Service.Models;
using System.Collections.Generic;

namespace DealerWizard.Service.Contracts
{
    public interface IDealershipRepository
    {
        List<VehicleMake> GetVehicleMakeByDealer(int DealerId);
        List<VehicleModel> GetVehicleMakeModelByDealer(int DealerId);

        IEnumerable<DealershipAttribute> GetDealershipDetails(int DealerId);
        DealerSetting GetDealerSetting(int dealerkey);
        IEnumerable<BookValueItem> GetDealerWiseBookValueList(int dealerkey);
    }
}
