﻿namespace DealerWizard.Service.Contracts
{
    public interface ICacheManager<T>
    {
        T GetCacheItemBy(string key);

        void InsertCacheItem(T item, string key, CacheExpirationType expiration = CacheExpirationType.Absolute);

        void DeleteCacheItem(string key);
        
    }

    public enum CacheExpirationType
    {
        Sliding,
        Absolute
    }
}
