﻿using DealerWizard.Service.Models;
using System.Collections.Generic;

namespace DealerWizard.Service.Contracts
{
    public interface IForecastRepository
    {
        List<ForecastedSale> GetForecastedSale(int dealerId, string startDate, string endDate);

        List<ActualSale> GetActualSale(int dealerId, int makeId, int modelId);

        IEnumerable<VehicleInventory> GetVehicleInventory(int dealerId);

        List<ServiceActual> GetServiceActual(int dealerId, int makeId, int modelId);

        List<ServiceForecast> GetServiceForecast(int dealerId, string startDate, string endDate);
        
    }
}
