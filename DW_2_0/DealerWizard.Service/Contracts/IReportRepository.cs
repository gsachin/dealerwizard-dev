﻿using DealerWizard.Service.Models;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace DealerWizard.Service.Contracts
{
    public interface IReportRepository
    {
        IEnumerable<Columns> ReportColumnsMetadata(int reportkey);
        JObject ReportData(int dealerKey, string selectedDate, string endDate,int reportkey, string filterType, string filterValue, string param, IEnumerable<Columns> metaDataCollection);

        IEnumerable<Department> DepartmentData();
        Report ReportDataByReortKey(int ReportKey);
        ReportDataByDepartmentKey ReportDataByDepartmentKey(int DepartmentKey);
        IEnumerable<Report> AllReports();
        int InsertReportData(ReportConfiguratorModel data);
        JObject DealershipOverviewDefaultDownload(int dealerKey, string selectedDate, string endDate, string filterType, string filterValue, string param, IEnumerable<Columns> metaDataCollection, int reportkey = 64);
        IEnumerable<ReportObjectModel> GetReportObjectByDepartmentDealerKey(int DealerKey, string DepartmentCode);
        JObject ReportDataDealershipOverviewDefault(int dealerKey, string selectedDate, string endDate, string filterType, string filterValue, string param, IEnumerable<Columns> metaDataCollection ,int reportkey);
        JObject ReportDataDownload(int dealerKey, string selectedDate, string endDate, int reportkey, string filterType, string filterValue, string param, IEnumerable<Columns> metaDataCollection);
        JObject GetUsedCarEquityDetail(int dealerKey, string startDate, string selectedDate,IEnumerable<BookValueItem> columnlist);
        JObject ReportDataDealTypeSummary(int dealerKey, string selectedDate, string endDate, string filterType, string filterValue, string param, IEnumerable<Columns> metaDataCollection, int reportkey = 49);

        JObject ReportDataDealTypeSummaryDownload(int dealerKey, string selectedDate, string endDate, string filterType, string filterValue, string param, IEnumerable<Columns> metaDataCollection, int reportkey = 49);
        IEnumerable<UserFriendlyName> UserFriendlyName();
    }
}
