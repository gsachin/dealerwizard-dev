﻿using System;
using System.Net.Http;

namespace DealerWizard.Service.Contracts
{
    public interface ICsvSerializer
    {
        HttpResponseMessage jsonToCSV(string jsonContent);
        HttpResponseMessage writeStream(Type type, object value);
    }
}
