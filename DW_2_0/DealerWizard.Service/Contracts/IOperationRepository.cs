﻿using DealerWizard.Service.Models;
using System.Collections.Generic;

namespace DealerWizard.Service.Contracts
{
    public interface IOperationRepository
    {
        object[] GetSalesMeasures(string type, int dealerKey, string startDate, string endDate, string periodType, char previousPeriod);
        IEnumerable<KpiItems> GetKpiData(string type, int dealerKey, string startDate, string endDate);
        List<object> FilterResultSales(IEnumerable<SmallMultipleItem> result, string type);
        List<object> FilterSalesAggregatedResult(IEnumerable<SmallMultipleItem> result, string type);
    }
}
