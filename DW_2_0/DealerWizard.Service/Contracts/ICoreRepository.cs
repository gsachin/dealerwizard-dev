﻿using DealerWizard.Service.Models;
using System;
using System.Collections.Generic;

namespace DealerWizard.Service.Contracts
{
    public interface ICoreRepository
    {
        UserPreference GetUserPreferences(Guid user);

        IEnumerable<UserAssociatedDealership> GetAllDealers();
        IEnumerable<UserAssociatedDealership> GetAllActiveDealers();

        IEnumerable<UserAssociatedDealership> GetAssociatedDealersByUser(Guid user);
        IEnumerable<MenuItems> MenuItemsByUserId(Guid UserId);
        string GetUserNameByEmailTrackingCode(string etc);

        UserLoginTrack LogUserLoginTrack(Guid userId, string remoteIpAddress, string userAgent); 

    }
}
