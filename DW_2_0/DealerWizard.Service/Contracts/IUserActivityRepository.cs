﻿using DealerWizard.Service.Models;
using System.Collections.Generic;

namespace DealerWizard.Service.Contracts
{
    public interface IUserActivityRepository
    {
        bool SaveUserActivityLogs(IEnumerable<UserActivity> UserActivity);
    }
}
