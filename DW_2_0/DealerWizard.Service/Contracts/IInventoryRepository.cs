﻿using DealerWizard.Service.Models;
using System.Collections.Generic;

namespace DealerWizard.Service.Contracts
{
    public interface IInventoryRepository
    {
        IEnumerable<VehicleInventoryItem> GetCurrentInventory(int dealerId);
    }
}
