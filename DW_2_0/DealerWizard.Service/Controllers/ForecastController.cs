﻿using DealerWizard.Service.Contracts;
using DealerWizard.Service.ErrorHelper;
using DealerWizard.Service.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;

namespace DealerWizard.Service.Controllers
{
    [Authorize]
    public class ForecastController : ApiController
    {
        private IForecastRepository forecastRepository;
        private IInventoryRepository inventoryRepository;
        private IDealershipRepository dealerReporsitory;
        private ICsvSerializer csvSerializer;

        public ForecastController(IForecastRepository forecastRepository, IDealershipRepository dealerRepository, ICsvSerializer Serializer, IInventoryRepository inventoryRepository)
        {
            this.forecastRepository = forecastRepository;
            this.dealerReporsitory = dealerRepository;
            this.csvSerializer = Serializer;
            this.inventoryRepository = inventoryRepository;
        }

        [HttpGet]
        [Route("api/dealership/details")]
        public JsonResult<IEnumerable<DealershipAttribute>> GetDealershipDetails(int dealerId, string fromDate = "", string toDate = "")
        {
            var details = dealerReporsitory.GetDealershipDetails(dealerId);
            IEnumerable<DealershipAttribute> filteredList = null;

            if (fromDate.Equals(string.Empty) || toDate.Equals(string.Empty))
            {
                filteredList = details;
            }
            else
            {
                DateTime startDate = DateTime.ParseExact(fromDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                DateTime endDate = DateTime.ParseExact(toDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                filteredList = details.Where(o => (o.Dated >= startDate) && (o.Dated <= endDate));
            }

            return Json(filteredList);
        }

        [HttpGet]
        [Route("api/getvehiclemake")]
        public JsonResult<JArray> GetVehicleMake(int DealerKey)
        {

            JArray array = new JArray();

            var result = dealerReporsitory.GetVehicleMakeByDealer(DealerKey);

            foreach (var d in result)
            {
                JObject JObj = new JObject();
                JObj["MakeId"] = d.MakeId;
                JObj["MakeName"] = d.MakeName;
                array.Add(JObj);
            }

            return Json(array);

        }


        [HttpGet]
        [Route("api/getvehiclemakemodel")]
        public JsonResult<JObject> GetVehicleMakeModel(int DealerKey)
        {


            JObject JobjMain = new JObject();

            var result = dealerReporsitory.GetVehicleMakeModelByDealer(DealerKey);

            var Make = result.Select(x => new { MakeId = x.MakeId, MakeName = x.MakeName, VehicleType=x.VehicleType }).Distinct().ToList();

            JArray arrayMake = new JArray();
            foreach (var d in Make)
            {
                JObject JObj = new JObject();
                JObj["MakeId"] = d.MakeId;
                JObj["MakeName"] = d.MakeName;
                JObj["VehicleType"] = d.VehicleType;
                arrayMake.Add(JObj);
            }

            JArray arrayModel = new JArray();
            foreach (var d in result)
            {
                JObject JObj = new JObject();
                JObj["MakeId"] = d.MakeId;
                JObj["ModelId"] = d.ModelId;
                JObj["ModelName"] = d.ModelName;
                JObj["VehicleType"] = d.VehicleType;
                arrayModel.Add(JObj);
            }

            JobjMain["Makes"] = arrayMake;
            JobjMain["Models"] = arrayModel;

            return Json(JobjMain);

        }


        [HttpGet]
        [Route("api/vehicleinventory")]
        public JsonResult<IEnumerable<VehicleInventory>> GetVehicleInventoryByDealer(int dealerId)
        {
            return Json(forecastRepository.GetVehicleInventory(dealerId));
        }

        [HttpGet]
        [Route("api/currentinventory")]
        public JsonResult<IEnumerable<VehicleInventory>> GetCurrentInventoryByDealer(int dealerId)
        {
            var currentInventory = inventoryRepository.GetCurrentInventory(dealerId);

            var result = (from vehicle in currentInventory
                          group vehicle by new { vehicle.ModelKey, vehicle.Model, vehicle.MakeKey, vehicle.Make, vehicle.NewOrUsed } into newGroup
                          orderby newGroup.Key.MakeKey
                          select new VehicleInventory
                          {
                              DealerKey = dealerId,
                              Make = newGroup.Key.Make,
                              Model = newGroup.Key.Model,
                              NewOrUsed = newGroup.Key.NewOrUsed,
                              AsOnDate = DateTime.Now.ToString("yyyy-MM-dd"),
                              MakeKey = newGroup.Key.MakeKey,
                              ModelKey = newGroup.Key.ModelKey,
                              Count = newGroup.Count(),
                              Value = newGroup.Average(i => i.Value)
                          }).AsEnumerable();

            // currentInventory.GroupBy(
            return Json(result);
        }

        [HttpGet]
        [Route("api/getactualsale")]
        public JsonResult<JArray> GetActualSale(int dealerId, int makeId, int modelId)
        {
            JArray array = new JArray();

            try
            {
                var result = forecastRepository.GetActualSale(dealerId, makeId, modelId);

                foreach (var d in result)
                {
                    JObject JObj = new JObject();

                    JObj["Dealer_Key"] = d.Dealer_Key;
                    JObj["Dealer_Name"] = d.Dealer_Name;
                    JObj["Make_Key"] = d.Make_Key;
                    JObj["Model_Key"] = d.Model_Key;
                    JObj["Make_Desc"] = d.Make_Desc;
                    JObj["Model_Desc"] = d.Model_Desc;
                    JObj["New_Used"] = d.New_Used;
                    JObj["Actual_Date"] = d.Actual_Date;
                    JObj["Units_Delivered"] = d.Units_Delivered;

                    array.Add(JObj);
                }
            }
            catch (Exception ex)
            {
                throw new ApiDataException(1001, ex.Message, System.Net.HttpStatusCode.InternalServerError);
            }
            return Json(array);
        }

        [HttpGet]
        [Route("api/getforecastedsale")]
        public JsonResult<JArray> GetForecastedSale(int dealerid, string startdate, string enddate)
        {
            JArray array = new JArray();

            var result = forecastRepository.GetForecastedSale(dealerid, startdate, enddate);

            foreach (var d in result)
            {
                JObject JObj = new JObject();

                JObj["Dealer_Key"] = d.Dealer_Key;
                JObj["Dlr_Name"] = d.Dlr_Name;
                JObj["Make_Key"] = d.Make_Key;
                JObj["Make_Desc"] = d.Make_Desc;
                JObj["Model_Key"] = d.Model_Key;
                JObj["Model_Desc"] = d.Model_Desc;
                JObj["New_Used"] = d.New_Used;
                //JObj["Date_Key"] = d.Date_Key;
                JObj["FullDate"] = d.FullDate;
                JObj["Forecast_Units_Delivered"] = d.Forecast_Units_Delivered;
                JObj["Forecast_Total_Gross"] = d.Forecast_Total_Gross;
                JObj["Forecast_Sales_Gross"] = d.Forecast_Sales_Gross;
                JObj["Forecast_FI_Gross"] = d.Forecast_FI_Gross;
                //JObj["Forecast_Financed"] = d.Forecast_Financed;
                //JObj["Forecast_GAP"] = d.Forecast_GAP;
                //JObj["Forecast_Maint"] = d.Forecast_Maint;
                //JObj["Forecast_SVC"] = d.Forecast_SVC;
                JObj["Lower80_Units_Delivered"] = d.Lower80_Units_Delivered;
                //JObj["Lower80_Total_Gross"] = d.Lower80_Total_Gross;
                //JObj["Lower80_Sales_Gross"] = d.Lower80_Sales_Gross;
                //JObj["Lower80_FI_Gross"] = d.Lower80_FI_Gross;
                //JObj["Lower80_Financed"] = d.Lower80_Financed;
                //JObj["Lower80_GAP"] = d.Lower80_GAP;
                //JObj["Lower80_Maint"] = d.Lower80_Maint;
                //JObj["Lower80_SVC"] = d.Lower80_SVC;
                JObj["Upper80_Units_Delivered"] = d.Upper80_Units_Delivered;
                //JObj["Upper80_Total_Gross"] = d.Upper80_Total_Gross;
                //JObj["Upper80_Sales_Gross"] = d.Upper80_Sales_Gross;
                //JObj["Upper80_FI_Gross"] = d.Upper80_FI_Gross;
                //JObj["Upper80_Financed"] = d.Upper80_Financed;
                //JObj["Upper80_GAP"] = d.Upper80_GAP;
                //JObj["Upper80_Maint"] = d.Upper80_Maint;
                //JObj["Upper80_SVC"] = d.Upper80_SVC;

                array.Add(JObj);
            }

            return Json(array);

        }

        [HttpGet]
        [Route("api/getfiactualsale")]
        public JsonResult<JArray> GetFiActualSale(int dealerId, int makeId, int modelId)
        {
            JArray array = new JArray();

            var result = forecastRepository.GetActualSale(dealerId, makeId, modelId);

            foreach (var d in result)
            {
                JObject JObj = new JObject();

                JObj["Dealer_Key"] = d.Dealer_Key;
                JObj["Dealer_Name"] = d.Dealer_Name;
                JObj["Make_Key"] = d.Make_Key;
                JObj["Model_Key"] = d.Model_Key;
                JObj["Make_Desc"] = d.Make_Desc;
                JObj["Model_Desc"] = d.Model_Desc;
                JObj["New_Used"] = d.New_Used;
                JObj["Actual_Date"] = d.Actual_Date;
                JObj["Units_Delivered"] = d.Units_Delivered;
                JObj["Total_Gross"] = d.Total_Gross;
                JObj["Sales_Gross"] = d.Sales_Gross;
                JObj["FI_Gross"] = d.FI_Gross;
                JObj["Financed"] = d.Financed;
                JObj["GAP"] = d.GAP;
                JObj["Maint"] = d.Maint;
                JObj["SVC"] = d.SVC;

                array.Add(JObj);
            }

            return Json(array);
        }

        [HttpGet]
        [Route("api/getfiforecastedsale")]
        public JsonResult<JArray> GetFiForecastedSale(int dealerid, string startdate, string enddate)
        {
            JArray array = new JArray();

            var result = forecastRepository.GetForecastedSale(dealerid, startdate, enddate);

            foreach (var d in result)
            {
                JObject JObj = new JObject();

                JObj["Dealer_Key"] = d.Dealer_Key;
                JObj["Dlr_Name"] = d.Dlr_Name;
                JObj["Make_Key"] = d.Make_Key;
                JObj["Make_Desc"] = d.Make_Desc;
                JObj["Model__Key"] = d.Model_Key;
                JObj["Model_Desc"] = d.Model_Desc;
                JObj["New_Used"] = d.New_Used;
                JObj["Date_Key"] = d.Date_Key;
                JObj["FullDate"] = d.FullDate;
                JObj["Forecast_Units_Delivered"] = d.Forecast_Units_Delivered;
                JObj["Forecast_Total_Gross"] = d.Forecast_Total_Gross;
                JObj["Forecast_Sales_Gross"] = d.Forecast_Sales_Gross;
                JObj["Forecast_FI_Gross"] = d.Forecast_FI_Gross;
                JObj["Forecast_Financed"] = d.Forecast_Financed;
                JObj["Forecast_GAP"] = d.Forecast_GAP;
                JObj["Forecast_Maint"] = d.Forecast_Maint;
                JObj["Forecast_SVC"] = d.Forecast_SVC;
                JObj["Lower80_Units_Delivered"] = d.Lower80_Units_Delivered;
                JObj["Lower80_Total_Gross"] = d.Lower80_Total_Gross;
                JObj["Lower80_Sales_Gross"] = d.Lower80_Sales_Gross;
                JObj["Lower80_FI_Gross"] = d.Lower80_FI_Gross;
                JObj["Lower80_Financed"] = d.Lower80_Financed;
                JObj["Lower80_GAP"] = d.Lower80_GAP;
                JObj["Lower80_Maint"] = d.Lower80_Maint;
                JObj["Lower80_SVC"] = d.Lower80_SVC;
                JObj["Upper80_Units_Delivered"] = d.Upper80_Units_Delivered;
                JObj["Upper80_Total_Gross"] = d.Upper80_Total_Gross;
                JObj["Upper80_Sales_Gross"] = d.Upper80_Sales_Gross;
                JObj["Upper80_FI_Gross"] = d.Upper80_FI_Gross;
                JObj["Upper80_Financed"] = d.Upper80_Financed;
                JObj["Upper80_GAP"] = d.Upper80_GAP;
                JObj["Upper80_Maint"] = d.Upper80_Maint;
                JObj["Upper80_SVC"] = d.Upper80_SVC;

                array.Add(JObj);
            }

            return Json(array);

        }

        [HttpGet]
        [Route("api/getserviceactual")]
        public JsonResult<JArray> GetServiceActual(int dealerId, int makeId, int modelId)
        {
            JArray array = new JArray();

            var result = forecastRepository.GetServiceActual(dealerId, makeId, modelId);

            foreach (var d in result)
            {
                JObject JObj = new JObject();

                JObj["DealerKey"] = d.DealerKey;
                JObj["DealerName"] = d.DealerName;
                JObj["MakeKey"] = d.MakeKey;
                JObj["ModelKey"] = d.ModelKey;
                JObj["MakeDesc"] = d.MakeDesc;
                JObj["ModelDesc"] = d.ModelDesc;
                JObj["LaborType"] = d.LaborType;
                JObj["ActualDate"] = d.ActualDate;
                JObj["ServiceELR"] = d.ServiceELR;
                JObj["SvcHoursPerRO"] = d.SvcHoursPerRO;
                JObj["SvcHoursSold"] = d.SvcHoursSold;
                JObj["SvcLaborGross"] = d.SvcLaborGross;
                JObj["SvcLaborSales"] = d.SvcLaborSales;
                JObj["SvcLaborSalesPerRO"] = d.SvcLaborSalesPerRO;
                JObj["SvcROCount"] = d.SvcROCount;

                array.Add(JObj);
            }

            return Json(array);
        }

        [HttpGet]
        [Route("api/getserviceforecasted")]
        public JsonResult<JArray> GetServiceForecasted(int dealerid, string startdate, string enddate)
        {
            JArray array = new JArray();

            var result = forecastRepository.GetServiceForecast(dealerid, startdate, enddate);

            foreach (var d in result)
            {
                JObject JObj = new JObject();

                JObj["DealerKey"] = d.DealerKey;
                JObj["DealerName"] = d.DealerName;
                JObj["MakeKey"] = d.MakeKey;
                JObj["MakeDesc"] = d.MakeDesc;
                JObj["ModelKey"] = d.ModelKey;
                JObj["ModelDesc"] = d.ModelDesc;
                JObj["LaborType"] = d.LaborType;
                JObj["DateKey"] = d.DateKey;
                JObj["FullDate"] = d.FullDate;

                JObj["ForecastServiceELR"] = d.ForecastServiceELR;
                JObj["ForecastSvcHoursPerRO"] = d.ForecastSvcHoursPerRO;
                JObj["ForecastSvcHoursSold"] = d.ForecastSvcHoursSold;
                JObj["ForecastSvcLaborGross"] = d.ForecastSvcLaborGross;
                JObj["ForecastSvcLaborSales"] = d.ForecastSvcLaborSales;
                JObj["ForecastSvcLaborSalesPerRO"] = d.ForecastSvcLaborSalesPerRO;
                JObj["ForecastSvcROCount"] = d.ForecastSvcROCount;

                JObj["Lower80ServiceELR"] = d.Lower80ServiceELR;
                JObj["Lower80SvcHoursPerRO"] = d.Lower80SvcHoursPerRO;
                JObj["Lower80SvcHoursSold"] = d.Lower80SvcHoursSold;
                JObj["Lower80SvcLaborGross"] = d.Lower80SvcLaborGross;
                JObj["Lower80SvcLaborSales"] = d.Lower80SvcLaborSales;
                JObj["Lower80SvcLaborSalesPerRO"] = d.Lower80SvcLaborSalesPerRO;
                JObj["Lower80SvcROCount"] = d.Lower80SvcROCount;

                JObj["Upper80ServiceELR"] = d.Upper80ServiceELR;
                JObj["Upper80SvcHoursPerRO"] = d.Upper80SvcHoursPerRO;
                JObj["Upper80SvcHoursSold"] = d.Upper80SvcHoursSold;
                JObj["Upper80SvcLaborGross"] = d.Upper80SvcLaborGross;
                JObj["Upper80SvcLaborSales"] = d.Upper80SvcLaborSales;
                JObj["Upper80SvcLaborSalesPerRO"] = d.Upper80SvcLaborSalesPerRO;
                JObj["Upper80SvcROCount"] = d.Upper80SvcROCount;

                array.Add(JObj);
            }

            return Json(array);

        }

        [HttpPost]
        [Route("api/vehiclesalesforecastcsv")]
        public HttpResponseMessage vehiclesalesforecastcsv(ForecastedDownload<ForecastVechicleSalesDownload> ForecastedUserEditData)
        {    
            var json = JsonConvert.SerializeObject(ForecastedUserEditData.ForecastedData.ToList() , Formatting.Indented);
            return csvSerializer.jsonToCSV(json.ToString());
        }

        private bool filterMakeModel(int makeId, int modelId, ForecastedSale forecastedSale)
        {

            if (makeId != 0)
            {
                if (modelId != 0)
                {
                    return (forecastedSale.Make_Key.Equals(makeId) && forecastedSale.Model_Key.Equals(modelId));
                }
                return (forecastedSale.Make_Key.Equals(makeId));
            }
            return true; ;
        }

    }
}