﻿using DealerWizard.Service.Contracts;
using DealerWizard.Service.Extension;
using DealerWizard.Service.Models;
using DealerWizard.Service.Repository;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Http;
using System.Web.Http.Results;
using System.Xml;

namespace DealerWizard.Service.Controllers
{
    [Authorize]
    public class ActivityController : ApiController
    {
        private IUserActivityRepository userActivityRepository;
        public ActivityController(IUserActivityRepository userActivityRepository) {
            this.userActivityRepository = userActivityRepository;
        }

        [HttpPost]
        [Route("api/saveuseractivity")]
        public IHttpActionResult SaveUserActivity(IEnumerable<UserActivity> userActivity)
        {
            var result = userActivityRepository.SaveUserActivityLogs(userActivity);
            return Ok(result);
        }

        [HttpGet]
        [Route("api/getuserrouteactivity")]
        public IHttpActionResult GetUserRouteActivity(string startDate, string endDate, bool allDealers)
        {
            var diffInMonths = Convert.ToDecimal((DateTime.Parse(endDate) - DateTime.Parse(startDate)).TotalDays / 30);
            var divideBy = 6000000;
            var multiplyBy = 1;

            if (allDealers == true)
            {
                 divideBy = 600000;
                 multiplyBy = 2;
            }
          
            diffInMonths = (diffInMonths == 0) ? 0.01m : diffInMonths;
            Random r = new Random();
            var data = new[] {
                          new { TabRoute = "Dealership Overview", AverageTimeSpent = Math.Floor(((r.Next() * diffInMonths)/divideBy)) * multiplyBy },
                          new { TabRoute = "New Car", AverageTimeSpent =  Math.Floor(((r.Next() * diffInMonths)/divideBy)) * multiplyBy  },
                          new { TabRoute = "Variable", AverageTimeSpent =  Math.Floor(((r.Next() * diffInMonths)/divideBy)) * multiplyBy },
                          new { TabRoute = "Used Car", AverageTimeSpent =  Math.Floor(((r.Next() * diffInMonths)/divideBy)) * multiplyBy },
                          new { TabRoute = "Service", AverageTimeSpent =  Math.Floor(((r.Next() * diffInMonths)/divideBy)) * multiplyBy },
                          new { TabRoute = "Inventory", AverageTimeSpent =  Math.Floor(((r.Next() * diffInMonths)/divideBy)) * multiplyBy },
                          new { TabRoute = "Vehicle Sale Forecasting", AverageTimeSpent =  Math.Floor(((r.Next() * diffInMonths)/divideBy)) * multiplyBy },
                           new { TabRoute = "Service Forecasting", AverageTimeSpent =  Math.Floor(((r.Next() * diffInMonths)/divideBy)) * multiplyBy}
                };

            return Ok(data);
        }

        [HttpGet]
        [Route("api/getuserrouteactivity")]
        public IHttpActionResult GetUserReportActivity()
        {            
            return Ok();
        }
    }
}
