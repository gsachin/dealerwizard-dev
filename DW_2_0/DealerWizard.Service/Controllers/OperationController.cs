﻿using DealerWizard.Service.Contracts;
using DealerWizard.Service.Extension;
using DealerWizard.Service.Models;
using DealerWizard.Service.Repository;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Http;
using System.Web.Http.Results;
using System.Xml;

namespace DealerWizard.Service.Controllers
{
    [Authorize]
    public class OperationController : ApiController
    {
        private IReportRepository reportRepository;
        private IOperationRepository _operationRepo;
        private ICsvSerializer csvSerializer;
        private IDealershipRepository dealershipRepository;
        public OperationController(IOperationRepository OperationRepo, IReportRepository reportRepository,ICsvSerializer Serializer, IDealershipRepository dealershipRepository)
        {
            this._operationRepo = OperationRepo;
            this.reportRepository = reportRepository;
            this.csvSerializer = Serializer;
            this.dealershipRepository = dealershipRepository;
        }

        [HttpGet]
        [Route("api/getKpiData")]
        public JsonResult<JArray> GetKpiData(string type, int dealer, string startdate, string enddate)
        {
            //var list = new KpiItems();
            JArray array = new JArray();
            JObject JObj2 = new JObject();
            var result = _operationRepo.GetKpiData(type, dealer, startdate, enddate);
            int i = 1;

            foreach (var d in result)
            {
                JObject JObj = new JObject();
                JObj["title"] = d.measurename;
                JObj["ActualValue"] = d.ActualValue;
                JObj["RangeMax"] = d.GY;
                JObj["RangeMin"] = d.YR;
                //JObj["DataFormat"] = d.DataFormat;
                JObj2["Tiles" + (i++)] = JObj;
            }
            array.Add(JObj2);
            return Json(array);
        }

        [HttpGet]
        [Route("api/getsalesmeasures")]
        public JsonResult<SalesService> GetSalesMeasures(string type, int dealer, string startdate, string enddate, string periodType, bool showPreviodPeriod)
        {
            var list = new SalesService();
            var result = _operationRepo.GetSalesMeasures(type, dealer, startdate, enddate, periodType, 'N');
            list.Current = _operationRepo.FilterResultSales((List<SmallMultipleItem>)result[0], "Sales");
            list.AggregatedSum = _operationRepo.FilterSalesAggregatedResult((List<SmallMultipleItem>)result[1], "Sales");

            if (showPreviodPeriod)
            {
                var previous_year_startdate = DateTime.ParseExact(startdate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).AddYears(-1).ToString("dd-MM-yyyy");
                var previous_year_enddate = DateTime.ParseExact(enddate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).AddYears(-1).ToString("dd-MM-yyyy");
                var result_Pre = _operationRepo.GetSalesMeasures(type, dealer, previous_year_startdate, previous_year_enddate, periodType, 'N');
                list.previous = _operationRepo.FilterResultSales((List<SmallMultipleItem>)result_Pre[0], "Sales");
            }
            return Json(list);

        }

        [HttpGet]
        [Route("api/getcsvdownload")]
        public HttpResponseMessage GetCsvDownload(string partname, string type, int dealer, string startdate, string enddate, string periodType, bool showPreviodPeriod, int reportid, string operationFilterType, string operationFilterValue, string filters)
        {
            if (partname == "kpi")
            {
                var data = _operationRepo.GetKpiData(type, dealer, startdate, enddate).ToList();
                JArray array = new JArray();
                JObject JObj1;

                foreach (var item in data)
                {
                    JObj1 = new JObject();
                    JObj1["MeasureName"] = item.measurename.ToString();
                    JObj1["Value"] = item.ActualValue.ToString();
                    array.Add(JObj1);
                }
                return csvSerializer.jsonToCSV(array.ToString());
            }
            if (partname == "SM")
            {
               
                var data = _operationRepo.GetSalesMeasures(type, dealer, startdate, enddate, periodType, 'N');
                JArray array = new JArray();
                JObject JObj1;
                
                foreach (var SMItems in (List<SmallMultipleItem>)data[0])
                {
                    JObj1 = new JObject();
                    foreach (var item in SMItems.Columns) 
                    {
                        if (item.ColumnName != "Year" && item.ColumnName != "Month")
                            JObj1[item.ColumnName] = item.ColumnValue.ToString();
                    }
                    array.Add(JObj1);
                }

                if (showPreviodPeriod && periodType == "Y")
                {
                    var previous_year_startdate = DateTime.ParseExact(startdate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).AddYears(-1).ToString("dd-MM-yyyy");
                    var previous_year_enddate = DateTime.ParseExact(enddate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).AddYears(-1).ToString("dd-MM-yyyy");
                    var result_Pre = _operationRepo.GetSalesMeasures(type, dealer, previous_year_startdate, previous_year_enddate, periodType, 'N');

                    foreach (var SMItems in (List<SmallMultipleItem>)result_Pre[0])
                    {
                        JObj1 = new JObject();
                        foreach (var item in SMItems.Columns)
                        {
                            if (item.ColumnName != "Year" && item.ColumnName != "Month")
                                JObj1[item.ColumnName] = item.ColumnValue.ToString();
                        }
                        array.Add(JObj1);
                    }
                }

                return csvSerializer.jsonToCSV(array.ToString());
            }
            if (partname == "table")
            {
                var columns = reportRepository.ReportColumnsMetadata(reportid);
                if (reportid == 64)
                {
                    var resultDefaultOverview = reportRepository.DealershipOverviewDefaultDownload(dealer, startdate, enddate, operationFilterType, operationFilterValue, filters, columns, 64);
                    string Collection = resultDefaultOverview["download"].ToString();// as JArray;
                    
                    return csvSerializer.jsonToCSV(Collection);
                }
                else if(reportid ==49)
                 {
                    var resultDefaultOverview = reportRepository.ReportDataDealTypeSummaryDownload(dealer, startdate, enddate, operationFilterType, operationFilterValue, filters, columns, 49);
                    string Collection = resultDefaultOverview["download"].ToString();// as JArray;

                    return csvSerializer.jsonToCSV(Collection);
                }
                else
                {
                    var result = reportRepository.ReportDataDownload(dealer, startdate, enddate, reportid, operationFilterType, operationFilterValue, filters, columns);
                    string Collection = result["download"].ToString();

                    return csvSerializer.jsonToCSV(Collection);
                }
               
             
            }
            return null;
        }


        [HttpGet]
        [Route("api/getusedcarequitydownload")]
        public HttpResponseMessage GetUsedCarEquityCsvDownload(int dealerkey, string startdate, string enddate, string bookvalue, double percent, string filtervalue)
        {
            var dealerBookValue = dealershipRepository.GetDealerWiseBookValueList(dealerkey);

            var listbookitem = new List<BookValueItem>();
            listbookitem.Add(new BookValueItem { BookValueName = "Booked", BookValueText = "Booked", ColumnType = "String" });
            listbookitem.Add(new BookValueItem { BookValueName = "Stock_Number", BookValueText = "Stock_Number", ColumnType = "String" });
            listbookitem.Add(new BookValueItem { BookValueName = "P/T", BookValueText = "P/T", ColumnType = "String" });
            listbookitem.Add(new BookValueItem { BookValueName = "Age", BookValueText = "Age", ColumnType = "String" });
            listbookitem.Add(new BookValueItem { BookValueName = "VIN", BookValueText = "VIN", ColumnType = "String" });
            listbookitem.Add(new BookValueItem { BookValueName = "Year", BookValueText = "Year", ColumnType = "String" });
            listbookitem.Add(new BookValueItem { BookValueName = "Make", BookValueText = "Make", ColumnType = "String" });
            listbookitem.Add(new BookValueItem { BookValueName = "Model", BookValueText = "Model", ColumnType = "String" });
            listbookitem.Add(new BookValueItem { BookValueName = "Miles", BookValueText = "Miles", ColumnType = "String" });
            listbookitem.Add(new BookValueItem { BookValueName = "Inventory_Value", BookValueText = "Inventory_Value", ColumnType = "Double" });

            listbookitem.Add(dealerBookValue.FirstOrDefault(x=> x.BookValueText==bookvalue));

            var result = reportRepository.GetUsedCarEquityDetail(dealerkey, startdate, enddate, listbookitem)["data"];
            JArray array = new JArray();
             
            foreach (var item in result)
            {
                item["Inventory_Value"] = Math.Round(Convert.ToDouble(item["Inventory_Value"]));
                item[bookvalue] = Math.Round(Convert.ToDouble(item[bookvalue]) * percent,MidpointRounding.AwayFromZero);
                item["Diff Inv vs Book"] = Convert.ToDouble(item[bookvalue]) - Convert.ToDouble(item["Inventory_Value"]);
                if (filtervalue == "G" && Convert.ToDouble(item["Diff Inv vs Book"]) >= 500)
                {
                    array.Add(item);
                }
                else if (filtervalue == "Y" && Convert.ToDouble(item["Diff Inv vs Book"]) < 500 && Convert.ToDouble(item["Diff Inv vs Book"]) > -500)
                {
                    array.Add(item);
                }
                else if (filtervalue == "R" && Convert.ToDouble(item["Diff Inv vs Book"]) <= -500)
                {
                    array.Add(item);
                }
                else if (filtervalue == "ALL")
                {
                    array.Add(item);
                }

            }

            if (array.Count == 0)
            {
                JObject obj1 = new JObject();
                obj1["No Record to display"] = "";
                JArray arr = new JArray();
                arr.Add(obj1);
                return csvSerializer.jsonToCSV(arr.ToString());
            }
            else
            {
                JArray sorted = new JArray(array.OrderBy(obje => obje["Diff Inv vs Book"]));
                return csvSerializer.jsonToCSV(sorted.ToString());
            }
        }

    }

   
}