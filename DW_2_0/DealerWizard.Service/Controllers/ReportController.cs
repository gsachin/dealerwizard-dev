﻿using DealerWizard.Service.Contracts;
using DealerWizard.Service.Models;
using Newtonsoft.Json.Linq;
using Percolator.AnalysisServices;
using Percolator.AnalysisServices.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.Results;

namespace DealerWizard.Service.Controllers
{

    public class MdxReportQuery
    {
        public string CubeName { get; set; }
        public string[] Measures { get; set; }
        public string[] Dimensions { get; set; }

    }

    public class ReportController : ApiController
    {

        private IReportRepository reportRepository;
        //private IDealershipRepository dealershipRepository;
        List<Translation> _translations = new List<Translation>();
        List<ICubeObject> _axis = new List<ICubeObject>();
        byte _currentAxis = 0;


        public ReportController(IReportRepository reportRepository)
        {
            this.reportRepository = reportRepository;
            //this.dealershipRepository = dealershipRepository;
        }
        [HttpPost]
        [Route("api/getqueryresult")]
        public JsonResult<JObject> GetQueryResult(MdxReportQuery reportQuery)
        {
            JArray column = new JArray();
            JObject JObj2 = new JObject();

            var cubeBase = new CubeBase("Data Source=DWCyberTestDB01");

            var query = new Models.MdxQuery();

            query.Cube = reportQuery.CubeName;
            List<Columns> columns = new List<Columns>();

            foreach (var item in reportQuery.Dimensions)
            {

                if (query.OnRows == null)
                {
                    query.OnRows = new Member(item);
                }else
                {
                    query.OnRows = query.OnRows as Member & new Member(item);

                }

            }

            foreach (var item in reportQuery.Measures)
            {

                if (query.OnColumns == null)
                {
                    query.OnColumns = new Measure(item);
                }else
                {
                    query.OnColumns = query.OnColumns as Measure & new Measure(item);

                }

            }

           
           //query.OnColumns = new Measure("Total FI Gross") & new Measure("Total Sales Gross");

            //query.OnRows = new Member("[Dealer].[Name].[Name]") * new Member("[Dealer].[Dealer Key].[Dealer Key]");

            if (query.OnColumns != null)
            this._axis.Add(query.OnColumns);

            if (query.OnRows != null)
                this._axis.Add(query.OnRows);

            query.MdxCommand = prepareMdxCommand(query);

            var table = cubeBase.Execute(query.MdxCommand);

            JArray array = new JArray();
            var tabColumn = table.Columns;

            foreach (DataColumn item in tabColumn)
            {
                columns.Add(new Columns { ColumnDataType = "String", ColumnDisplayName = item.ColumnName, ColumnName = item.ColumnName.Remove(" ", "[","]",".","_"), DataType = "String" });
            }

            foreach (DataRow row in table.Rows)
            {
                JObject JObj1 = new JObject();
                foreach (DataColumn item in tabColumn)
                {
                    JObj1[item.ColumnName.Remove(" ", "[", "]",".", "_")] = row[item.ColumnName].ToString();
                }
                
                array.Add(JObj1);
            }

            JObj2["data"] = array;
            JObj2["aggregates"] = JObj2["data"];

            /*

            var columns = reportRepository.ReportColumnsMetadata(reportid);
            if (reportid == 64)
            {
                var resultDefaultOverview = reportRepository.ReportDataDealershipOverviewDefault(dealer, startdate, endDate, operationFilterType, operationFilterValue, filters, columns, 64);
                JObj2["data"] = resultDefaultOverview["data"];
                JObj2["aggregates"] = JObj2["data"];
            }
            */

            foreach (var item in columns)
            {
                JObject JObj = new JObject();
                JObj["title"] = item.ColumnDisplayName;
                JObj["data"] = item.ColumnName;
                JObj["UserFriendlyName"] = item.UserFriendlyName;
                JObj["reportname"] = item.reportname;
                JObj["ismeasure"] = item.Ismeasure;
                JObj["Columndatatype"] = item.ColumnDataType;
                JObj["IsLink"] = item.IsLink;
                JObj["IsLinkOnHeader"] = item.IsLinkOnHeader;
                JObj["IsHidden"] = item.IsHidden;
                JObj["ReportRefKey"] = item.ReportRefKey;
                JObj["NextReportName"] = item.NextReportName;
                JObj["ReportFilters"] = item.ReportFilters;
                JObj["ReportType"] = item.ReportType;
                JObj["DataType"] = item.DataType;
                JObj["UrlName"] = item.UrlName;
                JObj["UrlPath"] = item.UrlPath;
                JObj["UrlRefKey"] = item.UrlRefKey;
                column.Add(JObj);
            }

            JObj2["columns"] = column;

            return Json(JObj2);
        }
        
        public string prepareMdxCommand(Models.MdxQuery query)
        {
            this._translations.Add(new Translation(191, string.Format("FROM [{0}]", query.Cube)));
            this._currentAxis = 193;
            setTranslation(query.OnColumns, true);

            // this._currentAxis = 1;
            setTranslation(query.OnRows, true);

            return this.assembleTranslations();
        }

        private void setTranslation(object obj, bool isNonEmpty)
        {
            if (obj != null)
            {
                if (obj is IEnumerable<ICubeObject>)
                {
                    foreach (var o in (IEnumerable<ICubeObject>)obj)
                    {
                        this._translations.Add(new Translation(this._currentAxis, o.ToString(), isNonEmpty));
                    }
                }
                else
                {
                    this._translations.Add(new Translation(this._currentAxis, obj.ToString(), isNonEmpty));
                }
            }
        }

        private string assembleTranslations()
        {
            byte _WMEMBER = 193, _WSET = 194, _SUBCUBE = 195, _FROM = 191;

            var sb = new StringBuilder("").AppendLine();
            sb.AppendLine();
            var members = this._translations.Where(x => x.Type == _WMEMBER);
            var sets = this._translations.Where(x => x.Type == _WSET);
            var combined = members.Union(sets).OrderBy(x => x.DeclarationOrder);
            /*
            if (combined.Count() > 0)
            {
                sb.AppendLine("");
                sb.AppendLine("WITH");
                foreach (var com in combined.OrderBy(x => x.DeclarationOrder))
                {
                    var type = com.Type == _WMEMBER ? "MEMBER" : "SET";
                    if (com.Name.Contains("_set"))
                    {
                        sb.AppendLine("");
                    }

                    if (com.Name.Contains("_member"))
                    {
                        sb.AppendLine("");
                    }
                    sb.AppendLine($"{type} {com.Name} AS");
                    sb.AppendLine(com.Value);
                    sb.AppendLine();
                }
            }
            */
            sb.AppendLine("");
            sb.AppendLine("SELECT");

            this._axis
                //.OrderBy(x => x.AxisNumber)
                .Select(x => x.ToString())
                .Aggregate((a, b) => $"{a} on 0,\r\n{b} on 1")
                .To(sb.AppendLine);

            if (this._axis.Count() == 1)
            {
                sb.AppendLine(" on 0");
            }

            sb.AppendLine("");
            var subCube = this._translations.FirstOrDefault(x => x.Type == _SUBCUBE);
            if (subCube != null)
            {
                sb.AppendLine("FROM")
                    .AppendLine("(")
                    .AppendLine("\tSELECT")
                    //.AppendLine("\t{0}", subCube.Value)
                    //.AppendLine($"\tON 0 {this._translations.First(x => x.Type == _FROM).Value}")
                    .AppendLine(")");
            }
            else
            {
                sb.AppendLine(this._translations.First(x => x.Type == _FROM).Value);
            }
            /*
            var slicers = this._translations.Where(x => x.Type == _WHERE);
            var slicerCount = slicers.Count();
            if (slicerCount > 0)
            {
                sb.AppendLine(Comment.FOR_SLICER_REGION);
                sb.AppendLine("WHERE\r\n(");

                slicers
                    .Select(x => x.Value)
                    .Aggregate((a, b) => $"\t{a},\r\n\t{b}")
                    .To(sb.AppendLine)
                    .To(s => s.AppendLine(")"));
            }
            */
            return sb.ToString();
        }
    }

    internal class Translation
    {
        public Translation(byte type, string value)
            : this(type, value, false)
        {
        }

        public Translation(byte type, string value, bool isNonEmpty)
        {
            this.Name = "";
            this.Type = type;
            this.Value = value;
            this.IsNonEmpty = isNonEmpty;
        }

        public byte Type { get; set; }

        public string Name { get; set; }

        public string Value { get; set; }

        public byte? DeclarationOrder { get; set; }

        public bool IsNonEmpty { get; set; }
    }



}
