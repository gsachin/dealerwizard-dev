﻿using DealerWizard.Service.Contracts;
using DealerWizard.Service.Models;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;

namespace DealerWizard.Service.Controllers
{
    [Authorize]
    public class AdminController : ApiController
    {
        private IReportRepository reportRepository;
        private IDealershipRepository dealershipRepository;

        public AdminController(IReportRepository reportRepository, IDealershipRepository dealershipRepository)
        {
            this.reportRepository = reportRepository;
            this.dealershipRepository = dealershipRepository;
        }

        [HttpGet]
        [Route("api/getreportdatabydepartmentkey")]
        public JsonResult<ReportDataByDepartmentKey> GetReportDataByDepartmentKey(int key)
        {
            JObject rep = new JObject();

            JArray reportsArray = new JArray();
            JArray columnsArray = new JArray();
            JObject reps = new JObject();
            var reportdata = reportRepository.ReportDataByDepartmentKey(key);

            foreach (var item in reportdata.ColumnsData)
            {

                JObject JObjcol = new JObject();
                JObjcol["Column_key"] = item.Column_key;
                JObjcol["Column_Display_Name"] = item.Column_Display_Name;
                JObjcol["IsMeasure"] = item.IsMeasure;

                columnsArray.Add(JObjcol);



            }
            rep["ColumnsData"] = columnsArray;


            foreach (var item in reportdata.ReportsData)
            {

                JObject JObjcol = new JObject();
                JObjcol["Report_Key"] = item.Report_Key;
                JObjcol["Report_Name"] = item.Report_Name;
                JObjcol["Status"] = item.Status;

                reportsArray.Add(JObjcol);



            }
            rep["ReportsData"] = reportsArray;

            return Json(reportdata);
        }

        [HttpGet]
        [Route("api/getallreports")]
        public JsonResult<JArray> GetAllReports()
        {
            JArray report = new JArray();
            var columns = reportRepository.AllReports();
            foreach (var item in columns)
            {
                JObject rep = new JObject();
                rep["Report_Key"] = item.ReportKey;
                rep["Report_Name"] = item.ReportName;
                rep["Dealer_Key"] = item.Dealer_Key;
                rep["Department_Key"] = item.DepartmentKey;
                rep["Status"] = item.Status;
                rep["Created_By"] = item.Created_By;
                rep["Report_Scope"] = item.ReportScope;
                rep["Report_Type"] = item.ReportType;
                rep["Department_Name"] = item.Department_Name;

                report.Add(rep);
            }
            return Json(report);
        }

        [HttpGet]
        [Route("api/getreportsdatabyid")]
        public JsonResult<Report> GetReportsDataById(int reportkey)
        {

            JObject rep = new JObject();

            JArray columnArray = new JArray();
            JArray filterArray = new JArray();
            JObject reps = new JObject();
            var reportdata = reportRepository.ReportDataByReortKey(reportkey);
            reps["Report_Key"] = reportdata.ReportKey;
            reps["Report_Name"] = reportdata.ReportName;
            reps["Dealer_Key"] = reportdata.Dealer_Key;
            reps["Department_Key"] = reportdata.DepartmentKey;
            reps["Status"] = reportdata.Status;
            reps["Created_By"] = reportdata.Created_By;
            reps["Report_Scope"] = reportdata.ReportScope;
            reps["Report_Type"] = reportdata.ReportType;

            //columnArray.Add(reps);
            rep["report"] = reps;


            foreach (var item in reportdata.Columnsdata)
            {

                JObject JObjcol = new JObject();
                JObjcol["Report_Param_Key"] = item.Report_Param_Key;
                JObjcol["Column_Key"] = item.Column_Key;
                JObjcol["Column_Display_Name"] = item.Column_Display_Name;
                JObjcol["Link_Enable"] = item.Link_Enable;
                JObjcol["Report_Ref_Key"] = item.Report_Ref_Key;
                JObjcol["URL_Ref_Key"] = item.URL_Ref_Key;
                JObjcol["Order_By"] = item.Order_By;
                JObjcol["IsHidden"] = item.IsHidden;
                JObjcol["Sequence"] = item.Sequence;
                JObjcol["Ismeasure"] = item.Ismeasure;
                columnArray.Add(JObjcol);



            }
            rep["Column"] = columnArray;


            foreach (var item in reportdata.Filterdata)
            {
                JObject JObjfilter = new JObject();

                JObjfilter["Report_Filter_Key"] = item.Report_Filter_Key;
                JObjfilter["Report_Key"] = item.Report_Key;
                JObjfilter["Column_Key"] = item.Column_Key;
                filterArray.Add(JObjfilter);

            }
            rep["filter"] = filterArray;

            return Json(reportdata);
        }

        [HttpGet]
        [Route("api/getreportdata")]
        public JsonResult<JObject> GetReportData(int dealer, string startdate, string endDate, int reportid, string operationFilterType, string operationFilterValue, string filters)
        { 
            JArray column = new JArray();
            JObject JObj2 = new JObject();

            var columns = reportRepository.ReportColumnsMetadata(reportid);
            if (reportid == 64)
            {
                var resultDefaultOverview = reportRepository.ReportDataDealershipOverviewDefault(dealer, startdate, endDate, operationFilterType, operationFilterValue, filters, columns, 64);
                JObj2["data"] = resultDefaultOverview["data"];
                JObj2["aggregates"] = JObj2["data"];
            }
            else if(reportid==49)
            {
                var resultDefaultOverview = reportRepository.ReportDataDealTypeSummary(dealer, startdate, endDate, operationFilterType, operationFilterValue, filters, columns, 49);
                JObj2["data"] = resultDefaultOverview["data"];
                JObj2["aggregates"] = JObj2["data"];
            }
            else
            {
                var result = reportRepository.ReportData(dealer, startdate, endDate, reportid, operationFilterType, operationFilterValue, filters, columns);
                JObj2["data"] = result["data"];
                JObj2["aggregates"] = result["aggregates"];
            }

            foreach (var item in columns)
            {
                JObject JObj = new JObject();
                JObj["title"] = item.ColumnDisplayName;
                JObj["data"] = item.ColumnName;
                JObj["UserFriendlyName"] = item.UserFriendlyName;
                JObj["reportname"] = item.reportname;
                JObj["ismeasure"] = item.Ismeasure;
                JObj["Columndatatype"] = item.ColumnDataType;
                JObj["IsLink"] = item.IsLink;
                JObj["IsLinkOnHeader"] = item.IsLinkOnHeader;
                JObj["IsHidden"] = item.IsHidden;
                JObj["ReportRefKey"] = item.ReportRefKey;
                JObj["NextReportName"] = item.NextReportName;
                JObj["ReportFilters"] = item.ReportFilters;
                JObj["ReportType"] = item.ReportType;
                JObj["DataType"] = item.DataType;
                JObj["UrlName"] = item.UrlName;
                JObj["UrlPath"] = item.UrlPath;
                JObj["UrlRefKey"] = item.UrlRefKey;
                column.Add(JObj);
            }

            JObj2["columns"] = column;

            return Json(JObj2);
        }

        [HttpPost]
        [Route("api/Reportdata/reportsave")]
        public HttpResponseMessage reportsave(ReportConfiguratorModel json)
        {
            var data = reportRepository.InsertReportData(json);
            HttpResponseMessage msg = new HttpResponseMessage();
            return msg;
        }

        [HttpGet]
        [Route("api/getreportobject")]
        public JsonResult<IEnumerable<ReportObjectModel>> GetReportObject(int dealerid, string departmentcode)
        {
            var result = reportRepository.GetReportObjectByDepartmentDealerKey(dealerid, departmentcode);
            return Json(result);
        }

        [HttpGet]
        [Route("api/getdepartments")]
        public JsonResult<IEnumerable<Department>> GetDepartments()
        {
            return Json(reportRepository.DepartmentData());
        }

        [HttpGet]
        [Route("api/getusedcarequitydetail")]
        public JsonResult<JObject> GetUsedCarEquityDetail(int dealerkey, string startdate, string enddate)
        {
            var dealerBookValue = dealershipRepository.GetDealerWiseBookValueList(dealerkey);

            var listbookitem = new List<BookValueItem>();
            listbookitem.Add(new BookValueItem { BookValueName = "Booked", BookValueText = "Booked", ColumnType = "String" });
            listbookitem.Add(new BookValueItem { BookValueName = "Stock_Number", BookValueText = "Stock_Number", ColumnType = "String" });
            listbookitem.Add(new BookValueItem { BookValueName = "P/T", BookValueText = "P/T", ColumnType = "String" });
            listbookitem.Add(new BookValueItem { BookValueName = "Age", BookValueText = "Age", ColumnType = "String" });
            listbookitem.Add(new BookValueItem { BookValueName = "VIN", BookValueText = "VIN", ColumnType = "String" });
            listbookitem.Add(new BookValueItem { BookValueName = "Year", BookValueText = "Year", ColumnType = "String" });
            listbookitem.Add(new BookValueItem { BookValueName = "Make", BookValueText = "Make", ColumnType = "String" });
            listbookitem.Add(new BookValueItem { BookValueName = "Model", BookValueText = "Model", ColumnType = "String" });
            listbookitem.Add(new BookValueItem { BookValueName = "Miles", BookValueText = "Miles", ColumnType = "String" });
            listbookitem.Add(new BookValueItem { BookValueName = "Inventory_Value", BookValueText = "Inventory_Value", ColumnType = "Double" });

            listbookitem.AddRange(dealerBookValue);
            var result = reportRepository.GetUsedCarEquityDetail(dealerkey, startdate, enddate, listbookitem);

            return Json(result);

        }

        [HttpGet]
        [Route("api/getuserfriendlyname")]
        public JsonResult<JArray> GetUserFriendlyName()
        {
           
            JArray userdisplayname = new JArray();
            JObject rep = new JObject();
            var names = reportRepository.UserFriendlyName();
            foreach (var item in names)
            {
                rep["ShortName"] = item.Display_Name;
                rep["UserFriendlyName"] = item.User_Friendly_Name;
                userdisplayname.Add(rep);
            }
            return Json(userdisplayname);
        }
    }
}