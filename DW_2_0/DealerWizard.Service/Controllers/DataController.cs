﻿using DealerWizard.Service.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Results;
using DealerWizard.Service.Contracts;
using DealerWizard.Service.Constants;
using Newtonsoft.Json.Linq;
using System;

namespace DealerWizard.Web.Api
{
    [Authorize]
    public class DataController : ApiController
    {
        private IReportRepository reportRepository;
        private ICoreRepository _coreRepository;
        private IForecastRepository _forecast;
        private IDealershipRepository _dealershipRepository;
        IUserSessionManager userSessionManager;


        public DataController( IReportRepository reportRepository, ICoreRepository coreRepository,
            IForecastRepository Forecast, IUserSessionManager userSessionManager, IDealershipRepository dealershipRepository) {
            this.reportRepository = reportRepository;
            this._coreRepository = coreRepository;
            this._forecast = Forecast;
            this.userSessionManager = userSessionManager;
            this._dealershipRepository = dealershipRepository;
        }

        [HttpGet]
        [Route("api/currentuser")]
        public JsonResult<LoggedInUser> GetUserPreference()
        {
            var user = userSessionManager.GetCurrentUser();
            var userPreference = _coreRepository.GetUserPreferences(user.Id);
            var loggedInUser = new LoggedInUser
            {
                User = user,
                DefaultDealer = new UserAssociatedDealership() { ReferenceDealerId = userPreference.DLR_ID, Name = userPreference.DLR_Name },
                Preference = userPreference
            };
            return Json(loggedInUser);
        }


        [HttpGet]
        [Route("api/getdealers")]
        public JsonResult<IEnumerable<UserAssociatedDealership>> GetDealers()
        {
            var associatedDealerships = new List<UserAssociatedDealership>();
            var userRoles = userSessionManager.UserRoles;
            if (userRoles.Contains(UserRoles.dwAdmin) || userRoles.Contains(UserRoles.sAdmin))
            {
                if (userRoles.Contains(UserRoles.dwInactiveDlr))
                {
                    associatedDealerships.AddRange(_coreRepository.GetAllActiveDealers());
                }
                else
                {
                    associatedDealerships.AddRange(_coreRepository.GetAllDealers());
                }
            }
            else
            {
                var user = userSessionManager.GetCurrentUserId();
                associatedDealerships.AddRange(_coreRepository.GetAssociatedDealersByUser(user));

            }
            return Json(associatedDealerships.AsEnumerable());
        }


        [HttpGet]
        [Route("api/getMenuItems")]
        public JsonResult<JObject> getMenuItems()
        {

            JArray array = new JArray();
            JArray menuItemsData = new JArray();
            JObject JObj2 = new JObject();
            var menuItems = _coreRepository.MenuItemsByUserId(userSessionManager.GetCurrentUser().Id);

            foreach (var item in menuItems)
            {
                JObject JObj = new JObject();
                JObj["MenuId"] = item.MenuId;
                JObj["Name"] = item.Name;
                JObj["URL"] = item.URL;
                JObj["Active"] = item.Active;
                JObj["ParentMenuId"] = item.ParentMenuId;
                JObj["Order"] = item.Order;
                JObj["IconClassName"] = item.IconClassName;
                menuItemsData.Add(JObj);
            }

            JObj2["menuItems"] = menuItemsData;

            return Json(JObj2);
        }


        [HttpGet]
        [Route("api/dealersetting")]
        public JsonResult<DealerSetting> GetDealerSetting(int dealer)
        {
            var dealerSetting = _dealershipRepository.GetDealerSetting(dealer);
            return Json(dealerSetting);
        }
        [HttpGet]
        [Route("api/dealerbookitemlist")]
        public JsonResult<JArray> GetDealerWiseBookItemList(int dealerkey)
        {
            var dealerBookValue = _dealershipRepository.GetDealerWiseBookValueList(dealerkey);

            JArray BookValueList = new JArray();

            foreach (var item in dealerBookValue)
            {
                JObject JObj = new JObject();
                JObj["DealerNumber"] = item.DealerNumber;
                JObj["BookValueText"] = item.BookValueText;
                JObj["BookValueName"] = item.BookValueName;
                JObj["SortOrder"] = item.SortOrder;
                BookValueList.Add(JObj);
            }

            return Json(BookValueList);
        }
    }
}
