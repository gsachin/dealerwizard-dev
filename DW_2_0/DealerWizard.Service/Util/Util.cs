﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AnalysisServices.AdomdClient;
using System.Text;
using DealerWizard.Web.Domain;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace DealerWizard.Web
{
    public static class Util
    {
        public static List<SalesData> ListOfDealerSales(string measure, int dealer, string periodtype, string date)
        {
            var ls = new List<SalesData>();


            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dwconnection"].ToString()))
            {
                conn.Open();
                SqlDataAdapter sda = new SqlDataAdapter("sp_TimeSeriesChart", conn);
                sda.SelectCommand.CommandType = CommandType.StoredProcedure;
                sda.SelectCommand.Parameters.AddWithValue("@EndDate", DateTime.ParseExact(date, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy-MM-dd"));
                sda.SelectCommand.Parameters.AddWithValue("@Dealer", dealer);
                sda.SelectCommand.Parameters.AddWithValue("@measure", measure);
                sda.SelectCommand.Parameters.AddWithValue("@PeriodType", periodtype);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                if (periodtype.ToLower() == "mtd")
                {
                    foreach (DataRow dr in dt.Rows)
                    {

                        ls.Add(
                               new SalesData()
                               {

                                   date = dr["Period"].ToString(),
                                   currentamount = decimal.Parse(dr["Current_Amt"].ToString()),
                                   Previousdate = dr["PRV_Period"].ToString(),
                                   Previousamount = decimal.Parse(dr["Prv_Amt"].ToString()),
                                   PreviousYearMonthdate = dr["LastPrv_Period"].ToString(),
                                   PreviousYearMonthAmount = decimal.Parse(dr["LastPrv_Amt"].ToString())
                               });
                    }
                }
                else
                {
                    foreach (DataRow dr in dt.Rows)
                    {

                        ls.Add(
                               new SalesData()
                               {

                                   date = dr["Period"].ToString(),
                                   currentamount = decimal.Parse(dr["Current_Amt"].ToString()),
                                   Previousdate = dr["PRV_Period"].ToString(),
                                   Previousamount = decimal.Parse(dr["Prv_Amt"].ToString()) 
                               });

                    }
                }
            }
            return ls;



        }



        public static List<SalesView> GetVehicleDataSalesWise(int Dealer, string date)
        {
            var ls = new List<SalesView>();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dwconnection"].ToString()))
            {
                SqlDataAdapter sda = new SqlDataAdapter("spGetSalesDataByDealerandDate", conn);
                sda.SelectCommand.CommandType = CommandType.StoredProcedure;
                sda.SelectCommand.Parameters.AddWithValue("@Dealer", Dealer);
                sda.SelectCommand.Parameters.AddWithValue("@ReportDate", DateTime.ParseExact(date, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy-MM-dd"));
                DataTable dt = new DataTable();
                sda.Fill(dt);
                foreach (DataRow dr in dt.Rows)
                {

                    ls.Add(
                           new SalesView()
                           {
                               Make = dr["Make"].ToString(),
                               Model = dr["Model"].ToString(),
                               SalesPerson = dr["Sales Person"].ToString(),
                               FiManager = dr["FI Manager"].ToString(),
                               UnitsSold = decimal.Parse(dr["Units Sold"].ToString()),
                               FiGross = decimal.Parse(dr["FI Gross"].ToString()),
                               SalesGross = decimal.Parse(dr["Sales Gross"].ToString()),
                           });
                }
            }
            return ls;
        }

        public static List<ServiceView> GetVehicleDataServiceWise(int Dealer, string year)
        {
            var ls = new List<ServiceView>();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dwconnection"].ToString()))
            {
                SqlDataAdapter sda = new SqlDataAdapter("spGetServiceDataByDealerandDate", conn);
                sda.SelectCommand.CommandType = CommandType.StoredProcedure;
                sda.SelectCommand.Parameters.AddWithValue("@Dealer", Dealer);
                sda.SelectCommand.Parameters.AddWithValue("@ReportDate", DateTime.ParseExact(year, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy-MM-dd"));
                DataTable dt = new DataTable();
                sda.Fill(dt);
                foreach (DataRow dr in dt.Rows)
                {

                    ls.Add(
                           new ServiceView()
                           {
                               Make = dr["Make"].ToString(),
                               Model = dr["Model"].ToString(),
                               Technician = dr["Technician"].ToString(),
                               PartsGross = decimal.Parse(dr["Parts Gross"].ToString()),
                               ServiceGross = decimal.Parse(dr["Service Gross"].ToString()),
                               DealershipGross = decimal.Parse(dr["DealershipServiceGross"].ToString())
                           });

                }
            }
            return ls;
        }

        public static List<ProductDealData> ListofSalesGrossUserWise(string Dealer, int year)
        {
            var Productdeals = new List<ProductDealData>();

            using (var conn = new AdomdConnection("Data Source=DWDEVDB002;Catalog=DealerWizard"))
            {
                conn.Open();
                using (var command = new AdomdCommand())
                {
                    command.Connection = conn;
                    command.CommandType = System.Data.CommandType.Text;

                    StringBuilder sb = new StringBuilder();

                    sb.Append(" SELECT NON EMPTY { [Measures].[Total Sales Gross], [Measures].[Units Sold - Tbl Deals Fact]  } ");
                    sb.Append(" ON COLUMNS, NON EMPTY { ([Sales Manager].[Dlr Staff Name].[Dlr Staff Name].ALLMEMBERS ) } ");
                    sb.Append(" DIMENSION PROPERTIES MEMBER_CAPTION, MEMBER_UNIQUE_NAME ON ROWS FROM[ProductWiseDeals] ");
                    sb.Append(" WHERE([Deal Date].[Year].&[" + Convert.ToInt32(year) + "],[Dealer].[Dealer Key].&[" + Convert.ToInt32(Dealer) + "] ) ");
                    sb.Append(" CELL PROPERTIES VALUE, ");
                    sb.Append(" BACK_COLOR, FORE_COLOR, FORMATTED_VALUE, FORMAT_STRING, FONT_NAME, FONT_SIZE, FONT_FLAGS ");


                    command.CommandText = sb.ToString();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Productdeals.Add(
                                new ProductDealData
                                {

                                    SalesManager = reader[0].ToString(),
                                    TotalSalesGross = double.Parse(reader[2].ToString()) / 100000,
                                    UnitSold = int.Parse(reader[3].ToString())
                                });
                        }
                    }
                }
            }
            return Productdeals;
        }

        public static List<GrossData> BulletGrossData(int Dealer, string year, string measure)
        {
            var GrossDataSet = new List<GrossData>();

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dwconnection"].ToString()))
            {
                conn.Open();
                SqlDataAdapter sda = new SqlDataAdapter("sp_BulletChart", conn);
                sda.SelectCommand.CommandType = CommandType.StoredProcedure;
                sda.SelectCommand.Parameters.AddWithValue("@Dealer", Dealer);
                sda.SelectCommand.Parameters.AddWithValue("@Date", DateTime.ParseExact(year, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy-MM-dd"));
                sda.SelectCommand.Parameters.AddWithValue("@measure", measure);
                DataSet ds = new DataSet();
                sda.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    DataRow dataRow1 = ds.Tables[0].Rows[0];
                    var tempGrossData = new GrossData();
                    tempGrossData.type = dataRow1["Period"].ToString();
                    tempGrossData.CurrentYData = Convert.ToDouble(dataRow1["Gross_YTD"]);
                    tempGrossData.PreviousYData = Convert.ToDouble(dataRow1["Gross_PYTD"]);
                    tempGrossData.diff = Convert.ToDouble(dataRow1["Gross_PYTD_Diff"]);
                    GrossDataSet.Add(tempGrossData);

                    var tempGrossData2 = new GrossData();
                    DataRow dataRow2 = ds.Tables[0].Rows[1];
                    tempGrossData2.type = dataRow2["Period"].ToString();
                    tempGrossData2.CurrentYData = Convert.ToDouble(dataRow2["Gross_YTD"]);
                    tempGrossData2.PreviousYData = Convert.ToDouble(dataRow2["Gross_PYTD"]);
                    tempGrossData2.diff = Convert.ToDouble(dataRow2["Gross_PYTD_Diff"]);
                    GrossDataSet.Add(tempGrossData2);

                    var tempGrossData3 = new GrossData();

                    DataRow dataRow3 = ds.Tables[0].Rows[2];
                    tempGrossData3.type = dataRow3["Period"].ToString();
                    tempGrossData3.CurrentYData = Convert.ToDouble(dataRow3["Gross_YTD"]);
                    tempGrossData3.PreviousYData = Convert.ToDouble(dataRow3["Gross_PYTD"]);
                    tempGrossData3.diff = Convert.ToDouble(dataRow3["Gross_PYTD_Diff"]);
                    GrossDataSet.Add(tempGrossData3);
                }
            }
            return GrossDataSet;
        }
        public static GrossData BulletServiceData(int Dealer, int year)
        {

            var GrossDataSet = new GrossData();

            using (var conn = new AdomdConnection("Data Source=DWDEVDB002;Catalog=DealerWizard"))
            {
                conn.Open();

                using (var command = new AdomdCommand())
                {
                    #region For Current Year
                    command.Connection = conn;
                    command.CommandType = System.Data.CommandType.Text;

                    StringBuilder sb = new StringBuilder();

                    sb.Append(" With Member Measures.YTDServiceGross as SUM (YTD([ClosedDate].[Date Hierarchy].CurrentMember), ");
                    sb.Append(" [Measures].[LaborGross] ) Select Measures.YTDServiceGross on Columns, { ");
                    sb.Append(" [ClosedDate].[Date Hierarchy].[Year].&[" + year + "] ");
                    sb.Append(" } on Rows From[ServiceDept] ");
                    sb.Append(" WHERE([Dealer].[Dealer Key].&[" + Dealer + "] ) ");

                    command.CommandText = sb.ToString();
                    double tempVar = 0.0d;
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            tempVar = Convert.ToDouble(reader[1]);
                        }
                    }
                    #endregion

                    #region Previous Year

                    sb = new StringBuilder();

                    sb.Append(" With Member Measures.LastYTDServiceGross as SUM ( ");
                    sb.Append(" YTD(ParallelPeriod([ClosedDate].[Date Hierarchy].[Year], 1, [ClosedDate].[Date Hierarchy].CurrentMember)),[Measures].[LaborGross] ");
                    sb.Append(" ) Select Measures.LastYTDServiceGross on Columns, ");
                    sb.Append(" {[ClosedDate].[Date Hierarchy].[Year].&[" + (year - 1) + "]} on Rows ");
                    sb.Append(" From[ServiceDept] ");
                    sb.Append(" WHERE([Dealer].[Dealer Key].&[" + Dealer + "] ) ");

                    command.CommandText = sb.ToString();
                    double tempVar2 = 0.0d;
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            tempVar2 = Convert.ToDouble(reader[1]);
                        }
                    }

                    #endregion


                    GrossDataSet.CurrentYData = tempVar;
                    GrossDataSet.PreviousYData = tempVar2;
                }
            }
            return GrossDataSet;
        }
    }
}