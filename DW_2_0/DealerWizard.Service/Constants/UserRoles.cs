﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DealerWizard.Service.Constants
{
    public static class UserRoles
    {
        public const string dwAdmin = "dwAdmin";
        public const string sAdmin = "sAdmin";
        public const string createinquiry700 = "createinquiry";
        public const string viewinquiry700 = "viewinquiry";
        public const string dAdmin = "dAdmin";
        public const string dMail = "dMail";
        public const string dmSales = "dmSales";
        public const string dmService = "dmService";
        public const string dwAdmin_Edit = "dwAdmin-Edit";
        public const string dwInactiveDlr = "dwInactiveDlr";
        public const string FI = "FI";
        public const string GLDoc = "GLDoc";
        public const string newCars = "newCars";
        public const string sAdmin_ClientOnly = "sAdmin-ClientOnly";
        public const string sAdmin_ClientOnlyBasic = "sAdmin-ClientOnlyBasic";
        public const string salesOpps = "salesOpps";
        public const string service = "service";
        public const string serviceOpps = "serviceOpps";
        public const string usageReport = "usageReport";
        public const string usedCars = "usedCars";
        public const string wizDocReport = "wizDocReport";
    }
}