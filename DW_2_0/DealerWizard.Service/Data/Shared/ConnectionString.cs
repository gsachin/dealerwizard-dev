﻿using System.Configuration;

namespace DealerWizard.Data.Shared
{
    public class ConnectionString
    {
        public static string Get()
        {
            return ConfigurationManager.ConnectionStrings["dwconnection"].ToString();
        }
        public static string GetAdminConString()
        {
            return ConfigurationManager.ConnectionStrings["dwadminconnection"].ToString();
        }

        public static string GetDWConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["DMSConnectionString"].ToString();
        }
    }
}