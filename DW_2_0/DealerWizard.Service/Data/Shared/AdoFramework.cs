﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq;
using System.Data.SqlClient;
using System.Text;
using System.Xml.Linq;

namespace DealerWizard.Data.Shared
{
    public class AdoFramework
    {
        #region Parameter types

        public class Parameter
        {
            public string Name { get; private set; }
            public SqlDbType Type { get; private set; }
            public Object Value { get; private set; }
            public int? Size { get; private set; }

            public Parameter(string name, SqlDbType type, Object value)
            {
                Name = name;
                Type = type;
                Value = value;
            }
            
            public Parameter(string name, SqlDbType type, Object value, int size) : this(name, type, value)
            {
                Size = size;
            }

        }

        public class StructuredParameter : Parameter
        {
            public string TypeName { get; private set; }

            public StructuredParameter(string name, string typeName, Object value)
                : base(name, SqlDbType.Structured, value)
            {
                TypeName = typeName;
            }
        }

        public class OutputParameter<T> : Parameter
        {
            public OutputParameter(string name, SqlDbType type)
                : base(name, type, default(T))
            {
            }

            public OutputParameter(string name, SqlDbType type, int size)
                : base(name, type, default(T), size)
            {
            }

        }

        public delegate void ReaderProcessor(SqlDataReader reader);

        public delegate void OutputParamaeterProcessor(SqlParameter parameter);

        #endregion

        public static string SafeSqlString(string inputval)
        {
            return inputval.Replace(@"\", @"\\").Replace(@"%", @"\%").Replace(@"_", @"\_").Replace(@"[", @"\[");

        }
        public static T ExecuteNonQuery<T>(string connectionString, string storedProcedureName, IEnumerable<Parameter> parameters, int timeoutInSeconds = 30)
        {
            T t = default(T);

            ExecuteInternal(
                connectionString,
                storedProcedureName,
                parameters,timeoutInSeconds,
                outputParameterProcessor: x =>
                {
                    t = (T)x.Value;
                });

            return t;
        }

        public static void ExecuteNonQuery(string connectionString, string storedProcedureName, IEnumerable<Parameter> parameters)
        {
            ExecuteInternal(connectionString, storedProcedureName, parameters);
        }

        private static void ExecuteInternal(string connectionString, string storedProcedureName, IEnumerable<Parameter> parameters, int timeoutInSeconds = 30, OutputParamaeterProcessor outputParameterProcessor = null, params ReaderProcessor[] processors)
        {

            using (var connection = new SqlConnection(connectionString))
            using (var command = connection.CreateCommand())
            {
                connection.Open();
                command.CommandText = storedProcedureName;
                command.CommandType = CommandType.StoredProcedure;
                command.CommandTimeout = timeoutInSeconds;

                try
                {

                    foreach (Parameter parameter in parameters)
                    {

                        var sqlParameter = new SqlParameter(parameter.Name, parameter.Type);
                        var value = parameter.Value;
                        if (parameter.Type == SqlDbType.DateTime
                         || parameter.Type == SqlDbType.DateTime2
                         || parameter.Type == SqlDbType.DateTimeOffset)
                        {
                            DateTime dateValue;
                            if (DateTime.TryParse(Convert.ToString(value), out dateValue))
                            {

                                value = (dateValue == DateTime.MinValue)
                                            ? new DateTime(1900, 01, 01, 00, 00, 00)
                                            : dateValue;
                            }else
                            {
                                // set parameter value to null when supplied value is not a valid date.
                                value = null;
                            }
                            ////var dateValue = value as DateTime;
                            //if (dateValue == DateTime.MinValue)
                            //{
                            //    value = new DateTime(1900, 01, 01, 00, 00, 00);
                            //}
                        }
                        sqlParameter.Value = value ?? DBNull.Value;
                        if (parameter.Size.HasValue)
                        {
                            sqlParameter.Size = parameter.Size.Value;
                        }


                        //var sqlParameter = new SqlParameter(parameter.Name, parameter.Type);
                        //sqlParameter.Value = parameter.Value ?? DBNull.Value;
                        if (parameter.GetType().IsGenericType 
                            && parameter.GetType().GetGenericTypeDefinition() == typeof(OutputParameter<>))
                        {
                            sqlParameter.Direction = ParameterDirection.Output;
                        }

                        var structuredParameter = parameter as StructuredParameter;
                        if (structuredParameter != null)
                        {
                            sqlParameter.TypeName = structuredParameter.TypeName;
                        }

                        command.Parameters.Add(sqlParameter);
                    }

                    if (processors.Length == 0)
                    {
                        command.ExecuteNonQuery();
                    }

                    if (outputParameterProcessor != null)
                    {
                        for (var i = 0; i < command.Parameters.Count; i++)
                        {
                            if (command.Parameters[i].Direction == ParameterDirection.Output)
                                outputParameterProcessor(command.Parameters[i]);
                        }
                    }

                    if (processors.Length != 0)
                    {
                        using (var reader = command.ExecuteReader(CommandBehavior.CloseConnection))
                        {
                            foreach (ReaderProcessor processor in processors)
                            {
                                while (reader.Read())
                                {
                                    processor(reader);
                                }

                                reader.NextResult();
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    throw new Exception(
                        string.Format("AdoFramework.ReadRecords - {0} - Command: {1}",
                        e.Message, CommandToString(command)), e);
                }
            }
        }

        #region Reading records from stored procedures

        /// <summary>
        /// Reads one or more record sets from a stored procedure.
        /// </summary>
        /// <param name="connectionString">
        /// Connection string to the database.
        /// </param>
        /// <param name="storedProcedureName">
        /// Name of  the stored procedure
        /// </param>
        /// <param name="parameters">
        /// Collection specifying the names, types and values of the parameters to pass to the stored procedure.
        ///  </param>
        /// <param name="processors">
        /// One or more methods that process each record.
        /// List one method per record set returned by the stored procedure.
        /// 
        /// If the stored procedure returns just one record set, pass in one method. This method will be called for all records in that one
        /// record set.
        /// If the stored procedure returns two record sets, pass in two methods. The first method will be called for each record in the
        /// first record set. The second method will be called for each record in the second record set.
        /// 
        /// If your stored procedure returns 3 records sets, pass in 3 methods, etc.
        /// 
        /// Search for usages of ReadRecords (this method) to see how all this is used in practice.
        /// </param>
        public static void ReadRecords(string connectionString, string storedProcedureName, IEnumerable<Parameter> parameters, params ReaderProcessor[] processors)
        {
            ExecuteInternal(connectionString, storedProcedureName, parameters, processors: processors);
        }
        
        /// <summary>
        /// This method is to used to expand the timeout period for sql execution instead of the default 30 seconds.
        /// This is ONLY used to serve the special cases that users expect to wait longer, e.g. complicated search.
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="storedProcedureName"></param>
        /// <param name="timeoutInSeconds"></param>
        /// <param name="parameters"></param>
        /// <param name="processors"></param>
        public static void ReadRecordsWithLongTimeout(string connectionString, string storedProcedureName,
            int timeoutInSeconds, IEnumerable<Parameter> parameters, params ReaderProcessor[] processors)
        {
            ExecuteInternal(connectionString, storedProcedureName, parameters, timeoutInSeconds, processors: processors);
        }

        #endregion

        #region Conversion methods

        // ------------------
        // Conversion methods
        //
        // These methods read a value from a field in a SqlDataReader and return a strongly typed value.
        //
        // In general, you'll find up to 3 methods per type (not all types have all 3):
        //
        // * A method for reading non-nullable types from non-nullable fields - such as
        //   int ReadInt(string fieldName, SqlDataReader reader)
        //
        // * A method for reading non-nullable types from nullable fields, using a default value for null database value - such as 
        //   int ReadInt(string fieldName, int defaultValue, SqlDataReader reader)
        //
        // * A method for reading nullable types - such as
        //   int? ReadIntNullable(string fieldName, SqlDataReader reader)
        //
        // Feel free to add conversion methods if needed.

        public static TResult Read<TResult>(string fieldName, SqlDataReader reader, Func<object, TResult> map)
        {
            var data = reader[fieldName];

            return map(data);
        }

        public static TResult ReadNullable<TResult>(string fieldName, SqlDataReader reader, Func<object, TResult> map)
        {
            var data = reader[fieldName];

            return data == DBNull.Value
                ? default(TResult)
                : map(data);
        }

        public static Binary ReadBinary(string fieldName, SqlDataReader reader)
        {
            var data = reader[fieldName];

            if (data == DBNull.Value)
            {
                return null;
            }

            return new Binary((byte[])data);
        }

        public static byte[] ReadByteArray(string fieldName, SqlDataReader reader)
        {
            var data = reader[fieldName];

            if (data == DBNull.Value)
            {
                return null;
            }

            return (byte[])data;
        }

        public static XElement ReadXElement(string fieldName, SqlDataReader reader)
        {
            Object data = reader[fieldName];

            if (data == DBNull.Value)
            {
                return null;
            }

            return XElement.Parse(data.ToString());
        }

        public static string ReadString(string fieldName, SqlDataReader reader)
        {
            Object data = reader[fieldName];

            if (data == DBNull.Value)
            {
                return null;
            }

            return data.ToString();
        }

        public static char ReadChar(string fieldName, SqlDataReader reader)
        {
            Object data = reader[fieldName];

            return Char.Parse(data.ToString());
        }

        public static char? ReadCharNullable(string fieldName, SqlDataReader reader)
        {
            Object data = reader[fieldName];

            if (data == DBNull.Value)
            {
                return null;
            }

            return Char.Parse(data.ToString());
        }

        public static Guid? ReadGuidNullable(string fieldName, SqlDataReader reader)
        {
            Object data = reader[fieldName];

            if (data == DBNull.Value)
            {
                return null;
            }

            return Guid.Parse(data.ToString());
        }

        public static Guid ReadGuid(string fieldName, SqlDataReader reader)
        {
            Object data = reader[fieldName];

            if (data == DBNull.Value)
            {
                return Guid.Empty;
            }

            return Guid.Parse(data.ToString());
        }

        public static DateTime ReadDateTime(string fieldName, SqlDataReader reader)
        {
            Object data = reader[fieldName];

            return DateTime.Parse(data.ToString());
        }

        public static TimeSpan? ReadTime(string fieldName, SqlDataReader reader)
        {
            Object data = reader[fieldName];
            if (data == DBNull.Value)
            {
                return null;
            }
            return TimeSpan.Parse(data.ToString());
        }

        public static DateTime? ReadDateTimeNullable(string fieldName, SqlDataReader reader)
        {
            Object data = reader[fieldName];

            if (data == DBNull.Value)
            {
                return null;
            }

            return DateTime.Parse(data.ToString());
        }

        public static long ReadLong(string fieldName, SqlDataReader reader)
        {
            Object data = reader[fieldName];

            return long.Parse(data.ToString());
        }

        public static int ReadInt(string fieldName, SqlDataReader reader)
        {
            Object data = reader[fieldName];

            return int.Parse(data.ToString());
        }

        public static short ReadShort(string fieldName, SqlDataReader reader)
        {
            Object data = reader[fieldName];

            return short.Parse(data.ToString());
        }

        public static byte ReadByte(string fieldName, SqlDataReader reader)
        {
            Object data = reader[fieldName];

            return byte.Parse(data.ToString());
        }

        public static bool ReadBool(string fieldName, SqlDataReader reader)
        {
            Object data = reader[fieldName];

            return bool.Parse(data.ToString());
        }

        public static long? ReadLongNullable(string fieldName, SqlDataReader reader)
        {
            Object data = reader[fieldName];

            if (data == DBNull.Value)
            {
                return null;
            }

            return long.Parse(data.ToString());
        }

        public static int? ReadIntNullable(string fieldName, SqlDataReader reader)
        {
            Object data = reader[fieldName];

            if (data == DBNull.Value)
            {
                return null;
            }

            return int.Parse(data.ToString());
        }

        public static double? ReadDoubleNullable(string fieldName, SqlDataReader reader)
        {
            Object data = reader[fieldName];

            if (data == DBNull.Value)
            {
                return null;
            }

            return double.Parse(data.ToString());
        }

        public static double ReadDouble(string fieldName, SqlDataReader reader)
        {
            Object data = reader[fieldName];

            return double.Parse(data.ToString());
        }

        public static short? ReadShortNullable(string fieldName, SqlDataReader reader)
        {
            Object data = reader[fieldName];

            if (data == DBNull.Value)
            {
                return null;
            }

            return short.Parse(data.ToString());
        }

        public static byte? ReadByteNullable(string fieldName, SqlDataReader reader)
        {
            Object data = reader[fieldName];

            if (data == DBNull.Value)
            {
                return null;
            }

            return byte.Parse(data.ToString());
        }

        public static bool? ReadBoolNullable(string fieldName, SqlDataReader reader)
        {
            Object data = reader[fieldName];

            if (data == DBNull.Value)
            {
                return null;
            }

            return bool.Parse(data.ToString());
        }


      



        public static long ReadLong(string fieldName, long defaultValue, SqlDataReader reader)
        {
            Object data = reader[fieldName];

            if (data == DBNull.Value)
            {
                return defaultValue;
            }

            return long.Parse(data.ToString());
        }

        public static int ReadInt(string fieldName, int defaultValue, SqlDataReader reader)
        {
            Object data = reader[fieldName];

            if (data == DBNull.Value)
            {
                return defaultValue;
            }

            return int.Parse(data.ToString());
        }

        public static short ReadShort(string fieldName, short defaultValue, SqlDataReader reader)
        {
            Object data = reader[fieldName];

            if (data == DBNull.Value)
            {
                return defaultValue;
            }

            return short.Parse(data.ToString());
        }

        public static byte ReadByte(string fieldName, byte defaultValue, SqlDataReader reader)
        {
            Object data = reader[fieldName];

            if (data == DBNull.Value)
            {
                return defaultValue;
            }

            return byte.Parse(data.ToString());
        }

        public static bool ReadBool(string fieldName, bool defaultValue, SqlDataReader reader)
        {
            Object data = reader[fieldName];

            if (data == DBNull.Value)
            {
                return defaultValue;
            }

            return bool.Parse(data.ToString());
        }

        public static decimal ReadDecimal(string fieldName, SqlDataReader reader)
        {
            Object data = reader[fieldName];

            return Decimal.Parse(data.ToString());
        }

        public static decimal ReadDecimal(string fieldName, decimal defaultValue, SqlDataReader reader)
        {
            Object data = reader[fieldName];

            if (data == DBNull.Value)
            {
                return defaultValue;
            }

            return Decimal.Parse(data.ToString());
        }

        public static Decimal? ReadDecimalNullable(string fieldName, SqlDataReader reader)
        {
            Object data = reader[fieldName];

            if (data == DBNull.Value)
            {
                return null;
            }

            return Decimal.Parse(data.ToString());
        }

        public static double ReadDouble(string fieldName, double defaultValue, SqlDataReader reader)
        {
            Object data = reader[fieldName];

            if (data == DBNull.Value)
            {
                return defaultValue;
            }

            return Double.Parse(data.ToString());
        }

        #endregion

        #region Exception handling related

        // -------------------
        // Methods used in exception handling

        /// <summary>
        /// Returns a string describing stored procedure name, parameter values, etc. of a SqlCommand.
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public static string CommandToString(SqlCommand command)
        {
            var parameterValues = new StringBuilder();

            foreach (SqlParameter parameter in command.Parameters)
            {
                parameterValues.AppendFormat("{0} - Value: {1}, SqlValue: {2} | ",
                                            parameter.ParameterName, ValueToString(parameter.Value), ValueToString(parameter.SqlValue));
            }

            string commandAsString =
                string.Format(
                    "CommandText: {0}, CommandType: {1}, Parameters: {2}",
                    command.CommandText, command.CommandType, parameterValues);

            return commandAsString;
        }

        /// <summary>
        /// Returns string with value of a passed in object. If the object is null, returns "null". 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private static string ValueToString(Object value)
        {
            if (value == null)
            {
                return "null";
            }

            return value.ToString();
        }

        #endregion

    }
}