﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Reflection;

namespace DealerWizard.Service.Extension
{
    public class CustomContentNegotiator: IContentNegotiator
    {
        public ContentNegotiationResult Negotiate(Type type, HttpRequestMessage request, IEnumerable<MediaTypeFormatter> formatters)
        {
            var result = new ContentNegotiationResult(new CSVMediaTypeFormatter(), new MediaTypeHeaderValue("text/csv"));
            return result;
        }

    }
    public class CSVMediaTypeFormatter : MediaTypeFormatter
    {

        public CSVMediaTypeFormatter()
        {
            SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/csv"));

        }
        public CSVMediaTypeFormatter(
        MediaTypeMapping mediaTypeMapping) : this()
        {

            MediaTypeMappings.Add(mediaTypeMapping);
        }

        public CSVMediaTypeFormatter(
            IEnumerable<MediaTypeMapping> mediaTypeMappings) : this()
        {

            foreach (var mediaTypeMapping in mediaTypeMappings)
            {
                MediaTypeMappings.Add(mediaTypeMapping);
            }
        }
        public override bool CanReadType(Type type)
        {
            return false;
        }
        public override bool CanWriteType(Type type)
        {

            if (type == null)
                throw new ArgumentNullException("type");

            return isTypeOfIEnumerable(type);
        }

        private bool isTypeOfIEnumerable(Type type)
        {

            foreach (Type interfaceType in type.GetInterfaces())
            {

                if (interfaceType == typeof(IEnumerable))
                    return true;
            }

            return false;
        }
        public override Task WriteToStreamAsync(Type type, object value, Stream stream, HttpContent contentHeaders, System.Net.TransportContext transportContext)
        {

            writeStream(type, value, stream, contentHeaders);
            var tcs = new TaskCompletionSource<int>();
            tcs.SetResult(0);
            return tcs.Task;

        }
        private void writeStream(Type type, object value, Stream stream, HttpContent contentHeaders)
        {


            Type itemType = type.GetGenericArguments()[0];

            StringWriter _stringWriter = new StringWriter();

            _stringWriter.WriteLine(
                string.Join<string>(
                    ",", itemType.GetProperties().Select(x => x.Name)
                )
            );

            foreach (var obj in (IEnumerable<object>)value)
            {

                var vals = obj.GetType().GetProperties().Select(
                    pi => new {
                        Value = pi.GetValue(obj)
                    }
                );

                string _valueLine = string.Empty;

                foreach (var val in vals)
                {

                    if (val.Value != null)
                    {

                        var _val = val.Value.ToString();
                        if (_val.Contains(","))
                            _val = string.Concat("\"", _val, "\"");
                        if (_val.Contains("\r"))
                            _val = _val.Replace("\r", " ");
                        if (_val.Contains("\n"))
                            _val = _val.Replace("\n", " ");

                        _valueLine = string.Concat(_valueLine, _val, ",");

                    }
                    else
                    {

                        _valueLine = string.Concat(string.Empty, ",");
                    }
                }

                _stringWriter.WriteLine(_valueLine.TrimEnd(','));
            }

            using (var streamWriter = new StreamWriter(stream))
            {
                streamWriter.Write(_stringWriter.ToString());
                //streamWriter.Close();
            }
            
        }



    }

}