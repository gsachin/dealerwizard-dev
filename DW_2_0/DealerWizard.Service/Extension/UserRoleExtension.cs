﻿using DealerWizard.Service.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DealerWizard.Service.Extension
{
    public static class UserRoleExtension
    {
        public static bool HasAdminRole(this IUserSessionManager userManager)
        {
            return userManager.UserRoles.Contains("dwAdmin") || userManager.UserRoles.Contains("sAdmin");
        }
    }
}