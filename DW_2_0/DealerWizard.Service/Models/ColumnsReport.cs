﻿namespace DealerWizard.Service.Models
{
    public class ColumnsReportModel
    {
        public int Report_Param_Key { get; set; }
        public int Report_Key { get; set; }
        public string Column_Display_Name { get; set; }
        public int Column_Key { get; set; }
        public bool Link_Enable { get; set; }
        public int Report_Ref_Key { get; set; }
        public int? URL_Ref_Key { get; set; }
        public int Order_By { get; set; }
        public bool? IsHidden { get; set; }
        public int Sequence { get; set; }
        public bool Ismeasure { get; set; }
    }
}