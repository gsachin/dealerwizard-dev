﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DealerWizard.Service.Models
{
    public class MenuItems
    {
        public int MenuId { get; set; }
        public string Name { get; set; }
        public string URL { get; set; }
        public bool Active { get; set; }
        public int? ParentMenuId { get; set; }
        public int? Order { get; set; } 
        public string IconClassName { get; set; }


    }
}