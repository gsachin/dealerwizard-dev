﻿using System;

namespace DealerWizard.Service.Models
{
    public class DealershipAttribute
    {
        public int DealerId { get; set; }

        public DateTime Dated { get; set; }

        public double NoOfTechnician { get; set; }

        public double WorkingDays { get; set; }

        public double ServiceActualHours { get; set; }

    }
}