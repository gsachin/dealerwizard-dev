﻿namespace DealerWizard.Service.Models
{
    public class ReportObjectModel
    {
        public int key { get; set; }
        public int reportId { get; set; }
        public string reportName { get; set; }
        public string reportType { get; set; }
        public string filterData { get; set; } = "";
        public string departmentName { get; set; }
        public string filterType { get; set; }
        public string filterValues { get; set; }
        public bool isActive { get; set; }
        public string defaultFilter { get; set; }

    }
}