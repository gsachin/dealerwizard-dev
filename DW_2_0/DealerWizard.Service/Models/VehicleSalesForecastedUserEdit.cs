﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DealerWizard.Service.Models
{
    public class VehicleSalesForecastedUserEdit
    {
        public string  FullDate { get; set; }
        public int Make_Key { get; set; }
        public int Model_Key { get; set; }
        public int New_Used { get; set; }
        public double UnitsDeliveredUserEdit { get; set; }

        public double Inventory { get; set; }
        public double InventoryValue { get; set; }

        public double RecommendedInventory { get; set; }
        public double TotalGross { get; set; }
        public double InventoryCostRate { get; set; }

    }
}