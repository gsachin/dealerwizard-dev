﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DealerWizard.Service.Models
{
    public class DealerSetting
    {
        public string DefaultBookColumn { get; set; }

        public int ? DefaultPercentValue { get; set; }
    }
}