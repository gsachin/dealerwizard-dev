﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DealerWizard.Service.Models
{
    public class ForecastVechicleSalesDownload
    {
        [JsonProperty("Date")]
        public string FullDate { get; set; }
        [JsonProperty("Veh Make")]
        public string MakeDesc { get; set; }
        [JsonProperty("Veh Model")]
        public string ModelDesc { get; set; }
        public string NewUsed { get; set; }
        [JsonProperty("Recommended Inventory")]
        [JsonIgnore]
        public double RecommendedInventory { get; set; }
        [JsonProperty("Current Inventory")]
        public double CurrentInventory { get; set; }
        [JsonProperty("Original Forecast")]
        public double OriginalForecasted { get; set; }
        [JsonProperty("Revised Forecast")]
        public double RevisedForecasted { get; set; }
        [JsonProperty("Total Gross")]
        public double TotalGross { get; set; }
        [JsonProperty("Inventory Value")]
        public double InventoryValue { get; set; }  
        [JsonProperty("Excess Inventory")]
        public double ExcessInventory { get; set; }
        [JsonProperty("Annual Carrying Cost")]
        public double CarryingCost { get; set; }

        [JsonProperty("Shortage Inventory")]
        public double ShortageInventory { get; set; }

        [JsonProperty("Annual Opportunity Cost")]
        public double OpportunityCost { get; set; }
        [JsonProperty("Total Inventory Variance")]
        public double TotalInventoryVariance
        {
            get
            {
                return Math.Abs(ExcessInventory - ShortageInventory);
            }
        }

        [JsonProperty("Total Annual Inefficiency Cost")]
        public double TotalInefficiencyCost
        {
            get
            {
                return CarryingCost + OpportunityCost;
            }
        }        
    }
}