﻿using Newtonsoft.Json.Linq;

namespace DealerWizard.Service.Models
{
    public class VehicleSaleView
    {
        
        public int Id { get; set; }
        
        public string CustomData { get; internal set; }
        public string Make { get; internal set; }
        public string Model { get; internal set; }
        public string SalesPerson { get; internal set; }
        public string FIManager { get; set; }
        public long UnitSold { get; set; }
        public double SalesGross { get; set; }
        public double FIGross { get; set; }
        public double ServiceGross { get; set; }
        public double PartGross { get; set; }
        public double DealershipGross { get; set; }

    }
}