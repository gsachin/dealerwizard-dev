﻿namespace DealerWizard.Service.Models
{
    public class ForecastedSale
    {
        public int Dealer_Key { get; set; }
        public string Dlr_Name { get; set; }
        public int Make_Key { get; set; }
        public string Make_Desc { get; set; }
        public int Model_Key { get; set; }
        public string Model_Desc { get; set; }
        public int New_Used { get; set; }
        public int Date_Key { get; set; }
        public string FullDate { get; set; }
        public double Forecast_Units_Delivered { get; set; }
        public double Forecast_Total_Gross { get; set; }
        public double Forecast_Sales_Gross { get; set; }
        public double Forecast_FI_Gross { get; set; }
        public double Forecast_Financed { get; set; }
        public double Forecast_GAP { get; set; }
        public double Forecast_Maint { get; set; }
        public double Forecast_SVC { get; set; }
        public double Lower80_Units_Delivered { get; set; }
        public double Lower80_Total_Gross { get; set; }
        public double Lower80_Sales_Gross { get; set; }
        public double Lower80_FI_Gross { get; set; }
        public double Lower80_Financed { get; set; }
        public double Lower80_GAP { get; set; }
        public double Lower80_Maint { get; set; }
        public double Lower80_SVC { get; set; }
        public double Upper80_Units_Delivered { get; set; }
        public double Upper80_Total_Gross { get; set; }
        public double Upper80_Sales_Gross { get; set; }
        public double Upper80_FI_Gross { get; set; }
        public double Upper80_Financed { get; set; }
        public double Upper80_GAP { get; set; }
        public double Upper80_Maint { get; set; }
        public double Upper80_SVC { get; set; }

    }

}