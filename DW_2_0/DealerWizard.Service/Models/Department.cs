﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DealerWizard.Service.Models
{
    public class Department
    {
        public int Department_Key { get; set; }
        public string Department_Name { get; set; }
    }
}