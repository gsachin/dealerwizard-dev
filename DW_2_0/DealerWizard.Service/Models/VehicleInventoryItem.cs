﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DealerWizard.Service.Models
{
    public class VehicleInventoryItem
    {
        public int DealerKey { get; set; }
        public long VehicleKey { get; set; }
        public int MakeKey { get; set; }
        public string Make { get; set; }
        public int ModelKey { get; set; }
        public string Model { get; set; }
        public int NewOrUsed { get; set; }
        public double Value { get; set; }
    }
}