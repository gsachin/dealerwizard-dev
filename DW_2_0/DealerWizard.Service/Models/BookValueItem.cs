﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DealerWizard.Service.Models
{
    public class BookValueItem
    {
        public int DealerNumber { get; set; }
        public string BookValueText { get; set; }
        public string BookValueName { get; set; }
        public int SortOrder { get; set; }
        public string ColumnType { get; set; } = "Double";
    }
}