﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DealerWizard.Service.Models
{
    public class UserActivity
    {
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("dealerId")]
        public int DealerId { get; set; }
        [JsonProperty("route")]
        public string Route { get; set; }
        [JsonProperty("activityTime")]
        public string ActivityTime { get; set; }
        [JsonProperty("activityDetail")]
        public string ActivityDetail { get; set; }
        [JsonProperty("activityToken")]
        public Guid LoggingTrackingId { get; set; }
    }
}