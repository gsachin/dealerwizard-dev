﻿using System;

namespace DealerWizard.Service.Models
{
    public class UserAssociatedDealership
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Guid ReferenceDealerId { get; set; }
        public bool IsDefaultDealer { get; set; }

    }
}