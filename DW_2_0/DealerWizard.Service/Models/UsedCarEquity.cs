﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DealerWizard.Service.Models
{
    public class UsedCarEquity
    {
        public string Booked { get; set; }
        public string Stock_Number { get; set; }
        public string PT { get; set; }
        public int Age { get; set; }
        public string VIN { get; set; }
        public int Year { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public int Miles { get; set; }
        public double Inventory_Value { get; set; }
        public double NADA_Clean_Trade_In { get; set; }
        public double NADA_Average_Trade_In { get; set; }
        public double NADA_Rough_Trade_In { get; set; }
        public double NADA_Clean_Loan { get; set; }
        public double NADA_Clean_Retail { get; set; }
        public double? KBB_Auction_Excellent { get; set; }
        public double? KBB_Auction_Very_Good { get; set; }
        public double? KBB_Auction_Good { get; set; }
        public double? KBB_Auction_Fair { get; set; }
        public double? KBB_Trade_In_Excellent { get; set; }
        public double? KBB_Trade_In_Very_Good { get; set; }
        public double? KBB_Trade_In_Good { get; set; }
        public double? KBB_Trade_In_Fair { get; set; }

    }
}