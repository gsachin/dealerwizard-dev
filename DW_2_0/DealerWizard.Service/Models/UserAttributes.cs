﻿using System;

namespace DealerWizard.Service.Models
{
    public class UserAttributes
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }

    }
}