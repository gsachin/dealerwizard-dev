﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DealerWizard.Service.Models
{
    public class KpiItems
    {
        public string measurename { get; set; }
        public double ActualValue { get; set; }
        public double GY { get; set; }
        public double YR { get; set; }
        //public char DataFormat { get; set; }
    }
}