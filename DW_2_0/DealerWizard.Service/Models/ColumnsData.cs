﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DealerWizard.Service.Models
{
    public class ColumnsData
    {
        public int Column_key { get; set; }
        public string Column_Display_Name { get; set; }
        public bool IsMeasure { get; set; }
    }
}