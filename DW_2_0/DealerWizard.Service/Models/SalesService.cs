﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DealerWizard.Service.Models
{
    public class SalesService
    {
        public List<object> Current { get; set; }
        public List<object> previous { get; set; }
        public List<object> AggregatedSum { get; set; }
    }
}