﻿using System;

namespace DealerWizard.Service.Models
{
    public class LoggedInUser
    {
        public UserAttributes User { get; set; }
        public UserAssociatedDealership DefaultDealer { get; set; }

        public UserPreference Preference { get; set; }


    }
}