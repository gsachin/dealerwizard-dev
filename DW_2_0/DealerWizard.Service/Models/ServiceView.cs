﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DealerWizard.Web.Domain
{
    public class ServiceView
    {
        public string Make { get; set; }
        public string Model { get; set; }

        public string Techinician { get; set; }
        public decimal PartsGross { get; set; }
        public decimal ServiceGross { get; set; }
        public decimal EffectiveLaborRate { get; set; }

        public decimal HoursSold { get; set; }

        public decimal HoursPerRO { get; set; }

    }
}