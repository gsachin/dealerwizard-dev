﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DealerWizard.Web.Domain
{
    public class Deal
    {
        public int Year { get; set; }
        public int UnitSold { get; set; }

    }
}