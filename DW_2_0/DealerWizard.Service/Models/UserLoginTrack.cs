﻿using System;

namespace DealerWizard.Service.Models
{
    public class UserLoginTrack
    {
        public long Id { get; set; }

        public Guid LoginTrackingId { get; set; }

        public Guid UserId { get; set; }

    }
}