﻿namespace DealerWizard.Service.Models
{
    public class ReportData
    {
        public int Column_key { get; set; }
        public string Column_Display_Name { get; set; }
        public bool IsMeasure { get; set; }
        public int Report_Key { get; set; }
        public string Report_Name { get; set; }
        public string Status { get; set; }
    }
}