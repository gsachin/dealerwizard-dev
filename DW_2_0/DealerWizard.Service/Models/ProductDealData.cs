﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DealerWizard.Web.Domain
{
    public class ProductDealData
    {
        public string SalesManager { get; set; }
        public double TotalSalesGross { get; set; }
        public int UnitSold { get; set; }
        
    }
}