﻿using System;

namespace DealerWizard.Service.Models
{
    public class ServiceMeasuresItem
    {
        public DateTime? Dated { get; set; }
        public int HoursSold { get; set; }
        public double PartGross { get; set; }
        public double ServiceGross { get; set; }

        public double LaborSalesPerRO { get; set; }
        public double HoursPerRO { get; set; }
        public double EffectiveLaborRate { get; set; }

    }
}