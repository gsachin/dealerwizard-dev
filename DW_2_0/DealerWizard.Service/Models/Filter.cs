﻿namespace DealerWizard.Service.Models
{
    public class FilterModel
    {
        public int Report_Filter_Key { get; set; }
        public int Report_Key { get; set; }
        public int Column_Key { get; set; }
    }
}