﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DealerWizard.Web.Domain
{
    public class SalesView
    {       
        public string Make { get; set; }
        public string Model { get; set; }         
        public string  SalesPerson { get; set; }
        public string FiManager { get; set; }

        public string year { get; set; }

        public string month { get; set; }
        public decimal UnitsSold { get; set; }
        public decimal SalesGross { get; set; }
        public decimal FiGross { get; set; }

        public double AverageSalesGrossForNewCar { get; set; }
        public double AverageSalesGrossForUsedCar { get; set; }
        public double AverageWholeSaleUsed { get; set; }
        public double AverageFIGrossUsed { get; set; }
        public double AverageFIGrossNew { get; set; }
    }
}