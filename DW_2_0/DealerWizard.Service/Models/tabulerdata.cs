﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DealerWizard.Service.Models
{
    public class tabulerdata
    {
        public string make { get; set; }
        public string model { get; set; }
        public int totalunitssold { get; set; }
        public double totalgrosssales { get; set; }
    }
}