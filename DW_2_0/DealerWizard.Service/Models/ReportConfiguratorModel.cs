﻿using System.Collections.Generic;

namespace DealerWizard.Service.Models
{
    public class ReportConfiguratorModel
    {
        public string ReportName { get; set; }
        public long DepartmentKey { get; set; }
        public string Filter { get; set; }
        public string ReportScope { get; set; }
        public string ReportType { get; set; }
        public int? ReportKey { get; set; }
        public string Status { get; set; }
        public string CreatedBy { get; set; }
        public List<MeasuresModel> Measures { get; set; }
        public List<DimensionsModel> Dimensions { get; set; }
    }
}