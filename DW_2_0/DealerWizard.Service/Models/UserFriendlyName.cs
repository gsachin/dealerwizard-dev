﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DealerWizard.Service.Models
{
    public class UserFriendlyName
    {
        public string Display_Name { get; set; }
        public string User_Friendly_Name { get; set; }
    }
}