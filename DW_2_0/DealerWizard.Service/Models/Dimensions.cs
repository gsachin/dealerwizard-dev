﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DealerWizard.Service.Models
{
    public class Dimensions
    {
        public string departmentname { get; set; }
        public string dimensions { get; set; }
        public string department { get; set; }
    }
}