﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DealerWizard.Service.Models
{
    public class ServiceActual
    {
        public int DealerKey { get; set; }
        public string DealerName { get; set; }
        public int MakeKey { get; set; }
        public int ModelKey { get; set; }
        public string MakeDesc { get; set; }
        public string ModelDesc { get; set; }
        public string ActualDate { get; set; }
        public string LaborType { get; set; }
        public double ServiceELR { get; set; }
        public double SvcLaborSalesPerRO { get; set; }
        public double SvcHoursPerRO { get; set; }
        public double SvcLaborSales { get; set; }
        public double SvcLaborGross { get; set; }
        public double SvcHoursSold { get; set; }
        public int SvcROCount { get; set; }        
    }
}