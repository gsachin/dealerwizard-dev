﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DealerWizard.Service.Models
{
    public class ActualSale
    {
        public int Dealer_Key { get; set; }
        public string Dealer_Name { get; set; }
        public int Make_Key { get; set; }
        public int Model_Key { get; set; }
        public string Make_Desc { get; set; }
        public string Model_Desc { get; set; }
        public int New_Used { get; set; }
        public string Actual_Date { get; set; }
        public int Units_Delivered { get; set; }
        public double Total_Gross { get; set; }
        public double Sales_Gross { get; set; }
        public double FI_Gross { get; set; }
        public int Financed { get; set; }
        public int GAP { get; set; }
        public int Maint { get; set; }
        public int SVC { get; set; }

    }
}