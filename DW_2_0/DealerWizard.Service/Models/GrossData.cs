﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DealerWizard.Web.Domain
{
    public class GrossData
    {
        public double CurrentYData { get; set; }
        public double PreviousYData { get; set; }

        public double diff { get; set; }

        public string type { get; set; }
    }
}