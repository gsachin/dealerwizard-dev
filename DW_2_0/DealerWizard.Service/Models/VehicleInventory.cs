﻿using System;

namespace DealerWizard.Service.Models
{
    public class VehicleInventory
    {
        public string AsOnDate { get; set; }
        public int DealerKey { get; set; }
        public int MakeKey { get; set; }
        public string Make { get; set; }
        public int ModelKey { get; set; }
        public string Model { get; set; }
        public int NewOrUsed { get; set; }
        public int Count { get; set; }
        public double Value { get; set; }
        
    }
}