﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DealerWizard.Service.Models
{
    public class Columns
    {
        public string ColumnDisplayName { get; set; }
        public string reportname { get; set; }

        public string ColumnName { get; set; }

        public string UserFriendlyName { get; set; }

        public bool Ismeasure { get; set; }

        public string ColumnDataType { get; set; }

        public bool IsLink { get; set; }

        public bool IsLinkOnHeader { get; set; }
        public int ReportRefKey { get; set; }
        public string NextReportName { get; set; }
        public string ReportFilters { get; set; }

        public string ReportType { get; set; }
        public string DataType { get; set; }
        public int UrlRefKey { get; set; }
        public string UrlName { get; set; }
        public string UrlPath { get; set; }
        public bool IsHidden { get; set; }
    }
}