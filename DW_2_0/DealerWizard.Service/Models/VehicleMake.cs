﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DealerWizard.Service.Models
{
    public class VehicleMake
    {
        public int MakeId { get; set; }
        public string MakeName { get; set; }
    }
}