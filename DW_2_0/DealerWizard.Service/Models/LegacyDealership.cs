﻿using System;

namespace DealerWizard.Service.Models
{
    public class LegacyDealership
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
        public bool IsDefault { get; set; }

    }
}