﻿using Percolator.AnalysisServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DealerWizard.Service.Models
{
    public class MdxQuery
    {
        public string Cube { get; set; }

        public ICubeObject OnRows { get; set; }
        public ICubeObject OnColumns { get; set; }

        public string MdxCommand { get; set; }



    }
}