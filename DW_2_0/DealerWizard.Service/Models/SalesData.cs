﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DealerWizard.Web.Domain
{
    public class SalesData
    {
        public string date { get; set; }
        public decimal currentamount { get; set; }
        public string Previousdate { get; set; }
        public decimal Previousamount { get; set; }
        public string PreviousYearMonthdate { get; set; }
        public decimal PreviousYearMonthAmount { get; set; }
    }
}