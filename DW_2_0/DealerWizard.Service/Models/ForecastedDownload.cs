﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DealerWizard.Service.Models
{
    public class ForecastedDownload<T>
    {
        public int Dealer { get; set; }
        public string Startdate { get; set; }
        public string Enddate { get; set; }
        public int MakeId { get; set; }
        public int ModelId { get; set; }
        public int SelectedType { get; set; }
        public List<T> ForecastedData { get; set; }
    }
}