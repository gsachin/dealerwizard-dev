﻿using System;
using System.Collections.Generic;

namespace DealerWizard.Service.Models
{
    public class SmallMultipleItem
    {
        public List<Item> Columns { get; set; }

        //public DateTime? Dated { get; set; }
        //public int UnitsDelivered { get; set; }
        //public double FIGross { get; set; }
        //public double FIPRU { get; set; }
        //public double SalesGross { get; set; }
        //public double SalesPRU { get; set; }
        //public double AverageDayToSale { get; set; }
        // public double WholeSale { get; set; }

        //public double AverageSalesGross { get; set; }
        //public double AverageSalesProfitabilityForUsedCar { get; set; }
        // public double AverageFiProfitabilityForNewCar { get; set; }
        //public double AverageFIGross { get; set; }

    }

    public class Item
    {
        public DateTime? ReportDate { get; set; }
        public Type ColumnType;
        public string ColumnName { get; set; }
        public object ColumnValue { get; set; }
    }
}