﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DealerWizard.Service.Models
{
    public class ReportDataByDepartmentKey
    {
        public List<ColumnsData> ColumnsData { get; set; }
        public List<ReportsData> ReportsData { get; set; }
    }
}