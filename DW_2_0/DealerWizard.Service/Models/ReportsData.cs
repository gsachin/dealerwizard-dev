﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DealerWizard.Service.Models
{
    public class ReportsData
    {
        public int Report_Key { get; set; }
        public string Report_Name { get; set; }
        public string Status { get; set; }
    }
}