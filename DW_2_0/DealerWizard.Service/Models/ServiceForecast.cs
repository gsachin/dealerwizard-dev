﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DealerWizard.Service.Models
{
    public class ServiceForecast
    {
        public int DealerKey { get; set; }
        public string DealerName { get; set; }
        public int MakeKey { get; set; }
        public string MakeDesc { get; set; }
        public int ModelKey { get; set; }
        public string ModelDesc { get; set; }
        public string LaborType { get; set; }
        public int DateKey { get; set; }
        public DateTime FullDate { get; set; }
        public double ForecastServiceELR { get; set; }
        public double ForecastSvcLaborSalesPerRO { get; set; }
        public double ForecastSvcHoursPerRO { get; set; }
        public double ForecastSvcLaborSales { get; set; }
        public double ForecastSvcLaborGross { get; set; }
        public double ForecastSvcHoursSold { get; set; }
        public double ForecastSvcROCount { get; set; }
        public double Lower80ServiceELR { get; set; }
        public double Lower80SvcLaborSalesPerRO { get; set; }
        public double Lower80SvcHoursPerRO { get; set; }
        public double Lower80SvcLaborSales { get; set; }
        public double Lower80SvcLaborGross { get; set; }
        public double Lower80SvcHoursSold { get; set; }
        public double Lower80SvcROCount { get; set; }
        public double Upper80ServiceELR { get; set; }
        public double Upper80SvcLaborSalesPerRO { get; set; }
        public double Upper80SvcHoursPerRO { get; set; }
        public double Upper80SvcLaborSales { get; set; }
        public double Upper80SvcLaborGross { get; set; }
        public double Upper80SvcHoursSold { get; set; }
        public double Upper80SvcROCount { get; set; }
    }
}