﻿using System.Collections.Generic;

namespace DealerWizard.Service.Models
{
    public class Report
    {
        public int ReportKey { get; set; }
        public string ReportName { get; set; }
        public int Dealer_Key { get; set; }
        public int DepartmentKey { get; set; }
        public string Status { get; set; }
        public string Created_By { get; set; }
        public string ReportScope { get; set; }
        public string ReportType { get; set; }
        public string Department_Name { get; set; }

        public List<ColumnsReportModel> Columnsdata { get; set; }
        public List<FilterModel> Filterdata { get; set; }
    }
}