﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DealerWizard.Service.Models
{
    public class UserPreference
    {
        public string DLR_Name { get; set; }
        public Guid DLR_ID { get; set; }
        public string Landing_Page { get; set; }
        public string Title { get; set; }
    }
}