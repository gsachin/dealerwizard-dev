﻿namespace DealerWizard.Service.Models
{
    public class MeasuresModel
    {
        public string Column_Display_Name { get; set; }
        public int Column_Key { get; set; }
        public int Report_Ref_Key { get; set; }
        public int Sequence { get; set; }
        public int Order_By { get; set; }
        public bool Link_Enable { get; set; }
    }
}