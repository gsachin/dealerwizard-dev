﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DealerWizard.Service.Models
{
    public class Measures
    {
        public string columnname { get; set; }
        public string measure { get; set; }
        public string reportname { get; set; }
    }
}