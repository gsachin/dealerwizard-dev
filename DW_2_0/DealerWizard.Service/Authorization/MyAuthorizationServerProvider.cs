﻿using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using System.Security.Claims;
using System.Web.Security;
using System.Data;
using DealerWizard.Data.Shared;
using DealerWizard.Service.Repository;
using DealerWizard.Service.Contracts;

namespace DealerWizard.Service
{
    public class MyAuthorizationServerProvider: OAuthAuthorizationServerProvider
    {        
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();// implies the user is validated
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            // this method will validate the credentials and if validated, generates token using which user can access the authorized resources of server
            var data = await context.Request.ReadFormAsync() as IEnumerable<KeyValuePair<string, string[]>>;
            var etc = (data.FirstOrDefault(x => x.Key == "etc").Value != null)?data.FirstOrDefault(x => x.Key == "etc").Value[0].ToString():null;

            var UserName = "";
            bool validUser = false;
            ICoreRepository coreRepository = new CoreRepository();

            if (!String.IsNullOrEmpty(etc))
            {   
                UserName = coreRepository.GetUserNameByEmailTrackingCode(etc);
                if (!String.IsNullOrEmpty(UserName))
                {
                    validUser = true;
                }
            }
            else
            {
                validUser = Membership.ValidateUser(context.UserName, context.Password);
                UserName = context.UserName;
            }

            if (validUser == true)
            {
                var membershipUser = Membership.GetUser(UserName);
                var userGuid = Guid.Parse(membershipUser.ProviderUserKey.ToString());
                var remoteIpAddress = context.Request.RemoteIpAddress;
                var userAgent = context.Request.Headers["User-Agent"];

                var loginTrack = coreRepository.LogUserLoginTrack(userGuid, remoteIpAddress, userAgent);

                var identity = new ClaimsIdentity(context.Options.AuthenticationType);
                identity.AddClaim(new Claim(ClaimTypes.Name, UserName));

                var userRoles = Roles.GetRolesForUser(UserName);
                foreach (var role in userRoles)
                {
                    identity.AddClaim(new Claim(ClaimTypes.Role, role));
                }
                identity.AddClaim(new Claim(ClaimTypes.UserData, loginTrack.LoginTrackingId.ToString()));
                context.Validated(identity);
            }
            else if (validUser == false && String.IsNullOrEmpty(etc))
            {
                context.SetError("invalid_grant", "Provided UserName/Password Is Incorrect.");
                return;
            }
            else if(validUser == false && !String.IsNullOrEmpty(etc))
            {
                context.SetError("invalid_grant", "Provided ETC Is Incorrect.");
                return;
            }
        }
    }
}