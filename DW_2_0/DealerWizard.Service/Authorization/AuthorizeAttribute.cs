﻿using DealerWizard.Service.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace DealerWizard.Service
{
    public class AuthorizeAttribute: System.Web.Http.AuthorizeAttribute
    {   
        public IUserSessionManager userSessionManager { get; set; }

        protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
        {
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                base.HandleUnauthorizedRequest(actionContext);
            }
            else
            {
                actionContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.Forbidden);
            }

        }

        public override void OnAuthorization(HttpActionContext actionContext)
        {
            if (SkipAuthorization(actionContext))
            {
                return;
            }

            
            //if (userSessionManager.ReValidateSession())
            //{
            //    base.OnAuthorization(actionContext);
            //}
            //else
            //{
            //    actionContext.Response = actionContext.ControllerContext.Request.CreateErrorResponse(
            //        HttpStatusCode.Unauthorized, "user token session expried or not valid.");
            //}

            base.OnAuthorization(actionContext);
        }

        private static bool SkipAuthorization(HttpActionContext actionContext)
        {
            return actionContext.ActionDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Any()
                   || actionContext.ControllerContext.ControllerDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Any();
        }
    }
}