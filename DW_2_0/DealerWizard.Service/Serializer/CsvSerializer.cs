﻿using DealerWizard.Service.Contracts;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace DealerWizard.Service.Serializer
{
    public class CsvSerializer : ICsvSerializer
    {
        private DataTable jsonStringToTable(string jsonContent)
        {
            DataTable dt = JsonConvert.DeserializeObject<DataTable>(jsonContent);
            return dt;
        }
        public  HttpResponseMessage jsonToCSV(string jsonContent)
        {
            string csv = string.Empty;

            using (var dt = jsonStringToTable(jsonContent))
            {
                foreach (DataColumn column in dt.Columns)
                {
                    csv += column.ColumnName + ',';
                }
                csv += "\r\n";


                foreach (DataRow row in dt.Rows)
                {
                    foreach (DataColumn column in dt.Columns)
                    {
                        csv += row[column.ColumnName].ToString().Replace(",", ";") + ',';
                    }
                    csv += "\r\n";
                }
            }

            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            result.Content = new StringContent(csv.ToString());
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/csv");
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment") { FileName = "NEWREPORT.csv" };
            return result;
        }



        public HttpResponseMessage writeStream(Type type, object value)//, Stream stream)
        {
            StringWriter _stringWriter = new StringWriter();
            _stringWriter.WriteLine(
               string.Join<string>(
                   ",", type.GetProperties().Select(x => x.Name)
               )
           );

            foreach (var obj in (IEnumerable<object>)value)
            {

                var vals = obj.GetType().GetProperties().Select(
                    pi => new
                    {
                        Value = pi.GetValue(obj)
                    }
                );

                string _valueLine = string.Empty;

                foreach (var val in vals)
                {

                    if (val.Value != null)
                    {

                        var _val = val.Value.ToString();
                        if (_val.Contains(","))
                            _val = string.Concat("\"", _val, "\"");
                        if (_val.Contains("\r"))
                            _val = _val.Replace("\r", " ");
                        if (_val.Contains("\n"))
                            _val = _val.Replace("\n", " ");

                        _valueLine = string.Concat(_valueLine, _val, ",");

                    }
                    else
                    {

                        _valueLine = string.Concat(string.Empty, ",");
                    }
                }

                _stringWriter.WriteLine(_valueLine.TrimEnd(','));
            }


            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            result.Content = new StringContent(_stringWriter.ToString());
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/csv");
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment") { FileName = "NEWCARKPI.csv" };
            return result;


        }
    }
}