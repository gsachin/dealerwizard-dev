﻿(function (app) {
    'use strict';

    app.controller('serviceforecastCtrl', serviceforecastCtrl);

    serviceforecastCtrl.$inject = ['$scope', 'apiService', 'valueFormatter', 'applicationObject', 'dealerService', '$rootScope'];

    function serviceforecastCtrl($scope, apiService, valueFormatter, applicationObject, dealerService, $rootScope) {

         dealerService.getCurrentDealer().then(function (dealer) {
            $scope.Dealer = dealer;
            Init();
        });
        $scope.moneyFormatter = valueFormatter.getformatter("money");
        $scope.numberFormatter = valueFormatter.getformatter("number");
        $scope.percentFormatter = valueFormatter.getformatter("percent");
        $scope.decimal2Formatter = valueFormatter.getformatter("decimal2");
        $scope.openHoursFor = { sunday: 0, monday: 8, tuesday: 8, wednesday: 8, thursday: 8, friday: 8, saturday: 0 };
        $scope.selectedNewOrUsed = 2;
        $scope.totalOpenHours = $scope.hoursSold = $scope.dollarSold = 0;
        $scope.hoursAvailable = $scope.dollarAvailable = $scope.hoursVariance = $scope.dollarVariance = 0;
        $scope.lostHours = $scope.shortHours = 0;
        $scope.numberOfServiceTechs = -1;
        $scope.serviceTechEfficiency = 1.1;
        $scope.ShowLoader = true;
        $scope.TotalOriginal = 0;
        $scope.TotalUserEdit = 0;
        $scope.TotalDiff = 0;
        $scope.ActualSales = [],
        $scope.forecastedSales = [];
        $scope.AcDataset = [];
        $scope.FrDataset = [];
        $scope.hoursVarianceColor = "black";
        $scope.hoursVarianceIndicator = "";
        $scope.dollarVarianceIndicator = "";
        $scope.isEfficiencySliderChanged = false;
        $scope.isTechEfficenyChangeAllowed = true;

        var serviceActualHours = 0;
        var metricChartObject = {
            elr: [{
                dataSetId: "data2",
                chart: {
                    type: "area",
                    x: "FullDate",
                    y1: "Lower80ServiceELR",
                    y2: "Upper80ServiceELR",
                    classes: ["steelbluefill"]
                },
                fnTooltip: function (d, i) {
                    return d;
                }
            }, {
                dataSetId: "data1",
                chart: {
                    type: "line",
                    x: "ActualDate",
                    y: "ServiceELR",
                    classes: ["line"]
                },
                fnTooltip: function (d, i) {
                    return d;
                }
            },
                {
                    dataSetId: "data1",
                    chart: {
                        type: "circle",
                        cx: "ActualDate",
                        cy: "ServiceELR",
                        r: 3,
                        classes: ["actualCircle", "dot"],
                        opacity: 1e-6
                    },
                    fnTooltip: function (d, i) {
                        var TooltipHtml = "Svc Elr:" + d.ServiceELR + "<br />";
                        TooltipHtml += "Period : " + moment(d.ActualDate).format("MMMM-YYYY");
                        return TooltipHtml;
                    }
                },
                {
                    dataSetId: "data2",
                    chart: {
                        type: "line",
                        x: "FullDate",
                        y: "ForecastServiceELREdit",
                        classes: ["line", "frLine"]
                    },
                    fnTooltip: function (d, i) {
                        return d;
                    }
                },
                {
                    dataSetId: "data2",
                    chart: {
                        type: "circle",
                        cx: "FullDate",
                        cy: "ForecastServiceELREdit",
                        r: 3,
                        classes: ["forecastCircle", "dot"],
                        opacity: 1
                    },
                    fnTooltip: function (d, i) {
                        var TooltipHtml = "Forecasted Svc Elr: " + $scope.decimal2Formatter(d.ForecastServiceELREdit) + "<br />";
                        TooltipHtml += "period : " + moment(d.FullDate).format("MMMM-YYYY") + "<br />";
                        TooltipHtml += "Lower threshold : " + $scope.decimal2Formatter(d.Lower80ServiceELR) + " <br />";
                        TooltipHtml += "Upper threshold : " + $scope.decimal2Formatter(d.Upper80ServiceELR);
                        return TooltipHtml;
                    }
                }
            ],
            hoursPerRo: [{
                dataSetId: "data2",
                chart: {
                    type: "area",
                    x: "FullDate",
                    y1: "Lower80SvcHoursPerRO",
                    y2: "Upper80SvcHoursPerRO",
                    classes: ["steelbluefill"]
                },
                fnTooltip: function (d, i) {
                    return d;
                }
            }, {
                dataSetId: "data1",
                chart: {
                    type: "line",
                    x: "ActualDate",
                    y: "SvcHoursPerRO",
                    classes: ["line"]
                },
                fnTooltip: function (d, i) {
                    return d;
                }
            },
                {
                    dataSetId: "data1",
                    chart: {
                        type: "circle",
                        cx: "Actual_Date",
                        cy: "SvcHoursPerRO",
                        r: 3,
                        classes: ["actualCircle", "dot"],
                        opacity: 1e-6
                    },
                    fnTooltip: function (d, i) {
                        var TooltipHtml = "Svc Hours/RO : " + d.SvcHoursPerRO + "<br />";
                        TooltipHtml += "Period : " + moment(d.ActualDate).format("MMMM-YYYY");
                        return TooltipHtml;
                    }
                },
                {
                    dataSetId: "data2",
                    chart: {
                        type: "line",
                        x: "FullDate",
                        y: "ForecastSvcHoursPerROEdit",
                        classes: ["line", "frLine"]
                    },
                    fnTooltip: function (d, i) {
                        return d;
                    }
                },
                {
                    dataSetId: "data2",
                    chart: {
                        type: "circle",
                        cx: "FullDate",
                        cy: "ForecastSvcHoursPerROEdit",
                        r: 3,
                        classes: ["forecastCircle", "dot"],
                        opacity: 1
                    },
                    fnTooltip: function (d, i) {
                        var TooltipHtml = "Forecasted Svc Hours/RO : " + $scope.decimal2Formatter(d.ForecastSvcHoursPerROEdit) + "<br />";
                        TooltipHtml += "period : " + moment(d.FullDate).format("MMMM-YYYY") + "<br />";
                        TooltipHtml += "Lower threshold : " + $scope.decimal2Formatter(d.Lower80SvcHoursPerRO) + " <br />";
                        TooltipHtml += "Upper threshold : " + $scope.decimal2Formatter(d.Upper80SvcHoursPerRO);
                        return TooltipHtml;
                    }
                }
            ],
            roCount: [{
                dataSetId: "data2",
                chart: {
                    type: "area",
                    x: "FullDate",
                    y1: "Lower80SvcROCount",
                    y2: "Upper80SvcROCount",
                    classes: ["steelbluefill"]
                },
                fnTooltip: function (d, i) {
                    return d;
                }
            }, {
                dataSetId: "data1",
                chart: {
                    type: "line",
                    x: "ActualDate",
                    y: "SvcROCount",
                    classes: ["line"]
                },
                fnTooltip: function (d, i) {
                    return d;
                }
            },
                {
                    dataSetId: "data1",
                    chart: {
                        type: "circle",
                        cx: "ActualDate",
                        cy: "SvcROCount",
                        r: 3,
                        classes: ["actualCircle", "dot"],
                        opacity: 1e-6
                    },
                    fnTooltip: function (d, i) {
                        var TooltipHtml = "RO Count : " + d.SvcROCount + "<br />";
                        TooltipHtml += "Period : " + moment(d.ActualDate).format("MMMM-YYYY");
                        return TooltipHtml;
                    }
                },
                {
                    dataSetId: "data2",
                    chart: {
                        type: "line",
                        x: "FullDate",
                        y: "ForecastSvcROCountEdit",
                        classes: ["line", "frLine"]
                    },
                    fnTooltip: function (d, i) {
                        return d;
                    }
                },
                {
                    dataSetId: "data2",
                    chart: {
                        type: "circle",
                        cx: "FullDate",
                        cy: "ForecastSvcROCountEdit",
                        r: 3,
                        classes: ["forecastCircle", "dot"],
                        opacity: 1
                    },
                    fnTooltip: function (d, i) {
                        var TooltipHtml = "Forecasted RO Count : " + $scope.numberFormatter(d.ForecastSvcROCountEdit) + "<br />";
                        TooltipHtml += "period : " + moment(d.FullDate).format("MMMM-YYYY") + "<br />";
                        TooltipHtml += "Lower threshold : " + $scope.numberFormatter(d.Lower80SvcROCount) + " <br />";
                        TooltipHtml += "Upper threshold : " + $scope.numberFormatter(d.Upper80SvcROCount);
                        return TooltipHtml;
                    }
                }
            ]
        };

        var clearFrDatasetWatch;

        /*slider region*/
        $scope.sliderSvcTechs = { //requires angular-bootstrap to display tooltips
            value: 5,
            options: {
                floor: 0,
                ceil: 75,
                step: 0.5,
                precision: 1,
                showTicksValues: 10,
                showSelectionBar: true,
                onEnd: function () {
                    $scope.numberOfServiceTechs = $scope.sliderSvcTechs.value;
                    $scope.updateChart();
                },
                translate: function (value) {
                    return value;
                },
                ticksTooltip: function (v) {
                    return 'Tooltip for ' + v;
                }
            }
        };
        $scope.sliderSvcTechEfficiency = { //requires angular-bootstrap to display tooltips
            value: 1.1,
            options: {
                floor: 0,
                ceil: 3,
                step: 0.1,
                precision: 1,
                showTicksValues: 0.5,
                showSelectionBar: true,
                onEnd: function () {
                    $scope.isEfficiencySliderChanged = true;
                    $scope.isTechEfficenyChangeAllowed = false;
                    $scope.serviceTechEfficiency = $scope.sliderSvcTechEfficiency.value;
                    $scope.updateChart();
                },
                translate: function (value) {
                    return value;
                },
                ticksTooltip: function (v) {
                    return 'Tooltip for ' + v;
                }
            }
        };
        $scope.sliderHoursPerRepairOrder = { //requires angular-bootstrap to display tooltips
            value: 2.1,
            options: {
                step: 0.1,
                precision: 1,
                showTicksValues: 1.5,
                showSelectionBar: true,
                onEnd: function () {
                    $scope.hoursPerRoStart = $scope.hoursPerRoChange;
                    $scope.hoursPerRoChange = $scope.sliderHoursPerRepairOrder.value;

                    onHoursPerRepairOrderChange($scope.hoursPerRoChange, $scope.hoursPerRoStart);
                },
                translate: function (value) {
                    return value;
                },
                ticksTooltip: function (v) {
                    return 'Tooltip for ' + v;
                }
            }
        };

        $scope.UpdateHours = function () {
            $scope.updateChart();
        }

        $scope.sliderNoOfRepairOrder = { //requires angular-bootstrap to display tooltips
            value: 10,
            options: {
                floor: 0,
                ceil: 5,
                step: 1,
                showTicksValues: 10,
                showSelectionBar: true,
                onEnd: function () {
                    $scope.repairOrderStart = $scope.repairOrderChage;
                    $scope.repairOrderChage = $scope.sliderNoOfRepairOrder.value;
                    onNoOfRepairOrderChange($scope.repairOrderChage, $scope.repairOrderStart);
                },
                translate: function (value) {
                    return value;
                },
                ticksTooltip: function (v) {
                    return 'Tooltip for ' + v;
                }
            }
        };
        function onNoOfRepairOrderChange(roValue, roStartValue) {

            var ChangedValue = roValue / roStartValue;

            _.each($scope.FrDataset, function (o) {

                var propotionDiff = 0;

                $scope.forecastedSales = _.map($scope.forecastedSales, function (e) {

                    if (moment(e.FullDate).isSame(moment(o.FullDate))) {
                        if ($scope.filterResultForType == "A") {
                            e.ForecastSvcROCountEdit = ParseNumberWoRounding(e.ForecastSvcROCountEdit * ChangedValue);
                        }
                        else {
                            if ($scope.filterResultForType == "C" && e.LaborType == "C") {
                                var temp = e.ForecastSvcROCountEdit;
                                e.ForecastSvcROCountEdit = ParseNumberWoRounding((e.ForecastSvcROCountEdit / roStartValue) * roValue);
                                propotionDiff = e.ForecastSvcROCountEdit - temp;

                            }
                            if ($scope.filterResultForType == "I" && e.LaborType == "I") {
                                var temp = e.ForecastSvcROCountEdit;
                                e.ForecastSvcROCountEdit = ParseNumberWoRounding((e.ForecastSvcROCountEdit / roStartValue) * roValue);
                                propotionDiff = e.ForecastSvcROCountEdit - temp;
                            }
                            if ($scope.filterResultForType == "W" && e.LaborType == "W") {
                                var temp = e.ForecastSvcROCountEdit;
                                e.ForecastSvcROCountEdit = ParseNumberWoRounding((e.ForecastSvcROCountEdit / roStartValue) * roValue);
                                propotionDiff = e.ForecastSvcROCountEdit - temp;
                            }

                            if (($scope.filterResultForType == "C" || $scope.filterResultForType == "I" || $scope.filterResultForType == "W") && e.LaborType == "A") {
                                e.ForecastSvcROCountEdit += propotionDiff;
                            }

                        }
                        return e;
                    } else {
                        return e;
                    }
                });
            });

            $scope.updateChart();
        }

        function onHoursPerRepairOrderChange(valNew, valStart) {

            var ChangedValue = valNew / valStart;

            _.each($scope.FrDataset, function (o) {
                var propotionDiff = 0;
                $scope.forecastedSales = _.map($scope.forecastedSales, function (e) {
                    if (moment(e.FullDate).isSame(moment(o.FullDate))) {
                        if ($scope.filterResultForType == "A") {
                            e.ForecastSvcHoursPerROEdit = ParseNumberWoRounding(e.ForecastSvcHoursPerROEdit * ChangedValue);
                        }
                        else {
                            if ($scope.filterResultForType == "C" && e.LaborType == "C") {
                                propotionDiff = ParseNumberWoRounding((valNew - valStart) * e.ForecastSvcROCountEdit);
                                e.ForecastSvcHoursPerROEdit = ParseNumberWoRounding((e.ForecastSvcHoursPerROEdit / valStart) * valNew);
                            }
                            if ($scope.filterResultForType == "I" && e.LaborType == "I") {
                                propotionDiff = ParseNumberWoRounding((valNew - valStart) * e.ForecastSvcROCountEdit);
                                e.ForecastSvcHoursPerROEdit = ParseNumberWoRounding((e.ForecastSvcHoursPerROEdit / valStart) * valNew);
                            }
                            if ($scope.filterResultForType == "W" && e.LaborType == "W") {
                                propotionDiff = ParseNumberWoRounding((valNew - valStart) * e.ForecastSvcROCountEdit);
                                e.ForecastSvcHoursPerROEdit = ParseNumberWoRounding((e.ForecastSvcHoursPerROEdit / valStart) * valNew);
                            }

                            if (($scope.filterResultForType == "C" || $scope.filterResultForType == "I" || $scope.filterResultForType == "W") && e.LaborType == "A") {
                                e.ForecastSvcHoursPerROEdit = ParseNumberWoRounding(((e.ForecastSvcHoursPerROEdit * e.ForecastSvcROCountEdit) + propotionDiff) / e.ForecastSvcROCountEdit);
                            }

                        }
                        return e;
                    } else {
                        return e;
                    }
                });
            });
            $scope.updateChart();
        }

        $scope.sliderEffectiveLabourRate = { //requires angular-bootstrap to display tooltips
            value: 50,
            options: {
                floor: 0,
                ceil: 100,
                step: 1,
                showTicksValues: 10,
                showSelectionBar: true,
                onEnd: function () {
                    $scope.LabourRateHourStart = $scope.LabourRateHourChange;
                    $scope.LabourRateHourChange = $scope.sliderEffectiveLabourRate.value;
                    onEffectiveLabourRateChange($scope.LabourRateHourChange, $scope.LabourRateHourStart);
                },
                translate: function (value) {
                    return value;
                },
                ticksTooltip: function (v) {
                    return 'Tooltip for ' + v;
                }
            }
        };

        function onEffectiveLabourRateChange(valNew, valStart) {
            var ChangedValue = valNew / valStart;

            _.each($scope.FrDataset, function (o) {
                var propotionDiff = 0;
                $scope.forecastedSales = _.map($scope.forecastedSales, function (e) {
                    if (moment(e.FullDate).isSame(moment(o.FullDate))) {
                        if ($scope.filterResultForType == "A") {
                            e.ForecastServiceELREdit = ParseNumberWoRounding(e.ForecastServiceELREdit * ChangedValue);
                        }
                        else {
                            if ($scope.filterResultForType == "C" && e.LaborType == "C") {
                                propotionDiff = ParseNumberWoRounding((valNew - valStart) * e.ForecastSvcROCountEdit);
                                //
                                e.ForecastServiceELREdit = ParseNumberWoRounding((e.ForecastServiceELREdit / valStart) * valNew);
                            }
                            if ($scope.filterResultForType == "I" && e.LaborType == "I") {
                                propotionDiff = ParseNumberWoRounding((valNew - valStart) * e.ForecastSvcROCountEdit);
                                e.ForecastServiceELREdit = ParseNumberWoRounding((e.ForecastServiceELREdit / valStart) * valNew);
                            }
                            if ($scope.filterResultForType == "W" && e.LaborType == "W") {
                                propotionDiff = ParseNumberWoRounding((valNew - valStart) * e.ForecastSvcROCountEdit);
                                e.ForecastServiceELREdit = ParseNumberWoRounding((e.ForecastServiceELREdit / valStart) * valNew);
                            }

                            if (($scope.filterResultForType == "C" || $scope.filterResultForType == "I" || $scope.filterResultForType == "W") && e.LaborType == "A") {
                                e.ForecastServiceELREdit = ParseNumberWoRounding(((e.ForecastSvcROCountEdit * e.ForecastServiceELREdit) + propotionDiff) / e.ForecastSvcROCountEdit);
                            }

                        }
                        return e;
                    } else {
                        return e;
                    }
                });
            });
            $scope.updateChart();
        }

        function ParseNumber(val) {
            if (isNaN(val))
                return 0;
            if (!isFinite(val))
                return 0;
            else
                return parseFloat(val.toFixed(2));
        }

        function ParseNumberWoRounding(val) {
            if (isNaN(val))
                return 0;
            if (!isFinite(val))
                return 0;
            else
                return val;
        }
       
        $scope.metricList = [
			{
			    metricName: '# of Repair Orders',
			    metricValue: 'roCount',
			    frOriginalValueId: 'ForecastSvcROCount',
			    frRevisedValueId: 'ForecastSvcROCountEdit'
			},
			{
			    metricName: 'Hours per Repair Order',
			    metricValue: 'hoursPerRo',
			    frOriginalValueId: 'ForecastSvcHoursPerRO',
			    frRevisedValueId: 'ForecastSvcHoursPerROEdit'
			},
            {
                metricName: 'Effective Labor Rate',
                metricValue: 'elr',
                frOriginalValueId: 'ForecastServiceELR',
                frRevisedValueId: 'ForecastServiceELREdit'
            }];

        $scope.currentMetric = {
            metricName: '# of Repair Orders',
            metricValue: 'roCount'
        };
        $scope.legends = [
                            { text: 'Actual ' + $scope.currentMetric.metricName, color: 'steelblue', dashedLine: '0,0', circleVisible: 0, width: 120 },

                            { text: 'Forecast ' + $scope.currentMetric.metricName, color: 'steelblue', dashedLine: '0,0', circleVisible: 1, width: 170 }
        ];
        $scope.chartObject = metricChartObject[$scope.currentMetric.metricValue];

        $scope.SetNewMetric = function (val) {
            $scope.currentMetric = val;
            $scope.chartObject = metricChartObject[$scope.currentMetric.metricValue];
            $scope.legends = [
                          { text: 'Actual ' + $scope.currentMetric.metricName, color: 'steelblue', dashedLine: '0,0', circleVisible: 0, width: 120 },
                          { text: 'Forecast ' + $scope.currentMetric.metricName, color: 'steelblue', dashedLine: '0,0', circleVisible: 1, width: 170 }
            ];
        }

        $scope.setupSliders = function () {
            $scope.sliderPositions = {};
        }

        function resetFilters() {
            var currentDate = moment();
            $scope.dates = {
                startDate: moment().startOf('Month'),
                endDate: moment().add(3, "M").endOf('Month')
            };
            $scope.selectedStartdate = moment($scope.dates.startDate).format("MMM-YYYY");
            $scope.selectedEnddate = moment($scope.dates.endDate).format("MMM-YYYY");
            $scope.filterResultForType = "A";
            $scope.isEfficiencySliderChanged = false;
            $scope.isTechEfficenyChangeAllowed = true;
        }

        $scope.ResetChart = function () {
            $scope.ShowLoader = true;
            resetFilters();
            $('input[type = "daterange"]').val(moment($scope.dates.startDate).format('MMM/YYYY') + ' - ' + moment($scope.dates.endDate).format('MMM/YYYY'));
            $('input[type="daterange"]').trigger('apply.daterangepicker');
        }

        function FilterActualData(ActualData) {
            var filtereddata = ActualData

            if ($scope.filterResultForType == "C") {
                filtereddata = _.filter(filtereddata, function (d) {
                    return d.LaborType == "C"
                });
            }
            if ($scope.filterResultForType == "I") {
                filtereddata = _.filter(filtereddata, function (d) {
                    return d.LaborType == "I"
                });
            }
            if ($scope.filterResultForType == "W") {
                filtereddata = _.filter(filtereddata, function (d) {
                    return d.LaborType == "W"
                });
            }

            filtereddata = _.sortBy(filtereddata, function (d) {
                return d.ActualDate;
            });

            return filtereddata;
        }

        function FilterForecastedData(ForecastData) {
            var filtereddata = ForecastData;

            if ($scope.filterResultForType == "C") {
                filtereddata = _.filter(filtereddata, function (d) {
                    return d.LaborType == "C"
                });
            }
            if ($scope.filterResultForType == "I") {
                filtereddata = _.filter(filtereddata, function (d) {
                    return d.LaborType == "I"
                });
            }
            if ($scope.filterResultForType == "W") {
                filtereddata = _.filter(filtereddata, function (d) {
                    return d.LaborType == "W"
                });
            }

            if ($scope.filterResultForType == "A") {
                filtereddata = _.filter(filtereddata, function (d) {
                    return d.LaborType == "A"
                });
            }

            return filtereddata;
        }

        $('input[type=daterange]').on('apply.daterangepicker', function (ev, picker) {
            $scope.ShowLoader = true;
            $scope.isEfficiencySliderChanged = false;
            $scope.forecastedSales = [];
            GetDealershipDetails();
            GetForecastSales();
        });

        $scope.filterDataForType = function (val) {
            $scope.ShowLoader = true;
            $scope.filterResultForType = val;
            $scope.isEfficiencySliderChanged = false;           
            $scope.updateChart();
        }

        function getAllGroupedData() {
            var filtereddata = _.filter($scope.forecastedSales, function (d) {
                return d.LaborType == "A"
            });
            var test = _.map(_.groupBy(filtereddata, function (v) { return v.FullDate, v.LaborType; }), function (val, key) {
                return {
                    ForecastSvcROCountEdit: (_.reduce(val, function (res, co) { res = res + co.ForecastSvcROCountEdit; return res; }, 0) / val.length),
                    ForecastSvcHoursPerROEdit: (_.reduce(val, function (res, co) { res = res + co.ForecastSvcHoursPerROEdit; return res; }, 0) / val.length),
                    ForecastServiceELREdit: (_.reduce(val, function (res, co) { res = res + co.ForecastServiceELREdit; return res; }, 0) / val.length),
                    ForecastSvcLaborGross: (_.reduce(val, function (res, co) { res = res + co.ForecastSvcLaborGross; return res; }, 0) / val.length),
                    ForecastSvcLaborSales: (_.reduce(val, function (res, co) { res = res + co.ForecastSvcLaborSales; return res; }, 0) / val.length)
                };
            });
            return test;
        }

        $scope.updateChart = function () {

            if ($scope.ActualSales.length == 0 || $scope.forecastedSales.length == 0 || $scope.numberOfServiceTechs == -1) {
                return;
            }

            if (clearFrDatasetWatch !== undefined) {
                clearFrDatasetWatch();
            }

            var parseTime = d3.timeParse("%d-%m-%Y");
            var _ActualDataSet = FilterActualData($scope.ActualSales);
            var _ForecastDataSet = FilterForecastedData($scope.forecastedSales);

            var actualSvcROCount = 0, actualHoursPerRepairOrder = 0;
            var reverseSortedActualSales = _.sortBy($scope.ActualSales, function (d) { return d.ActualDate }).reverse();

            var lastActualCp = _.find(reverseSortedActualSales, function (d) { return d.LaborType == "C" });
            var lastActualInt = _.find(reverseSortedActualSales, function (d) { return d.LaborType == "I" });
            var lastActualWarr = _.find(reverseSortedActualSales, function (d) { return d.LaborType == "W" });

            actualSvcROCount += ParseNumber(lastActualCp.SvcROCount);
            actualSvcROCount += ParseNumber(lastActualInt.SvcROCount);
            actualSvcROCount += ParseNumber(lastActualWarr.SvcROCount);


            var actualSvcHoursSold = ParseNumber(lastActualCp.SvcHoursSold) + ParseNumber(lastActualInt.SvcHoursSold) + ParseNumber(lastActualCp.SvcHoursSold);

            actualHoursPerRepairOrder = ParseNumber(actualSvcHoursSold / actualSvcROCount)

            var mappedActualData = _.map(_.groupBy(_ActualDataSet, function (d) {
                return d.ActualDate
            }), function (value, key) {
                return [parseTime(moment(key).format('DD-MM-YYYY')), _.reduce(value, function (result, currentObject) {
                    result = result + currentObject.ServiceELR;
                    return result;
                }, 0),
                    _.reduce(value, function (result, currentObject) {
                        result = result + currentObject.SvcHoursPerRO;
                        return result;
                    }, 0),
                    _.reduce(value, function (result, currentObject) {
                        result = result + currentObject.SvcHoursSold;
                        return result;
                    }, 0),
                    _.reduce(value, function (result, currentObject) {
                        result = result + currentObject.SvcLaborGross;
                        return result;
                    }, 0),
                    _.reduce(value, function (result, currentObject) {
                        result = result + currentObject.SvcLaborSales;
                        return result;
                    }, 0),
                    _.reduce(value, function (result, currentObject) {
                        result = result + currentObject.SvcLaborSalesPerRO;
                        return result;
                    }, 0),
                    _.reduce(value, function (result, currentObject) {
                        result = result + currentObject.SvcROCount;
                        return result;
                    }, 0)
                ];
            });

            var mappedForecastedData = _.map(_.groupBy(_ForecastDataSet, function (d) {
                return d.FullDate
            }), function (value, key) {
                return [parseTime(moment(key).format('DD-MM-YYYY')),
					_.reduce(value, function (result, currentObject) {
					    result = result + currentObject.ForecastServiceELR;
					    return result;
					}, 0) / value.length,
                    _.reduce(value, function (result, currentObject) {
                        result = result + currentObject.ForecastServiceELREdit;
                        return result;
                    }, 0) / value.length,
                    _.reduce(value, function (result, currentObject) {
                        result = result + currentObject.ForecastSvcHoursPerRO;
                        return result;
                    }, 0),
                    _.reduce(value, function (result, currentObject) {
                        result = result + currentObject.ForecastSvcHoursPerROEdit;
                        return result;
                    }, 0),
                    _.reduce(value, function (result, currentObject) {
                        result = result + currentObject.ForecastSvcHoursSold;
                        return result;
                    }, 0),
                    _.reduce(value, function (result, currentObject) {
                        result = result + currentObject.ForecastSvcHoursSoldEdit; //6
                        return result;
                    }, 0),
                    _.reduce(value, function (result, currentObject) {
                        result = result + currentObject.ForecastSvcLaborGross;
                        return result;
                    }, 0),
                    _.reduce(value, function (result, currentObject) {
                        result = result + currentObject.ForecastSvcLaborGrossEdit;
                        return result;
                    }, 0),
                    _.reduce(value, function (result, currentObject) {
                        result = result + currentObject.ForecastSvcLaborSales;
                        return result;
                    }, 0),
                    _.reduce(value, function (result, currentObject) {
                        result = result + currentObject.ForecastSvcLaborSalesEdit; //10
                        return result;
                    }, 0),
                    _.reduce(value, function (result, currentObject) {
                        result = result + currentObject.ForecastSvcLaborSalesPerRO;
                        return result;
                    }, 0),
                    _.reduce(value, function (result, currentObject) {
                        result = result + currentObject.ForecastSvcLaborSalesPerROEdit;
                        return result;
                    }, 0),
                    _.reduce(value, function (result, currentObject) {
                        result = result + currentObject.ForecastSvcROCount;
                        return result;
                    }, 0),
                    _.reduce(value, function (result, currentObject) {
                        result = result + currentObject.ForecastSvcROCountEdit;
                        return result;
                    }, 0),
                    _.reduce(value, function (result, currentObject) {
                        result = result + currentObject.Lower80SvcLaborGross;
                        return result;
                    }, 0),
                    _.reduce(value, function (result, currentObject) {
                        result = result + currentObject.Upper80SvcLaborGross;
                        return result;
                    }, 0),
                    _.reduce(value, function (result, currentObject) {
                        result = result + currentObject.Lower80SvcLaborSales;
                        return result;
                    }, 0),
                    _.reduce(value, function (result, currentObject) {
                        result = result + currentObject.Upper80SvcLaborSales;
                        return result;
                    }, 0),
                    _.reduce(value, function (result, currentObject) {
                        result = result + currentObject.Lower80SvcROCount;//19
                        return result;
                    }, 0),
                    _.reduce(value, function (result, currentObject) {
                        result = result + currentObject.Upper80SvcROCount;
                        return result;
                    }, 0),
                    _.reduce(value, function (result, currentObject) {
                        result = result + currentObject.Lower80ServiceELR;
                        return result;
                    }, 0),
                    _.reduce(value, function (result, currentObject) {
                        result = result + currentObject.Upper80ServiceELR;
                        return result;
                    }, 0),
					_.reduce(value, function (result, currentObject) {
					    result = result + currentObject.Lower80SvcHoursPerRO;
					    return result;
					}, 0),
					_.reduce(value, function (result, currentObject) {
					    result = result + currentObject.Upper80SvcHoursPerRO;
					    return result;
					}, 0)

                ];
            });

            $scope.AcDataset = [], $scope.FrDataset = [];

            mappedActualData.forEach(function (item, index) {
                $scope.AcDataset.push({
                    ActualDate: item[0],
                    ServiceELR: ParseNumber(item[5] / item[3]),//1
                    SvcHoursPerRO: ParseNumber(item[3] / item[7]),//2
                    SvcHoursSold: ParseNumber(item[3]),//3
                    SvcLaborGross: item[4],//4
                    SvcLaborSales: ParseNumber(item[5]),//5
                    SvcLaborSalesPerRO: ParseNumber(item[5] / item[7]),//6
                    SvcROCount: ParseNumber(item[7])//7
                });
            });

            mappedForecastedData.forEach(function (item, index) {
                $scope.FrDataset.push({
                    FullDate: item[0],

                    ForecastServiceELR: ParseNumber(item[1]),
                    ForecastServiceELREdit: ParseNumberWoRounding(item[2]),

                    ForecastSvcHoursPerRO: ParseNumber(item[3]),
                    ForecastSvcHoursPerROEdit: ParseNumberWoRounding(item[4]),

                    ForecastSvcHoursSold: ParseNumber(item[5]),
                    ForecastSvcHoursSoldEdit: item[6],

                    ForecastSvcLaborGross: ParseNumber(item[7]),
                    ForecastSvcLaborGrossEdit: item[8],

                    ForecastSvcLaborSales: ParseNumber(item[9]),
                    ForecastSvcLaborSalesEdit: item[10],

                    ForecastSvcLaborSalesPerRO: ParseNumber(item[9] / item[13]),
                    ForecastSvcLaborSalesPerROEdit: ParseNumberWoRounding(item[10] / item[14]),

                    ForecastSvcROCount: ParseNumber(item[13]),
                    ForecastSvcROCountEdit: item[14],

                    Lower80ServiceELR: ParseNumber(item[21]),
                    Lower80SvcHoursPerRO: ParseNumber(item[23]),
                    Lower80SvcHoursSold: ParseNumber(item[21]),
                    Lower80SvcLaborGross: ParseNumber(item[15]),
                    Lower80SvcLaborSales: ParseNumber(item[17]),
                    Lower80SvcLaborSalesPerRO: ParseNumber(item[17] / item[19]),
                    Lower80SvcROCount: ParseNumber(item[19]),

                    Upper80ServiceELR: ParseNumber(item[22]),
                    Upper80SvcHoursPerRO: ParseNumber(item[24]),
                    Upper80SvcHoursSold: ParseNumber(item[22]),
                    Upper80SvcLaborGross: ParseNumber(item[16]),
                    Upper80SvcLaborSales: ParseNumber(item[18]),
                    Upper80SvcLaborSalesPerRO: ParseNumber(item[18] / item[20]),
                    Upper80SvcROCount: ParseNumber(item[20])
                });
            });

            $scope.AcDataset = _.sortBy($scope.AcDataset, function (d) {
                return d.ActualDate
            });
            $scope.FrDataset = _.sortBy($scope.FrDataset, function (d) {
                return d.FullDate
            });

            var totalDaysFor = getWeekDaysCount();
            $scope.totalOpenHours = Math.round((($scope.openHoursFor.sunday * totalDaysFor.sunday) +
			($scope.openHoursFor.monday * totalDaysFor.monday) +
			($scope.openHoursFor.tuesday * totalDaysFor.tuesday) +
			($scope.openHoursFor.wednesday * totalDaysFor.wednesday) +
			($scope.openHoursFor.thursday * totalDaysFor.thursday) +
			($scope.openHoursFor.friday * totalDaysFor.friday) +
			($scope.openHoursFor.saturday * totalDaysFor.saturday)));

            var _totalRepairOrder = Math.round(getAvgSliderValue("ForecastSvcROCountEdit"));
            var _effectiveLaborRate = parseFloat(getAvgSliderValue("ForecastServiceELREdit").toFixed(2));
            var _hoursPerRepairOrder = parseFloat(getAvgSliderValue("ForecastSvcHoursPerROEdit").toFixed(2));

            var groupedAvgOfAllData = getAllGroupedData();

            $scope.hoursSoldOflastActual = parseFloat((actualSvcROCount * actualHoursPerRepairOrder).toFixed(2));

            $scope.hoursSold = parseFloat((_totalRepairOrder * _hoursPerRepairOrder).toFixed(2));

            $scope.hoursSoldOfAll = parseFloat((Math.round(groupedAvgOfAllData[0].ForecastSvcROCountEdit) * parseFloat(groupedAvgOfAllData[0].ForecastSvcHoursPerROEdit).toFixed(2)));

            if ($scope.isTechEfficenyChangeAllowed == true) {
                // $scope.serviceTechEfficiency = ((serviceActualHours == 0) ? 0 : ($scope.hoursSoldOflastActual / serviceActualHours));
                $scope.serviceTechEfficiency = 1;
                $scope.sliderSvcTechEfficiency.value = $scope.serviceTechEfficiency;
            }

            if ($scope.isEfficiencySliderChanged == false) {
                $scope.sliderNoOfRepairOrder.value = _totalRepairOrder;
                $scope.sliderEffectiveLabourRate.value = _effectiveLaborRate;
                $scope.sliderHoursPerRepairOrder.value = _hoursPerRepairOrder;
                $scope.sliderSvcTechs.value = $scope.numberOfServiceTechs;
            }


            $scope.hoursAvailable = parseFloat((($scope.numberOfServiceTechs * $scope.totalOpenHours * $scope.serviceTechEfficiency) / $scope.FrDataset.length).toFixed(2));

            $scope.dollarAvailable = ($scope.hoursAvailable * _effectiveLaborRate);

            $scope.hoursVariance = parseFloat(($scope.hoursSold - $scope.hoursAvailable).toFixed(2));

            $scope.hoursVarianceAll = parseFloat(($scope.hoursSoldOfAll - $scope.hoursAvailable).toFixed(2));

            $scope.dollarSold = parseFloat(($scope.hoursSold * _effectiveLaborRate).toFixed(2));

            $scope.dollarVariance = parseFloat(($scope.dollarSold - $scope.dollarAvailable).toFixed(2));

            $scope.lostHours = ($scope.hoursVariance > 0 ? $scope.hoursVariance : 0);
            $scope.shortHours = ($scope.hoursVariance < 0 ? (-1 * $scope.hoursVariance) : 0);

            $scope.lostHoursAll = ($scope.hoursVarianceAll > 0 ? $scope.hoursVarianceAll : 0);
            $scope.shortHoursAll = ($scope.hoursVarianceAll < 0 ? (-1 * $scope.hoursVarianceAll) : 0);

            UpdateSliderScaleAndValue();
            UpdateKpiData();
            $scope.ShowLoader = false;

            $scope.repairOrderStart = $scope.sliderNoOfRepairOrder.value;
            $scope.repairOrderChage = $scope.sliderNoOfRepairOrder.value;

            $scope.hoursPerRoChange = $scope.sliderHoursPerRepairOrder.value;
            $scope.hoursPerRoStart = $scope.sliderHoursPerRepairOrder.value;

            $scope.LabourRateHourChange = $scope.sliderEffectiveLabourRate.value;
            $scope.LabourRateHourStart = $scope.sliderEffectiveLabourRate.value;

            $scope.isEfficiencySliderChanged = true;
            $scope.isTechEfficenyChangeAllowed = false;
        }

        function getAvgSliderValue(metricColumnName) {
            return (_.reduce($scope.FrDataset, function (res, c) {
                res = res + c[metricColumnName];
                return res
            }, 0) / $scope.FrDataset.length);
        }

        function UpdateSliderScaleAndValue() {
            function sliderTickSize(val) {
                if (val > 100000) {
                    return 100000;
                }
                if (val > 10000) {
                    return 10000;
                }
                else if (val > 1000 && val < 10000) {
                    return 1000;
                }
                else {
                    return (val <= 10) ? 1 : (val >= 10 && val < 20) ? 2 : (val >= 100) ? 10 : 5;
                }
            }

            //this sets the slider scale, value and also set the start and change value (used by table for recalculating forecasting value)
            $scope["sliderEffectiveLabourRate"].options.floor = Math.round(($scope["sliderEffectiveLabourRate"].value * 0.5));
            $scope["sliderEffectiveLabourRate"].options.ceil = Math.round(($scope["sliderEffectiveLabourRate"].value * 1.5));
            $scope["sliderEffectiveLabourRate"].options.showTicksValues = Math.round($scope["sliderEffectiveLabourRate"].options.ceil * 0.15);

            $scope["sliderNoOfRepairOrder"].options.floor = Math.round(($scope["sliderNoOfRepairOrder"].value * 0.5));
            $scope["sliderNoOfRepairOrder"].options.ceil = (($scope["sliderNoOfRepairOrder"].value * 1.5) < 10 ? 10 : Math.round(($scope["sliderNoOfRepairOrder"].value * 1.5)));
            $scope["sliderNoOfRepairOrder"].options.showTicksValues = Math.round($scope["sliderNoOfRepairOrder"].options.ceil * 0.1);
            $scope["sliderNoOfRepairOrder"].options.step = 1;

            $scope["sliderHoursPerRepairOrder"].options.floor = parseFloat(($scope["sliderHoursPerRepairOrder"].value * 0.5).toFixed(1));
            $scope["sliderHoursPerRepairOrder"].options.ceil = (($scope["sliderHoursPerRepairOrder"].value * 1.5 < 0.5) ? 0.5 : parseFloat(($scope["sliderHoursPerRepairOrder"].value * 1.5).toFixed(1)));
            $scope["sliderHoursPerRepairOrder"].options.showTicksValues = Math.round(($scope["sliderHoursPerRepairOrder"].options.ceil * 0.2));//sliderTickSize($scope["sliderHoursPerRepairOrder"].options.ceil);
        }

        function UpdateKpiData() {
            var groupedAvgOfAllData = getAllGroupedData();

            var _effectiveLaborRate = parseFloat(getAvgSliderValue("ForecastServiceELREdit").toFixed(2));

            var _effectiveLaborRateOfAll = parseFloat(groupedAvgOfAllData[0].ForecastServiceELREdit);

            var tSvcLaborSales = ($scope.hoursSold - $scope.lostHours) * _effectiveLaborRate;
            var marginRate = parseFloat((getAvgSliderValue("ForecastSvcLaborGross") / getAvgSliderValue("ForecastSvcLaborSales")).toFixed(2));

            var marginRateAll = parseFloat((groupedAvgOfAllData[0].ForecastSvcLaborGross / groupedAvgOfAllData[0].ForecastSvcLaborSales).toFixed(2));

            var totalSvcLaborSales = parseFloat(getAvgSliderValue("ForecastSvcLaborSales").toFixed(2)),
			totalSvcLaborGross = (tSvcLaborSales * marginRate),
			svcUtilization = 0,
            lostSvcLaborGross = ($scope.lostHoursAll * _effectiveLaborRateOfAll * marginRateAll),
			oppSvcLaborGross = ($scope.shortHoursAll * _effectiveLaborRateOfAll * marginRateAll),
			svcUtilizationCost = lostSvcLaborGross + oppSvcLaborGross,
			kpiLostSvcLaborGrossValue = ($scope.filterResultForType == 'A' ? $scope.moneyFormatter(lostSvcLaborGross) : 'N/A'),
			kpiOppSvcLaborGrossValue = ($scope.filterResultForType == 'A' ? $scope.moneyFormatter(oppSvcLaborGross) : 'N/A'),
			kpiSvcUtilizationCostValue = ($scope.filterResultForType == 'A' ? $scope.moneyFormatter(svcUtilizationCost) : 'N/A');

            if ($scope.hoursAvailable !== 0) {
                svcUtilization = Math.round(($scope.hoursSold / $scope.hoursAvailable) * 100)
            }
            $scope.kpiSvcLaborSales = {
                legendFor: "Svc Labor Sales", valueOfLegend: $scope.moneyFormatter(tSvcLaborSales), legendType: "Monthly", title: "Effective Labor Rate * Min{Hours Sold, Hours Available}"
            };
            $scope.kpiSvcLaborGross = {
                legendFor: "Svc Labor Gross", valueOfLegend: $scope.moneyFormatter(totalSvcLaborGross), legendType: "Monthly", title: "Svc Labor Sales * % Gross-to-Sales"
            };
            $scope.kpiSvcUtilization = {
                legendFor: "Svc Utilization", valueOfLegend: $scope.percentFormatter(svcUtilization), legendType: "Monthly", title: "Hours Sold/Hours Available * 100%"
            };
            $scope.kpiLostSvcLaborGross = {
                legendFor: "Lost Svc Labor Gross", valueOfLegend: kpiLostSvcLaborGrossValue, legendType: "Monthly", title: "Effective Labor Rate * (Hours Sold – Min{Hours Sold, Hours Available})"
            };
            $scope.kpiOppSvcLaborGross = {
                legendFor: "Opp Svc Labor Gross", valueOfLegend: kpiOppSvcLaborGrossValue, legendType: "Monthly", title: "Effective Labor Rate * (Hours Available – Min{Hours Sold, Hours Available})"
            };
            $scope.kpiSvcUtilizationCost = {
                legendFor: "Svc Inefficiency Cost", valueOfLegend: kpiSvcUtilizationCostValue, legendType: "Monthly", title: "Lost Svc Labor Gross + Opp Svc Labor Gross"
            };
           
                $scope.hoursVarianceColor = ((Math.abs($scope.hoursVariance / $scope.hoursAvailable) >= 0.1) ? "red" : Math.abs($scope.hoursVariance / $scope.hoursAvailable) >= 0.05 ? "yellow" : "green");
                $scope.hoursVarianceIndicator = ($scope.filterResultForType == 'A'?(($scope.hoursVarianceColor !== "green" && $scope.hoursSold > $scope.hoursAvailable) ? "fa fa-arrow-down fa-2" : ($scope.hoursVarianceColor !== "green" && $scope.hoursSold < $scope.hoursAvailable) ? "fa fa-arrow-up fa-2" : ""):"");
                $scope.dollarVarianceColor = ((Math.abs($scope.dollarVariance / $scope.dollarAvailable) >= 0.1) ? "red" : Math.abs($scope.dollarVariance / $scope.dollarAvailable) >= 0.05 ? "yellow" : "green");
                $scope.dollarVarianceIndicator = ($scope.filterResultForType == 'A'?(($scope.dollarVarianceColor !== "green" && $scope.dollarSold > $scope.dollarAvailable) ? "fa fa-arrow-down fa-2" : ($scope.dollarVarianceColor !== "green" && $scope.dollarSold < $scope.dollarAvailable) ? "fa fa-arrow-up fa-2" : ""):"");

                $scope.hoursSoldTitle = "(TotalRepairOrder * HoursPerRepairOrder)";
                $scope.hoursAvailableTitle = "(NumberOfServiceTechnician * TotalOpenHours * ServiceTechnicianEfficiency)"
                $scope.hoursVarianceTitle = "(Hours Sold - Hours Available)";

                $scope.dollarSoldTitle = "(Hours Sold * EffectiveLaborRate)";
                $scope.dollarAvailableTitle = "(HoursAvailable * EffectiveLaborRate)";
                $scope.dollarVarianceTitle = "(Dollar Sold - Dollar Available";
           
            

        }

        function GetActualSales() {
            apiService.get('api/getserviceactual?dealerid=' + $scope.Dealer.Id + '&makeid=0&modelid=0', null, function (data) {
                $scope.ActualSales = data.data;
                $scope.updateChart();
            });
        }

        function GetDealershipDetails() {
            apiService.get('api/dealership/details?dealerid=' + $scope.Dealer.Id + '&fromDate=01-08-2016&toDate=01-08-2016', null, function (data) {
                $scope.numberOfServiceTechs = data.data[0].NoOfTechnician;
                serviceActualHours = data.data[0].ServiceActualHours;
                $scope.updateChart();
            });
        }

        function getWeekDaysCount() {
            var startDate = moment($scope.dates.startDate).startOf('month').clone(),
			enddate = moment($scope.dates.endDate).endOf('month').clone(), index = 0;

            var days = enddate.diff(startDate, 'days');
            var weekDays = {
                sunday: 0, monday: 0, tuesday: 0, wednesday: 0, thursday: 0, friday: 0, saturday: 0
            };

            while (index < days) {
                var _day = startDate.add(1, 'days');
                if (_day.days() == 0) {
                    weekDays.sunday += 1;
                }
                if (_day.days() == 1) {
                    weekDays.monday += 1;
                }
                if (_day.days() == 2) {
                    weekDays.tuesday += 1;
                }
                if (_day.days() == 3) {
                    weekDays.wednesday += 1;
                }
                if (_day.days() == 4) {
                    weekDays.thursday += 1;
                }
                if (_day.days() == 5) {
                    weekDays.friday += 1;
                }
                if (_day.days() == 6) {
                    weekDays.saturday += 1;
                }
                index++;
            }

            return weekDays;
        }

        function GetForecastSales() {
            apiService.get('api/getserviceforecasted?dealerid=' + $scope.Dealer.Id + '&startdate=' + moment($scope.dates.startDate).format('DD-MM-YYYY') + '&enddate=' + moment($scope.dates.endDate).format('DD-MM-YYYY'), null, function (data) {

                $scope.forecastedSales = _.map(data.data, function (element) {
                    return _.extend({
                    }, element, {
                        ForecastServiceELREdit: element.ForecastServiceELR,
                        ForecastSvcHoursPerROEdit: element.ForecastSvcHoursPerRO,
                        ForecastSvcHoursSoldEdit: element.ForecastSvcHoursSold,
                        ForecastSvcLaborGrossEdit: element.ForecastSvcLaborGross,
                        ForecastSvcLaborSalesEdit: element.ForecastSvcLaborSales,
                        ForecastSvcLaborSalesPerROEdit: element.ForecastSvcLaborSalesPerRO,
                        ForecastSvcROCountEdit: element.ForecastSvcROCount
                    });
                });

                $scope.setupSliders();
                $scope.updateChart();

            });
        }

        function Init() {
            resetFilters();
            GetDealershipDetails();
            GetActualSales();
            GetForecastSales();
        }

    }
})(angular.module('dealerWizard'));