﻿(function (app) {
    'use strict';

    app.component("timeseries", {
        templateUrl: "/Scripts/app/components/timeseries/timeseries.html",
        controller: ['$scope', '$element', '$attrs', '$log', timeSeriesCtrl],
        bindings: {
            data1: "<",
            data2: "<",
            data3: "<",
            chartObject: "<",
            legends:"<"
        }
    });

    function timeSeriesCtrl($scope, $element, $attrs, $log) {
        var ctrl = this;
        var legends=[];
        //$scope.AcDataset, "Actual_Date", "actualValue", Actualvalueline, "line", 1e-6, ["actualCircle", "dot"], tip

        var chartObject = [];

        //Data related Varaible
        var data = {
            data1: [],
            data2: [],
            data3: []
        };
        var chartObject = [];

        //Chart Related Variable
        var xBoundArr = [],
            yBoundArr = [];


        ctrl.$onInit = function () {
            chartObject = angular.copy(ctrl.chartObject);
            legends = angular.copy(ctrl.legends);
        }

        ctrl.$onChanges = function (ChangeObject) {

            if (ChangeObject.data1 != undefined) {
                if (!ChangeObject.data1.isFirstChange()) {
                    data.data1 = angular.copy(ctrl.data1);
                    var element = $element[0].querySelector('#forecast-chart');
                    xBoundArr = [];
                    yBoundArr = [];
                    renderChart(element, data, chartObject);
                }
            }

            if (ChangeObject.data2 != undefined) {
                if (!ChangeObject.data2.isFirstChange()) {
                    data.data2 = angular.copy(ctrl.data2);
                    var element = $element[0].querySelector('#forecast-chart');
                    xBoundArr = [];
                    yBoundArr = [];
                    renderChart(element, data, chartObject);
                }
            }

            if (ChangeObject.data3 != undefined) {
                if (!ChangeObject.data3.isFirstChange()) {
                    data.data3 = angular.copy(ctrl.data3);
                    var element = $element[0].querySelector('#forecast-chart');
                    xBoundArr = [];
                    yBoundArr = [];
                    renderChart(element, data, chartObject);
                }
            }
            if (ChangeObject.legends != undefined) {
                if (!ChangeObject.legends.isFirstChange()) {
                    legends = angular.copy(ctrl.legends);
                    var element = $element[0].querySelector('#forecast-chart');
                    xBoundArr = [];
                    yBoundArr = [];
                    renderChart(element, data, chartObject,legends);
                }
            }

            if (ChangeObject.chartObject != undefined) {
                if (!ChangeObject.chartObject.isFirstChange()) {
                    chartObject = angular.copy(ctrl.chartObject);
                    var element = $element[0].querySelector('#forecast-chart');
                    xBoundArr = [];
                    yBoundArr = [];
                    renderChart(element, data, chartObject);
                }
            }
        }


        //D3 Chart Helper Methods Starts
        // define the line
        function drawLineInfo(xDataColumn, yDataColumn, xAxis, yAxis) {
            return d3.line().x(function (d) { return xAxis(d[xDataColumn]); }).y(function (d) { return yAxis(d[yDataColumn]); });
        }

        function createToolTip(fnToolTipHtml) {
            return d3.tip()
               .attr('class', 'd3-tip ttip')
               .offset([-10, 0])
               .html(fnToolTipHtml);
        }

        function drawAreaInfo(xAxis, yAxis, xDataColumn, yDataColumn1, yDataColumn2) {
            return d3.area()
                     .x(function (d) { return xAxis(d[xDataColumn]); })//d.FullDate); })
                     .y0(function (d) { return yAxis(d[yDataColumn1]); })//d.lowerThresholdUnitDel); })
                     .y1(function (d) { return yAxis(d[yDataColumn2]); });//d.upperThresholdUnitDel); });
        }
        //End here

        function fetchXAxisData(data) {
            _.each(chartObject, function (d) {
                var _boundArr = [];
                if (d.chart.type == "line") {
                    _boundArr = d3.extent(data[d.dataSetId], function (f) { return f[d.chart.x]; });
                }
                else if (d.chart.type == "circle") {
                    _boundArr = d3.extent(data[d.dataSetId], function (f) { return f[d.chart.cx]; });
                }
                else if (d.chart.type == "area") {
                    _boundArr = d3.extent(data[d.dataSetId], function (f) { return f[d.chart.x]; });
                }
                //combining both array as one for each loop
                xBoundArr = xBoundArr.concat(_boundArr);
            });
            xBoundArr = d3.extent(xBoundArr, function (d) { return d; });
        }
        function fetchYAxisData(data) {
            var _boundArr = [];
            _.each(chartObject, function (d) {
                if (d.chart.type == "line") {
                    _boundArr = d3.extent(data[d.dataSetId], function (f) { return f[d.chart.y]; });
                }
                else if (d.chart.type == "circle") {
                    _boundArr = d3.extent(data[d.dataSetId], function (f) { return f[d.chart.cy]; });
                }
                else if (d.chart.type == "area") {
                    var bound1 = [], bound2 = [];
                    bound1 = d3.extent(data[d.dataSetId], function (f) { return f[d.chart.y1]; });
                    bound2 = d3.extent(data[d.dataSetId], function (f) { return f[d.chart.y2]; });
                    _boundArr = bound1.concat(bound2);
                }
                //combining both array as one for each loop
                yBoundArr = yBoundArr.concat(_boundArr);
            });
            yBoundArr = d3.extent(yBoundArr, function (d) { return d; });
        }

        function createArea(svg, data, area, classes) {
            // add the area
            svg.append("path")
               .data([data])
               .attr("class", classes.join(' '))
               .attr("d", area);
        }
        function createDataLines(svg, data, xValueProp, yValueProp, LineFor, LineClasses) {
            svg.append("path")
                     .data([data])
                     .attr("class", LineClasses.join(' '))
                     .attr("d", LineFor);
        }
        function createCircleFor(svg, xAxis, yAxis, data, xValueProp, yValueProp, radius, CircleVisible, circleCLasses, tooltip) {
            svg.selectAll("." + circleCLasses[0])
                 .data(data)
                 .enter()
                 .append("circle")
                 .attr('class', circleCLasses.join(' '))
                 .attr("r", radius)
                 .attr("cx", function (d) { return xAxis(d[xValueProp]); })
                 .attr("cy", function (d) { return yAxis(d[yValueProp]); })
                 .attr('fill', 'blue')
                .attr("opacity", CircleVisible)
                 .on("mouseover", function (d, i) {
                     tooltip.show(d, i);
                 })
                .on("mouseout", function (d, i) {
                    tooltip.hide(d, i);
                });
        }

        function renderChart(element, dataset, chartObject) {
            //dataset contain all 3 data

            //Verifying data is available for all report object
            var uniqueDatasetsId = _.uniq(chartObject, function (d) { return d.dataSetId; });

            var returnRenderChartCall = false;
            _.each(uniqueDatasetsId, function (d) {
                if (dataset[d.dataSetId].length == 0) {
                    returnRenderChartCall = true;
                }
            });
            if (returnRenderChartCall) {
                $log.log("timeseries: not dataset to render, returnig from timeseries");
            }

            //clear any old content and redraw fresh svg
            d3.select(element).html("");

            //create X and Y domain array from data
            fetchXAxisData(dataset);
            fetchYAxisData(dataset);

            var margin = { top: 72, right: 20, bottom: 30, left: 50 };
            var width = Math.ceil((d3.select(element).node().clientWidth)) - margin.left - margin.right;
            var height = 247 - margin.top - margin.bottom;

            // set the ranges
            var x = d3.scaleTime().range([0, width]);
            var y = d3.scaleLinear().range([height, 0]);

            // scale the range of the data
            x.domain(xBoundArr);
            y.domain(yBoundArr);

            var svg = d3.select(element).append("svg")
               .attr("width", width + margin.left + margin.right)
               .attr("height", height + margin.top + margin.bottom)
             .append("g")
               .attr("transform",
                     "translate(" + margin.left + "," + margin.top + ")");

            _.each(chartObject, function (d) {
                if (d.chart.type == "line") {
                    var valueline = drawLineInfo(d.chart.x, d.chart.y, x, y);
                    createDataLines(svg, data[d.dataSetId], d.chart.x, d.chart.y, valueline, d.chart.classes);
                }
                else if (d.chart.type == "circle") {
                    var tip = createToolTip(d.fnTooltip);
                    createCircleFor(svg, x, y, data[d.dataSetId], d.chart.cx, d.chart.cy, d.chart.r, d.chart.opacity, d.chart.classes, tip);
                    svg.call(tip);
                }
                else if (d.chart.type == "area") {
                    var area = drawAreaInfo(x, y, d.chart.x, d.chart.y1, d.chart.y2);
                    createArea(svg, data[d.dataSetId], area, d.chart.classes);
                }

            });

            // add the X Axis
            svg.append("g")
                .attr("transform", "translate(0," + height + ")")
                .call(d3.axisBottom(x).ticks(d3.timeMonth.every(GetTicksCountByContainerWidth(width))).tickFormat(d3.timeFormat("%b %y")));
            // add the Y Axis
            svg.append("g")
                .call(d3.axisLeft(y));

            //Plot legends on chart
            if (legends != undefined)
                PlotLegends(svg, width, legends);
        }

        function PlotLegends(svg, width,legendData) {
            var legend = svg.selectAll(".legend")
                  .data(legendData)
                .enter().append("g")
                  .attr("class", "legend")
                  .attr("transform", function (d, i) { return "translate(0,0)"; });
                  
            legend.append("line")
                .attr("x1", function (d, i) { return width-25; })
                .attr("x2", function (d, i) { return width; })
                .attr("y1", function (d, i) { return (i * 15) - 48; })
                .attr("y2", function (d, i) { return (i * 15) - 48; })
                .style("stroke-dasharray", function (d) { return d.dashedLine })
                .style('font-size', '10px')
                .style("stroke", function (d) { return d.color });

            legend.append("circle")
                            .attr("r", 3)
                            .attr("cx", function (d, i) { return width -12; })
                            .attr("cy", function (d, i) { return (i * 15) - 48; })
                            .attr("opacity", function (d) { return d.circleVisible; })
                            .style('font-size', '10px')
                            .style("fill", "blue");
                           
            legend.append("text")
                .attr("x", width-35)
                .attr("y", function (d, i) { return (i * 15) - 50; })
                .attr("dy", ".35em")
                .style('font-size', '10px')
                .style("text-anchor", "end")
                .text(function (d) { return d.text; });
        }

        function GetTicksCountByContainerWidth(width) {
            var TicksCount = 0;
            if (width < 300) {
                TicksCount = 12;
            }
        else if (width < 400) {
                TicksCount= 6;
            }
            else if(width >=400 && width <900){
                TicksCount = 4;
            }
            else if(width >=900) {
                TicksCount = 3;
            }

            return TicksCount;
        }

        var resizeTimmer;
        $(window).on("resize", function (e) {
            clearTimeout(resizeTimmer);
            resizeTimmer = setTimeout(function () {
                var element = $element[0].querySelector('#forecast-chart');
                renderChart(element, data, chartObject)
            }, 350)
        });

    }

})(angular.module("dealerWizard"));