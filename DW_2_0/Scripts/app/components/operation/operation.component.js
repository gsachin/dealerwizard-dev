﻿import dwActions from '../../actions/index';

angular.module('dealerWizard')
    .component('operation',
    {
        templateUrl: "/Scripts/app/components/operation/operation.html",
        controller: ['$scope', '$element', '$attrs', 'apiService', 'valueFormatter', 'applicationObject', '$timeout',
                'dealerService', 'tabsMetaDataService', '$rootScope', 'notificationService', '$window', 'userActivityService','$ngRedux', operationFunc],
        bindings: {
            data: "<"
        }
    });

    function operationFunc($scope, $element, $attrs, apiService, valueFormatter, applicationObject, $timeout,
        dealerService, tabsMetaDataService, $rootScope, notificationService, $window, userActivityService, $ngRedux) {
        var ctrl = this,
		dateFormatter = function (val) {
		    return valueFormatter.getformatter('date')(val, 'dd-MM-yyyy');
		};
        var ageingReportObject = {
            fi: [
                        { reportId: 50, ChartObject: ["0-30", "31-60", "61-90", "91-120", "121+"] },
                        { reportId: 54, ChartObject: ["0-30", "31-60", "61-90", "91-120", "121+"] }],
            inventory: [
                        { reportId: 18, ChartObject: ["0-30", "31-60", "61-90", "91-120", "121+"] },
                        { reportId: 19, ChartObject: ["0-30", "31-60", "61-90", "91-120", "121+"] },
                        { reportId: 95, ChartObject: ["0-30", "31-60", "61-90", "91-120", "121+"] },
                        { reportId: 97, ChartObject: ["0-30", "31-60", "61-90", "91-120", "121+"] },
                        { reportId: 99, ChartObject: ["0-30", "31-60", "61-90", "91-120", "121+"] }],
            newcar: [
                        { reportId: 18, ChartObject: ["0-30", "31-60", "61-90", "91-120", "121+"] },
                        { reportId: 19, ChartObject: ["0-30", "31-60", "61-90", "91-120", "121+"] }],
            usedcar: [
                        { reportId: 18, ChartObject: ["0-30", "31-60", "61-90", "91-120", "121+"] },
                        { reportId: 19, ChartObject: ["0-30", "31-60", "61-90", "91-120", "121+"] },
                        { reportId: 36, ChartObject: ["0-30", "31-60", "61-90", "91-120", "121+"] },
                        { reportId: 99, ChartObject: ["0-30", "31-60", "61-90", "91-120", "121+"] },
                        { reportId: 95, ChartObject: ["0-30", "31-60", "61-90", "91-120", "121+"] },
                        { reportId: 97, ChartObject: ["0-30", "31-60", "61-90", "91-120", "121+"] }],
            variable: [
                        { reportId: 18, ChartObject: ["0-30", "31-60", "61-90", "91-120", "121+"] },
                        { reportId: 19, ChartObject: ["0-30", "31-60", "61-90", "91-120", "121+"] }],
            dealership: [
                       { reportId: 36, ChartObject: ["0-30", "31-60", "61-90", "91-120", "121+"] }]
        };
        var tabConfig;

        ctrl.$onInit = function () {
            $scope.globalFilter = ($rootScope.defaultGlobalFilter ? $rootScope.defaultGlobalFilter : {});
            $scope.reportFilterList = [];
            $scope.smallMultipleDataItems = "$";
            $scope.dataTableItems = {};
            $scope.dtListItem = [];
            $scope.kpiDataItems = []; //initialise 6 gauges 
            $scope.showItemList = true;
            $scope._iframeUrl = "#";
            $scope.dataTableConfig = { ShowLoader: true };
            $scope.aggregatePeriod = "YTD";
            $scope.showPriorPeriod = true;
            $scope.uereportFilterValue = "ALL";
            $scope.KpiCollapsed = "";
            $scope.ToggleCollapse = function (Obj) {
                if ($scope[Obj] == "") {
                    $scope[Obj] = "collapsedDiv";
                }
                else {
                    $scope[Obj] = "";
                }
            }
            $scope.kpiConfig = { ShowLoader: true, gauges: [] };
            $scope.smallMultipleConfig = { ShowLoader: true, bars: [] };
            $scope.ReportList = [];

            var unsubscribe = $ngRedux.connect(ctrl.mapStateToThis, dwActions)(ctrl);
            $scope.$on('$destroy', unsubscribe);
        }

        ctrl.mapStateToThis = (state) => {
            return {
              tabs: state.tabs
            };
        }

        ctrl.$postLink = () => {
           // ctrl.fetchTabConfig($attrs.departmentCode);
            //return;
            tabsMetaDataService.getTabObject({ key: $attrs.type, departmentCode: $attrs.departmentCode }).then(function (tabConf) {
                tabConfig = tabConf;
                $scope.defaultReport = tabConfig.getDefaultReport();
                $scope.reportFilterValue = $scope.defaultReport.defaultFilter;

                if ($scope.defaultReport && $scope.defaultReport.filterValues != null) {
                    var filterValuesArray = $scope.defaultReport.filterValues.split('|');
                    $scope.reportFilterList = _.map(filterValuesArray, function (d) { return { value: d, color: "" } });
                }
                $scope.operationType = tabConfig.name;
                $scope.ReportList = [$scope.defaultReport];
                $scope.kpiConfig = { ShowLoader: true, gauges: tabConfig.getGaugeMetadata().config };
                $scope.smallMultipleConfig = { ShowLoader: true, bars: tabConfig.getBarsMetadata().config };
                $scope.otherReports = _.sortBy(tabConfig.reports, function (d) { return d.reportName; });//tabConfig.reports;

                if ($attrs.type == "usedcar" || $attrs.type == "inventory") {
                    var popobj = {
                        reportId: -1, reportName: "Used Car Equity Detail", reportType: "usedEquityTable",
                        url: "api/getusedcarequitydetail", isActive: true,
                        uefilterValues: [{ value: "ALL", Color: "#337ab7" }, { value: "G", Color: "#4c9457" },
                            { value: "Y", Color: "#F4C549" }, { value: "R", Color: "#f53d50" }],
                        uedefaultfilterValue: "ALL"
                    };
                    $scope.otherReports[$scope.otherReports.length] = popobj;
                }

                dealerService.getCurrentDealer().then(function (dealer) {
                    var pageFilter = ($rootScope.defaultGlobalFilter ? $rootScope.defaultGlobalFilter : applicationObject.getDefaultPageFilter());
                    pageFilter.dealerKey = dealer.Id;
                    $scope.globalFilter = pageFilter;
                    $rootScope.defaultGlobalFilter = $scope.globalFilter;
                    $scope.showPriorPeriod = true;
                    $scope.aggregatePeriod = "YTD";
                    bindWidgets();
                });

            });
        }

        function bindWidgets() {
            ctrl.ShowLoaderForKPI = true;
            getKpidata();
            ctrl.ShowLoaderForSM = true;
            getMultipleBarChartData();
            ctrl.ShowLoaderForReport = true;
            getDataTabledata($scope.ReportList[0], function (data) { $scope.dataTableItems = data.data; ctrl.ShowLoaderForReport = false; });
        }

        $scope.filterSelectionForMonth = function (obj) {
            $scope.globalFilter = {
                dealerKey: $scope.globalFilter.dealerKey,
                startDate: moment(obj.selectedDate).startOf('month'),
                endDate: moment(obj.selectedDate).endOf('month')
            };
            $rootScope.defaultGlobalFilter = $scope.globalFilter;
            $scope.aggregatePeriod = "MTD";
            $scope.showPriorPeriod = false;
            $scope.Showloader = true;
            bindWidgets();
        }

        $scope.reload = function (data) {
            $scope.globalFilter = {
                dealerKey: data.dealerKey,
                startDate: data.startDate,
                endDate: data.endDate,
            };
            $rootScope.defaultGlobalFilter = $scope.globalFilter;
            $scope.Showloader = true;
            bindWidgets();
        }

        $scope.resetPageFilter = function () {
            $scope.kpiConfig = { ShowLoader: true, gauges: tabConfig.getGaugeMetadata().config };
            $scope.smallMultipleConfig = { ShowLoader: true, bars: tabConfig.getBarsMetadata().config };
            $scope.dataTableConfig = { ShowLoader: true };
            var defaultFilter = applicationObject.getDefaultPageFilter();
            defaultFilter.dealerKey = dealerService.currentDealer.Id;
            $scope.ReportList = [$scope.defaultReport];
            $scope.globalFilter = defaultFilter;
            $rootScope.defaultGlobalFilter = $scope.globalFilter;
            $scope.aggregatePeriod = "YTD";
            $scope.showPriorPeriod = true;
            $scope.Showloader = true;
            bindWidgets();
        }

        $scope.reportFilterChange = function (arg) {
            $scope.dataTableConfig = { ShowLoader: true };
            $scope["reportFilterValue"] = arg.Value;
            $scope.ReportList[$scope.ReportList.length - 1].defaultFilter = arg.Value;
            $scope.ReportList[$scope.ReportList.length - 1].filterButtonChange = true;
            getDataTabledata($scope.ReportList[$scope.ReportList.length - 1], function (data) { $scope.dataTableItems = data.data; });
        }

        $scope.reportUEFilterChange = function (arg) {
            if (arg.Value !== $scope.ReportList[$scope.ReportList.length - 1].defaultFilter) {
                $scope["dataTableConfig"] = { ShowLoader: true };

                $timeout(testRun, 30);

                function testRun() {
                    $scope.ReportList[$scope.ReportList.length - 1].defaultFilter = arg.Value;
                    $scope.ReportList[$scope.ReportList.length - 1].filterButtonChange = true;
                    $scope["uereportFilterValue"] = arg.Value;
                }

            }
        }

        $scope.downloadFile = function (partName) {
            startDownload(partName);
        };

        $scope.print = function (elem) {
            $window.print();
        };

        $scope.fullScreenWindow = function (elem) {
            var windowWidth = $(window).width() - 30;
            var contentHeight = $("#" + elem).siblings(".panel-heading").parent().height();
            var windowHeight = $(window).height();
            var dialogHeight = 0;
            if (windowHeight < contentHeight)
                dialogHeight = windowHeight;
            else
                dialogHeight = contentHeight;

            var dialogOptions = {
                "title": "",
                "width": windowWidth,
                "height": dialogHeight,
                "modal": true,
                "resizable": true,
                "draggable": true,
                "close": function () {
                    $(this).remove();
                }
            };

            var dialogExtendOptions = {
                "closable": true,
                "maximizable": false,
                "minimizable": false,
                "minimizeLocation": "right",
                "collapsable": false,
                "dblclick": "collapse",
                "titlebar": "transparent"
            };

            var htmlToOpen = "";
            htmlToOpen = $("#" + elem).html();
            $('<div/>').html(htmlToOpen).dialog(dialogOptions).dialogExtend(dialogExtendOptions).dialogExtend({
                "load": function (evt, dlg) {
                    $(".ui-dialog").css("zIndex", '2147483647');
                },
                "maximize": function (evt) {
                    $(".ui-dialog").css("zIndex", '2147483647');
                },
                "minimize": function (evt) {
                    $("#dialog-extend-fixed-container").css("zIndex", '2147483647');
                }
            });
        };

        function startDownload(partName) {
            //$scope.downloadFile = function (partName) {
            var currentReport = $scope.ReportList[$scope.ReportList.length - 1];
            if (currentReport.reportId == -1) {
                var reportParam = prepareReportApiParametersForUsedCarEquity(currentReport);
                reportParam.access_token = sessionStorage.user;
                reportParam.bookvalue = $scope.bookedvalue;
                reportParam.percent = $scope.percentvalue;

                reportParam.filtervalue = currentReport.defaultFilter == undefined ? "ALL" : currentReport.defaultFilter;


                var apiUrlWith = [$rootScope.baseUrl + '/api/getusedcarequitydownload', ObjectToUrlParams(reportParam)];
                //Create Generic URL Path for all parts
                var httpPath = apiUrlWith.join('?');

                //Send Request
                var a = document.createElement("a");
                document.body.appendChild(a);
                a.href = httpPath;
                //a.download = fileName;
                a.click();
                document.body.removeChild(a);

            }
            else {
                var currentReport = $scope.ReportList[$scope.ReportList.length - 1],
                reportParam = prepareReportApiParameters(currentReport);

                reportParam.access_token = sessionStorage.user;
                reportParam.partName = partName;
                reportParam.type = $attrs.type;
                reportParam.showPreviodPeriod = $scope.showPriorPeriod;
                reportParam.periodType = ($scope.aggregatePeriod == "YTD") ? "Y" : "M";


                var apiUrlWith = [$rootScope.baseUrl + '/api/getcsvdownload', ObjectToUrlParams(reportParam)];


                //Create Generic URL Path for all parts
                var httpPath = apiUrlWith.join('?');

                //Send Request
                var a = document.createElement("a");
                document.body.appendChild(a);
                a.href = httpPath;
                //a.download = fileName;
                a.click();
                document.body.removeChild(a);
            }
            //};
        }

        function prepareReportApiParameters(reportObj) {

            var operationFilterType = "", operationFilterValue = "";
            if ($scope.ReportList[$scope.ReportList.length - 1].filterButtonChange) {
                operationFilterType = ((reportObj.filterType == null) ? "" : reportObj.filterType);
                operationFilterValue = $scope.reportFilterValue;
            }
            else {
                if (reportObj.isParentLinkDetailsReq) {
                    operationFilterType = ((reportObj.parentFilterType == null) ? "" : reportObj.parentFilterType);
                    operationFilterValue = reportObj.parentFilterValue;
                }
                else {
                    operationFilterType = ((reportObj.filterType == null) ? "" : reportObj.filterType);
                    operationFilterValue = $scope.reportFilterValue;
                }
            }

            return {
                dealer: $scope.globalFilter.dealerKey,
                startdate: moment($scope.globalFilter.startDate).format('DD-MM-YYYY'),
                enddate: moment($scope.globalFilter.endDate).format('DD-MM-YYYY'),
                reportID: reportObj.reportId,
                operationFilterType: operationFilterType,
                operationFilterValue: operationFilterValue,
                filters: ((reportObj.filterData == 'undefined' || reportObj.filterData == undefined) ? "" : reportObj.filterData)
            };
        }
        function prepareReportApiParametersForUsedCarEquity(reportObj) {
            return {
                dealerkey: $scope.globalFilter.dealerKey,
                startdate: moment($scope.globalFilter.startDate).format('DD-MM-YYYY'),
                enddate: moment($scope.globalFilter.endDate).format('DD-MM-YYYY'),
            };
        }

        function ObjectToUrlParams(obj) {
            var p = [];
            for (var key in obj) {
                p.push(key + '=' + encodeURIComponent(obj[key]));
            }
            return p.join('&');
        }

        function getMultipleBarChartData() {
            var barChartApiParam = {
                type: $attrs.type,
                dealer: $scope.globalFilter.dealerKey,
                startdate: moment($scope.globalFilter.startDate).format('DD-MM-YYYY'),
                enddate: moment($scope.globalFilter.endDate).format('DD-MM-YYYY'),
                periodType: ($scope.aggregatePeriod == "YTD") ? "Y" : "M",
                showPreviodPeriod: $scope.showPriorPeriod
            };

            var barChartApiUrl = [tabConfig.getBarsMetadata().url, ObjectToUrlParams(barChartApiParam)];

            apiService.get(barChartApiUrl.join('?'), null, function (data) {
                $scope.smallMultipleDataItems = data.data; ctrl.ShowLoaderForSM = false;
            }, function (errorMsg) {
                notificationService.error("error occured while fetching records from the server.");
            });
            $scope.Showloader = false;
        }

        var buttonGroupColorForNew = { "0-30": "bg-green", "31-59": "bg-yellow", "60+": "bg-red", "All": "bg-grey" };
        var buttonGroupColorForUsed = { "0-20": "bg-green", "21-44": "bg-yellow", "45+": "bg-red", "All": "bg-grey" };

        function setReportFilterValues(filterArrayData) {
            if (filterArrayData.filterValues != null) {
                var filterValuesArray = filterArrayData.filterValues.split('|');
                var filterValuesRes = [];
                if (filterArrayData.reportId == "4" || filterArrayData.reportId == "44") {
                    $scope.reportFilterList = _.map(filterValuesArray, function (d) { return { value: d, color: buttonGroupColorForNew[d] } });
                }
                else if (filterArrayData.reportId == "34") {
                    $scope.reportFilterList = _.map(filterValuesArray, function (d) { return { value: d, color: buttonGroupColorForUsed[d] } });
                }
                else {

                    $scope.reportFilterList = _.map(filterValuesArray, function (d) {
                        if (d == $scope.reportFilterValue)
                            return { value: d, color: "btn-primary" }
                        else {
                            return { value: d, color: "btndisabled" }
                        }
                    });
                }
            }
            else {
                $scope.reportFilterList = [];
            }
        }

        function getDataTabledata(reportObj, cb_success) {
            if (reportObj.url && reportObj.url != "") {
                var reportApiUrlAndQueryStringParams = [reportObj.url, ObjectToUrlParams(prepareReportApiParametersForUsedCarEquity(reportObj))];
            }
            else {
                var reportApiUrlAndQueryStringParams = [tabConfig.getTableMetadata().url, ObjectToUrlParams(prepareReportApiParameters(reportObj))];
            }

            if (reportObj.reportType == "usedEquityTable" && reportObj.uefilterValues != null) {
                $scope.uereportFilterList = reportObj.uefilterValues;
                //$scope.uereportFilterValue = reportObj.uedefaultfilterValue;
            }
            setReportFilterValues(reportObj);

            apiService.get(reportApiUrlAndQueryStringParams.join('?'), null, cb_success, function (errorMsg) {

                if (errorMsg.data.StatusCode == 50002 && errorMsg.data.StatusMessage.indexOf("Records Not Found") != -1) {

                    $scope.dataTableConfig = { ShowLoader: false };
                    ctrl.ShowLoaderForReport = false;

                    if ($scope.ReportList.length == 1) {
                        $scope.ReportList = [reportObj];
                    }
                    else {
                        $scope.ReportList[$scope.ReportList.length] = $scope.ReportList;
                    }

                    $scope.dataTableItems = {
                        aggregates: [],
                        columns: [{
                            Columndatatype: "Varchar",
                            DataType: "String",
                            IsHidden: false,
                            IsLink: false,
                            IsLinkOnHeader: false,
                            NextReportName: "",
                            ReportFilters: "",
                            ReportRefKey: -1,
                            ReportType: "Table",
                            UrlName: null,
                            UrlPath: null,
                            UrlRefKey: 0,
                            UserFriendlyName: "",
                            data: "",
                            ismeasure: true,
                            reportname: "",
                            title: ""
                        }],
                        data: []
                    };




                }
                else {
                    notificationService.error("error occured while fetching records from the server.");
                }
            });
        }

        function getKpidata() {

            var kpiChartApiParam = {
                type: $attrs.type,
                dealer: $scope.globalFilter.dealerKey,
                startdate: moment($scope.globalFilter.startDate).format('DD-MM-YYYY'),
                enddate: moment($scope.globalFilter.endDate).format('DD-MM-YYYY')
            };

            var gaugesMetaData = tabConfig.getGaugeMetadata();
            var kpiChartApiUrl = [gaugesMetaData.url, ObjectToUrlParams(kpiChartApiParam)];

            apiService.get(kpiChartApiUrl.join('?'), null, function (data) {
                //console.log("kpi data", data);
                let serviceData = data.data[0];
                let guagesData = gaugesMetaData.config.map((o) => Object.assign({}, serviceData[o.key], o));

                $scope.kpiDataItems = guagesData;
                ctrl.ShowLoaderForKPI = false;
            }, function (errorMsg, resp) {

                if (errorMsg.data.StatusCode == 50002 && errorMsg.data.StatusMessage.indexOf("Records Not Found") != -1) {
                    $scope.Showloader = false;
                    ctrl.ShowLoaderForKPI = false;

                   // $scope.kpiDataItems = [{}, {}, {}, {}, {}, {}];
                }
                else {
                    notificationService.error("error occured while fetching records from the server.");
                }
            });

        }

        $scope.aggregatePeriodChange = function (d) {
            $scope.smallMultipleConfig = { ShowLoader: true, bars: tabConfig.getBarsMetadata().config };
            $scope.Showloader = true;
            $scope.aggregatePeriod = d;
            getMultipleBarChartData()
        }

        $scope.InvokePriorPeriod = function (d) {
            $scope.smallMultipleConfig = { ShowLoader: true, bars: tabConfig.getBarsMetadata().config };
            $scope.showPriorPeriod = d;
            getMultipleBarChartData()
        }

        $scope.invokeKpiReport = function (d) {
            $scope.dataTableConfig = { ShowLoader: true };
            var calledReport = angular.copy(tabConfig.getReportByReportId(d.report.reportId));
            angular.extend(calledReport, d.report);

            if (calledReport.defaultFilter != undefined)
                $scope.reportFilterValue = calledReport.defaultFilter;
            else
                $scope.reportFilterValue = "";

            getDataTabledata(calledReport, function (data) {
                $scope.ReportList = [calledReport];
                if (calledReport.filterValues != undefined) {
                    setReportFilterValues(calledReport);
                }
                $scope.dataTableItems = data.data;

                userActivityService.reportSelectionLogger({
                    calledReport: calledReport,
                    intializeFrom: "Invoke From Kpi"
                });
            });
        }

        $scope.invokeBarReport = function (d) {
            $scope.dataTableConfig = { ShowLoader: true };
            var calledReport = angular.copy(tabConfig.getReportByReportId(d.report.reportId));
            angular.extend(calledReport, d.report);
            //var rptObj = tabConfig.getReportByReportId(d.reportId);
            //var calledReport = ((rptObj) ? rptObj : d);
            if (calledReport.defaultFilter != undefined)
                $scope.reportFilterValue = calledReport.defaultFilter;
            else
                $scope.reportFilterValue = "";

            getDataTabledata(calledReport, function (data) {
                $scope.ReportList = [calledReport];
                if (calledReport.filterValues != undefined) {
                    setReportFilterValues(calledReport);
                }
                $scope.dataTableItems = data.data;
                //calledReport.reportName = data.data.columns[0].reportname;
                //calledReport.reportType = "Table";
                //$scope.ReportList = [calledReport];

                userActivityService.reportSelectionLogger({
                    calledReport: calledReport,
                    intializeFrom: "Invoke From SM"
                });
            });
        }

        $scope.closePopup = function () {
            $scope.showpopup = false;
        }

        var check = function (d) {
            if ($scope.ReportList.length > 1) {
                    
                if ($scope.ReportList[$scope.ReportList.length - 2].reportId == 6) {
                    if (d.reportId == 110) {
                        var _ReportObj = d;
                        if ($scope.ReportList[$scope.ReportList.length - 2].defaultFilter == 'New') {
                            _ReportObj = _.find($scope.otherReports, function (f) {
                                return f.reportId == 4;
                            });
                        }
                        if ($scope.ReportList[$scope.ReportList.length - 2].defaultFilter == 'Used') {
                            _ReportObj = _.find($scope.otherReports, function (f) {
                                return f.reportId == 34;
                            });

                        }
                        if ($scope.ReportList[$scope.ReportList.length - 2].defaultFilter == 'All') {
                            _ReportObj = _.find($scope.otherReports, function (f) {
                                return f.reportId == 110;
                            });
                        }
                        return angular.extend(_ReportObj, { isParentLinkDetailsReq: true });
                    }
                    else if (d.reportId == 1) {
                        var _ReportObj = d;
                        if ($scope.ReportList[$scope.ReportList.length - 2].defaultFilter == 'New') {
                            _ReportObj = _.find($scope.otherReports, function (f) {
                                return f.reportId == 1;
                            });
                        }
                        if ($scope.ReportList[$scope.ReportList.length - 2].defaultFilter == 'Used') {
                            _ReportObj = _.find($scope.otherReports, function (f) {
                                return f.reportId == 23;
                            });

                        }
                        if ($scope.ReportList[$scope.ReportList.length - 2].defaultFilter == 'All') {
                            _ReportObj = _.find($scope.otherReports, function (f) {
                                return f.reportId == 119;
                            });
                        }
                        return angular.extend(_ReportObj, { isParentLinkDetailsReq: true });
                    }
                    else {
                        return d;
                    }
                }
            }
            return d;
        }

        $scope.invokeReport = function (d) {

            d = check(d);
            $scope.ReportList[$scope.ReportList.length - 1] = d;
            var _filtersReportObj = _.find($scope.otherReports, function (f) {
                return f.reportId == d.reportId;
            });


            if (_filtersReportObj !== undefined) {
                d.defaultFilter = d.defaultFilter == undefined ? _filtersReportObj.defaultFilter : d.defaultFilter;
                d.filterType = _filtersReportObj.filterType;
                d.filterValues = _filtersReportObj.filterValues;
            }

            if ($scope.ReportList.length > 1) {
                if (d.isParentLinkDetailsReq) {
                    angular.extend(d, { parentFilterType: $scope.ReportList[$scope.ReportList.length - 2].filterType, parentFilterValue: $scope.ReportList[$scope.ReportList.length - 2].defaultFilter });
                }
                else {
                    angular.extend(d, { parentFilterType: "", parentFilterValue: "" });
                }
            }
            else {
                angular.extend(d, { parentFilterType: "", parentFilterValue: "" });
            }

            if ($scope.ReportList.length == 1) {
                $scope.reportFilterValue = d.defaultFilter;
            }
            else if (d.defaultFilter) {
                if (d.isParentLinkDetailsReq && $scope.ReportList[$scope.ReportList.length - 2].filterType == d.filterType) {
                    $scope.reportFilterValue = $scope.ReportList[$scope.ReportList.length - 2].defaultFilter;
                }
                else {
                    $scope.reportFilterValue = d.defaultFilter;
                }
            }

            if (d.reportType == "GroupBarChart") {
                var _chartObj = _.find(ageingReportObject[$attrs.type], function (f) {
                    return f.reportId == d.reportId
                });
                if (_chartObj)
                    angular.extend($scope.ReportList[$scope.ReportList.length - 1], _chartObj);

                getDataTabledata(d, function (data) {
                    $scope.dataTableItems = data.data;
                });
            }
            else if (d.reportType == "ExternalLink") {
                $scope.path = d.Path;
                $scope.title = d.reportName;
                $scope.showpopup = true;
                $scope.$apply();
            }
            else if (d.reportType == "BarChart") {
                var _chartObj = _.find(ageingReportObject[$attrs.type], function (f) {
                    return f.reportId == d.reportId
                });
                if (_chartObj)
                    angular.extend($scope.ReportList[$scope.ReportList.length - 1], _chartObj);

                getDataTabledata(d, function (data) {
                    $scope.dataTableItems = data.data;
                });
            }
            else if (d.reportType == "Table" || d.reportType == "usedEquityTable") {
                getDataTabledata(d, function (data) {
                    $scope.dataTableItems = data.data;

                    userActivityService.reportSelectionLogger({
                        calledReport: calledReport,
                        intializeFrom: "Invoke from Breadcrumb/Link Reports"
                    });
                });
            }
            //else if (d.reportType == "usedEquityTable") {
            //    getDataTabledata(d, function (data) {
            //        $scope.dataTableItems = data.data;
            //    });
            //}
        }

        $scope.invokeOtherReport = function (d) {
            if (d.reportType != "usedEquityTable") {
                $scope.uereportFilterList = [];
            }

            if (d.reportType == "ExternalLink") {
                $scope.path = d.Path;
                $scope.title = d.reportName;
                $scope.showpopup = true;
            }
            else {
                $scope.dataTableConfig = { ShowLoader: true };
                var calledReport = tabConfig.getReportByReportId(d.key);
                if (!calledReport) calledReport = d;

                if (calledReport.reportType == "GroupBarChart") {
                    var _chartObj = _.find(ageingReportObject[$attrs.type], function (f) {
                        return f.reportId == calledReport.reportId
                    });
                    if (_chartObj)
                        angular.extend(calledReport, _chartObj);
                }
                else if (calledReport.reportType == "BarChart") {
                    var _chartObj = _.find(ageingReportObject[$attrs.type], function (f) {
                        return f.reportId == calledReport.reportId
                    });
                    if (_chartObj)
                        angular.extend(calledReport, _chartObj);
                }
                $scope.reportFilterValue = calledReport.defaultFilter;
                getDataTabledata(calledReport, function (data) {
                    $scope.ReportList = [calledReport];
                    $scope.dataTableItems = data.data;

                    userActivityService.reportSelectionLogger({
                        calledReport: calledReport,
                        intializeFrom: "Invoke From Report Picker"
                    });
                });

            }
        }

        $scope.getUsedCarEquitycaptured = function (d) {
            $scope.bookedvalue = d.bv;
            $scope.percentvalue = d.bp;
        }
    }
