﻿(function (app) {

    app.component("multiComp", {
        templateUrl: "/Scripts/app/components/multicontainer/multicontainer.html",
        controller: ['$scope', '$element', '$attrs', multiContainerCtrl],
        bindings: {
            data: "<",
            wConfig: "<",
            reportList: "<",
            filterList: "<",
            uefilterList: "<",
            showItemList: "<",
            defaultTabFilter: "<",
            uedefaultTabFilter: "<",
            onListItemClick: "&",
            onFilterChange: "&",
            onUsedEquityFilterChange: "&",
            onUsedEquityCaptured: "&"
        }
    });

    function multiContainerCtrl($scope, $element, $attrs) {
        var ctrl = this;

        ctrl._showItemList = false;
        ctrl.ShowLoader = true;
        ctrl.CountByColor = {
            r: 0, g: 0, y: 0
        }

        $scope.uedefaultTabFilter = "ALL";

        var showElement = {
            table: false,
            bar: false,
            bubble: false,
            usedEquityTable: false,
            groupBarChart:false
        };

        ctrl.showElement = angular.copy(showElement);
        ctrl.$onInit = function () {
            ctrl.containerData = undefined;//ctrl.data;
            ctrl.reportList = ctrl.reportList;
            ctrl.ShowLoader = ctrl.wConfig.ShowLoader;
            ctrl.defaultTabFilter = ctrl.defaultTabFilter;
            $scope.uedefaultTabFilter = ctrl.uedefaultTabFilter;
        }

        ctrl.$onChanges = function (changedObj) {            
            if (changedObj.uedefaultTabFilter != undefined) {                
                $scope["uedefaultTabFilter"] = changedObj.uedefaultTabFilter.currentValue;
            }
            if (changedObj.data != undefined && ctrl.reportList.length > 0) {

                ctrl.showElement = angular.copy(showElement);
                if (ctrl.reportList[ctrl.reportList.length - 1].reportType == "BarChart") {
                    ctrl.showElement.bar = true;
                }
                else if (ctrl.reportList[ctrl.reportList.length - 1].reportType == "BubbleChart") {
                    ctrl.showElement.bubble = true;
                }
                else if (ctrl.reportList[ctrl.reportList.length - 1].reportType == "usedEquityTable") {
                    ctrl.showElement.usedEquityTable = true;
                }
                else if (ctrl.reportList[ctrl.reportList.length - 1].reportType == "GroupBarChart") {
                    ctrl.showElement.groupBarChart = true;
                }
                else {
                    ctrl.showElement.table = true;
                }

                ctrl.containerData = ctrl.data;
                ctrl.ShowLoader = false;
            }

            if (changedObj.reportList != undefined && ctrl.reportList.length > 0) {
                ctrl.showElement = angular.copy(showElement);
                if (ctrl.reportList[ctrl.reportList.length - 1].reportType == "BarChart") {
                    ctrl.showElement.bar = true;
                }
                else if (ctrl.reportList[ctrl.reportList.length - 1].reportType == "GroupBarChart") {
                    ctrl.showElement.groupBarChart = true;
                }
                else if (ctrl.reportList[ctrl.reportList.length - 1].reportType == "BubbleChart") {
                    ctrl.showElement.bubble = true;
                }
                else if (ctrl.reportList[ctrl.reportList.length - 1].reportType == "usedEquityTable") {
                    ctrl.showElement.usedEquityTable = true;
                }
                else {
                    ctrl.showElement.table = true;
                }
            }
        };

        ctrl.ReportItemClick = function (data) {            
            if (data.isParentLinkDetailsReq != undefined) {
                angular.extend(data, { isParentLinkDetailsReq: data.isParentLinkDetailsReq });
            }
            else {
                angular.extend(data, { isParentLinkDetailsReq: false });
            }
            if (data.reportType != "ExternalLink")
                ctrl.ShowLoader = true;

            ctrl.reportList = ctrl.reportList;
            
            ctrl.onListItemClick({ ctx: data });
        }


        ctrl.invokeReport = function (val) {
            angular.extend(val, { isParentLinkDetailsReq: true });

            if (val.reportType != "ExternalLink") {
                ctrl.ShowLoader = true;
                ctrl.reportList[ctrl.reportList.length] = val;
            }

            ctrl.onListItemClick({ ctx: val });
        }

        ctrl.filterChange = function (val) {
            ctrl.onFilterChange({ ctx: val });
        }

        ctrl.UsedEquityfilterChange = function (val) {
            ctrl.onUsedEquityFilterChange({ ctx: val });
        }

        ctrl.filterRecordsByColor = function (val) {
            ctrl.CountByColor = val;

        }
        ctrl.usedCarCapture = function (val) {
            ctrl.onUsedEquityCaptured({ ctx: val });

        }
    }

})(angular.module("dealerWizard"));