﻿angular.module('dealerWizard')
.component("mdxfiltertable", {
    transclude: true,
    controller: mdxFilterTableController,
    require: {
        datatableCtrl: '^datatable'
    },
    bindings: {
        wConfig: "<",
        dimensionData:"<"
    }
});



function mdxFilterTableController() {
    let ctrl = this;
    let columnData = {
        columns: [],
        data: []
    };
    
    let dimension = [];
    let dimensionHierarchy = {};

    ctrl.$onInit = () => {

        columnData = {
            columns: [{
                Columndatatype: "String", DataType: "String", IsLink: false, NextReportName: null, ReportFilters: "",
                ReportRefKey: 1, ReportType: null, UrlName: null, UrlPath: null, UrlRefKey: 0, data: "Dimension", ismeasure: false, reportname: "User Activity Log", title: "Dimension",ContextType:"selectForDimension"
            },
            {
                Columndatatype: "String", DataType: "String", IsLink: false, NextReportName: null, ReportFilters: "",
                ReportRefKey: -1, ReportType: null, UrlName: null, UrlPath: null, UrlRefKey: 0, data: "Hierarchy", ismeasure: false, reportname: "User Activity Log", title: "Dimension Hierarchy", ContextType: "selectForDimensionHierarchy"
            },
            {
                Columndatatype: "String", DataType: "String", IsLink: false, NextReportName: null, ReportFilters: "",
                ReportRefKey: -1, ReportType: null, UrlName: null, UrlPath: null, UrlRefKey: 0, data: "Operator", ismeasure: false, reportname: "User Activity Log", title: "Operator", ContextType: "selectForOperator"
            },
            {
                Columndatatype: "String", DataType: "String", IsLink: false, NextReportName: null, ReportFilters: "",
                ReportRefKey: -1, ReportType: null, UrlName: null, UrlPath: null, UrlRefKey: 0, data: "FilterExpression", ismeasure: false, reportname: "User Activity Log", title: "Filter expression",ContextType:"inputForExpression"
            },
            {
                Columndatatype: "barChart", DataType: "barChart", IsLink: false, NextReportName: null, ReportFilters: "",
                ReportRefKey: -1, ReportType: null, UrlName: null, UrlPath: null, UrlRefKey: 0, data: "diff", ismeasure: false, reportname: "User Activity Log", title: "Diff expression", ContextType: "barChart"
            }
            ],
            data: []
        };

        ctrl.datatableCtrl.extendDataTable({
            //"drawCallback": function (settings) {
            //    console.log('DataTables has redrawn the table', settings);
            //    var api = this.api();

            //    // Output the data for the visible rows to the browser's console
            //    console.log(api.rows[0]); 
            //},
            "rowCallback": function (row, data, index) {
                console.log(row, data);

            }
        });

        ctrl.datatableCtrl.setConfig(ctrl.wConfig);
        ctrl.datatableCtrl.setData(columnData);
        ctrl.datatableCtrl.renderTable();
        ctrl.datatableCtrl.stopLoader();

        _.each(ctrl.dimensionData, (o) => {
            if (o.contentType == "Dimension") {
                dimension.push(o.Name);
                dimensionHierarchy[o.Name] = [];
                _.each(o.children, (d) => {
                    dimensionHierarchy[o.Name].push(d.Name);
                });
            }
        });

         
    }

    let fiterType = ["Not Equal","In","Not In","Contains","Begins With","Range (Inclusive)","Range (Exclusive)","MDX"];

    let selectFilter = (data,fiterValue) => {
        let _html="<select style='width:100%' class='form-control'>"
        _.each(fiterValue, (d) => {
            _html += "<option " + ( (data==d)?'selected=true':'') + ">" + d + "</option>";
        });
        return _html + "</select>";

    };

    let max_val=12;
    let colorFromMiddle = (value, color1 = 'red', color2 = 'green') => {
        
        return ((isNaN(parseFloat(value)) || value < 0) ? 
        `linear-gradient(90deg, transparent, transparent ${(50 + value/max_val * 50)}%, ${color1} ${(50 + value/max_val * 50)}%,${color1} 50%, transparent 50%)`: 
        `linear-gradient(90deg, transparent, transparent 50%, ${color2} 50%, ${color2} ${(50 + value/max_val * 50)}%, transparent ${(50 + value/max_val * 50)}%)`);
    }

    let Columns = function (cc, data1, getValueFormatter, _linkedColumndefs, _linkedHeaderColumndefs, _isHdnColdefs) {
        var arr = [];
        for (var i = 0; i < cc; i++) {
            var colObj = {
                "targets": [i],
                render: function (data, type, full, meta) {
                    var fnFormatter = getValueFormatter(data1.columns[meta.col]);
                    if (data1.columns[meta.col].ContextType == "selectForOperator") {
                        return selectFilter(data, fiterType);
                    }
                    if (data1.columns[meta.col].ContextType == "selectForDimension") {
                        return selectFilter(data, dimension);
                    }
                    if (data1.columns[meta.col].ContextType == "selectForDimensionHierarchy") {                        
                        return selectFilter(data, dimensionHierarchy["Dealer"]);
                    }
                    if(data1.columns[meta.col].ContextType=="inputForExpression")
                    {
                    return "<input type='text' class='form-control'/>"
                    }
                    if(data1.columns[meta.col].ContextType= "barChart")
                    {
                        //=[{},{}]
                        let tempMax=d3.extent(columnData.data,(d)=>{
                            return d[data1.columns[meta.col].data];
                        });

                        max_val=(Math.abs(tempMax[0])>Math.abs(tempMax[1]))?Math.abs(tempMax[0]):Math.abs(tempMax[1]);

                        var $div=$("<div />");
                        $div.css("color","white").css("text-align","center").css("height","34px").html(data);
                        console.log($div.css("background",colorFromMiddle(data))[0]);
                        return  $div.css("background",colorFromMiddle(data))[0].outerHTML;                        
                    }
                    if (data1.columns[meta.col].IsLink == true) {
                        return '<span class="LinkedItem CustomLinks">' + fnFormatter(data) + '</span>';
                    }
                    return fnFormatter(data);
                }
            }

            if (data1.columns[i].IsLink) {
                _linkedColumndefs.push(i);
            }
            if (data1.columns[i].IsLinkOnHeader) {
                _linkedHeaderColumndefs.push(i);
            }
            if (data1.columns[i].IsHidden) {
                data1.columns[i].visible = false;
                _isHdnColdefs.push(i);
            }
            arr.push(colObj);
        }
        return arr;
    }

    ctrl.$postLink = () => {
        console.log(this.datatableCtrl);        
    }

    const handleDropEvent = (event, ui) => {
        if (ui.draggable.lastDropObject !== undefined) {
            ui.draggable.lastDropObject.droppable('enable');
        }

        $('.ui-draggable-helper').removeClass('ui-draggable-helper');

        let contentType = $(ui.draggable[0]).attr('data-contentType');
        if (contentType != "Measure") {
            let dimension = ($(ui.draggable[0]).attr('data-value'));
            var dim = _.find(columnData.data, (o) => {
                return o.Dimension == dimension;
            })
            console.log($(ui.draggable[0]).attr('data-value'));
            if (dim == undefined || dim=="") {
                columnData.data.push({ Dimension: dimension, Hierarchy: "", Operator: "Begins With", FilterExpression: "", diff : Math.floor(10 * Math.random())});

                

                ctrl.datatableCtrl.setColumnsDefn(Columns);
                ctrl.datatableCtrl.setData(columnData);
                ctrl.datatableCtrl.renderTable();
            }
        }
    }

    const getDroppableObject = () => {
        return {
            drop: handleDropEvent,
            tolerance: "touch",
        }
    }

    $('#mdxFilterContainer').droppable(getDroppableObject());
     
}