﻿
(function (app) {
    'use strict';

    app.component('usedEquityButtonGroup',
        {
            templateUrl: "/Scripts/app/components/usedEquityButtonGroup.html",
            controller: ['$scope', '$element', '$attrs', buttonGroupCtrl],
            bindings: {
                data: "<",
                defaultTabFilter: "<",
                onChange: "&",
                countByColor:"<"
            }
        });

    function buttonGroupCtrl($scope, $element, $attrs) {
        var ctrl = this;
        $scope.buttonList = [];

        ctrl.$onInit = function () {
        };

        ctrl.onButtonClick = function (obj) {
            angular.forEach($scope.buttonList, function (val) {
                if (obj.Id == val.Id) {
                    val.Selected = true;
                } else {
                    val.Selected = false;
                }
            });

            $scope.buttonList.IsClick = true;
            ctrl.onChange({ ctx: obj });
        }

        ctrl.$onChanges = function (changedObj) {
            if (ctrl.data && !$scope.buttonList.IsClick) {
                $scope.buttonList = [];
                var count = ctrl.countByColor;
                angular.forEach(ctrl.data, function (val, i) {
                    $scope.buttonList.push({
                        Id: "grpButton" + i,
                        Value: val.value,
                        Selected: ((val.value.toLowerCase() == ctrl.defaultTabFilter.toLowerCase()) ? true : false),
                        Color: val.Color,
                        Text: (val.value.toLowerCase() == "all") ? ("All (" + (count.g + count.r + count.y) + ")") : ("(" + count[val.value.toLowerCase()] + ")")
                    });
                });
            }
            if (ctrl.countByColor && !$scope.buttonList.IsClick) {
                $scope.buttonList = [];
                var count=ctrl.countByColor;
                angular.forEach(ctrl.data, function (val, i) {
                   
                    $scope.buttonList.push({
                        Id: "grpButton" + i,
                        Value: val.value,
                        Selected: ((val.value.toLowerCase() == ctrl.defaultTabFilter.toLowerCase()) ? true : false),
                        Color: val.Color,
                        Text: (val.value.toLowerCase() == "all") ? ("All (" + (count.g + count.r + count.y)+")") :("("+count[val.value.toLowerCase()]+")")

                    });
                });
            }
            $scope.buttonList.IsClick = false;
        }
    }
})(angular.module('dealerWizard'));;
