﻿(function (app) {
    'use strict';

    app.component('managedashboard',
     {
         templateUrl: "/Scripts/app/components/dashboard/managedashboard.html",
         controller: ['$scope', 'apiService', '$location', 'dashboardRepository', '$timeout', dashboardListCtrl]
     });

    function dashboardListCtrl($scope, apiService, $location, dashboardRepository, $timeout) {
        var ctrl = this;

        $scope.dashboardConfig = { ShowLoader: true };
        $scope.dashboardList = { columns: [], data: [] };


        //$scope.editReport = function (report) {
        //    $location.path("/reportconfigurator/" + report.bindObject.Report_Key);
        //    $scope.$apply();
        //}

        $scope.go = (path) => {
            $location.path(path);
        };

        $scope.editTemplate = (template) => {
            $location.path("/managedashboard/" + template.bindObject.id);
            $scope.$apply();
        }

        ctrl.$onInit = function () {
            

            $timeout(getDashboardList, 30);
        }

        let getDashboardList = () => {

            $scope.dashboardList = {
                columns: [{
                    Columndatatype: "Int",
                    DataType: "Int", IsLink: true, NextReportName: null, ReportFilters: "",
                    ReportRefKey: 1, ReportType: null, UrlName: null, UrlPath: null, UrlRefKey: 0, data: "id", ismeasure: false, reportname: "Dashboard", title: "Dashboard Key"
                },
				{
				    Columndatatype: "String",
				    DataType: "String", IsLink: false, NextReportName: null, ReportFilters: "",
				    ReportRefKey: -1, ReportType: null, UrlName: null, UrlPath: null, UrlRefKey: 0, data: "name", ismeasure: false, reportname: "Dashboard", title: "Dashboard Name"
				},
				{
				    Columndatatype: "Int",
				    DataType: "String", IsLink: false, NextReportName: null, ReportFilters: "",
				    ReportRefKey: -1, ReportType: null, UrlName: null, UrlPath: null, UrlRefKey: 0, data: "createdOn", ismeasure: false, reportname: "Dashboard", title: "Created On"
				}],
                data: dashboardRepository.getAll()
            };
        }

    }

})(angular.module('dealerWizard'));