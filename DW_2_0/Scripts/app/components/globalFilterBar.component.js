﻿(function (app) {
    'use strict';

    app.component('globalFilterBar',
        {
            templateUrl: "/Scripts/app/components/globalFilterBar.html",
            controller: ['$scope', '$element', '$attrs', '$log', globalFilterBarCtrl],
            bindings: {
                data: "<",
                onFilterChange: "&",
                onReset: "&"
            }
        });

    function globalFilterBarCtrl($scope, $element, $attrs, $log) {
        var ctrl = this;
        var _curDate = moment().subtract(1, 'd');
        $scope.ranges = {
            'This Month': [moment(_curDate).startOf('month'), _curDate],
            'Last Month': [(moment(_curDate).startOf('month').subtract(1, 'days')).startOf('month'), moment(_curDate).startOf('month').subtract(1, 'days')],
            'Last 90 Days': [moment(_curDate).subtract(90, 'days'), _curDate],
            'Last 180 Days': [moment(_curDate).subtract(180, 'days'), _curDate],
            'Calender YTD': [moment(_curDate).startOf('year'), _curDate]
        };

        var filterObj = {
            setStartDate: function (_date) {
                this.startDate = _date;
                return this;
            },
            setEndDate: function (_date) {
                this.endDate = _date;
                return this;
            }
        }

        ctrl.change = function (obj) {
            ctrl.onFilterChange({ ctx: obj });
        };

        ctrl.reset = function () {
            ctrl.onReset();
        };
              
        ctrl.$onInit = function () {
            var $datePicker = $('input[type=daterange]');

            $datePicker.on('apply.daterangepicker', function (ev, picker) {
                filterObj.setStartDate($scope.dates.startDate);
                filterObj.setEndDate($scope.dates.endDate);

                $datePicker.data('daterangepicker').setStartDate($scope.dates.startDate);
                $datePicker.data('daterangepicker').setEndDate($scope.dates.endDate);

                $(this).val(moment($scope.dates.startDate).format('MM/DD/YYYY') + ' - ' + moment($scope.dates.endDate).format('MM/DD/YYYY'));

                ctrl.onFilterChange({ ctx: filterObj });
            });

        };

        ctrl.$onChanges = function (changedObj) {
            $log.debug('global filter updated');
            $log.debug(ctrl.data);
            //Fix this for better null check 
            angular.extend(filterObj, ctrl.data);
            $scope.dates = {
                startDate: ctrl.data.startDate,
                endDate: ctrl.data.endDate
            }
            //$('input[type="daterange"]').trigger('apply.daterangepicker');
            $('input[type=daterange]').val(moment($scope.dates.startDate).format('MM/DD/YYYY') + ' - ' + moment($scope.dates.endDate).format('MM/DD/YYYY'));
        };
    }

})(angular.module('dealerWizard'));