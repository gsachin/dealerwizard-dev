﻿(function (app) {

    app.component("datatable", {
        templateUrl: "/Scripts/app/components/datatable/datatable.html",
        controller: ['$scope', '$element', '$attrs', 'valueFormatter', datatableCtrl],
        transclude: true,
        bindings: {
            data: "<",
            wConfig: "<",
            renderElement: "<",
            showItemList: "<",
            showSearch: "<",
            showPaging: "<",
            onListItemClick: "&"
        }
    });

    function datatableCtrl($scope, $element, $attrs, valueFormatter) {
        var datatable = null, $tblHeaderRow = [], $tblTotalHeaderRow = [];

        var ctrl = this, _data = {};
        $scope.ShowLoader = false;
        $scope._showItemList = angular.copy(ctrl.showItemList);
        ctrl.ColumnsDefn;

        ctrl.$onInit = function () {
            if (ctrl.wConfig) {
                $scope.ShowLoader = ctrl.wConfig.ShowLoader;
            }

            ctrl.showPaging = (ctrl.showPaging == undefined) ? true : ctrl.showPaging;
            ctrl.showSearch = (ctrl.showSearch == undefined) ? true: ctrl.showSearch;

        }

        ctrl.$onChanges = function (changedObj) {
            if (changedObj.data != undefined) {
                if (changedObj.data && !changedObj.data.isFirstChange()) {//Fix this for better null check 
                    _data = angular.copy(changedObj.data.currentValue);                    
                    if (ctrl.renderElement) {
                        getVehicleData();
                    }
                    ctrl.wConfig.ShowLoader = false;
                    $scope.ShowLoader = false;
                }
            }

            if (changedObj.wConfig != undefined) {
                if (!changedObj.wConfig.isFirstChange()) { 
                    $scope.ShowLoader = ctrl.wConfig.ShowLoader;
                }
            }

            if (changedObj.renderElement != undefined) {
                if (ctrl.renderElement) {
                    _data = angular.copy(ctrl.data); 
                    if (ctrl.renderElement) { 
                    }
                    //ctrl.wConfig.ShowLoader = false;
                    $scope.ShowLoader = false;
                }
            }

            if (changedObj.showItemList != undefined) {
                if (!changedObj.showItemList.isFirstChange()) {
                    $scope._showItemList = angular.copy(ctrl.showItemList);
                }
            }
        };

        ctrl.setItemList = (itemList) => {
            $scope._showItemList = angular.copy(itemList);
        }

        ctrl.stopLoader = () => {
            $scope.ShowLoader = false;
        }

        ctrl.startLoader = () => {
            $scope.ShowLoader = true;
        }

        ctrl.setData = (data) => {
            ctrl.data = data;
            _data = angular.copy(ctrl.data);
            //if (ctrl.renderElement) {
            //    getVehicleData();
            //}
        }

        ctrl.setColumnsDefn = (fnColumns) => {
            ctrl.ColumnsDefn = angular.copy(fnColumns);
        }

        ctrl.renderTable = () => {
            ctrl.renderElement = true;
            _data = angular.copy(ctrl.data);
            getVehicleData();
        }

        ctrl.setConfig = (config) => {
            $scope.ShowLoader = config.ShowLoader;
        }

        ctrl.$onDestroy = function () {
            //$(window).off('resize');     
            if (datatable != null) {
                datatable.off('responsive-resize');
            }
        }

        $scope.ReportItemClick = function (data) {
            ctrl.onListItemClick({ ctx: data });
        }

        $scope.ReportItemPopUpClick = function (data) {
            ctrl.onListItemClick({ ctx: data });
        }

        var linkHandler = function (elem, e) {
            var element = this;
            if ($(this).is('span')) {
                element = $(this).parent('td')[0];
            }

            if (element == undefined) {
                return;
            }

            var _reportType = "Table",
			_urlpath = "",
			filterString = "",
			_rptName = "",
			_id = element.attributes['data-report'].value,
			_filters = element.attributes['data-ReportFilters'],
			_index = element.attributes['data-index'];

            if (element.attributes['data-reportName'] != undefined) {
                _rptName = element.attributes['data-reportName'].value;
            }

            if (element.attributes['data-reportType'] != undefined) {
                _reportType = element.attributes['data-reportType'].value;
            }

            if (_filters) {
                _filters = _filters.value.split('|');
            }

            if (_index) {
                _filters.forEach(function (item) {
                    if (_data.data[_index.value] && item != "") {
                        var filterVal = "";
                        //if filter column data type is date then send formated date required by backend procedure
                        if (_.find(_data.columns, function (d) { return d.data == item; }).DataType == "Date") {
                            filterVal = valueFormatter.getformatter('date')(_data.data[_index.value][item], 'yyyy-MM-dd');
                        }
                        else {
                            filterVal = _data.data[_index.value][item];
                        }
                        filterString += filterVal + "|";
                    }
                });
                filterString = filterString.substr(0, filterString.length - 1);
            }
            //Required For PopUp
            if (_id == -1) {
                if (element.attributes['data-urlname'] != undefined)
                    _rptName = element.attributes['data-urlname'].value;
                if (element.attributes['data-urlpath'] != undefined) {
                    _reportType = "ExternalLink";
                    var pathArray = element.attributes['data-urlpath'].value.split('<');
                    _urlpath = ""; pathArray.forEach(function (val) { (val.includes('>') ? _urlpath += _data.data[_index.value][val.replace('>', '')] : _urlpath += val) })
                }
                if (element.attributes['data-urlrefkey'] != undefined)
                    _id = element.attributes['data-urlrefkey'].value;
            }

            var _Obj = {
                reportName: _rptName,
                reportId: _id,
                filterData: filterString,
                reportType: _reportType,
                Path: _urlpath
            };

            _Obj.bindObject = _data.data[_index.value];
            ctrl.onListItemClick({
                ctx: _Obj
            });
        }

        var sumTotalRowCreated = false;
        var $tblHeaderRows;
        var $tblTotalHeaderRows;

        function getVehicleData() {

            if (!angular.equals(_data, {})) {
                var _tableDiv = $element[0].querySelector('#data-table');
                if (datatable != null) {
                    datatable.destroy();
                    $(_tableDiv).empty();
                    $(_tableDiv).find('.LinkedItem').off("click", linkHandler);
                    sumTotalRowCreated = false;
                    datatable.off('responsive-resize');
                    $(_tableDiv).off("click.dtr");
                }


                $(_tableDiv).find('.LinkedItem').off("click", linkHandler);

                function addLinkFunctionality() {
                    $(_tableDiv).find('.LinkedItem').off("click");
                    $(_tableDiv).find('.LinkedItem').on("click", linkHandler);
                }

                $(_tableDiv).one('init.dt', function (a, b, c) {
                    $(_tableDiv).off('draw.dt');
                    $(_tableDiv).off('page.dt');
                    $(_tableDiv).find('.LinkedItem').off("click", linkHandler);

                    addLinkFunctionality();

                    $(_tableDiv).on('page.dt', function (a, b, c) {
                        $(_tableDiv).off('draw.dt');
                        $(_tableDiv).on('draw.dt', function () {
                            $(_tableDiv).find('.LinkedItem').off("click", linkHandler);
                            addLinkFunctionality();

                            _.each($tblHeaderRows, function (head, i) {
                                $tblTotalHeaderRows[i].style.display = head.style.display;
                            });
                        });
                    });

                    $(_tableDiv).on('order.dt', function (a, b, c) {
                        $(_tableDiv).off('draw.dt');
                        $(_tableDiv).on('draw.dt', function () {
                            $(_tableDiv).find('.LinkedItem').off("click", linkHandler);
                            addLinkFunctionality();

                            _.each($tblHeaderRows, function (head, i) {
                                $tblTotalHeaderRows[i].style.display = head.style.display;
                            });
                        });
                    });

                    $(_tableDiv).on('search.dt', function (a, b, c) {
                        $(_tableDiv).off('draw.dt');
                        $(_tableDiv).on('draw.dt', function () {
                            $(_tableDiv).find('.LinkedItem').off("click", linkHandler);
                            addLinkFunctionality();

                            _.each($tblHeaderRows, function (head, i) {
                                $tblTotalHeaderRows[i].style.display = head.style.display;
                            });
                        });
                    });

                    $(_tableDiv).one('destroy.dt', function () {
                        $(_tableDiv).find('.LinkedItem').off("click", linkHandler);
                    });
                });

                $(_tableDiv).on('responsive-display.dt', function (e, datatable, row, showHide, update) {
                    
                    addLinkFunctionality();
                });

                datatable = $(_tableDiv).DataTable(getDataTableObject(_data, _tableDiv));

                //first time handling of total header responsiveness
                _.each($tblHeaderRows, function (head, i) {
                    $tblTotalHeaderRows[i].style.display = head.style.display;
                });

                var headerRowTotalHandler=function (e) {
                    
                    var $selectedRow = $(e.target.parentElement);
                    
                    if ($selectedRow.hasClass("parent")) { 
                        $selectedRow.removeClass("parent");
                        $selectedRow.next().remove();
                    }
                    else { 
                        var _childRow = '<tr class="child"><td class="child" colspan="' + $tblHeaderRows.length + '"><table>';
                        _.each($tblHeaderRows, function (head, i) {
                            if (head.style.display == "none") {
                                _childRow += '<tr><td><span class="dtr-title" >' + head.innerText + ':' + '</span></td> ' + '<td><span>' + $tblTotalHeaderRows[i].innerText + '</span></td>' + '</tr>';
                            }
                        });

                        _childRow += '</table></td></tr>';

                         
                        $selectedRow.addClass("parent");
                        $selectedRow.after(_childRow);
                    }
                };

                $(_tableDiv).on("click.dtr", "thead>tr:nth-child(2)>th:eq(0)", headerRowTotalHandler);

                if (!sumTotalRowCreated) {
                    $($tblTotalHeaderRows[0]).addClass("removetheadChildStyle");
                    $(_tableDiv).off("click.dtr");                     
                }
                else {
                    $($tblTotalHeaderRows[0]).removeClass("removetheadChildStyle");
                    if ($.trim($($tblTotalHeaderRows[0]).html()) == "") {
                        $($tblTotalHeaderRows[0]).html("&nbsp");
                    }
                    $(_tableDiv).off("click.dtr");
                    $(_tableDiv).on("click.dtr", "thead>tr:nth-child(2)>th:eq(0)", headerRowTotalHandler);
                }

                datatable.on('responsive-resize', function (e, dt, columns) {                    
                    _.each(columns, function (o, i) {
                        //if ($tblTotalHeaderRows[i] != undefined && $tblHeaderRows[i] != undefined)
                            $tblTotalHeaderRows[i].style.display = $tblHeaderRows[i].style.display;
                    });

                    var totalHeaderChildRow = $(this).find("thead tr.child"),
					totalHeaderParentRow = $(this).find("thead tr.parent"),
					anyHiddenColumn = _.some(columns, function (o) { return o == false; });

                    if (totalHeaderChildRow.length > 0) {
                        totalHeaderChildRow.remove();
                    }

                    var _childRow = '<tr class="child"><td class="child" colspan="' + $tblHeaderRows.length + '"><table>';
                    _.each($tblHeaderRows, function (head, i) {
                        if (head.style.display == "none") {
                            _childRow += '<tr><td><span class="dtr-title" >' + head.innerText + ':' + '</span></td> ' + '<td><span>' + $tblTotalHeaderRows[i].innerText + '</span></td>' + '</tr>';
                        }
                    });
                    _childRow += '</table></td></tr>';
                     
                    if (anyHiddenColumn && totalHeaderParentRow != null) {
                        totalHeaderParentRow.after(_childRow);
                    }

                     
                    if (!sumTotalRowCreated) {
                        $($tblTotalHeaderRows[0]).addClass("removetheadChildStyle");
                        $(_tableDiv).off("click.dtr");                         
                    }
                    else {
                        $($tblTotalHeaderRows[0]).removeClass("removetheadChildStyle");
                        if ($.trim($($tblTotalHeaderRows[0]).html()) == "") {
                            $($tblTotalHeaderRows[0]).html("&nbsp");
                        }
                        $(_tableDiv).off("click.dtr");
                        $(_tableDiv).on("click.dtr", "thead>tr:nth-child(2)>th:eq(0)", headerRowTotalHandler);
                    }

                });

            }
        }

        function getDataTableObject(data1, tableElm) {
            if ($(tableElm).find('thead').length == 0) {
                $(tableElm).html('<thead></thead>');
            }

            var _$thead = $(tableElm).find('thead');

            _$thead.html('');
            var ColumCount = (data1.data.length == 0) ? 0 : Object.keys(data1.data[0]).length;
            var tr = "<tr>";

            for (var i = 0; i < ColumCount; i++) {
                tr += "<th style='vertical-align: top;' title='" + data1.columns[i].UserFriendlyName + "'></th>";
            }
            tr += "</tr>";
            _$thead.append(tr);
            _$thead.append(tr);
            //_$thead.find('tr:gt(0)').addClass('hidden-xs')
            var _linkedColumndefs = [];
            var _linkedHeaderColumndefs = [];
            var _isHdnColdefs = [];
            var th = _$thead.find('tr:gt(0)').find('th');
            var Columns = function (cc) {
                var arr = [];
                for (var i = 0; i < cc; i++) {
                    var colObj = {
                        "targets": [i],
                        render: function (data, type, full, meta) {
                            var fnFormatter = getValueFormatter(data1.columns[meta.col]);
                            if (data1.columns[meta.col].IsLink == true) {
                                return '<span class="LinkedItem CustomLinks">' + fnFormatter(data) + '</span>';
                            }
                            return fnFormatter(data);
                        }
                    }

                    if (data1.columns[i].IsLink) {
                        _linkedColumndefs.push(i);
                    }
                    if (data1.columns[i].IsLinkOnHeader) {
                        _linkedHeaderColumndefs.push(i);
                    }
                    if (data1.columns[i].IsHidden) {
                        data1.columns[i].visible = false;
                        _isHdnColdefs.push(i);
                    }
                    arr.push(colObj);
                }
                return arr;
            }

            var _coldefs = (ctrl.ColumnsDefn == undefined) ? Columns(ColumCount) : ctrl.ColumnsDefn(ColumCount, data1,getValueFormatter, _linkedColumndefs, _linkedHeaderColumndefs, _isHdnColdefs);

            return {
                "data": data1.data,
                "columns": data1.columns,
                "iDisplayLength": 50,
                "oLanguage": {
                    "sLengthMenu": 'Show <select data-toggle="dropdown">' +
								   '<option value="10">10</option>' +
								   '<option value="20">20</option>' +
								   '<option value="50">50</option>' +
								   '<option value="100">100</option>' +
								   '</select> Records'
                },
                "columnDefs": _coldefs,
                "createdRow": function (row, data, dataIndex) {
                    _linkedColumndefs.forEach(function (item, index) {
                        var _$tCell = $(row).find('td:eq(' + item + ')');
                        if (_$tCell) {
                            _$tCell.attr('data-index', dataIndex);
                            _$tCell.attr('data-ReportFilters', data1.columns[item].ReportFilters);
                            _$tCell.attr('data-report', data1.columns[item].ReportRefKey);
                            _$tCell.attr('data-reportName', data1.columns[item].NextReportName);
                            _$tCell.attr('data-reportType', data1.columns[item].ReportType);
                            _$tCell.attr('data-urlname', data1.columns[item].UrlName);
                            _$tCell.attr('data-urlpath', data1.columns[item].UrlPath);
                            _$tCell.attr('data-urlrefkey', data1.columns[item].UrlRefKey);
                        }
                    });
                },
                "retrieve": true,
                "autoWidth": false,
                "responsive": {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRow,
                        renderer: function (api, rowIdx, columns) {
                            var data = $.map(columns, function (col, i) {
                                var linkColumnAttributeData = "", linkColumnHeaderAttributeData = "";
                                if (col.hidden) {
                                    if (data1.columns[col.columnIndex].IsLink) {
                                        linkColumnAttributeData = " data-index='" + col.columnIndex +
										"' data-reportfilters='" + data1.columns[col.columnIndex].ReportFilters +
										"' data-report='" + data1.columns[col.columnIndex].ReportRefKey +
										"' data-reportname='" + data1.columns[col.columnIndex].NextReportName +
										"' data-reporttype='" + data1.columns[col.columnIndex].ReportType +
										"' data-urlname='" + data1.columns[col.columnIndex].UrlName +
										"' data-urlpath='" + data1.columns[col.columnIndex].UrlPath +
										"' data-urlrefkey='" + data1.columns[col.columnIndex].UrlRefKey + "' ";
                                    }

                                    if (data1.columns[col.columnIndex].IsLinkOnHeader) {
                                        linkColumnAttributeData = " data-report='" + data1.columns[col.columnIndex].ReportRefKey +
										"' data-reportname='" + data1.columns[col.columnIndex].NextReportName +
										"' data-reporttype='" + data1.columns[col.columnIndex].ReportType + "' ";
                                    }
                                }
                                return col.hidden ?
									'<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
										'<td ' + linkColumnAttributeData + ' ><span class="dtr-title ' + (data1.columns[col.columnIndex].IsLinkOnHeader ? 'LinkedItem CustomLinks' : '') + '" >' + col.title + ':' + '</span></td> ' +
										'<td ' + linkColumnAttributeData + '  ><span ' + (data1.columns[col.columnIndex].IsLink ? 'class="LinkedItem CustomLinks"' : '') + '>' + col.data + '</span></td>' +
									'</tr>' :
									'';
                            }).join('');

                            return data ?
								$('<table/>').append(data) :
								false;
                        }
                    }
                },
                "orderCellsTop": true,
                "deferRender": true,
                "bSortCellsTop": true,
                "paging": ctrl.showPaging,
                "searching": ctrl.showSearch,
                "order": [],
                "oLanguage": { "sEmptyTable": "No records available" },
                "headerCallback": function (thead, data, start, end, display) {

                    $tblHeaderRows = this.find('thead tr:first th');
                    $tblTotalHeaderRows = this.find('thead tr:eq(1) th');

                    _linkedHeaderColumndefs.forEach(function (item, index) {
                        var _$tCell = $(thead).find('th:eq(' + item + ')');
                        _$tCell[0].innerHTML = "<a>" + _$tCell[0].textContent + "</a>";
                        _$tCell = $(thead).find('a');
                        _$tCell.on("click", linkHandler);
                        if (_$tCell && !data1.columns[item].UrlPath) {
                            _$tCell.addClass('CustomLinks');
                            _$tCell.attr('data-report', data1.columns[item].ReportRefKey);
                            _$tCell.attr('data-reportName', data1.columns[item].NextReportName);
                            _$tCell.attr('data-reportType', data1.columns[item].ReportType);
                    }
                    });

                    // To Handle Total Count String Overlap If Only one row is returned
                    if (data1.data.length != 1) {
                        var RowCount = data1.columns.length;
                        var totalColVal = -1;
                        for (var i = 0; i < RowCount; i++) {
                            if (data1.columns[i].ismeasure === true) {                                
                                $(th[i]).html(getValueFormatter(data1.columns[i])(data1.aggregates[data1.columns[i].data]));
                                if (totalColVal === -1 && totalColVal < i) {
                                    totalColVal = i - 1;
                            }
                        }
                    }
                        $(th[totalColVal]).html("<b>Total/Avg:</b>");
                        sumTotalRowCreated = true;
                }
            }
            }
        }

        function getValueFormatter(col) {
            if (col.DataType === 'Number') {
                return valueFormatter.getformatter(col.DataType.toLowerCase());
            }
            else if (col.DataType === 'Money') {
                return valueFormatter.getformatter(col.DataType.toLowerCase());
            }
            else if (col.DataType === 'Int') {
                return valueFormatter.getformatter(col.DataType.toLowerCase());
            }
            else if (col.DataType === 'Percent') {
                return valueFormatter.getformatter(col.DataType.toLowerCase());
            }
            else if (col.DataType === 'Decimal1') {
                return valueFormatter.getformatter(col.DataType.toLowerCase());
            }
            else if (col.DataType === 'Decimal2') {
                return valueFormatter.getformatter(col.DataType.toLowerCase());
            }
            else if (col.DataType === 'Decimal3') {
                return valueFormatter.getformatter(col.DataType.toLowerCase());
            }
            else if (col.DataType === 'Date') {
                return function (val) {
                    return valueFormatter.getformatter('date')(val, 'MM/dd/yyyy');
                }
            }
            else {
                return function (val) {
                    return val;
                };
            }
        }
    }

})(angular.module("dealerWizard"));
