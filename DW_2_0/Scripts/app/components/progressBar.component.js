﻿(function (app) {
    'use strict';

    app.component('progressBar',
        {
            template: '<div class="messaging" ng-show="showLoader">' +
                      '<img class="spinner" src="../../../Content/images/loading-blue.gif" /> ' +
                      '</div>',
            controller: ['$scope', '$element', '$attrs', progressBarCtrl],
            bindings: {
                data: "<"
            }
        });


    function progressBarCtrl($scope, $element, $attrs) {
        $scope.showLoader = false;
        var ctrl=this;

        ctrl.$onInit = function () {
            $scope.showLoader = angular.copy(ctrl.data);
        }

        ctrl.$onChanges = function (changedObject) {
            if (changedObject.data != undefined) {
                $scope.showLoader = angular.copy(ctrl.data);
            }
        }

    }

})(angular.module('dealerWizard'));