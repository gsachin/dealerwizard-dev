﻿(function (app) {

    app.component("activityBar", {
        templateUrl: "/Scripts/app/components/useractivity/activitybar/activitybar.html",
        controller: ['$scope', '$element', 'valueFormatter','$location', activityBarCtrl],
        bindings: {
            data: "<",
            wConfig: "<",
            renderElement: "<"
        }
    });

    function activityBarCtrl($scope, $element, valueFormatter,$location) {
        var ctrl = this;
        $scope.ShowLoader = false;
        //var ageingGroup = [];

        ctrl.$onInit = function () {
            $scope.ShowLoader = ctrl.wConfig.ShowLoader;
            ctrl.data = ctrl.data;
            ctrl.renderElement = ctrl.renderElement;
            if (ctrl.renderElement) {
                RenderBarChart(ctrl.data, $element[0].querySelector('#divBarChart'), valueFormatter, ctrl);
            }
        }

        ctrl.$onChanges = function (ChangedObject) {
            if (ChangedObject.data && !ChangedObject.data != undefined) {
                if (ctrl.renderElement) {
                    $('.d3-tip').remove();
                    RenderBarChart(ctrl.data, $element[0].querySelector('#divBarChart'), valueFormatter, ctrl,$location,$scope);
                }
                ctrl.wConfig.ShowLoader = false;
                $scope.ShowLoader = false;
            }

            if (ChangedObject.wConfig != undefined) {
                if (!ChangedObject.wConfig.isFirstChange()) {
                    $scope.ShowLoader = ctrl.wConfig.ShowLoader;
                }
            }

            if (ChangedObject.renderElement != undefined) {
                if (ctrl.renderElement) {
                    $('.d3-tip').remove();
                    RenderBarChart(ctrl.data, $element[0].querySelector('#divBarChart'), valueFormatter, ctrl,$location,$scope);
                }
                ctrl.wConfig.ShowLoader = false;
                $scope.ShowLoader = false;
            }
        };

        ctrl.$onDestroy = function () {
            $(window).off('resize');
            $('.d3-tip').remove();
        }

    }

    function getValueFormatter(Type, Data, valueFormatter) {
        try {
            if (Type === 'Money') {
                return valueFormatter.money(Data);
            }
            else if (Type === 'Number') {
                return valueFormatter.number(Data);
            }
            else if (Type === 'Int') {
                return valueFormatter.int(Data);
            }
            else if (Type === 'Percent') {
                return valueFormatter.percent(Data);
            }
            else if (Type === 'Decimal1') {
                return valueFormatter.decimal1(Data);
            }
            else if (Type === 'Decimal2') {
                return valueFormatter.decimal2(Data);
            }
            else if (Type === 'Decimal3') {
                return valueFormatter.decimal3(Data);
            }
            else if (Type === 'Date') {
                return valueFormatter.date(Data, 'MM/dd/yyyy');
            }
            else {
                return Data;
            }
        }
        catch (ex) {
            return Data;
        }
    }

    function RenderBarChart(mdata, container, valueFormatter, ctrl,$location,$scope) {
        if (mdata == undefined) return;
        $(container).html('');

        // set the dimensions and margins of the graph
        var margin = { top: 20, right: 30, bottom: 45, left: 100 },
            width = 16600 - margin.left - margin.right,
            height = 220 - margin.top - margin.bottom;
        if (mdata.length == 0) return;
        var _keys = Object.keys(mdata[0]);

        var data = mdata;
        var keys = Object.keys(data[0]);


        var xValue = keys[0];
        var yValue = keys[1];
        var xValueDataType = 'String';
        var xAxisTitle = xValue;
        var yValueDataType = 'Number';
        var yAxisTitle = yValue

        // set the ranges
        var x = d3.scaleBand()
                  .range([0, width])
                  .padding(0.1);
        var y = d3.scaleLinear()
                  .range([height, 0]);

        var tip = d3.tip()
                  .attr('class', 'd3-tip ttip2')
                  .offset([-10, 0])
                  .html(function (d, i) {

                      var TooltipHtml = "";
                      keys.forEach(function (item, index) {
                          var dataType = "";
                          if (typeof d[item] === 'string') {
                              dataType = 'String';
                          }
                          else {
                              dataType = 'Number';
                          }

                          var val = getValueFormatter(dataType, d[item], valueFormatter);
                          if (val == "" || !val) val = d[item];

                          if (item == "TabRoute") {
                              item = "Department";
                          }
                          else if (item == "AverageTimeSpent") {
                              item = "Avg Time Spent (mins)";
                          }

                          TooltipHtml += item + ": " + val + "<br />";
                      });
                      return TooltipHtml;
                  })
        d3.select('body').append('div').attr('id', 'barTooltip').append('span').attr('id', 'value');
        // append the svg object to the body of the page
        // append a 'group' element to 'svg'
        // moves the 'group' element to the top left margin
        var svg = d3.select(container).append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
          .append("g")
            .attr("transform",
                  "translate(" + margin.left + "," + margin.top + ")");

        svg.call(tip);
        //format the data
        data.forEach(function (d) {
            d[xValue] = getValueFormatter(xValueDataType, d[xValue], valueFormatter);
            //d[yValue] = getValueFormatter(yValueDataType, d[yValue], valueFormatter);
        });

        var MaxBounValue = d3.max(data, function (d) { return parseFloat(d[yValue]); });
        var MinBounValue = d3.min(data, function (d) { return parseFloat(d[yValue]); });
        var PYMaxBounValue = 0, PYMinBounValue = 0;

        MaxBounValue = (MaxBounValue > PYMaxBounValue) ? MaxBounValue : PYMaxBounValue;
        MinBounValue = (MinBounValue < PYMinBounValue) ? MinBounValue : PYMinBounValue;

        if (MinBounValue == MaxBounValue && MinBounValue > 0) {
            MinBounValue = 0;
        }

        // Scale the range of the data in the domains
        x.domain(data.map(function (d) { return d[xValue]; }));
        y.domain([MinBounValue, MaxBounValue]);
        if (MinBounValue < 0) {
            //Reset Min Range Value
            MinBounValue = y.ticks()[0] + y.ticks()[0] - y.ticks()[1];
            y.domain([MinBounValue, MaxBounValue]);
        }
        // append the rectangles for the bar chart
        svg.selectAll(".bar")
            .data(data)
            .enter().append("rect")
            .attr("class", "bar")
            .attr("x", function (d) { return x(d[xValue]) + (x.bandwidth() / 4); })
            .attr("width", x.bandwidth() / 2)
            .attr("y", function (d) { return (d[yValue] < 0) ? y(0) : y(d[yValue]); })
            .attr("height", function (d) {
                if (MinBounValue < 0) {
                    return Math.abs(y(d[yValue]) - y(0));
                }
                else {
                    return Math.abs(height - y(d[yValue]));
                }
            })
            .attr("fill", "blue")
            .style("cursor", function (d) { return "default"; })
            .on("click", function (d, i) {
                tip.hide(d, i);
                $scope.$apply(() => { $location.path('/useractivitylogs/' + d[xValue]) });
            })
            .on("mouseover", function (d, i) {
                tip.show(d, i);
            })
            .on("mouseout", function (d, i) {
                tip.hide(d, i);
            });

        // add the x Axis
        svg.append("g")
            .attr("transform", "translate(0," + height + ")")
            .call(d3.axisBottom(x));
         //.selectAll("text")
         //  .style("text-anchor", "end")
         //  .attr("dx", "-.8em")
         //  .attr("dy", "-.55em")
           //.attr("transform", "rotate(-45)");

        // add the y Axis
        svg.append("g")
            .call(d3.axisLeft(y));

        // now add titles to the axes
        var xaxistext = svg.append("text")
            .attr("text-anchor", "middle")  // this makes it easy to centre the text as the transform is applied to the anchor
            .attr("transform", "translate(" + (600 / 2) + "," + (height + (margin.bottom - 5)) + ")")  // centre below axis
            .text("Department").style("Font-size","13");

        var yaxistext = svg.append("text")
                   .attr("text-anchor", "middle")  // this makes it easy to centre the text as the transform is applied to the anchor
                   .attr("transform", "translate(" + ((-margin.left - 20) / 2) + "," + (height / 2) + ")rotate(-90)")  // text is drawn off the screen top left, move down and out and rotate
                   .text("Avg Time Spent (mins)").style("Font-size", "13");


        var resizeTimmer;
        $(window).on("resize", function (e) {
            clearTimeout(resizeTimmer);
            resizeTimmer = setTimeout(function () {
                resized();
            }, 350)
        });

        function resized() {
            //Get the width of the window
            //var margin = { top: 20, right: 20, bottom: 30, left: 60 };
            var w = Math.ceil((d3.select('#divBarChart').node().clientWidth)) - margin.left - margin.right;
            if (w <= 0) w = Math.ceil((d3.select('#panel3').node().clientWidth)) - margin.left - margin.right - 30;
            var ss = d3.select('#divBarChart').selectAll('svg');

            x = x.range([0, w]).padding(.25);

            ss._groups.forEach(function (item, index) {
                angular.forEach(item, function (item2, index1) {
                    svg.attr('width', w + margin.left + margin.right);
                    var Bars = svg.selectAll(".bar");
                    angular.forEach(Bars._groups[0], function (bar, index2) {
                        d3.select(bar)
                            .attr("x", function (d) { return x(d[xValue]) + (x.bandwidth() / 4); })
                        .attr("width", x.bandwidth() / 2)
                    });

                    svg.select("g")
            .call(d3.axisBottom(x));

                    xaxistext.attr("transform", "translate(" + (w / 2) + "," + (height + (margin.bottom - 5)) + ")");
                });
            });


        }
        resized();
    }

})(angular.module('dealerWizard'));