﻿(function (app) {

    app.component("activitybubble", {
        templateUrl: "/Scripts/app/components/useractivity/activitybubble/activitybubble.html",
        controller: ['$scope', '$element', 'valueFormatter', activityBubbleCtrl],
        bindings: {
            data: "<",
            wConfig: "<",
            renderElement: "<"
        }
    });

    function activityBubbleCtrl($scope, $element, valueFormatter) {
        var ctrl = this;
        $scope.ShowLoader = false;

        ctrl.$onInit = function () {
            $scope.ShowLoader = ctrl.wConfig.ShowLoader;
            ctrl.data = ctrl.data;
            ctrl.renderElement = ctrl.renderElement;
            if (ctrl.renderElement) {
                RenderBubbleChart(ctrl.data, $element[0].querySelector('#divBubbleChart'), valueFormatter);
            }
        }

        ctrl.$onChanges = function (ChangedObject) {
            if (ChangedObject.data != undefined) {
                if (ctrl.renderElement) {
                    $('.d3-tip').remove();
                    console.log(ctrl.data);
                    RenderBubbleChart(ctrl.data, $element[0].querySelector('#divBubbleChart'), valueFormatter);
                }
                ctrl.wConfig.ShowLoader = false;
                $scope.ShowLoader = false;
            }

            if (ChangedObject.wConfig != undefined) {
                if (!ChangedObject.wConfig.isFirstChange()) {
                    $scope.ShowLoader = ctrl.wConfig.ShowLoader;
                }
            }

            if (ChangedObject.renderElement != undefined) {
                if (ctrl.renderElement) {
                    $('.d3-tip').remove();
                    RenderBubbleChart(ctrl.data, $element[0].querySelector('#divBubbleChart'), valueFormatter);
                }
                ctrl.wConfig.ShowLoader = false;
                $scope.ShowLoader = false;
            }

        };

        ctrl.$onDestroy = function () {
            $(window).off('resize');
            $('.d3-tip').remove();
        }

    }

    function getValueFormatter(Type, Data, valueFormatter) {
        try {
            if (Type === 'Money') {
                return valueFormatter.money(Data);
            }
            else if (Type === 'Number') {
                return valueFormatter.number(Data);
            }
            else if (Type === 'Int') {
                return valueFormatter.int(Data);
            }
            else if (Type === 'Percent') {
                return valueFormatter.percent(Data);
            }
            else if (Type === 'Decimal1') {
                return valueFormatter.decimal1(Data);
            }
            else if (Type === 'Decimal2') {
                return valueFormatter.decimal2(Data);
            }
            else if (Type === 'Decimal3') {
                return valueFormatter.decimal3(Data);
            }
            else if (Type === 'Date') {
                return valueFormatter.date(Data, 'MM/dd/yyyy');
            }
            else {
                return Data;
            }
        }
        catch (ex) {
            return Data;
        }
    }

    function RenderBubbleChart(mdata, container, valueFormatter) {
        $(container).html('');        

        // set the dimensions and margins of the graph
        var margin = { top: 30, right: 30, bottom: 70, left: 100 },
            width = 16600 - margin.left - margin.right,
            height = 250 - margin.top - margin.bottom;

        var keys = Object.keys(mdata[0]);

        var xValue = keys[0];//keys[0];//Get X Axis
        var xValueDataType = 'String';
        var xAxisTitle = keys[0];

        var yValue = keys[1];//keys[1];//Get Y Axis
        var yValueDataType = 'Number';
        var yAxisTitle = keys[1];

        var zValue = keys[1]; //Deal Counts for Dots Radius Calculation
        var zValueDataType = 'Number';
        if (keys.length > 2) {
            var nameValue = _.map(_.uniq(mdata, function (d) { return d.ReportName; }),
                function (o) {
                    return {
                        [keys[2]] : o[keys[2]],
                        color: getRandomColor()
                    }
                }); // Get Legend Name
        }
        // set the ranges
        var x = d3.scaleBand()
                  .range([width, 0])
                  .padding(0.1);
        var y = d3.scaleLinear()
                  .range([height, 0]);

        var tip = d3.tip()
                  .attr('class', 'ttip2 n ttipbubble')
                  .offset([-10, 0])
                  .html(function (d, i) {

                      var TooltipHtml = "";
                      keys.forEach(function (item, index) {
                          var dataType = "";
                          if (typeof d[item] === 'string') {
                              dataType = 'String';
                          }
                          else {
                              dataType = 'Number';
                          }

                          var val = getValueFormatter(dataType, d[item], valueFormatter);
                          if (val == "" || !val) val = d[item];
                          TooltipHtml += item + ": " + val + "<br />";
                      });
                      return TooltipHtml;
                  });

        d3.select('body').append('div').attr('id', 'bubbleTooltip').append('span').attr('id', 'value');
        // append the svg object to the body of the page
        // append a 'group' element to 'svg'
        // moves the 'group' element to the top left margin
        var svg = d3.select(container).append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
          .append("g")
            .attr("transform",
                  "translate(" + margin.left + "," + margin.top + ")");

        svg.call(tip);

        //var mdata = _.sortBy(mdata, function (d) { try { return parseFloat(d[xValue]) } catch (ex) { return d[xValue] } });

        //format the data
        mdata.forEach(function (d) {
            var val = ((xValueDataType == "Number" || xValueDataType == "Money") ? getValueFormatter(xValueDataType, d[xValue], valueFormatter) : d[xValue]);
            if (val == "" || !val) val = d[xValue];
            d[xValue] = val;
            //d[yValue] = getValueFormatter(yValueDataType, d[yValue], valueFormatter);
        });

        // Scale the range of the data in the domains
        x.domain(mdata.map(function (d) { return d[xValue]; }));
        var MinBounValue = d3.min(mdata, function (d) { return parseFloat(d[yValue]); });
        var MaxBounValue = d3.max(mdata, function (d) { return parseFloat(d[yValue]); });
        var diff = MinBounValue * 0.1;
        MinBounValue = MinBounValue - diff;
        var add = MaxBounValue * 0.1;
        MaxBounValue = MaxBounValue + add;
        y.domain([MinBounValue, MaxBounValue]);
        var maxCount = d3.max(mdata, function (d) { return ((zValueDataType == "Number" || zValueDataType == "Money") ? parseFloat(d[zValue]) : d[zValue]); });

        //For Radius Calculation, Converts 0 to 100 range to custum range
        var oldMin = 0, oldMax = 100;
        var newMin = 5, newMax = 30;
        var oldRange = (oldMax - oldMin);//(OldMax - OldMin)
        var newRange = (newMax - newMin);//(NewMax - NewMin)  
        //var NewValue = (((OldValue - OldMin) * NewRange) / OldRange) + NewMin
        console.log(mdata);
        // draw dots
        svg.selectAll(".dot")
          .data(mdata)
          .enter().append("circle")
          .attr("class", "dot")
          .attr("cx", function (d) { return x(d[xValue]) + (x.bandwidth() / 2); })
          .attr("cy", function (d) { return y(d[yValue]); })
          .attr("r", function (d) {
              var oldValue = ((d[zValue] / maxCount) * 100).toFixed(2);
              var newValue = (((oldValue - oldMin) * newRange) / oldRange) + newMin;
              if (newValue < newMin) return newMin;
              //if (newValue > newMax) return newMax;
              return newValue;
          })
          .attr("fill", function (d) {
              var color = _.find(nameValue, function (o) { return o[keys[2]] == d[keys[2]] }).color;
              return color;
          })
          .on("mouseover", function (d, i) {
              tip.show(d, i);
          })
          .on("mouseout", function (d, i) {
              tip.hide(d, i);
          });



        // add the x Axis
        svg.append("g")
            .attr("transform", "translate(0," + height + ")")
            .call(d3.axisBottom(x))
         .selectAll("text")
           .style("text-anchor", "end")
           .attr("dx", "-.8em")
           .attr("dy", "-.55em")
           .attr("transform", "rotate(-30)");

        // add the y Axis
        svg.append("g")
            .call(d3.axisLeft(y));


        // draw legend
        var legend = svg.selectAll(".legend")
            .data(nameValue)
          .enter().append("g")
            .attr("class", "legend")
            .attr("transform", function (d, i) { return "translate(0," + i * 20 + ")"; });

        // draw legend colored rectangles
        legend.append("rect")
            .attr("x", width - 10)
            .attr("width", 18)
            .attr("height", 18)
            .style("fill", function (d) {
                var color = _.find(nameValue, function (o) { return o[keys[2]] == d[keys[2]] }).color;
                return color;
            });

        // draw legend text
        legend.append("text")
            .attr("x", width - 24)
            .attr("y", 9)
            .attr("dy", ".35em")
            .style("text-anchor", "end")
            .style("font-size", "11px")
            .text(function (d) { return d[keys[2]] })

        // now add titles to the axes
        var xaxistext = svg.append("text")
            .attr("text-anchor", "middle")  // this makes it easy to centre the text as the transform is applied to the anchor
            .attr("transform", "translate(" + (600 / 2) + "," + (height + (margin.bottom - 5)) + ")")  // centre below axis
            .text(xAxisTitle);

        var yaxistext = svg.append("text")
                   .attr("text-anchor", "middle")  // this makes it easy to centre the text as the transform is applied to the anchor
                   .attr("transform", "translate(" + ((-margin.left - 20) / 2) + "," + (height / 2) + ")rotate(-90)")  // text is drawn off the screen top left, move down and out and rotate
                   .text(yAxisTitle);


        var resizeTimmer;
        $(window).on("resize", function (e) {
            clearTimeout(resizeTimmer);
            resizeTimmer = setTimeout(function () {
                resized();
            }, 350)
        });

        function resized() {
            if (d3.select('#divBubbleChart').node() == null) {
                return;
            }
            //Get the width of the window
            //var margin = { top: 30, right: 20, bottom: 30, left: 60 };
            var w = Math.ceil((d3.select('#divBubbleChart').node().clientWidth)) - margin.left - margin.right;
            if (w <= 0) w = Math.ceil((d3.select('#panel3').node().clientWidth)) - margin.left - margin.right - 30;
            var ss = d3.select('#divBubbleChart').selectAll('svg');

            x = x.range([w, 0]).padding(.25);

            ss._groups.forEach(function (item, index) {
                angular.forEach(item, function (item2, index1) {
                    svg.attr('width', w + margin.left + margin.right);
                    var Dots = svg.selectAll(".dot");
                    angular.forEach(Dots._groups[0], function (dot, index2) {
                        d3.select(dot)
                            .attr("cx", function (d) {
                                return x(d[xValue]) + (x.bandwidth() / 2);
                            })
                    });

                    var Rects = svg.selectAll(".legend").select("rect");
                    angular.forEach(Rects._groups[0], function (rect, index3) {
                        d3.select(rect)
                            .attr("x", w - 10);

                    });

                    var Texts = svg.selectAll(".legend").select("text");
                    angular.forEach(Texts._groups[0], function (text, index3) {
                        d3.select(text)
                            .attr("x", w - 24);
                    });
                    xaxistext.attr("transform", "translate(" + (w / 2) + "," + (height + (margin.bottom - 5)) + ")");

                    svg.select("g")
            .call(d3.axisBottom(x));
                });
            });


        }
        resized();

    }

    function getRandomColor() {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }

})(angular.module('dealerWizard'));