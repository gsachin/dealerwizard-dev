﻿(function (app) {
    'use strict';

    app.component('useractivitylog',
        {
            templateUrl: "/Scripts/app/components/useractivity/useractivitylog/useractivitylog.html",
            controller: ['$scope', '$element', '$attrs', 'apiService', 'valueFormatter', 'applicationObject', '$timeout',
                'dealerService', 'tabsMetaDataService', '$rootScope', 'notificationService', '$window','userActivityService','$location', logFunc]
        });

    function logFunc($scope, $element, $attrs, apiService, valueFormatter, applicationObject, $timeout,
        dealerService, tabsMetaDataService, $rootScope, notificationService, $window, userActivityService,$location) {
        var ctrl = this;
        $scope.allDealers = false;

        $scope.currnetInventoryLegends = [
                    { LegendName: "Avg Time Spend by user", legendValue: 45, legendFooter: "minutes", legendColor: "Black" },
                    { LegendName: "Most common browser", legendValue: "Chrome", legendFooter: "55%", legendColor: "Black" },
                    { LegendName: "Most Popular Report", legendValue: "Dealership Overview", legendFooter: "100 times", legendColor: "Black" },
                    { LegendName: "Popular Tab", legendValue: "Service Department", legendFooter: "30% time spent", legendColor: "Black" }
        ];

        ctrl.wConfig = {
            ShowLoader: true
        }
        ctrl.renderUserActivityChart = true;

        ctrl.$onInit = function () {
            var _curDate = moment().subtract(1, 'd'), _startDate = moment(_curDate).subtract(3, 'M');
            var AppObj = { startDate: _startDate, endDate: _curDate };
            $scope.globalFilter = AppObj;
            renderData();
            
            ctrl.containerBubbleData = [
                { Reports: "Sales Overview", Views: "2", ReportName: "Sales Overview" },
                { Reports: "Labour Sales Potential", Views: "22", ReportName: "Labour Sales Potential" },
                { Reports: "Inventory Overview Report", Views: "25", ReportName: "Inventory Overview Report" },
                { Reports: "RO Summary", Views: "27", ReportName: "RO Summary" },
                { Reports: "Service Overview", Views: "13", ReportName: "Service Overview" },
                { Reports: "Gross Profit by Age", Views: "19", ReportName: "Gross Profit by Age" },
                { Reports: "RO by Model Year Chart", Views: "26", ReportName: "RO by Model Year Chart" },
                { Reports: "FI Manager Performance", Views: "11", ReportName: "FI Manager Performance" },
                { Reports: "New Car Selling Price", Views: "5", ReportName: "New Car Selling Price" },
                { Reports: "Used Car Days Supply", Views: "48", ReportName: "Used Car Days Supply" },
                { Reports: "Sales Manager Performance Chart", Views: "62", ReportName: "Sales Manager Performance Chart" },
                { Reports: "Parts to Labour Ratio", Views: "33", ReportName: "Parts to Labour Ratio" },
                { Reports: "Advisor Performance Chart", Views: "41", ReportName: "Advisor Performance Chart" },
                { Reports: "Used Car Sales Trends", Views: "29", ReportName: "Used Car Sales Trends" },
                { Reports: "WholeSale Gross by Age", Views: "16", ReportName: "WholeSale Gross by Age" }
            ];
        
        }

        ctrl.$postLink = function () {

        }

        $scope.InvokeForAllDealers = function () {
            if ($element[0].querySelector("#checkAllDealers").checked === false) {
                $scope.allDealers = false;
            }
            else if ($element[0].querySelector("#checkAllDealers").checked === true) {
                $scope.allDealers = true;
            }
            renderData();
        }

        var renderData = () =>{
            userActivityService.getAnalyticDataForTabUsage($scope.globalFilter.startDate, $scope.globalFilter.endDate, $scope.allDealers).then(function (data) {
                ctrl.containerBarData = data;
            });
        }

        $scope.reload = function (data) {
            $scope.globalFilter = {               
                startDate: data.startDate,
                endDate: data.endDate,
            };
            renderData();

        }

        $scope.resetPageFilter = function () {

        }

       
    }


})(angular.module('dealerWizard'));