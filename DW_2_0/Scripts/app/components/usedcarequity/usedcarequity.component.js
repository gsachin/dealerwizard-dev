﻿(function (app) {

    app.component("usedcarequity", {
        templateUrl: "/Scripts/app/components/usedcarequity/usedcarequity.html",
        controller: ['$scope', '$element', '$attrs', 'valueFormatter', 'dealerService', 'notificationService', datatableCtrl],
        bindings: {
            data: "<",
            wConfig: "<",
            renderElement: "<",
            showItemList: "<",
            onListItemClick: "&",
            uedefaultTabFilter: "<",
            onFilterUsedEquityButtonGroup: "&",
            onUsedCarEquityCapture: "&"
        }
    });

    function datatableCtrl($scope, $element, $attrs, valueFormatter, dealerService, notificationService) {
        var datatable = null; var th = null;

        var ctrl = this, _data = {};
        ctrl.red = 0, ctrl.green = 0, ctrl.yellow = 0;

        ctrl.bookValueSelected = $('#ddlBookValue :selected').val(), ctrl.bookPercentSelected = parseFloat($('#ddlPercentOfBook :selected').val());


        $scope.ShowLoader = false;

        $scope["displayMessage"] = false;

        ctrl.$onInit = function () {
            $scope.ShowLoader = ctrl.wConfig.ShowLoader;
        }

        ctrl.$onChanges = function (changedObj) {
            if (changedObj.renderElement != undefined) {
                if (ctrl.renderElement) {
                    _data = angular.copy(ctrl.data);
                }
            }

            if (changedObj.data != undefined) {
                if (changedObj.data && !changedObj.data.isFirstChange()) {//Fix this for better null check 
                    _data = angular.copy(changedObj.data.currentValue);

                    if (ctrl.renderElement) {
                        dealerService.getDealerBookValueItemList(dealerService.currentDealer.Id).then(function (res) {
                            $scope.bookValueList = res;
                            dealerService.getDealerSettings(dealerService.currentDealer.Id).then(function (result) {
                                setDefault(result);
                                _data.data = filterData(_data.data);
                                if (_data != undefined) {
                                    if (_data.data.length > 0) {
                                        getVehicleData();
                                        //$($element[0].querySelector('#ddlBookValue'))[0].selectedIndex = 0;
                                        CalculateDiffInvBook();
                                        $scope["displayMessage"] = false;
                                        ctrl.onFilterUsedEquityButtonGroup({ ctx: { r: ctrl.red, g: ctrl.green, y: ctrl.yellow } });
                                        ctrl.bookValueSelected = $('#ddlBookValue :selected').text();
                                        ctrl.bookPercentSelected = parseFloat($('#ddlPercentOfBook :selected').val());
                                        ctrl.onUsedCarEquityCapture({ ctx: { bv: ctrl.bookValueSelected, bp: ctrl.bookPercentSelected } });
                                    }
                                    else {
                                        $scope["displayMessage"] = true;
                                    }
                                }
                                else {
                                    $scope["displayMessage"] = true;
                                }

                            },
                            function (result) {
                                notificationService.error(result);
                                ctrl.data = [];
                                $scope["displayMessage"] = true;
                            });
                        },
                        function (result) {
                            notificationService.error(result);
                            ctrl.data = [];
                            $scope["displayMessage"] = true;
                        });
                    }
                    ctrl.wConfig.ShowLoader = false;
                    $scope.ShowLoader = false;
                }
            }

            if (changedObj.uedefaultTabFilter != undefined) {
                if (!changedObj.uedefaultTabFilter.isFirstChange()) {
                    _data = angular.copy(ctrl.data);
                    ctrl.uedefaultTabFilter = changedObj.uedefaultTabFilter.currentValue;
                    if (ctrl.renderElement) {
                        _data.data = angular.copy(filterData(_data.data));
                        if (_data != undefined) {
                            if (_data.data.length > 0) {
                                getVehicleData();
                                CalculateDiffInvBook();
                                ctrl.onUsedCarEquityCapture({ ctx: { bv: ctrl.bookValueSelected, bp: ctrl.bookPercentSelected } });
                                $scope["displayMessage"] = false;
                            }
                            else {
                                $scope["displayMessage"] = true;
                            }
                        }
                        else {
                            $scope["displayMessage"] = true;

                        }
                    }
                    ctrl.wConfig.ShowLoader = false;
                    $scope.ShowLoader = false;
                }
            }

            if (changedObj.showItemList != undefined) {
                if (!changedObj.showItemList.isFirstChange()) {
                    $scope._showItemList = angular.copy(ctrl.showItemList);
                }
            }

            if (changedObj.wConfig != undefined) {           
                $scope["ShowLoader"] = ctrl.wConfig.ShowLoader;
            }

        };


        function setDefault(Obj) {
            if (Obj == null) {
                $($element[0].querySelector('#ddlBookValue'))[0].selectedIndex = 0;
                $($element[0].querySelector('#percentOfBook'))[0].selectedIndex = 1;
            }
            else {
                var _bookValue = Obj.DefaultBookColumn;
                if ($("#ddlBookValue option[value='" + _bookValue + "']").length > 0) {
                    $($element[0].querySelector('#ddlBookValue'))[0].selectedIndex = $("#ddlBookValue option[value='" + _bookValue + "']").index();
                }
                else {
                    $($element[0].querySelector('#ddlBookValue'))[0].selectedIndex = 0;
                }

                var _bookPercent = Obj.DefaultPercentValue / 100;
                if ($("#ddlPercentOfBook option[value='" + _bookPercent + "']").length > 0) {
                    $($element[0].querySelector('#ddlPercentOfBook'))[0].selectedIndex = $("#ddlPercentOfBook option[value='" + _bookPercent + "']").index();
                }
                else {
                    $($element[0].querySelector('#ddlPercentOfBook'))[0].selectedIndex = 1;
                }
            }
        }

        function filterData(data) {

            if (ctrl.uedefaultTabFilter == "ALL") {
                ctrl.red = 0, ctrl.green = 0, ctrl.yellow = 0;
                var temp = _.filter(data, function (num) {
                    var bookValue = $('#ddlBookValue :selected').text();
                    var bookPercent = parseFloat($('#ddlPercentOfBook :selected').val());

                    var diff = num[bookValue] * bookPercent - num["Inventory_Value"];

                    if (diff >= 500) {
                        ctrl.green += 1;
                    } else if (diff <= -500) {
                        ctrl.red += 1;
                    } else if (diff < 500 && diff > -500) {
                        ctrl.yellow += 1;
                    }
                });

                return data;
            }
            else {
                ctrl.red = 0, ctrl.green = 0, ctrl.yellow = 0;
                var temp = _.filter(data, function (num) {
                    var bookValue = $('#ddlBookValue :selected').text();
                    var bookPercent = parseFloat($('#ddlPercentOfBook :selected').val());

                    var diff = num[bookValue] * bookPercent - num["Inventory_Value"];

                    if (diff >= 500) {
                        ctrl.green += 1;
                    } else if (diff <= -500) {
                        ctrl.red += 1;
                    } else if (diff < 500 && diff > -500) {
                        ctrl.yellow += 1;
                    }

                    if (diff >= 500 && ctrl.uedefaultTabFilter == "G") {
                        return true;
                    }
                    else if (diff <= -500 && ctrl.uedefaultTabFilter == "R") {
                        return true;
                    }
                    else if (diff < 500 && diff > -500 && ctrl.uedefaultTabFilter == "Y") {
                        return true;
                    }
                });
                return temp;
            }
        }

        $scope._showItemList = angular.copy(ctrl.showItemList);

        $('#ddlBookValue').on('change', function (e) {
            $scope.ShowLoader = true;
            $scope.$apply();
            var changedvalue = $('#ddlBookValue :selected').text();
            var changedpercent = parseFloat($('#ddlPercentOfBook :selected').val());
            function ct(e) {
                _data.data = angular.copy(filterData(angular.copy(ctrl.data.data)));
                if (_data.data.length > 0) {
                    getVehicleData();
                    CalculateDiffInvBook();
                    $scope["displayMessage"] = false;
                    ctrl.onFilterUsedEquityButtonGroup({ ctx: { r: ctrl.red, g: ctrl.green, y: ctrl.yellow } });
                    ctrl.onUsedCarEquityCapture({ ctx: { bv: changedvalue, bp: changedpercent } });
                    $scope.$apply();
                }
                else {
                    $scope["displayMessage"] = true;
                    ctrl.onFilterUsedEquityButtonGroup({ ctx: { r: ctrl.red, g: ctrl.green, y: ctrl.yellow } });
                    $scope.$apply();
                }

                $scope.ShowLoader = false;
                $scope.$apply();
            }
            $(function () {
                ct(e);
            });

        });

        $('#ddlPercentOfBook').on('change', function (e) {
            $scope.ShowLoader = true;
            $scope.$apply();
            var changedvalue = $('#ddlBookValue :selected').text();
            var changedpercent = parseFloat($('#ddlPercentOfBook :selected').val());

            function ct(e) {
                _data.data = angular.copy(filterData(angular.copy(ctrl.data.data)));
                if (_data.data.length > 0) {
                    getVehicleData();
                    CalculateDiffInvBook();
                    $scope["displayMessage"] = false;
                    ctrl.onFilterUsedEquityButtonGroup({ ctx: { r: ctrl.red, g: ctrl.green, y: ctrl.yellow } });
                    ctrl.onUsedCarEquityCapture({ ctx: { bv: changedvalue, bp: changedpercent } });
                    $scope.$apply();
                }
                else {
                    ctrl.onFilterUsedEquityButtonGroup({ ctx: { r: ctrl.red, g: ctrl.green, y: ctrl.yellow } });
                    $scope.displayMessage = true;
                    $scope.$apply();
                }
                $scope.ShowLoader = false;
                $scope.$apply();
            }
            $(function () {
                ct(e);
            });
        });

        function CalculateDiffInvBook() {
            var bookValue = $('#ddlBookValue :selected').text();
            var bookPercent = parseFloat($('#ddlPercentOfBook :selected').val());
            var colidx = [];
            _.each(datatable.context[0].aoColumns, function (d) {
                if (d.data.startsWith("NADA") || d.data.startsWith("KBB")) {
                    colidx[colidx.length] = d.idx;
                }
            });
            var col = _.find(datatable.context[0].aoColumns, function (d) { if (d.data == bookValue) return d; })

            datatable.columns(colidx).visible(false);
            var column = datatable.column(col.idx);

            column.visible(true);
            var columnLength = 0;
            //Calculation For Diff in Inventory Vs Book
            datatable.rows().every(function (rowIdx, tableLoop, rowLoop) {
                var data = datatable.rows(rowIdx).data();
                var moneyFormatter = getValueFormatter(data[0]);                 
                 
                if (data[0][bookValue] == null) {
                    data[0][bookValue] = 0;
                }
                else if (_data.data[rowIdx] != undefined) {
                    data[0][bookValue] = Math.round(parseFloat(_data.data[rowIdx][bookValue]) * bookPercent);
                    data[0].Inventory_Value = Math.round(parseFloat(_data.data[rowIdx]["Inventory_Value"]));
                }

                data[0]["Diff Inv vs Book"] = moneyFormatter(data[0][bookValue] - data[0].Inventory_Value);
                data[0][bookValue] = moneyFormatter(data[0][bookValue]);
                data[0].Inventory_Value = moneyFormatter(data[0].Inventory_Value);
                columnLength = Object.keys(data[0]).length - 1;
                datatable.row(rowIdx).data(data[0]);
            });

            datatable.order([columnLength, 'asc']).draw();
            AddColorToRow();
            //Add Logic For Sum Calculation
            _.each(datatable.context[0].aoColumns, function (d) {
                if (d.ismeasure == true) {
                    var list = [];
                    datatable.rows().every(function (rowIdx, tableLoop, rowLoop) {
                        var data = datatable.rows(rowIdx).data();
                        list[list.length] = data[0][d.data];
                    });
                    $(th[d.idx]).html(getValueFormatter(d)(Math.round(_.reduce(list, function (d, num) { return d + num; }, 0), 2)));
                }
            });
            $(th[8]).html("<b>Totals:</b>");
        }

        function AddColorToRow() {

            $('#data-table-ue tbody tr').each(function (item, index) {
                var moneyFormatter = getValueFormatter({ DataType: "Money" });

                var val = $('td', index).eq(11).html().replace(/[\$,]/g, '') * 1;
                $('td', index).eq(11).html(moneyFormatter(val));
                var $td = $('td', index).eq(12);

                if (val >= 500) {
                    $td.html('<svg width="20px" height="20px"><circle cx="10" cy="10" r="5" fill="#4c9457" /></svg>');
                }
                else if (val <= -500) {
                    $td.html('<svg width="20px" height="20px"><circle cx="10" cy="10" r="5" fill="#f53d50" /></svg>');
                }
                else if (val < 500 && val > -500) {
                    $td.html('<svg width="20px" height="20px"><circle cx="10" cy="10" r="5" fill="#F4C549" /></svg>');
                }
            });
        }

        $scope.ReportItemClick = function (data) {
            ctrl.onListItemClick({ ctx: data });
        }

        $scope.ReportItemPopUpClick = function (data) {
            ctrl.onListItemClick({ ctx: data });
        }

        var linkHandler = function (elem, e) {
            var _reportType = "Table";
            var _urlpath = "";
            var filterString = "";

            var _id = this.attributes['data-report'].value;
            if (this.attributes['data-reportName'] != undefined) {
                var _rptName = this.attributes['data-reportName'].value;
            }

            if (this.attributes['data-reportType'] != undefined) {
                var _reportType = this.attributes['data-reportType'].value;
            }
            var _filters = this.attributes['data-ReportFilters'];
            if (_filters) {
                _filters = _filters.value.split('|');
            }
            var _index = this.attributes['data-index'];

            if (_index && _filters) {
                _filters.forEach(function (item) {

                    if (_data.data[_index.value] && item != "") {
                        var filterVal = "";
                        //if filter column data type is date then send formated date required by backend procedure
                        if (_.find(_data.columns, function (d) { return d.data == item; }).DataType == "Date") {
                            filterVal = valueFormatter.getformatter('date')(_data.data[_index.value][item], 'yyyy-MM-dd');
                        }
                        else {
                            filterVal = _data.data[_index.value][item];
                        }

                        filterString += filterVal + "|";

                    }
                });
                filterString = filterString.substr(0, filterString.length - 1);
            }
            //Required For PopUp
            if (_id == -1) {
                //var _DealStockNum = _data.data[_index]["DealStockNum"];
                if (this.attributes['data-urlname'] != undefined)
                    _rptName = this.attributes['data-urlname'].value;
                if (this.attributes['data-urlpath'] != undefined) {
                    _reportType = "ExternalLink";
                    var pathArray = this.attributes['data-urlpath'].value.split('<');
                    _urlpath = ""; pathArray.forEach(function (val) { (val.includes('>') ? _urlpath += _data.data[_index.value][val.replace('>', '')] : _urlpath += val) })
                }
                if (this.attributes['data-urlrefkey'] != undefined)
                    _id = this.attributes['data-urlrefkey'].value;
            }

            var _Obj = {
                reportName: _rptName,
                reportId: _id,
                filterData: filterString,
                reportType: _reportType,
                Path: _urlpath
            };

            _Obj.bindObject = _data.data[_index];
            ctrl.onListItemClick({
                ctx: _Obj
            });
            //ctrl.onListItemClick({ ctx: { value: { ReportName: _rptName, ReportID: _id, FilterData: filterString }, Action: 2 } });
        }

        var sumTotalRowCreated = true;
        var $tblHeaderRows;
        var $tblTotalHeaderRows;

        function getVehicleData() {

            if (!angular.equals(_data, {})) {
                var _tableDiv = $element[0].querySelector('#data-table-ue');
                if (datatable != null) {
                    datatable.destroy();
                    $(_tableDiv).empty();
                    $(_tableDiv).find('.LinkedItem').off("click", linkHandler);
                    sumTotalRowCreated = true;
                    datatable.off('responsive-resize');
                    $(_tableDiv).off("click.dtr");
                }

                $(_tableDiv).find('.LinkedItem').off("click", linkHandler);

                function addLinkFunctionality() {
                    $(_tableDiv).find('.LinkedItem').off("click");
                    $(_tableDiv).find('.LinkedItem').on("click", linkHandler);
                }

                $(_tableDiv).one('init.dt', function (a, b, c) {
                    $(_tableDiv).off('draw.dt');
                    $(_tableDiv).off('page.dt');
                    $(_tableDiv).find('.LinkedItem').off("click", linkHandler);

                    addLinkFunctionality();

                    $(_tableDiv).on('page.dt', function (a, b, c) {
                        $(_tableDiv).off('draw.dt');
                        $(_tableDiv).one('draw.dt', function () {
                            $(_tableDiv).find('.LinkedItem').off("click", linkHandler);
                            addLinkFunctionality();

                            _.each($tblHeaderRows, function (head, i) {
                                $tblTotalHeaderRows[i].style.display = head.style.display;
                            });
                            AddColorToRow();
                        });
                    });

                    $(_tableDiv).on('order.dt', function () {
                        AddColorToRow();
                    });

                    $(_tableDiv).one('destroy.dt', function () {
                        $(_tableDiv).find('.LinkedItem').off("click", linkHandler);
                    });
                });

                $(_tableDiv).on('responsive-display.dt', function (e, datatable, row, showHide, update) {
                    addLinkFunctionality();
                });

                datatable = $(_tableDiv).DataTable(getDataTableObject(_data, _tableDiv));

                //first time handling of total header responsiveness


                var headerRowTotalHandler = function (e) {

                    var $selectedRow = $(e.target.parentElement);

                    if ($selectedRow.hasClass("parent")) {
                        $selectedRow.removeClass("parent");
                        $selectedRow.next().remove();
                    }
                    else {
                        var _childRow = '<tr class="child"><td class="child" colspan="' + $tblHeaderRows.length + '"><table>';
                        _.each($tblHeaderRows, function (head, i) {
                            if (head.style.display == "none") {
                                _childRow += '<tr><td><span class="dtr-title" >' + head.innerText + ':' + '</span></td> ' + '<td><span>' + $tblTotalHeaderRows[i].innerText + '</span></td>' + '</tr>';
                            }
                        });

                        _childRow += '</table></td></tr>';


                        $selectedRow.addClass("parent");
                        $selectedRow.after(_childRow);
                    }
                };

                $(_tableDiv).on("click.dtr", "thead>tr:nth-child(2)>th:eq(0)", headerRowTotalHandler);

                if (!sumTotalRowCreated) {
                    $($tblTotalHeaderRows[0]).addClass("removetheadChildStyle");
                    $(_tableDiv).off("click.dtr");
                }
                else {
                    $($tblTotalHeaderRows[0]).removeClass("removetheadChildStyle");
                    if ($.trim($($tblTotalHeaderRows[0]).html()) == "") {
                        $($tblTotalHeaderRows[0]).html("&nbsp");
                    }
                    $(_tableDiv).off("click.dtr");
                    $(_tableDiv).on("click.dtr", "thead>tr:nth-child(2)>th:eq(0)", headerRowTotalHandler);
                }

                datatable.on('responsive-resize', function (e, dt, columns) {
                    $tblHeaderRows = $(_tableDiv).find('thead tr:first th');
                    $tblTotalHeaderRows = $(_tableDiv).find('thead tr:eq(1) th');

                    _.each(columns, function (o, i) {
                        if ($tblTotalHeaderRows[i] != undefined && $tblHeaderRows[i] != undefined)
                            $tblTotalHeaderRows[i].style.display = $tblHeaderRows[i].style.display;
                    });

                    var totalHeaderChildRow = $(this).find("thead tr.child"),
					totalHeaderParentRow = $(this).find("thead tr.parent"),
					anyHiddenColumn = _.some(columns, function (o) { return o == false; });

                    if (totalHeaderChildRow.length > 0) {
                        totalHeaderChildRow.remove();
                    }

                    var _childRow = '<tr class="child"><td class="child" colspan="' + $tblHeaderRows.length + '"><table>';
                    _.each($tblHeaderRows, function (head, i) {
                        if (head.style.display == "none") {
                            _childRow += '<tr><td><span class="dtr-title" >' + head.innerText + ':' + '</span></td> ' + '<td><span>' + $tblTotalHeaderRows[i].innerText + '</span></td>' + '</tr>';
                        }
                    });
                    _childRow += '</table></td></tr>';

                    if (anyHiddenColumn && totalHeaderParentRow != null) {
                        totalHeaderParentRow.after(_childRow);
                    }


                    if (!sumTotalRowCreated) {
                        $($tblTotalHeaderRows[0]).addClass("removetheadChildStyle");
                        $(_tableDiv).off("click.dtr");
                    }
                    else {
                        $($tblTotalHeaderRows[0]).removeClass("removetheadChildStyle");
                        if ($.trim($($tblTotalHeaderRows[0]).html()) == "") {
                            $($tblTotalHeaderRows[0]).html("&nbsp");
                        }
                        $(_tableDiv).off("click.dtr");
                        $(_tableDiv).on("click.dtr", "thead>tr:nth-child(2)>th:eq(0)", headerRowTotalHandler);
                    }

                });
            }
        }

        /*Added to get the color circle based on Diff Inv vs Book for responsive screen*/
        var getDiffData = function (val) {
            val = val.replace(/[\$,]/g, '') * 1;
            if ((val * 1) >= 500) {
                return '<svg width="20px" height="20px"><circle cx="10" cy="10" r="5" fill="#4c9457" /></svg>';
            }
            else if ((val * 1) <= -500) {
                return '<svg width="20px" height="20px"><circle cx="10" cy="10" r="5" fill="#f53d50" /></svg>';
            }
            else if ((val * 1) < 500 && (val * 1) > -500) {
                return '<svg width="20px" height="20px"><circle cx="10" cy="10" r="5" fill="#F4C549" /></svg>';
            }
        }

        function getDataTableObject(data1, tableElm) {
            if ($(tableElm).find('thead').length == 0) {
                $(tableElm).html('<thead></thead>');
            }

            var _$thead = $(tableElm).find('thead');

            _$thead.html('');
            var ColumCount = Object.keys(data1.data[0]).length;
            var tr = "<tr>";
            data1.columns = [];

            for (var i = 0; i < ColumCount; i++) {

                data1.columns[i] = { title: Object.keys(data1.data[0])[i].replace(/_/g, " "), data: Object.keys(data1.data[0])[i], DataType: "" };

                if (data1.columns[i].data == "Stock_Number") {
                    data1.columns[i].IsLink = true;
                    data1.columns[i].ReportRefKey = -1;
                    data1.columns[i].NextReportName = "Book Out";
                    data1.columns[i].UrlPath = "/popUps/bookout.aspx?stock=<Stock_Number>";
                }

                if (data1.columns[i].title == "PT") data1.columns[i].title = "P/T";

                if (data1.columns[i].data == "Age" || data1.columns[i].data == "Year" || data1.columns[i].data == "Miles") {
                    data1.columns[i]["DataType"] = "Int";
                }                
                else if (data1.columns[i].data == "Inventory_Value" || data1.columns[i].data.startsWith("NADA")  || data1.columns[i].data.startsWith("KBB") || data1.columns[i].data == "Diff Inv vs Book") {
                    data1.columns[i]["DataType"] = "Money";
                    data1.columns[i]["ismeasure"] = true;
                }
                else {
                    data1.columns[i]["DataType"] = "String";
                }

                tr += '<th style="vertical-align: top;"></th>';
            }

            //Add custom Control
            data1.columns[data1.columns.length] = {
                data: "Color",
                defaultContent: ''
            };
            tr += "<th style='width:7px'></th>";


            tr += "</tr>";
            _$thead.append(tr);
            _$thead.append(tr);
            var _linkedColumndefs = [];
            th = _$thead.find('tr:gt(0)').find('th');
            var Columns = function (cc) {
                var arr = [];
                for (var i = 0; i < cc; i++) {
                    var fnFormatter = getValueFormatter(data1.columns[i]);
                    var colObj = {
                        "targets": [i],
                        render: function (data, type, full, meta) {
                            var fnFormatter = getValueFormatter(data1.columns[meta.col]);
                            if (data1.columns[meta.col].IsLink == true) {
                                return '<span class="LinkedItem CustomLinks">' + fnFormatter(data) + '</span>';
                            }
                            return fnFormatter(data);
                        }
                    }
                    //if (i == cc - 1) {
                    //    colObj.orderable=true;
                    //    colObj.targets = cc - 2;
                    //}
                    arr.push(colObj);
                    if (data1.columns[i].data.startsWith("NADA") || data1.columns[i].data.startsWith("KBB")) {
                        arr[i].visible = false;
                    }
                    if (data1.columns[i].data == "NADA_Clean_Trade_In")
                        arr[i].visible = true;


                    if (data1.columns[i].IsLink) {
                        _linkedColumndefs.push(i);
                    }
                }
                return arr;
            }

            var _coldefs = Columns(ColumCount);
            return {
                "data": data1.data,
                "columns": data1.columns,
                "iDisplayLength": 50,
                "lengthChange": false,
                "bFilter": false,
                "columnDefs": _coldefs,
                "createdRow": function (row, data, dataIndex) {
                    _linkedColumndefs.forEach(function (item, index) {
                        var _$tCell = $(row).find('td:eq(' + item + ')');
                        if (_$tCell) {
                            _$tCell.attr('data-index', dataIndex);
                            _$tCell.attr('data-ReportFilters', data1.columns[item].ReportFilters);
                            _$tCell.attr('data-report', data1.columns[item].ReportRefKey);
                            _$tCell.attr('data-reportName', data1.columns[item].NextReportName);
                            _$tCell.attr('data-reportType', data1.columns[item].ReportType);
                            _$tCell.attr('data-urlname', data1.columns[item].UrlName);
                            _$tCell.attr('data-urlpath', data1.columns[item].UrlPath);
                            _$tCell.attr('data-urlrefkey', data1.columns[item].UrlRefKey);
                            _$tCell.on("click", linkHandler);
                        }
                    });
                },
                "retrieve": true,
                "autoWidth": false,
                "responsive": {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRow,
                        renderer: function (api, rowIdx, columns) {
                            var data = $.map(columns, function (col, i) {
                                var linkColumnAttributeData = "", linkColumnHeaderAttributeData = "";
                                if (col.hidden) {
                                    if (data1.columns[col.columnIndex].IsLink) {
                                        linkColumnAttributeData = " data-index='" + col.columnIndex +
										"' data-reportfilters='" + data1.columns[col.columnIndex].ReportFilters +
										"' data-report='" + data1.columns[col.columnIndex].ReportRefKey +
										"' data-reportname='" + data1.columns[col.columnIndex].NextReportName +
										"' data-reporttype='" + data1.columns[col.columnIndex].ReportType +
										"' data-urlname='" + data1.columns[col.columnIndex].UrlName +
										"' data-urlpath='" + data1.columns[col.columnIndex].UrlPath +
										"' data-urlrefkey='" + data1.columns[col.columnIndex].UrlRefKey + "' ";
                                    }

                                    if (data1.columns[col.columnIndex].IsLinkOnHeader) {
                                        linkColumnAttributeData = " data-report='" + data1.columns[col.columnIndex].ReportRefKey +
										"' data-reportname='" + data1.columns[col.columnIndex].NextReportName +
										"' data-reporttype='" + data1.columns[col.columnIndex].ReportType + "' ";
                                    }
                                }

                                return col.hidden ?
									'<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
										'<td ' + linkColumnAttributeData + ' ><span class="dtr-title ' + (data1.columns[col.columnIndex].IsLinkOnHeader ? 'LinkedItem CustomLinks' : '') + '" >' + col.title + ':' + '</span></td> ' +
										'<td ' + linkColumnAttributeData + '  ><span ' + (data1.columns[col.columnIndex].IsLink ? 'class="LinkedItem CustomLinks"' : '') + '>' + ((col.hidden && col.columnIndex == 24) ? getDiffData(columns[23].data) : col.data) + '</span></td>' +
									'</tr>' :
									'';
                            }).join('');

                            return data ?
								$('<table/>').append(data) :
								false;
                        }
                    }
                },
                "orderCellsTop": true,
                "deferRender": true,
                "order": [],
                "oLanguage": { "sEmptyTable": "No records available" },
                "bSortCellsTop": true,
                "headerCallback": function (thead, data, start, end, display) {
                    $tblHeaderRows = this.find('thead tr:first th');
                    $tblTotalHeaderRows = this.find('thead tr:eq(1) th');
                }
            }
        }


        function getValueFormatter(col) {

            if (col.DataType === 'Number') {
                return valueFormatter.getformatter(col.DataType.toLowerCase());
            }
            else if (col.DataType === 'Money') {
                return valueFormatter.getformatter(col.DataType.toLowerCase());
            }
            else if (col.DataType === 'Int') {
                return valueFormatter.getformatter(col.DataType.toLowerCase());
            }
            else if (col.DataType === 'Percent') {
                return valueFormatter.getformatter(col.DataType.toLowerCase());
            }
            else if (col.DataType === 'Decimal1') {
                return valueFormatter.getformatter(col.DataType.toLowerCase());
            }
            else if (col.DataType === 'Decimal2') {
                return valueFormatter.getformatter(col.DataType.toLowerCase());
            }
            else if (col.DataType === 'Decimal3') {
                return valueFormatter.getformatter(col.DataType.toLowerCase());
            }
            else if (col.DataType === 'Date') {
                return function (val) {
                    return valueFormatter.getformatter('date')(val, 'MM/dd/yyyy');
                }
            }
            else {
                return function (val) {
                    return val;
                };
            }
        }
    }

})(angular.module("dealerWizard"));
