﻿(function (app) {
    'use strict';

    app.component('buttonGroup',
        {
            templateUrl: "/Scripts/app/components/buttonGroup.html",
            controller: ['$scope', '$element', '$attrs', buttonGroupCtrl],
            bindings: {
                data: "<",
                defaultTabFilter: "<",
                onChange: "&"
            }
        });

    function buttonGroupCtrl($scope, $element, $attrs) {
        var ctrl = this;
        $scope.buttonList = [];

        ctrl.$onInit = function () {
        };

        ctrl.onButtonClick = function (obj) {
            angular.forEach($scope.buttonList, function (val) {
                if (obj.Id == val.Id) {
                    val.Selected = true;
                } else {
                    val.Selected = false;
                }
            });

            $scope.buttonList.IsClick = true;
            ctrl.onChange({ ctx: obj });
        }

        ctrl.$onChanges = function (changedObj) {
            if (ctrl.data && !$scope.buttonList.IsClick) {
                $scope.buttonList = [];
                angular.forEach(ctrl.data, function (val, i) {
                    $scope.buttonList.push({
                        Id: "grpButton" + i, Value: val.value, Selected: ((val.value.toLowerCase() == ctrl.defaultTabFilter.toLowerCase()) ? true : false), color:val.color
                    });
                });
            }
            $scope.buttonList.IsClick = false;
        }
    }
})(angular.module('dealerWizard'));;
