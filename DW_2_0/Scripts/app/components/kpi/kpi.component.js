﻿(function (app) {
    'use strict';

    app.component("kpi", {
        templateUrl: "/Scripts/app/components/kpi/kpi.html",
        controller: ['$scope', '$element', '$attrs', kpiCtrl],
        bindings: {
            data: "<"
        }
    });

    function kpiCtrl($scope, $element, $attrs) {
        var ctrl = this;
        $scope.kpiData = {};

        ctrl.$onInit = function () {
            $scope.kpiData = angular.copy(ctrl.data);
        }

        ctrl.$onChanges = function (ChangeObject) {            
            if (!ChangeObject.data.isFirstChange()) {
                $scope.kpiData = angular.copy(ctrl.data);
            }
        }        
    }

})(angular.module("dealerWizard"));