﻿(function (app) {
    "use strict"
    app.component("templatePopup", {
        templateUrl: "/Scripts/app/components/popup/templatePopup.html",
        controller: ['$scope', '$element', '$attrs', templatePopupCtrl],
        bindings: {
            showpopup: "<",
            title: "<",
            onCloseClick: "&",
            onTemplateSelection:"&"
        }
    });

    function templatePopupCtrl($scope, $element, $attrs) {
        var ctrl = this;
        
        ctrl.templateList = [
        {
            name: "template1",
            templateInfo:[{
                templateSize: "100%",
                size: 12
            }],
            templateSelected: true,
            templatePath: "/Scripts/app/components/userdefinedtemplating/usertemplateloader/template1.html"
        },
        {
            name: "template2",
            templateInfo: [{                 
                templateSize: "50%",
                size: 6
            }, {                
                templateSize: "50%",
                size: 6
            }],
            templateSelected: false,
            templatePath: "/Scripts/app/components/userdefinedtemplating/usertemplateloader/template2.html"
        },
        {
            name: "template3",
            templateInfo: [{                
                templateSize: "30%",
                size: 3
            }, {                
                templateSize: "70%",
                size: 9
            }],
            templateSelected: false,
            templatePath: "/Scripts/app/components/userdefinedtemplating/usertemplateloader/template3.html"
        },
        {
            name: "template4",
            templateInfo: [{
                templateSize: "70%",
                size: 9
            }, {
                templateSize: "30%",
                size: 3
            }],
            templateSelected: false,
            templatePath: "/Scripts/app/components/userdefinedtemplating/usertemplateloader/template4.html"
        },
        {
            name: "template5",
            templateInfo: [{
                templateSize: "25%",
                size: 3
            }, {
                templateSize: "25%",
                size: 3
            },
            {
                templateSize: "25%",
                size: 3
            }, {
                templateSize: "25%",
                size: 3
            }],
            templateSelected: false,
            templatePath: "/Scripts/app/components/userdefinedtemplating/usertemplateloader/template5.html"
        }, {
            name: "template6",
            templateInfo: [{
                templateSize: "25%",
                size: 3
            }, {
                templateSize: "50%",
                size: 6
            },
            {
                templateSize: "25%",
                size: 3
            }],
            templateSelected: false,
            templatePath: "/Scripts/app/components/userdefinedtemplating/usertemplateloader/template6.html"
        }];


        ctrl.$onInit = function () {
            ctrl.showpopup = ctrl.showpopup;            
            ctrl.title = ctrl.title;
        }

        ctrl.$onChanges = function (ChangeObject) {
            if (ChangeObject.showpopup) {
                $scope.showpopup = ctrl.showpopup;
            }

            if (ChangeObject.title) {
                $scope.title = ctrl.title;
            }
        }

        $scope.setTemplate = function (data) {
            console.log(data);
            let oldSelectedElem = _.find(ctrl.templateList, function (d) { return d.templateSelected == true });
            oldSelectedElem.templateSelected = false;

            let newSelectedElem = _.find(ctrl.templateList, function (d) { return d.name == data.name });
            newSelectedElem.templateSelected = true;

        }

        $scope.loadTemplate = () => {
            let selectedTemplate = _.find(ctrl.templateList, function (d) { return d.templateSelected == true });
            ctrl.onTemplateSelection({ ctx: selectedTemplate });
            ctrl.onCloseClick();
            ctrl.showpopup = false;
            $scope.showpopup = false;
        };

        $scope.popUpCloseClick = function () {
            ctrl.showpopup = false;
            $scope.showpopup = false;
            ctrl.onCloseClick();
        }
    }
})(angular.module("dealerWizard"));