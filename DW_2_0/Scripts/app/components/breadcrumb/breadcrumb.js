﻿(function (app) {

    app.component("breadcrumb", {
        templateUrl: "/Scripts/app/components/breadcrumb/breadcrumb.html",
        controller: ['$scope', '$element', '$attrs', breadcrumbCtrl],
        bindings: {
            items: "<",
            onListItemClick: "&"
        }
    });

    function breadcrumbCtrl($scope, $element, $attrs) {
        var ctrl = this;
        //$scope._items = [];

        ctrl.$onInit=function(){
            ctrl.items = ctrl.items;
        }

        ctrl.$onChanges = function (ChangeObject) {
            if (!ChangeObject.items.isFirstChange()) {
                 ctrl.items = ctrl.items;
            }
        }

        $scope.OpenReport = function (data) {

            if (ctrl.items.length > 1) {
                var index = ctrl.items.map(function (e) { return e.reportName; }).indexOf(data.reportName);
                index = (index == 0) ? 1 : (index + 1);
                ctrl.items.splice(index);
                ctrl.items = ctrl.items;
            }

            ctrl.onListItemClick({ctx:data});
        }
    }

})(angular.module("dealerWizard"));