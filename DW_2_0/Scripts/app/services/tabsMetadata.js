﻿(function (app) {
    'use strict';

    app.factory('tabsMetaDataService', tabsMetaDataService);

    tabsMetaDataService.$inject = ['apiService', 'dealerService', '$q'];

    function tabsMetaDataService(apiService, dealerService, $q) {

        var newcar = [], usedcar = [],
			kpiDataAPIURL = "api/getKpiData",
			smallMultipleAPIURL = "api/getsalesmeasures",
			reportAPIURL = "api/getreportdata";

        var tabsObjects = {
            newcar: {
                name: 'New Car Sales',
                widgets: [
                    {
                        config: [
                            { key: "Tiles1", format: 'money', ReverseOrder: 0, report: { reportId: 3 } },
                            { key: "Tiles2", format: 'money', ReverseOrder: 0, report: { reportId: 2 } },
                            { key: "Tiles3", format: 'numberOneDecimal', ReverseOrder: 0, report: { reportId: 2 } },
                            { key: "Tiles4", format: 'number', ReverseOrder: 1, report: { reportId: 6, defaultFilter: 'New' } },
                            { key: "Tiles5", format: 'number', ReverseOrder: 1, report: { reportId: 1 } },
                            { key: "Tiles6", format: 'number', ReverseOrder: 1, report: { reportId: 11 } }
                        ],
                        url: kpiDataAPIURL,
                        type: 'gauges'
                    },
                     {
                         config: [
                            { key: "Units Del", format: 'number', report: { reportId: 8 } },
                            { key: "Sales Gross", format: 'money', report: { reportId: 3 } },
                            { key: "Sales PRU", format: 'money', report: { reportId: 3 } },
                            { key: "FI Gross", format: 'money', report: { reportId: 2 } },
                            { key: "FI PRU", format: 'money', report: { reportId: 2 } },
                            { key: "Avg Days To Sale", format: 'number', report: { reportId: 11 } }
                         ],
                         url: smallMultipleAPIURL,
                         type: 'bars'
                     },
                 {
                     config: [],
                     url: reportAPIURL,
                     type: 'table'
                 }
                ],
                reports: [],
                defaultReport: { defaultReportId: 7 }
            },
            usedcar: {
                name: 'Used Car Sales',
                widgets: [
                    {
                        config: [
                            { key: "Tiles1", format: 'money', ReverseOrder: 0, report: { reportId: 8 } },
                            { key: "Tiles2", format: 'money', ReverseOrder: 0, report: { reportId: 2 } },
                            { key: "Tiles3", format: 'numberOneDecimal', ReverseOrder: 0, report: { reportId: 2 } },
                            { key: "Tiles4", format: 'number', ReverseOrder: 1, report: { reportId: 6, defaultFilter: 'Used' } },
                            { key: "Tiles5", format: 'number', ReverseOrder: 1, report: { reportId: 23 } },
                            { key: "Tiles6", format: 'money', ReverseOrder: 0, report: { reportId: 24 } }
                        ],
                        url: kpiDataAPIURL,
                        type: 'gauges'
                    },
                     {
                         config: [
                            { key: "Units Del", format: 'number', report: { reportId: 8 } },
                            { key: "Sales Gross", format: 'money', report: { reportId: 3 } },
                            { key: "Sales PRU", format: 'money', report: { reportId: 3 } },
                            { key: "FI Gross", format: 'money', report: { reportId: 2 } },
                            { key: "FI PRU", format: 'money', report: { reportId: 2 } },
                            { key: "Avg Days To Sale", format: 'number', report: { reportId: 33 } }
                         ],
                         url: smallMultipleAPIURL,
                         type: 'bars'
                     },
                 {
                     config: [],
                     url: reportAPIURL,
                     type: 'table'
                 }
                ],
                reports: [],
                defaultReport: { defaultReportId: 7 }
            },
            variable: {
                name: 'Variable',
                widgets: [
            {
                config: [
                                { key: "Tiles1", format: 'number', ReverseOrder: 1, report: { reportId: 6, defaultFilter: 'New' } },
                                { key: "Tiles2", format: 'number', ReverseOrder: 1, report: { reportId: 6, defaultFilter: 'Used' } },
                                { key: "Tiles3", format: 'money', ReverseOrder: 0, report: { reportId: 3 } },
                                { key: "Tiles4", format: 'money', ReverseOrder: 0, report: { reportId: 2 } },
                                { key: "Tiles5", format: 'numberOneDecimal', ReverseOrder: 0, report: { reportId: 2 } },
                                { key: "Tiles6", format: 'money', ReverseOrder: 0, report: { reportId: 24 } }
                ],
                url: kpiDataAPIURL,
                type: 'gauges'
            },
        {
            config: [
                            { key: "Units Del", format: 'number', report: { reportId: 8 } },
                            { key: "Sales Gross", format: 'money', report: { reportId: 3 } },
                            { key: "Sales PRU", format: 'money', report: { reportId: 3 } },
                            { key: "FI Gross", format: 'money', report: { reportId: 2 } },
                            { key: "FI PRU", format: 'money', report: { reportId: 2 } },
                            { key: "Whlsale Gross", format: 'money', report: { reportId: 24 } }
            ],
            url: smallMultipleAPIURL,
            type: 'bars'
        },
        {
            config: [],
            url: reportAPIURL,
            type: 'table'
        }
                ],
                reports: [],
                defaultReport: { defaultReportId: 7 }
            },
            fi: {
                name: 'Finance & Insurance Sales',
                widgets: [
                    {
                        config: [
                            { key: "Tiles1", format: 'money', ReverseOrder: 0, report: { reportId: 2 } },
                            { key: "Tiles2", format: 'numberOneDecimal', ReverseOrder: 0, report: { reportId: 2 } },
                            { key: "Tiles3", format: 'percent', ReverseOrder: 0, report: { reportId: 2 } },
                            { key: "Tiles4", format: 'percent', ReverseOrder: 0, report: { reportId: 2 } },
                            { key: "Tiles5", format: 'percent', ReverseOrder: 0, report: { reportId: 2 } },
                            { key: "Tiles6", format: 'percent', ReverseOrder: 0, report: { reportId: 2 } }
                        ],
                        url: kpiDataAPIURL,
                        type: 'gauges'
                    },
                     {
                         config: [
                            { key: "FI Gross", format: 'money', report: { reportId: 2 } },
                            { key: "FI PRU", format: 'money', report: { reportId: 2 } },
                            { key: "FI Fin Pct", format: 'percent', report: { reportId: 2 } },
                            { key: "FI Svc Cntrct Pct", format: 'percent', report: { reportId: 2 } },
                            { key: "FI GAP Pct", format: 'percent', report: { reportId: 2 } },
                            { key: "FI Maint Pct", format: 'percent', report: { reportId: 2 } }
                         ],
                         url: smallMultipleAPIURL,
                         type: 'bars'
                     },
                 {
                     config: [],
                     url: reportAPIURL,
                     type: 'table'
                 }
                ],
                reports: [],
                defaultReport: { defaultReportId: 2 }
            },
            inventory: {
                name: 'Inventory',
                widgets: [
                    {
                        config: [
                            { key: "Tiles1", format: 'number', ReverseOrder: 1, report: { reportId: 6, defaultFilter: 'New' } },
                            { key: "Tiles2", format: 'number', ReverseOrder: 1, report: { reportId: 6, defaultFilter: 'Used' } },
                            { key: "Tiles3", format: 'number', ReverseOrder: 1, report: { reportId: 1 } },
                            { key: "Tiles4", format: 'number', ReverseOrder: 1, report: { reportId: 23 } },
                            { key: "Tiles5", format: 'number', ReverseOrder: 1, report: { reportId: 11 } },
                            { key: "Tiles6", format: 'number', ReverseOrder: 1, report: { reportId: 33 } }
                        ],
                        url: kpiDataAPIURL,
                        type: 'gauges'
                    },
                     {
                         config: [
                            { key: "Inv Cnt New", format: 'number', report: { reportId: 6 } },
                            { key: "Inv Value New", format: 'money', report: { reportId: 6 } },
                            { key: "Inv Days Supl New", format: 'number', report: { reportId: 1 } },
                            { key: "Inv Cnt Used", format: 'number', report: { reportId: 6 } },
                            { key: "Inv Value Used", format: 'money', report: { reportId: 6 } },
                            { key: "Inv Days Supl Used", format: 'number', report: { reportId: 23 } }
                         ],
                         url: smallMultipleAPIURL,
                         type: 'bars'
                     },
                 {
                     config: [],
                     url: reportAPIURL,
                     type: 'table'
                 }
                ],
                reports: [],
                defaultReport: { defaultReportId: 6 }
            },
            service: {
                name: 'Service Department',
                widgets: [
                    {
                        config: [
                            { key: "Tiles1", format: 'money', ReverseOrder: 0, report: { reportId: 63 } },
                            { key: "Tiles2", format: 'money', ReverseOrder: 0, report: { reportId: 63, defaultFilter: 'CP' } },
                            { key: "Tiles3", format: 'money', ReverseOrder: 0, report: { reportId: 63 } },
                            { key: "Tiles4", format: 'money', ReverseOrder: 0, report: { reportId: 66, defaultFilter: 'CP' } },
                            { key: "Tiles5", format: 'numberOneDecimal', ReverseOrder: 0, report: { reportId: 66 } },
                            { key: "Tiles6", format: 'numberOneDecimal', ReverseOrder: 0, report: { reportId: 66, defaultFilter: 'CP' } }
                        ],
                        url: kpiDataAPIURL,
                        type: 'gauges'
                    },
                     {
                         config: [
                            { key: "Svc Labor Sales", format: 'money', report: { reportId: 63 } },
                            { key: "Svc Labor Gross", format: 'money', report: { reportId: 63 } },
                            { key: "Svc Total Hours Sold", format: 'number', report: { reportId: 63 } },
                            { key: "Svc RO Cnt", format: 'number', report: { reportId: 63 } },
                            { key: "Svc CP RO Cnt", format: 'number', report: { reportId: 63 } },
                            { key: "Svc One Line RO Cnt", format: 'number', report: { reportId: 63 } }
                         ],
                         url: smallMultipleAPIURL,
                         type: 'bars'
                     },
                 {
                     config: [],
                     url: reportAPIURL,
                     type: 'table'
                 }
                ],
                reports: [],
                defaultReport: { defaultReportId: 63 }
            },
            dealership: {
                name: 'Dealership Overview',
                widgets: [
                    {
                        config: [
                            { key: "Tiles1", format: 'money', ReverseOrder: 0, report: { reportId: 3 } },
                            { key: "Tiles2", format: 'money', ReverseOrder: 0, report: { reportId: 2 } },
                            { key: "Tiles3", format: 'money', ReverseOrder: 0, report: { reportId: 63 } },
                            { key: "Tiles4", format: 'numberOneDecimal', ReverseOrder: 0, report: { reportId: 66 } },
                            { key: "Tiles5", format: 'number', ReverseOrder: 1, report: { reportId: 1 } },
                            { key: "Tiles6", format: 'number', ReverseOrder: 1, report: { reportId: 23 } }
                        ],
                        url: kpiDataAPIURL,
                        type: 'gauges'
                    },
                     {
                         config: [
                            { key: "Units Del", format: 'number', report: { reportId: 7 } },
                            { key: "Sales Gross", format: 'money', report: { reportId: 3 } },
                            { key: "FI Gross", format: 'money', report: { reportId: 2 } },
                            { key: "Svc ELR", format: 'money', report: { reportId: 63 } },
                            { key: "Svc Total Hrs Sold", format: 'number', report: { reportId: 63 } },
                            { key: "Svc Total Hrs RO", format: 'numberOneDecimal', report: { reportId: 66 } }
                         ],
                         url: smallMultipleAPIURL,
                         type: 'bars'
                     },
                 {
                     config: [],
                     url: reportAPIURL,
                     type: 'table'
                 }
                ],
                reports: [],
                defaultReport: { defaultReportId: 64 }
            }
        };

        var extendedTabFunc = {
            getDefaultReport: function () {
                var localDefaultReportId = this.defaultReport.defaultReportId;
                return angular.copy(_.filter(this.reports, function (val) { return val.reportId == localDefaultReportId; })[0]);
            },
            getReportByReportId: function (key) {
                return angular.copy(_.filter(this.reports, function (val) { return val.reportId == key; })[0]);
            },
            getGaugeMetadata: function () {

                return angular.copy(_.filter(this.widgets, function (d) { if (d.type == "gauges") return d; })[0]);
            },
            getBarsMetadata: function () {
                return angular.copy(_.filter(this.widgets, function (d) { if (d.type == "bars") return d; })[0]);
            },
            getTableMetadata: function () {
                return angular.copy(_.filter(this.widgets, function (d) { if (d.type == "table") return d; })[0]);
            }
        }

        function getTabObject(tabs) {
            var deferred = $q.defer();
            if (tabsObjects[tabs.key].reports.length == 0) {
                apiService.get('api/getreportobject?dealerid=5&departmentcode=' + tabs.departmentCode, null, function (data) {
                    angular.copy(data.data, tabsObjects[tabs.key].reports);
                    var extendedTabObj = angular.extend({}, tabsObjects[tabs.key], extendedTabFunc);
                    deferred.resolve(extendedTabObj);
                });
                return deferred.promise;
            }
            else {
                var extendedTabObj = angular.extend({}, tabsObjects[tabs.key], extendedTabFunc);
                deferred.resolve(extendedTabObj);
                return deferred.promise;
            }
        }

        return {
            getTabObject: getTabObject
        };
    }

})(angular.module('dwServiceModule'));