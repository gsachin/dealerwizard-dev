﻿import toastr from 'toastr';

function notificationService() {
    var notification = {
        error: function (msg) {
            toastr.error(msg);
        },
        success: function (msg) {
            toastr.success(msg);
        },
        warning: function (msg) {
            toastr.warning(msg);
        },
        info: function (msg) {
            toastr.info(msg);
        }
    };

    return notification;
}

angular.module('dwServiceModule').factory('notificationService', notificationService);
