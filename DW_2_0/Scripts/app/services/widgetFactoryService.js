﻿import uuidV4 from 'uuid/v4';

//widgetFactoryFunc.$inject = ['']

const widgetFactoryFunc = () => {

    let widgetFactory = {
        getWidget: (widget) => {
            let _local = { id: uuidV4(), data: {} };

            switch (widget.type) {
                case 'kpi':
                    let _lData = {
                        title: "FI PRU",
                        legendFor: "FI PRU",
                        valueOfLegend: "2.4"
                    };
                    _local.data = { wData: _lData };
                    _local.markUp = '<div class="panel panel-default center" ><' + widget.type + ' data="wData" ></' + widget.type + '></div>';
                    break;
                case 'gauge':
                    var _lData = {
                        data: {data: { title: "Sales PRU", ActualValue: 46, RangeMax: 80, RangeMin: 40 }},
                        config: { ShowLoader: false, valueFormat:"money"}
                    };
                    _local.data = { wData: _lData.data, config: _lData.config };
                    _local.markUp = '<div class="center panel panel-default"><' + widget.type + ' data="wData", w-Config="config" ></' + widget.type + '><div style="clear:both" /></div>';
                    break;
                case 'table':
                    _local.markUp = '<div class="panel panel-default"><activitytable reportKey="' + widget.context.reportKey + '"></activitytable><div style="clear:both" /></div>';
                    break;
                default:
                    _local.markUp = '<div>'+widget.type+'</div>';
                    break;
            }
            return _local;
        }
    }
    
    return widgetFactory;
};


angular.module('dwServiceModule')
.factory('widgetFactory', widgetFactoryFunc);