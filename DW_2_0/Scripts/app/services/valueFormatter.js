(function (app) {
    'use strict';

    app.factory('valueFormatter', valueFormatterFunc);

    valueFormatterFunc.$inject = ['$filter'];

    function valueFormatterFunc($filter) {
        var _valueFormatter = {
            "money": function (val) {
                return $filter('currency')(val, '$', 0);
            },
            "numberOneDecimal": function (val) {
                return $filter('number')(val, 1);
            },
            "decimal1": function (val) {
                return $filter('number')(val, 1);
            },
            "numberTwoDecimal": function (val) {
                return $filter('number')(val, 2);
            },
            "decimal2": function (val) {
                return $filter('number')(val, 2);
            },
            "numberThreeDecimal": function (val) {
                return $filter('number')(val, 3);
            },
            "decimal3": function (val) {
                return $filter('number')(val, 3);
            },
            "number": function (val) {
                return $filter('number')(val, 0);
            },
            "date": function (val, format) {
                var dateVal = (typeof val == "object" ? val : new Date(val));
                return $filter('date')(dateVal, format);
            },
            "int": function (val) {
                return val;
            },
            "percent": function (val) {
			    return $filter('number')(val, 0) + "%";
			},
			"decimal1percent": function(val) {
			    return $filter('number')(val, 1) + "%";
			},
			"decimal2percent": function(val) {
			    return $filter('number')(val, 2) + "%";
            },
            getformatter: function (key) {
                return this[key];
            }
        }

        return _valueFormatter;
    }

})(angular.module('dwServiceModule'));