﻿  
function apiService($http, $location, $rootScope, $q, $window, dwEnvironment,$route) {
    $rootScope.baseUrl = dwEnvironment.apiUrl;
    var prefixURL = "~/analytics%23";
    var service = {
        get: get,
        post: post
    };

    function createCookie(name, value, days) {
        var expires;

        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date.toGMTString();
        } else {
            expires = "";
        }
        document.cookie = encodeURIComponent(name) + "=" + encodeURIComponent(value) + expires + "; path=/";
    }

    function readCookie(name) {
        var nameEQ = encodeURIComponent(name) + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) === ' ') c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) === 0) return decodeURIComponent(c.substring(nameEQ.length, c.length));
        }
        return null;
    }

    function eraseCookie(name) {
        createCookie(name, "", -1);
    }

       

    function get(url, config, success, failure) {

        if (sessionStorage.user == null) {
            var tokenFromCookies = readCookie("UserToken");

            if (readCookie("UserToken") != null) {
                sessionStorage.user = tokenFromCookies.replace("token=", "");
                eraseCookie("UserToken");
            }
        }

        if (sessionStorage.user == null) {           
            //$rootScope.previousState = $location.path();
            //$location.path('/login');   
               
                $window.location.href = 'Membership/Login.aspx#/ReturnURL=' + prefixURL + $route.current.$$route.originalPath;
                sessionStorage.clear();
                
        }
        else {
            var currentUser = '';

            var config = {
                headers: {}
            };
            var currentUser = sessionStorage.user;
            if (currentUser != null) {
                config.headers['Authorization'] = 'bearer' + ' ' + currentUser;
            }

            return $http.get($rootScope.baseUrl + url, config)
                    .then(function (result) {
                        success(result);
                    }, function (error) {
                        if (error.status == '401') {
                            $window.location.href = 'Membership/Login.aspx?ReturnURL=' + prefixURL + $route.current.$$route.originalPath;
                            sessionStorage.clear();
                        }
                        else if (failure != null) {
                            failure(error);
                        }
                    });
        }
               
    }
		
	function post(url, data, config, success, failure) {

        if (sessionStorage.user == null) {    
            $window.location.href = 'Membership/Login.aspx?ReturnURL=' + prefixURL + $route.current.$$route.originalPath;
        }
        else {
            var currentUser = '';

            var config = {
                headers: {}
            };
            var currentUser = sessionStorage.user;
            if (currentUser != null) {
                config.headers['Authorization'] = 'bearer' + ' ' + currentUser;
            }

            return $http.post($rootScope.baseUrl + url, data, config)
                    .then(function (result) {
                        success(result);
                    }, function (error) {
                        if (error.status == '401') {
                            $window.location.href = 'Membership/Login.aspx?ReturnURL=' + prefixURL + $route.current.$$route.originalPath;
                        }
                        else if (failure != null) {
                            failure(error);
                        }
                    });
        }

    }
    return service;
}

apiService.$inject = ['$http', '$location', '$rootScope', '$q', '$window', '__dw_env', '$route'];

export default apiService;


