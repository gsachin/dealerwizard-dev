(function (app) {
    'use strict';

    app.factory('businessNameServices', businessNameServicesFunc);

    businessNameServicesFunc.$inject = ['$q', 'apiService'];

    function businessNameServicesFunc($q, apiService) {
        var businessNames = {
            List: [
                       { ShortName: "VehicleMiles", UserFriendlyName: "Miles" },
                       { ShortName: "Whlsale Gross", UserFriendlyName: "Whlsale Profit Loss" },
                       { ShortName: "VehicleEquity", UserFriendlyName: "Equity" },
                       { ShortName: "Vehicle Book Value", UserFriendlyName: "Book Value" },
                       { ShortName: "Veh Age", UserFriendlyName: "Age" },
                       { ShortName: "Units Del", UserFriendlyName: "Units Delivered" },
                       { ShortName: "Svc Technician Count", UserFriendlyName: "Svc Tech Count" },
                       { ShortName: "Svc RO Cnt", UserFriendlyName: "RO Count" },
                       { ShortName: "Svc Parts Cost", UserFriendlyName: "Parts Cost" },
                       { ShortName: "Svc Misc Sales", UserFriendlyName: "Svc Misc Sales" },
                       { ShortName: "Svc Misc Gross", UserFriendlyName: "Svc Misc Gross" },
                       { ShortName: "Svc Hrs Per Day", UserFriendlyName: "Svc Hrs Per Day" },
                       { ShortName: "Svc GOG Gross", UserFriendlyName: "Gas Oil Grease Gross" },
                       { ShortName: "Svc GOG Sales", UserFriendlyName: "Gas Oil Grease Sales" },
                       { ShortName: "Svc CP Labor Sales RO", UserFriendlyName: "CP Labor Sales RO" },
                       { ShortName: "Svc CP Hrs RO", UserFriendlyName: "CP Hours per RO" },
                       { ShortName: "Svc CP ELR", UserFriendlyName: "CP Effective Labor Rate" },
                       { ShortName: "Svc Clock Hrs Avail", UserFriendlyName: "Svc Clock Hrs Avail" },
                       { ShortName: "Svc Bay Count", UserFriendlyName: "Svc Bay Count" },
                       { ShortName: "Sls Rev", UserFriendlyName: "Sales Revenue" },
                       { ShortName: "Sales Gross", UserFriendlyName: "Sales Gross" },
                       { ShortName: "Sls Cost of Sales", UserFriendlyName: "Cost of Sales" },
                       { ShortName: "Sls Avg Price Used", UserFriendlyName: "Avg Selling Priced Used" },
                       { ShortName: "Sls Avg Price New", UserFriendlyName: "Avg Selling Priced New" },
                       { ShortName: "FI Fin Cnt", UserFriendlyName: "FI Non-Cash Deal Count" },
                       { ShortName: "FI Fin Pct", UserFriendlyName: "FI Non-Cash Deal Pct" },
                       { ShortName: "FI Gap Cnt", UserFriendlyName: "FI Gap Count" },
                       { ShortName: "FI Maint Cnt", UserFriendlyName: "FI Maintenance Count" },
                       { ShortName: "FI Other Cnt", UserFriendlyName: "FI Other Prod Count" },
                       { ShortName: "FI Product Pct", UserFriendlyName: "FI Prod Pct" },
                       { ShortName: "FI Svc Cntrct Amt", UserFriendlyName: "Svc Contract Amount" },
                       { ShortName: "FI Svc Cntrct Cnt", UserFriendlyName: "Svc Contract Count" },
                       { ShortName: "FI Svc Cntrct Pct", UserFriendlyName: "Svc Contract Pct" },
                       { ShortName: "Inv Avg Age New", UserFriendlyName: "Avg Inventory Age New" },
                       { ShortName: "Inv Avg Age Used", UserFriendlyName: "Avg Inventory Age Used" },
                       { ShortName: "Inv Avg Value", UserFriendlyName: "Avg Inventory Value " },
                       { ShortName: "Inv Cnt New", UserFriendlyName: "Inv Count New Cars" },
                       { ShortName: "Inv Cnt Used", UserFriendlyName: "Inv Count Used Cars" },
                       { ShortName: "Inv Days Supl", UserFriendlyName: "Inv Days Supl" },
                       { ShortName: "Inv Days Supl Dollar", UserFriendlyName: "Inv Days Supl Dollar" },
                       { ShortName: "Inv Days Supl New", UserFriendlyName: "Inv Days Supply New" },
                       { ShortName: "Inv Days Supl Used", UserFriendlyName: "Inv Days Supply Used" },
                       { ShortName: "Inv Days to Sale", UserFriendlyName: "Days to Sale" },
                       { ShortName: "Inv Days to Sale New", UserFriendlyName: "Days to Sale New" },
                       { ShortName: "Inv Days to Sale Used", UserFriendlyName: "Days to Sale Used" },
                       { ShortName: "Inv Hi Cnt New", UserFriendlyName: "Inv Count New Cars" },
                       { ShortName: "Inv Hi Cnt Used", UserFriendlyName: "Inv Count Used Cars" },
                       { ShortName: "Inv Hi Days Supl New", UserFriendlyName: "Inv Days Supply New" },
                       { ShortName: "Inv Hi Days Supl Used", UserFriendlyName: "Inv Days Supply Used" },
                       { ShortName: "Inv Hi Value New", UserFriendlyName: "Total Inv Value New" },
                       { ShortName: "Inv Hi Value Used", UserFriendlyName: "Total Inv Value Used" },
                       { ShortName: "Inv Turn Rt", UserFriendlyName: "Inv Turn Rate" },
                       { ShortName: "Inv Value New", UserFriendlyName: "Total Inv Value New" },
                       { ShortName: "Inv Value Used", UserFriendlyName: "Total Inv Value Used" }



            ]

        }
        var bNames = null;
        var deferred;


        function getfriendlyname(name) {
            var deferred2 = $q.defer();
            if (bNames != null) {
                var userfriendlyname = getname(name);
                deferred2.resolve(userfriendlyname);
                return deferred2.promise;
            }
            else {
                getNames().then(function (result) {
                    bNames = [{}];
                    mergeing(result, businessNames.List, 'ShortName');
                    angular.copy(result, bNames);
                    var userfriendlyname = getname(name);
                    deferred2.resolve(userfriendlyname);
                    return deferred2.promise;
                });
            }
            return deferred2.promise;
        }
        function getname(name) {
            var result = _.find(bNames, function (v) { return v.ShortName == name; });
            if (result == undefined || result.UserFriendlyName == undefined) {
                return name;
            }
            else {
                return result.UserFriendlyName;
            }
        }

        function mergeing(arr1, arr2, prop) {
            _.each(arr2, function (arr2obj) {
                var arr1obj = _.find(arr1, function (arr1obj) {
                    return arr1obj[prop] === arr2obj[prop];
                });
                arr1obj ? _.extend(arr1obj, arr2obj) : arr1.push(arr2obj);
            });
        }


        function getNames() {
            if (deferred != undefined && deferred != null) {
                return deferred.promise;
            }
            deferred = $q.defer();

            apiService.get('api/getuserfriendlyname', null, function (data) {
                deferred.resolve(data.data);
                return deferred.promise;
            });
            return deferred.promise;
        }
        return {
            getUserFriendlyName: getfriendlyname
        };
    }
})(angular.module('dwServiceModule'));





