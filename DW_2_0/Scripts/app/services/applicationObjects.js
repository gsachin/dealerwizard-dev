﻿(function (app) {
    app.service('applicationObject', function () {
        var _curDate = moment().subtract(1, 'd'), _startDate = moment(_curDate).subtract(1, 'y').add(1,'d');
       
        var AppObj = { startDate: _startDate, endDate: _curDate };
        this.getDefaultPageFilter = function () {
            return angular.copy(AppObj);
        }
    });
})(angular.module('dwServiceModule'));