﻿import uuidV4 from 'uuid/v4';


let reportServiceFunc = (apiService) => {
    let reportList = [];
    return {
        get: (reportParam) => {
            return new Promise((resolve, reject) => {
                console.log(`api/getcustomreport?key{reportParam.id}`);
                let report = reportList.find((d) => d.id == reportParam.id);
                resolve(angular.copy(((report) ? report : undefined)));
                //apiService.get(`api/getcustomreport?key{reportParam.reportKey}`, null, resolve, reject);
            });
        },
        save: (name, report) => {
            return new Promise((resolve, reject) => {
                reportList.push({ 
                    id: uuidV4(), 
                    name: name, 
                    report:  report});

                console.log(reportList);
                resolve("success");
            });
        },
        getAll: () => {
            return new Promise((resolve, reject) => {
                resolve( _.map(reportList,(d) => {
                    return {
                        name:d.name,
                        id:d.id
                    };
                }));
            });
           
        }
    }
}

reportServiceFunc.$inject = ['apiService'];

angular.module('dwServiceModule')
.factory('reportService', reportServiceFunc);