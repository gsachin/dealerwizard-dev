﻿(function (app) {
    'use strict';

    app.factory('authorizationService', authorizationServiceFunc);

    authorizationServiceFunc.$inject = [ '$q', '$rootScope', '$location', 'userService'];

    function authorizationServiceFunc( $q, $rootScope, $location, userService) {
        
        var routeForUnauthorizedAccess = '';

        userService.getUserLandingPage().then(function (v)
        {
            routeForUnauthorizedAccess = v;
        });
        
        return {
            // We would cache the permission for the session,
            //to avoid roundtrip to server
            //for subsequent requests

            permissionModel: {
                permission: {},
                isPermissionLoaded: false
            },

            permissionCheck: function (requestedRoute) {

                // we will return a promise .
                var deferred = $q.defer();

                //this is just to keep a pointer to parent scope from within promise scope.
                var parentPointer = this;
                var requestedRouteUrl = requestedRoute.current.$$route.originalPath;

                //Checking if permission object(list of roles for logged in user) 
                //is already filled from service
                if (this.permissionModel.isPermissionLoaded) {
                    //Check if the current user has required role to access the route
                    this.getPermission(this.permissionModel, requestedRouteUrl, deferred);
                } else {
                    //if permission is not obtained yet, we will get it from  server.
                    // 'api/permissionService' is the path of server web service , used for this example.

                    userService.getMenuItems().then(function (userMenuItms) {
                        //when server service responds then we will fill the permission object
                        parentPointer.permissionModel.permission = userMenuItms.menuItems;

                        //Indicator is set to true that permission object is filled and 
                        //can be re-used for subsequent route request for the session of the user
                        parentPointer.permissionModel.isPermissionLoaded = true;

                        //Check if the current user has required role to access the route
                        parentPointer.getPermission(parentPointer.permissionModel, requestedRouteUrl, deferred);
                    });
                }
                return deferred.promise;
            },

            //Method to check if the current user has required role to access the route
            //'permissionModel' has permission information obtained from server for current user
            //'requestedRoute' is the list of route to check which are authorized to access route
            //'deferred' is the object through which we shall resolve promise
            getPermission: function (permissionModel, requestedRouteUrl, deferred) {
                var ifPermissionPassed = false;

                var IsValid = permissionModel.permission.find(function (c) { return c.URL == requestedRouteUrl; });

                if(IsValid != undefined)
                    ifPermissionPassed = true;
                
                if (!ifPermissionPassed) {
                    //If user does not have required access, 
                    //we will route the user to unauthorized access page
                    $location.path(routeForUnauthorizedAccess);
                    //As there could be some delay when location change event happens, 
                    //we will keep a watch on $locationChangeSuccess event
                    // and would resolve promise when this event occurs.
                    $rootScope.$on('$locationChangeSuccess', function (next, current) {
                        deferred.resolve();
                    });
                } else {
                    deferred.resolve();
                }
            }
        };
    }

})(angular.module('dwServiceModule'));