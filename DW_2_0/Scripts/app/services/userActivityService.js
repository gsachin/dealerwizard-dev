﻿(function (app) {
    'use strict';

    app.factory('userActivityService', userActivityService);

    userActivityService.$inject = ['$location', '$rootScope', '$q', '$window', '__dw_env', '$route', '$interval', 'apiService', 'dealerService'];

    function userActivityService($location, $rootScope, $q, $window, dwEnvironment, $route, $interval, apiService,dealerService) {
        var userActivity = []
        var defered;
        var service = {
            routeTimeLogger: routeTimeLogger,
            getActivityforRoute: getActivityforRoute,
            getAllCurrentUserActivity: getAllCurrentUserActivity,
            reportSelectionLogger: reportSelectionLogger,
            getAnalyticDataForTabUsage: getAnalyticDataForTabUsage
        };
        
        var activityToken = sessionStorage.activityToken;
        //_.each($route.routes, function (d) {
        //    let route = d.originalPath != undefined ? d.originalPath.replace(/\/$/, "") : "";            
        //    if (_.find(userActivity, function (d) { return d.route === route }) === undefined && route != "") {
        //        userActivity.push({
        //            route: route,
        //            totalTimeElapsed: 0,
        //            totalTimeElapsedMs: 0,
        //            visitCount: 0,
        //            startTime: moment()
        //        })
        //    }
        //});

        function checkAndPushActivitesToDb() {
            var deferedPush = $q.defer();
            console.log(userActivity);
            if (userActivity.length >= 10) {
                var data = JSON.stringify(userActivity);
                console.log(data);
                // var response = apiService.post('api/saveuseractivity', data, null, function (response) {
                    //console.log(response);
                    userActivity = [];
                    deferedPush.resolve(userActivity);
                    return deferedPush.promise;
               // });
            }
            deferedPush.resolve(userActivity);
            return deferedPush.promise;
        }

        function routeTimeLogger(next,current)
        {            
            //console.log(next, current);
            defered = $q.defer();
            let time = moment(new Date).format("YYYY-MM-DD HH:mm:ss");
            dealerService.getCurrentDealer().then(function (dealer) {
                console.log("log");
                checkAndPushActivitesToDb().then(function (res) {
                    console.log(userActivity.length);
                    if (current != undefined) {
                        if (next.originalPath != undefined && current.hasOwnProperty('$$route') == false) {
                            userActivity.push({
                                type: "RouteStart",
                                dealerId: dealer.Id,
                                route: next.originalPath,
                                activityTime: moment(new Date).format("YYYY-MM-DD HH:mm:ss"),                                
                                activityDetail: JSON.stringify({
                                    time: time
                                })
                            });
                        } else if (current.hasOwnProperty('$$route') == true) {
                            userActivity.push({
                                type: "RouteEnd",
                                dealerId: dealer.Id,
                                route: current.originalPath,
                                activityTime: moment(new Date).format("YYYY-MM-DD HH:mm:ss"),                                
                                activityDetail: JSON.stringify({
                                    time: time
                                })
                            });

                            userActivity.push({
                                type: "RouteStart",
                                dealerId: dealer.Id,
                                route: next.originalPath,
                                activityTime: moment(new Date).format("YYYY-MM-DD HH:mm:ss"),                                
                                activityDetail: JSON.stringify({
                                    time: time
                                })
                            });
                        }
                    }
                    defered.resolve(userActivity);
                });
            });
            return defered.promise;
        }

        function reportSelectionLogger(reportObj)
        { 
            defered = $q.defer(); 
            dealerService.getCurrentDealer().then(function (dealer) {
                checkAndPushActivitesToDb().then(function (res) {
                    userActivity.push({
                        type: "ReportLogger",
                        dealerId: dealer.Id,
                        route: $location.$$path,
                        activityTime: moment(new Date).format("YYYY-MM-DD HH:mm:ss"),                        
                        activityDetail: JSON.stringify({
                            initialized: reportObj.intializeFrom,
                            reportId: reportObj.calledReport.reportId,
                            reportName: reportObj.calledReport.reportName,
                            reportInvokeTime: moment(new Date).format("YYYY-MM-DD HH:mm:ss")
                        })
                    });

                    defered.resolve(userActivity);
                });
            });
            return defered.promise;
        }

        function getActivityforRoute(route) {
           return _.filter(userActivity, function (o) {
               return o.route == route && o.type == "RouteTimeLogger";
            });
        }

        function getAllCurrentUserActivity() {
            if (defered == undefined) {
                defered = $q.defer();
                defered.resolve(userActivity);
            }
            return defered.promise;
        }

        function getAnalyticDataForTabUsage(startDate, endDate, allDealers) {
            var userDataDeferred = $q.defer();

            apiService.get('api/getuserrouteactivity?startdate=' + moment(startDate).format('YYYY-MM-DD') + '&enddate='+ moment(endDate).format('YYYY-MM-DD') + '&alldealers=' + allDealers , null, function (data) {
                userDataDeferred.resolve(data.data); 
            }, function (errorMsg) {
                notificationService.error("error occured while fetching records from the server.");
                userDataDeferred.reject("Error Occured");
            });


            return userDataDeferred.promise;
        }

        return service;
    }

})(angular.module('dwServiceModule'));