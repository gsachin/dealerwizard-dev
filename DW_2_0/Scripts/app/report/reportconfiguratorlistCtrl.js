﻿(function (app) {

    'use strict';

    app.controller('reportconfiguratorlistCtrl', reportconfiguratorlistCtrl);

    reportconfiguratorlistCtrl.$inject = ['$scope', 'apiService', '$location'];

    function reportconfiguratorlistCtrl($scope, apiService, $location) {


        $scope.reportConfig = { ShowLoader: true };
        $scope.reportDataList = { columns: [], data: [] };
        $scope.reportList = [];

        $scope.editReport = function (report) {
            $location.path("/reportconfigurator/" + report.bindObject.Report_Key);
            $scope.$apply();
        }

        $scope.go = function (path) {
            $location.path(path);
        };

        apiService.get("api/getallreports", null, function (report) {
            $scope.reportDataList = {
                columns: [{
                    Columndatatype: "Int",
                    DataType: "Number", IsLink: true, NextReportName: null, ReportFilters: "",
                    ReportRefKey: 1, ReportType: null, UrlName: null, UrlPath: null, UrlRefKey: 0, data: "Report_Key", ismeasure: false, reportname: "Sales Overview", title: "Report Key"
                },
				{
				    Columndatatype: "String", DataType: "String", IsLink: false, NextReportName: null, ReportFilters: "",
				    ReportRefKey: -1, ReportType: null, UrlName: null, UrlPath: null, UrlRefKey: 0, data: "Report_Name", ismeasure: false, reportname: "Sales Overview", title: "Report Name"
				},
				{
				    Columndatatype: "Int", DataType: "Number", IsLink: false, NextReportName: null, ReportFilters: "",
				    ReportRefKey: -1, ReportType: null, UrlName: null, UrlPath: null, UrlRefKey: 0, data: "Department_Key", ismeasure: false, reportname: "Sales Overview", title: "Dept Key"
				},
				{
				    Columndatatype: "String", DataType: "String", IsLink: false, NextReportName: null, ReportFilters: "",
				    ReportRefKey: -1, ReportType: null, UrlName: null, UrlPath: null, UrlRefKey: 0, data: "Department_Name", ismeasure: false, reportname: "Sales Overview", title: "Department"
				},
				{
				    Columndatatype: "String", DataType: "String", IsLink: false, NextReportName: null, ReportFilters: "",
				    ReportRefKey: -1, ReportType: null, UrlName: null, UrlPath: null, UrlRefKey: 0, data: "Report_Scope", ismeasure: false, reportname: "Sales Overview", title: "Scope"
				},
				{
				    Columndatatype: "String", DataType: "String", IsLink: false, NextReportName: null, ReportFilters: "",
				    ReportRefKey: -1, ReportType: null, UrlName: null, UrlPath: null, UrlRefKey: 0, data: "Status", ismeasure: false, reportname: "Sales Overview", title: "Status"
				},
				{
				    Columndatatype: "String", DataType: "String", IsLink: false, NextReportName: null, ReportFilters: "",
				    ReportRefKey: -1, ReportType: null, UrlName: null, UrlPath: null, UrlRefKey: 0, data: "Report_Type", ismeasure: false, reportname: "Sales Overview", title: "Report Type"
				},
				{
				    Columndatatype: "String", DataType: "String", IsLink: false, NextReportName: null, ReportFilters: "",
				    ReportRefKey: -1, ReportType: null, UrlName: null, UrlPath: null, UrlRefKey: 0, data: "Created_By", ismeasure: false, reportname: "Sales Overview", title: "Created By"
				},
				{
				    Columndatatype: "Int", DataType: "Number", IsLink: false, NextReportName: null, ReportFilters: "",
				    ReportRefKey: -1, ReportType: null, UrlName: null, UrlPath: null, UrlRefKey: 0, data: "Dealer_Key", ismeasure: false, reportname: "Sales Overview", title: "Dealer"
				}
                ],
                data: report.data
            };;
        });

    }







})(angular.module('dealerWizard'));