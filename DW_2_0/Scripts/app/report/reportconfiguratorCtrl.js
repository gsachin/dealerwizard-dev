﻿(function (app) {
    'use strict';

    app.controller('reportconfiguratorCtrl', reportconfiguratorCtrl);

    reportconfiguratorCtrl.$inject = ['$scope', 'apiService', 'userService', '$http', '$rootScope', '$routeParams', '$location'];

    function reportconfiguratorCtrl($scope, apiService, userService, $http, $rootScope, $routeParams, $location) {

        $scope.default = "This is report Builder page";
        $scope.measure = [];
        $scope.incident = '';
        $scope.DepartmentKey = "-1";
        $scope.d = [];
        $scope.CreatedBy = '';

        $scope.addMeasure = function () {
            $scope.measure.push({});
            $scope.d.push($scope.d.length + 1);
        }
        $scope.dimension = [];
        $scope.addDimension = function () {
            $scope.dimension.push({});
            $scope.d.push($scope.d.length + 1);

        }

        $scope.Filter = [];
        angular.forEach(function (val) {

            $scope.Filter.push(value.Column_key.toString());

        });

        $scope.Remove = function () {
            $scope.measure.pop({});
            $scope.d.pop($scope.d.length - 1);
        };
        $scope.RemoveDimension = function () {
            $scope.dimension.pop({});
            $scope.d.pop($scope.d.length - 1);
        };

        apiService.get("api/getdepartments", null, function (response) {
            $scope.departments = response.data;
        }).then(function () {
            if ($routeParams.id != undefined) {

                $scope.loadSavedReportByReportKey($routeParams.id);
            }
        });

        $scope.getData = function (departmentkey) {
            return apiService.get("api/getreportdatabydepartmentkey?&key=" + departmentkey, null, function (response) {
                $scope.reportmetadata = response.data;

                $scope.reportmetadata.ColumnsData = _.sortBy($scope.reportmetadata.ColumnsData, function (d) { return d.Column_Display_Name; });
                $scope.reportmetadata.ReportsData = _.sortBy($scope.reportmetadata.ReportsData, function (d) { return d.Report_Name; });
                
            });

        };

        $scope.loadSavedReportByReportKey = function (reportkey) {
            apiService.get("api/getreportsdatabyid?&reportkey=" + reportkey, null, function (response) {
                var reportData = response.data;

                $scope.getData(reportData.DepartmentKey)
                .then(function () {
                    // return;
                    var objMeasure = [], objDimension = [], i;
                    var objFilter = [], objcolumn = [], f = [];
                    //Finding the sequence
                    f = _.max(response.data.Columnsdata, function (d) { return d.Sequence });
                    //Finding the measure
                    objMeasure = _.filter(response.data.Columnsdata, function (d) { if (d.Ismeasure == true) return d });
                    $scope.measure = objMeasure;
                    _.map($scope.measure, function (d) {
                        if (d.Order_By == 1) {
                            d.Order_By = (d.Order_By).toString();
                        }
                        else {
                            return d;
                        }
                    });

                    //Finding the dimension
                    objDimension = _.filter(response.data.Columnsdata, function (d) { if (d.Ismeasure == false) return d });
                    $scope.dimension = objDimension;
                    _.map($scope.dimension, function (d) {
                        if (d.Order_By == 1) {
                            d.Order_By = (d.Order_By).toString();
                        }
                        else {
                            return d;
                        }
                    });

                    //assigning the value to d(Array)

                    for (i = 0; i < f.Sequence; i++)//Push the length of the array
                    {
                        $scope.d.push($scope.d.length + 1);
                    }

                    $scope.ReportName = response.data.ReportName;

                    $scope.ReportScope = response.data.ReportScope;
                    $scope.DepartmentKey = response.data.DepartmentKey.toString();
                    $scope.ReportType = response.data.ReportType;
                    _.map(response.data.Filterdata, function (d) {
                        $scope.Filter.push(d.Column_Key.toString());
                    });

                    if (response.data.Status == "InActive") {
                        $scope.Status = true;
                    }
                    else if (response.data.Status == "Active") {
                        $scope.Status = false;
                    }
                  


                });
            });


        };

        $scope.changeOrder_Measure = function (Order_By, $index) {
            
            $scope.measure.map(function (value, idx) {
                if (idx == $index) {
                    value.Order_By = Order_By;
                }
                else {
                        value.Order_By = "0";
                }
            });

            $scope.dimension.map(function (value, idx) {
                value.Order_By = "0";

            });

        }

        $scope.changeOrder_Dimension = function (Order_By, $index) {

            $scope.dimension.map(function (value, idx) {
                if (idx == $index) {
                    value.Order_By = Order_By;
                }
                else {
                        value.Order_By = "0";
                }
            });

            $scope.measure.map(function (value, idx) {
                value.Order_By = "0";
            });

        }

        userService.getCurrentUser()
            .then(function (userPref) {
                $scope.CreatedBy = userPref.User.Name;
            });

        $scope.save = function () {
            var reportkey = (($routeParams.id == undefined) ? null : $routeParams.id);
            //for InActive CheckBox
            if ($scope.Status) {

                $scope.Status = "InActive";
            }
            else {
                $scope.Status = "Active";
            }

            //for Link_Enable CheckBox
            _.each($scope.measure, function (d) {
                if (!d.Link_Enable) {
                    d.Report_Ref_Key = -1;
                }
            })

            _.each($scope.dimension, function (d) {
                if (!d.Link_Enable) {
                    d.Report_Ref_Key = -1;
                }
            })

          
            $scope.report = {
                'ReportKey': reportkey,
                'ReportName': $scope.ReportName,
                'DepartmentKey': $scope.DepartmentKey,
                'ReportScope': $scope.ReportScope,
                'Measures': $scope.measure,
                'Dimensions': $scope.dimension,
                'ReportType': $scope.ReportType,
                'Status': $scope.Status,
                'Filter': $scope.Filter.join('|'),
                'CreatedBy': $scope.CreatedBy
            };

            var dataAsParameter = JSON.stringify($scope.report);

            var response = apiService.post('api/Reportdata/reportsave', dataAsParameter, null, function (response) {
                $location.path('/reportconfiguratorlist');
            });
            return response;
          
        }

    }
    //

})(angular.module('dealerWizard'));