﻿(function (app) {
    'use strict';

    app.controller('homeCtrl', homeCtrl);

    homeCtrl.$inject = ['$scope', 'apiService', '$filter'];

    function homeCtrl($scope, apiService, $filter) {
        var ctrl = this;
        $scope.pageHeight = (window.innerHeight - 80) + "px";
        $scope.title = "Home Page";
    }

})(angular.module('dealerWizard'));