﻿(function (app) {
    'use strict';

    app.controller('rootCtrl', rootCtrl);

    rootCtrl.$inject = ['$scope', '$location', 'apiService', 'dealerService', 'userService', '$rootScope', '$route'];

    function rootCtrl($scope, $location, apiService, dealerService, userService, $rootScope, $route) {
        $scope.dealerList = [];
        $scope.currentDealerRefId = "";
        $scope.logout = logout;
        $scope.loggedInUser = { name: "", selectedDealerName: "" };
        $scope.isActive = function (viewLocation) {
            if ($('.sub-menu').is(':visible') && $("#main-menu").hasClass("mini")) {
                $('.sub-menu').slideUp(200);
            }
            return viewLocation === $location.path();
        };

        $scope.selectDealer = function (dealerObj) {
            dealerService.setCurrentDealer(dealerObj).then(function () {
                $scope.loggedInUser.selectedDealerName = dealerObj.Name;
                $scope.currentDealerRefId = dealerObj.ReferenceDealerId;
                $route.reload();
            });
        }

        userService.getCurrentUser()
					.then(function (userPref) {

					    $scope.loggedInUser.name = userPref.User.Name;
					    $scope.currentDealerRefId = userPref.DefaultDealer.ReferenceDealerId;

					    dealerService.getUserAssociatedDealers(userPref.DefaultDealer.ReferenceDealerId)
						.then(function () {
						    $scope.dealerList = dealerService.associatedDealers;
						    $scope.loggedInUser.selectedDealerName = dealerService.currentDealer.Name;
						    $scope.currentDealerRefId = dealerService.currentDealer.ReferenceDealerId;
						    //$rootScope.$broadcast('dw.dealerChange', { dealer: dealerService.currentDealer });
						});
					});

        function logout() {
            sessionStorage.clear();
            window.location.href = "membership/login.aspx";
        }

    }

})(angular.module('dealerWizard'));