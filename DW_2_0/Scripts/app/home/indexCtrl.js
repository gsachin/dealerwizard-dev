﻿(function (app) {
    'use strict';

    app.controller('indexCtrl', indexCtrl);

    indexCtrl.$inject = ['$scope', 'apiService'];

    function indexCtrl($scope, apiService) {

        $scope.smData = "$";
        $scope.globalFilter = getDefaultFilter();

        function getDefaultFilter() {
            return {
                dealerKey: 3, date: '29-09-2016', periodFor: 'YTD', showPriorPeriod: false
            };
        }

        $scope.reload = function (data) {
            $scope.globalFilter = data;
            getMultipleBarChartData();
        }

        $scope.resetPageFilter = function () {
            $scope.globalFilter = getDefaultFilter();

        }

        function getMultipleBarChartData() {
            // var chkType = viewType;
            var dealerGroupFilter = $scope.globalFilter.dealerKey,
                dateVal = $scope.globalFilter.date,
                PeriodFor = $scope.globalFilter.periodFor,
                showPriorPeriod = $scope.globalFilter.showPriorPeriod;

            //var PeriodMeasure = $('.rdbPM:checked').val();
            //if (ui.defaultView == "Sales") {

             apiService.get('api/getsalesmeasures?dealer=' + dealerGroupFilter + '&date=' + dateVal + '&periodType=' + PeriodFor + '&showPreviodPeriod=' + showPriorPeriod, null, function (data) {
                 $scope.smData = data.data;
            });
           // }
            //else {
           //     return apiService.get(baseUrl + 'api/getservicemeasures?dealer=' + dealerGroupFilter + '&date=' + dateVal + '&periodType=' + PeriodFor + '&showPreviodPeriod=' + showPriorPeriod);
           // }
        }

        getMultipleBarChartData();

    }

})(angular.module('dealerWizard'));