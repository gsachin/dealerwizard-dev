﻿var path = require('path');
var ManifestPlugin = require('webpack-manifest-plugin');
var ChunkManifestPlugin = require('chunk-manifest-webpack-plugin');
var WebpackMd5Hash = require('webpack-md5-hash');
var HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack'),
    glob = require('glob');

module.exports = {
    entry: {
        jquery: ['./Scripts/thirdparty/jquery-3.1.1.min.js',
            './Scripts/thirdparty/jquery-ui.min.js',
            './Scripts/thirdparty/jquery.validate.min.js',
            './Scripts/thirdparty/JQueryTouchUI-min.js',
            './Scripts/thirdparty/jquery.scrollbar.min.js'
        ],
        helper: ['./Scripts/thirdparty/moment.js',
            './Scripts/thirdparty/underscore-min.js'
        ],
        d3: ['./Scripts/thirdparty/d3.v4.min.js',
            './Scripts/thirdparty/d3.tip.js'
        ],
        bootstrap: ['./Scripts/thirdparty/bootstrap.min.js',
            './Scripts/thirdparty/ui-bootstrap-tpls.js'
        ],
        datatablejs: ['./Scripts/thirdparty/dataTables.min.js',
            './Scripts/thirdparty/dataTables.bootstrap.min.js',
            './Scripts/thirdparty/dataTables.responsive.min.js'
        ],
        angular: ['./Scripts/thirdparty/angular.js',
            './Scripts/thirdparty/angular-route.js'
        ],
        daterangepicker: ['./Scripts/thirdparty/daterangepicker.js',
            './Scripts/thirdparty/ng-bs-daterangepicker.js'
        ],
        rzslider: ['./Scripts/thirdparty/rzslider.js'],
        toastr: ['./Scripts/thirdparty/toastr.min.js'],
        fullscreen: ['./Scripts/thirdparty/jquery.fullscreen.min.js'],
        appModule: ['./Scripts/app/services/applicationObjects.js',
            './Scripts/app/services/apiService.js',
            './Scripts/app/services/userService.js',
            './Scripts/app/services/dealerService.js',
            './Scripts/app/services/valueFormatter.js',
            './Scripts/app/services/tabsMetadata.js',
            './Scripts/app/services/authorizationService.js',
            './Scripts/app/services/dealerWizardWebService.js',
            './Scripts/app/services/notificationService.js',
            './Scripts/app/services/businessNameServices.js',
            './Scripts/app/services/userActivityService',
            './Scripts/app/components/progressBar.component.js',
            './Scripts/app/components/smallMultipleBars.component.js',
            './Scripts/app/components/globalFilterBar.component.js',
            './Scripts/app/components/kpi/kpi.component.js',
            './Scripts/app/components/timeseries/timeseries.component.js',
            './Scripts/app/components/gauge/gauge.component.js',
            './Scripts/app/components/gauge/gauge.list.component.js',
            './Scripts/app/components/bar/bar.component.js',
            './Scripts/app/components/bar/groupedbar.component.js',
            './Scripts/app/components/bubble/bubble.component.js',
            './Scripts/app/components/datatable/datatable.component.js',
            './Scripts/app/components/breadcrumb/breadcrumb.js',
            './Scripts/app/components/multicontainer/multi.container.component.js',
            './Scripts/app/components/datatable/datatable.popup.componenet.js',
            './Scripts/app/components/usedcarequity/usedcarequity.component.js',
            './Scripts/app/components/usedEquityButtonGroup.component.js',
            './Scripts/app/components/sidebar-dealerlist/sidebar-dealerlist.component.js',
            './Scripts/app/components/operation/operation.component.js',
            './Scripts/app/components/externalpage/externalpage.component.js',
            './Scripts/app/components/buttonGroup.component.js',
            './Scripts/app/components/menubar/menuBar.component.js',
            './Scripts/app/home/rootCtrl.js',
            './Scripts/app/home/indexCtrl.js',
            './Scripts/app/home/homeCtrl.js',
            './Scripts/app/forecasting/vehiclesalesforecastCtrl.js',
            './Scripts/app/forecasting/fiforecastCtrl.js',
            './Scripts/app/forecasting/serviceforecastCtrl.js',
            './Scripts/app/report/reportconfiguratorCtrl.js',
            './Scripts/app/report/reportconfiguratorlistCtrl.js'
        ],
        app: ['./Scripts/app/app.js'],
        env: ['./Scripts/env.js']
    },
    output: {
        filename: '[name].[chunkhash].js',
        chunkFilename: '[name].[chunkhash].js',
        path: path.resolve(__dirname, '../../dist')
    },
    module: {
        rules: [
        {
            test: /\.js$/,
            use: [{ loader: 'ng-annotate-loader' }],
        },
        {
                    test: /\.js$/,
                        exclude: /(node_modules|bower_components)/,
                        use: {
                                loader: 'babel-loader',
                                options: {
                            presets: ['env']
                        }
                }
                        },
            {
            test: require.resolve('./Scripts/thirdparty/jquery-3.1.1.min.js'),
            use: [{
                loader: 'expose-loader',
                options: 'jQuery'
            }, {
                loader: 'expose-loader',
                options: '$'
            }]
            },
            {
                test: require.resolve('./Scripts/thirdparty/d3.v4.min.js'),
                use: [{
                    loader: 'expose-loader',
                    options: 'd3'
                }]
            },
            {
                test: require.resolve('./Scripts/thirdparty/dataTables.min.js'),
                use: [{
                    loader: 'expose-loader',
                    options: 'datatables'
                }]
            },
            {
                test: require.resolve('./Scripts/thirdparty/moment.js'),
                use: [{
                    loader: 'expose-loader',
                    options: 'moment'
                }]
            },
            {
                test: require.resolve('./Scripts/thirdparty/underscore-min.js'),
                use: [{
                    loader: 'expose-loader',
                    options: '_'
                }]
            },
            {
                test: require.resolve('./Scripts/thirdparty/daterangepicker.js'),
                use: [{
                    loader: 'expose-loader',
                    options: 'daterangepicker'
                }]
            },
            {
                test: require.resolve('./Scripts/thirdparty/toastr.min.js'),
                use: [{
                    loader: 'expose-loader',
                    options: 'toastr'
                }]
            }
        ]
    },
    externals: {
        jquery: 'jQuery',
        moment: 'moment',
        _: '_',
        d3: 'd3',
        'datatables.net': 'datatables.net',
        angular: 'angular',
        toastr:'toastr'
    },
    plugins: [       
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery',
            'root.jQuery': 'jquery', 
            moment: 'moment',
            _: '_',
            d3: 'd3',
            'datatables.net': 'datatables.net',
            angular: 'angular',
            toastr:'toastr'
        }),
        new webpack.ContextReplacementPlugin(/\.\/locale$/, 'empty-module', false, /js$/),
        new webpack.optimize.CommonsChunkPlugin({
            name: ["appModule", "app", "env", "rzslider", "toastr", "fullscreen", "daterangepicker", "datatable", "helper", "d3", "bootstrap", "angular", "jquery"],
            minChunks: Infinity,
        }),
        new WebpackMd5Hash(),
        new ManifestPlugin(),
        new ChunkManifestPlugin({
            filename: "chunk-manifest.json",
            manifestVariable: "webpackManifest",
            inlineManifest: true
        }),
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, 'Views/Home/index.cshtml'),//'./Views/Home/index.cshtml',
            filename: '../Views/Home/index.cshtml',
            inject : false
        })
    ]
};