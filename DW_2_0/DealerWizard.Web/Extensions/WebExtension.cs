﻿namespace DealerWizard.Web.Extensions
{
    public static class WebExtension
    {
        public static string AppServiceUrl(this System.Web.Mvc.UrlHelper helper)
        {
            var appserviceUrl =System.Configuration.ConfigurationManager.AppSettings.Get("appServiceUrl");
            return appserviceUrl;
        }
    }
}