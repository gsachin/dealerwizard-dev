﻿var envVariable = window.__dwenv || {};

    // Base url
envVariable.baseUrl = '/';

    // Whether or not to enable debug mode
    // Setting this to false will disable console output
envVariable.enableDebug = true;

export default envVariable;
