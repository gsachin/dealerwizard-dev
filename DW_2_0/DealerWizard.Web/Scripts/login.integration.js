﻿if (sessionStorage.user != null) {
    sessionStorage.clear();
}

function afterLogin(userCtrlId) {

    var UserName = document.getElementById(userCtrlId + '_UserName').value;
    var Password = document.getElementById(userCtrlId + '_Password').value;

    var obj = { 'username': UserName, 'password': Password, 'grant_type': 'password' };
    Object.toparams = function ObjectToParams(obj) {
        var p = [];
        for (var key in obj) {
            p.push(key + '=' + encodeURIComponent(obj[key]));
        }
        return p.join('&');
    }
    $.ajax({
        type: "POST",
        url: __dwenv.apiUrl + "/token",
        data: Object.toparams(obj),
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        success: OnSuccess,
        failure: function (response) {
            alert('error');
        }
    });
};

function OnSuccess(response) {
    sessionStorage.user = response.access_token;
}
if (parent.frames.length > 0) {
    parent.location.href = self.document.location;
}