﻿$((function ($, window) {

    var baseUrl = window.dw.setting.baseUrl, $ui;

    var ui = {

        events: {
            triggerSearch: "page.ui.search",
            reload: "page.ui.reload"
        },
        initialize: function () {
            $ui = $(this);
            $ui.trigger(ui.events.reload);
        }
    };

    var pageFilter = function () {
        var keys = [];

        var _addFilter = function (key, val) {
            if (keys.map(function (a, b) { return a.split('=')[0]; }).indexOf(key) >= 0) {
                var index = keys.map(function (a, b) { return a.split('=')[0]; }).indexOf(key);
                keys[index] = key + "=" + val;
            } else {
                keys.push(key + "=" + val);
            }
        };

        return {
            addFilter: _addFilter,
            applyFilter: function(key,val){
                _addFilter(key, val);
                $ui.trigger(ui.events.reload);
            },
            getFilterQueryString: function () {
                if (keys.length == 0) return "";

                return keys.reduce(function (t, c) {
                    return (t + "&" + c);
                });
            },
            apply: function() {
                $ui.trigger(ui.events.reload);
            }
        };

    }();


    $(ui).bind(ui.events.reload, function (arg) {
        getCharts();
    });

    var dealerGroupFilter = $('#DealerGroupFilter').val();

    var measure = $("#lineGraphFilter").val();
    var periodtype = $("#lineGraphTypeFilter").val();

    var Bulletmeasure = $("#bulletGraphMeasureFilter").val();
    $('#datepicker').datepicker().datepicker("setDate", new Date()).datepicker("option", "dateFormat", "dd-mm-yy");
    var year = $('#datepicker').datepicker("getDate").getFullYear();
    var dateVal = $('#datepicker').datepicker({ dateFormat: 'dd-mm-yyyy' }).val();

    var datatable = null;
    $('#btn_calendar').click(function () {
        $('#datepicker').datepicker("show");
    });

    $(".removepanel").click(function () {
        var retValue = confirm("Are you sure you want to remove " + $(this).parent().find(".head").text() + " Chart?");
        if (retValue == true) {
            $(this).parent().parent().hide();
        }
    });

    $(".removebigpanel").click(function () {
        var retValue = confirm("Are you sure you want to remove " + $(this).parent().find(".head").text() + "?");
        if (retValue == true) {
            $(this).parent().parent().parent().hide();
        }
    });

    $(".callmodalBullet").click(function () {
        zingchart.exec('model1content', 'destroy');
        $("#model1content").empty();
        $(".modal-title").html($(this).parent().find(".head").text());
        bulletChart('model1content');
    });

    $(".callmodalScatter").click(function () {
        zingchart.exec('model1content', 'destroy');
        $("#model1content").empty();
        $(".modal-title").html($(this).parent().find(".head").text());
        scatterPlotChart('model1content');
    });

    $(".callmodalLine").click(function () {
        zingchart.exec('model1content', 'destroy');
        $("#model1content").empty();
        $(".modal-title").html($(this).parent().find(".head").text());
        lineChart('model1content');
    });

    $('[data-toggle="tooltip"]').tooltip();

    $('.testDownloadable').click(function () {
        alert("Downloading...");
    });

    $('.downloadable').click(function () {

        var chartFOr = $(this).attr('data-ChartFor');
        var dealerId = $('#DealerGroupFilter').val();
        var measure = $('#lineGraphFilter').val();
        var periodtype = $('#lineGraphTypeFilter').val();

        var year = $('#datepicker').datepicker("getDate").getFullYear();//$("#year_value option:selected").text();
        var dateVal = $('#datepicker').datepicker({ dateFormat: 'dd-mm-yyyy' }).val();
        $.when(getJsonData(chartFOr, dealerId, measure, periodtype, year)).done(function (JSONData) {
            JSONToCSVConvertor(JSON.parse(JSONData), true);
        });


    });

    $(".filterDemo").on('click', function (event) {
        $(".demo").toggle("show");
    });

    $(window.document).bind("mousedown", function (e) {
        // If the clicked element is not the menu
        if (!$(e.target).parents(".custom-menu").length > 0) {
            // Hide it
            $(".custom-menu").hide(100);
        }
    });

    $(".close_drill").on("click", function () {
        $("#drill").addClass("hide");
    })

    $('#datepicker').change(function () {
        year = $('#datepicker').datepicker("getDate").getFullYear();
        dateVal = $('#datepicker').datepicker({ dateFormat: 'dd-mm-yyyy' }).val();
        pageFilter.addFilter("dateVal", dateVal);
        pageFilter.addFilter("year", year);
        pageFilter.apply();
    });

    $("#DealerGroupFilter").change(function () {
        dealerGroupFilter = $(this).val();
        pageFilter.applyFilter("dealer", dealerGroupFilter);
    });

    //$("#year_value").change(function () {
    $("#lineGraphFilter, #lineGraphTypeFilter").change(function () {
        periodtype = $("#lineGraphTypeFilter").val();
        //});
        measure = $("#lineGraphFilter").val();
        //$('#line-example').html('');

        idfordisp = $(this).closest('.ChartArea').find('.Div_PlotChart').attr('id');

        $.when(getSales()).done(function (sales) {
            renderZingLineChart(sales, idfordisp, periodtype);
        });
    });

    $("#bulletGraphMeasureFilter").change(function () {
        Bulletmeasure = $("#bulletGraphMeasureFilter").val();
        idfordisp = $(this).closest('.ChartArea').find('.Div_PlotChart').attr('id');

        $.when(getBulletChartData()).done(function (bulletchartJson) {
            renderBulletChart(bulletchartJson, idfordisp);
        });
    });

    function getScatterPlot() {
        return $.get(baseUrl + 'api/productdealdata?dealer=' + dealerGroupFilter + '&year=' + year);
    }

    function getJsonData(chartFOr, dealerId, measure, periodtype) {
        return $.get(baseUrl + 'api/getcsvjsondata?JsonFor=' + chartFOr + '&dealerID=' + dealerId + '&measure=' + measure + '&topCount=' + periodtype + '&year=' + year);
    }

    function getBulletChartData() {
        return $.get(baseUrl + 'api/bulletgrossdata?Dealer=' + dealerGroupFilter + '&dateval=' + dateVal + '&measure=' + Bulletmeasure);
    }

    function getSales() {
        return $.get(baseUrl + 'api/salesdata?measure=' + measure + '&dealer=' + dealerGroupFilter + '&periodtype=' + periodtype + '&date=' + dateVal);
    }

    function scatterPlotChart(idfordisp) {
        $.when(getScatterPlot()).done(function (seriesd) {
            var myConfig4 = {
                "type": "scatter",
                "legend": {
                    "text": "%v",
                    "layout": "5x1",
                    "position": "100% 15%",
                    "item": {
                        "font-color": "#777",
                        "font-family": "Helvetica Neue"
                    },
                    "background-color": "white",
                    "alpha": 0.5,
                    "border-color": "none",
                    "height": "50px",
                    "shadow": false,
                    "marker": {
                        "type": "inherit"
                    }
                },
                "tooltip": {
                    "text": "Sales Manaer: %data-bank \n Sale: %v m \n Unit Sold: %kt",
                    "font-color": "black",
                    "font-family": "Georgia, serif",
                    "background-color": "white",
                    "border-color": "black",
                    "border-width": 2,
                    "text-align": "left"
                },
                /*"title": {
                    "text": "Bank by Sales and Margin",
                    "font-color": "#777",
                    "font-size": "18px",
                    "font-family": "Helvetica Neue,Helvetica,Arial,sans-serif"

                },*/
                "scale-x": {
                    "step": "10",
                    "label": {
                        "text": "Unit Sold"
                    },
                    "item": {
                        "font-size": 10
                    }
                },
                "scale-y": {
                    "step": "10",
                    "label": {
                        "text": "Sales (In Millions)"
                    },
                    "item": {
                        "font-size": 10
                    }
                },
                "series": JSON.parse(JSON.stringify(seriesd))
            };

            zingchart.render({
                id: idfordisp,
                data: myConfig4,
                height: 300,
                width: "100%"
            });

        });
    }

    function lineChart(idfordisp) {
        $.when(getSales()).done(function (sales) {
            // console.log(sales);
            renderZingLineChart(sales, idfordisp, periodtype);
        });
    }

    function bulletChart(idfordisp) {
        $.when(getBulletChartData()).done(function (bulletchartJson) {
            //      console.log(bulletchartJson);           
            renderBulletChart(bulletchartJson, idfordisp);
        });
    }

    function renderBulletChart(bulletchartJson, idfordisp) {
        $('#' + idfordisp).html('');
        var margin = { top: 5, right: 20, bottom: 20, left: 20 },
              //  width = ((d3.select("#myBulletChart").node().clientWidth > 960) ? 960 : d3.select("#myBulletChart").node().clientWidth) - margin.left - margin.right,
                width = d3.select("#myBulletChart").node().clientWidth - margin.left - margin.right,
              height = 80 - margin.top - margin.bottom;

        var data = [];
        var colorRange = ['steelblue', 'lightsteelblue'];

        bulletchartJson.forEach(function (item, index) {
            var obj = {};
            obj.title = (item.title == "YTD-PYTD" ? "Current vs. Prior YTD" : ((item.title == "MTD-PMTD") ? "Current vs. Prior MTD" : "Current vs. Prior Year MTD"));
            obj.count = "US$";
            obj.ranges = [item.pv];
            obj.measures = [item.cv];
            obj.markers = [];
            obj.tooltip = (item.title == "YTD-PYTD" ? $.validator.format("Current YTD: {0} <br>Prior YTD: {1}", item.cv, item.pv) : ((item.title == "MTD-PMTD") ? $.validator.format("Current MTD: {0} <br>Prior MTD: {1}", item.cv, item.pv) : $.validator.format("Current MTD : {0} <br>Prior Year MTD: {1}", item.cv, item.pv)));
            data.push(obj);
        });

        //console.log(data);
        var svg, chart, title = {};

        chart = d3.bullet();
        chart.width(width)
            .height(height)
            .rangeColors(colorRange);

        // add the tooltip area to the webpage
        var tooltip = d3.select('body').append("div")
         .attr("class", "tooltip tooltip-inner")
         .style("opacity", 0).style("display", "none");

        svg = d3.select('#' + idfordisp).selectAll("svg")
                            .data(data)
                            .enter().append("svg")
                            .attr("class", "bullet")
                            .attr("width", width + margin.left + margin.right)
                            .attr("height", height + margin.top + margin.bottom)
                            .append("g")
                            .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
                        .on("mouseover", function (d) {
                            var tooltipArray = $('body .tooltip-inner');
                            var tLength = tooltipArray.length;
                            if (tLength > 1) {
                                while (tLength > 1) {
                                    $('body .tooltip-inner')[0].remove();
                                    tLength = tLength - 1;
                                }
                            }

                            tooltip.transition()
                                 .duration(200)
                                 .style("opacity", .9).style("display", "block");
                            tooltip.html(d.tooltip)
                                 .style("left", (d3.event.pageX + 10) + "px")
                            .style("top", (d3.event.pageY + margin.top) + "px");
                        })
                        .on("mouseout", function (d) {
                            tooltip.transition()
                                 .duration(500)
                                 .style("opacity", 0).style("display", "none");
                        })
                           .call(chart);

        title = svg.append("g")
            .style("text-anchor", "start")
            .attr("transform", "translate(" + 0 + "," + margin.top + ")");

        title.append("text")
            .attr("class", "title")
            .text(function (d) { return d.title; });

        //title.append("text")
        //    .attr("class", "subtitle")
        //    .attr("dy", "1em")
        //    .text(function (d) { return d.subtitle; });

        var resizeTimmer;

        d3.select(window).on("resize.one", function (e) {
            clearTimeout(resizeTimmer);
            resizeTimmer = setTimeout(function () {
                resized();
            }, 350)
        });

        function resized() {

            //Get the width of the window
            var w = d3.select("#myBulletChart").node().clientWidth - margin.left - margin.right;

            chart
                .width(w);

            svg
             .attr("width", w + margin.left + margin.right)
            .call(chart);

        };
        resized();

    }

    function getCharts() {

        $.when(scatterPlotChart('myChart'), lineChart('zingline'), bulletChart('myBulletChart'), getVehicleData(dateVal, 'make|model|bank', $('#tabularColumnVal').find('li.active').attr('data-viewtypeof')));
    };

    // Zing Line Chart
    var dates = [];

    function MonthName(index) {
        var month = new Array();
        month[1] = "Jan";
        month[2] = "Feb";
        month[3] = "Mar";
        month[4] = "Apr";
        month[5] = "May";
        month[6] = "Jun";
        month[7] = "Jul";
        month[8] = "Aug";
        month[9] = "Sep";
        month[10] = "Oct";
        month[11] = "Nov";
        month[12] = "Dec";

        return month[index];
    }

    function renderZingLineChart(sales, idfordisp, periodtype) {
        //  console.log(sales);
        var titleObj1 = {
            title: "",
            fontSize: "1px",
            adjustLayout: true,
            ScaleXlabel: 'Period',
            ScaleYlabel: $('#lineGraphFilter :selected').text()//+' '+$('#lineGraphTypeFilter :selected').text()

        };
        var DataText = (periodtype.toLowerCase() == 'ytd') ? ['CYTD', 'PYTD'] : ['CMTD', 'PMTD', 'PYMTD'];
        var DateObj = {};
        DateObj.minDate = sales.UnixDt;
        DateObj.dateFormat = (periodtype.toLowerCase() == 'ytd') ? '%d-%m-%y' : '%d';
        var SeriesObj = GetZingConfigSeries(sales.values, DataText);
        var ConfigObj = GetZingConfig(titleObj1, SeriesObj, DateObj);
        //console.log(ConfigObj);

        zingchart.render({
            id: idfordisp,
            data: ConfigObj,
            height: 300,
            width: '100%',
        });

    };

    function GetZingConfigSeries(res, text) {
        var SeriesObj = [];
        var ColorArray = {
            "background": ["#007790", "#009872", "#da534d"],
            "borderColor": ["#69dbf1", "#69f2d0", "#da534d"],
            "fontColor": ["white", "white", "white"]
        };
        for (var i = 0; i < res.length; i++) {
            SeriesObj.push({
                "values": res[i],
                "text": (text[i] == undefined) ? "" : text[i],
                "line-color": ColorArray.background[i],
                "legend-item": {
                    "background-color": ColorArray.background[i],
                    "borderRadius": 5,
                    "font-color": ColorArray.fontColor[i]
                },
                "legend-marker": {
                    "visible": false
                },
                "marker": {
                    "background-color": ColorArray.background[i],
                    "border-width": 1,
                    "shadow": 0,
                    "border-color": ColorArray.borderColor[i]
                },
                "highlight-marker": {
                    "size": 6,
                    "background-color": ColorArray.background[i],
                }
            });
        }
        return SeriesObj;
    }

    var GetZingConfig = function (titleObj, data, DateObj) {
        return {
            "type": "line",
            "utc": true,
            "timezone": -5,
            "title": {
                "text": titleObj.title,
                "font-size": titleObj.fontSize,
                "adjust-layout": titleObj.adjustLayout
            },
            "plotarea": {
                "margin": "dynamic 45 60 dynamic",
            },
            "legend": {
                "layout": "float",
                "background-color": "none",
                "border-width": 0,
                "shadow": 0,
                "align": "right",
                "adjust-layout": true,
                "item": {
                    "padding": 7,
                    "marginRight": 17,
                    "cursor": "hand"
                }
            },
            "scale-x": {
                "zooming": true,
                "min-value": DateObj.minDate,
                "shadow": 0,
                "step": "day",
                "transform": {
                    "type": "date",
                    "all": DateObj.dateFormat,
                    "guide": {
                        "visible": false
                    },
                    "item": {
                        "visible": false
                    }
                },
                "label": {
                    "visible": true,
                    "text": titleObj.ScaleXlabel
                },
                "minor-ticks": 0
            },
            "scale-y": {
                // "values": "0:1000:250",
                "line-color": "black",
                "shadow": 0,
                "guide": {
                    "line-style": "dashed"
                },
                "label": {
                    "text": titleObj.ScaleYlabel,
                },
                "minor-ticks": 0,
                "thousands-separator": ","
            },
            "crosshair-x": {
                "line-color": "#efefef",
                "plot-label": {
                    "border-radius": "5px",
                    "border-width": "1px",
                    "border-color": "#f6f7f8",
                    "padding": "10px",
                    "font-weight": "bold"
                },
                "scale-label": {
                    "font-color": "#000",
                    "background-color": "#f6f7f8",
                    "border-radius": "5px"
                }
            },
            "tooltip": {
                "visible": false
            },
            "plot": {
                "highlight": true,
                "tooltip-text": "%t views: %v<br>%k",
                "shadow": 0,
                "line-width": "2px",
                "marker": {
                    "type": "circle",
                    "size": 3
                },
                "highlight-state": {
                    "line-width": 3
                }
                //,"animation": {
                //    "effect": 1,
                //    "sequence": 2,
                //    "speed": 1,
                //}
            },
            "series": data
        };
    }

    function drillDownPopup(make, model, customData) {
        $("#selMake").html(make);
        $("#selModel").html(model);
        $.when(getDrillDownSalesPersonData(2016, customData[0], customData[1])).done(function () {
            $("#drill").removeClass("hide");
            $("#drill").offset({ top: $("#salesTable").offset().top });
        });
    }

    //Show/Hide Datatable Columns
    $('.toggle-vis').on('click', function (e) {
        $(this).closest('ul').find('li').removeClass('active');
        $(this).addClass('active');
        if ($(this).text() == "Sales") {
            getVehicleData(dateVal, 'make|model|bank', $(this).attr('data-viewtypeof'));
        }
        else {
            getVehicleData(dateVal, 'make|model|bank', $(this).attr('data-viewtypeof'));
        }
    });

    $('.toggle-group').on('click', function (e) {
        var groupByCol = $(".toggle-group").filter("input:checked");
        var groupby = "";

        // if only model groupBy is selected, autoSelect make groupBy
        if ($("input[data-group='model']").attr("checked") == "checked") {
            $(".toggle-group").filter("input[data-group='make']").prop('checked', true);
            groupByCol = $(".toggle-group").filter("input:checked");
        }

        if (groupByCol.length == 0) {
            groupby = "none";
        } else {
            groupByCol.each(function (i, ele) {
                groupby += $(ele).attr("data-group") + "|";
            });
            groupby = groupby.substring(0, groupby.length - 1);
        }
        // destroy current table * IMPACT PERFORMANCE IN DESTROYING table *
        datatable.destroy();
        $.when(getVehicleData(year, groupby)).done(function () {
            if (groupby.indexOf('bank') == -1) {
                datatable.columns(4).header().to$().addClass("hide");
                datatable.columns(4).footer().to$().addClass("hide");
                var column = datatable.column(1);
                $(column.footer()).html("Total:");
            }
            else {
                datatable.columns(4).header().to$().removeClass("hide");
                datatable.columns(4).footer().to$().removeClass("hide");
                var column = datatable.column(1);
                $(column.footer()).html("");
            }

            if (groupby.indexOf("model") == -1) {
                datatable.columns(1).header().to$().addClass("hide");
                datatable.columns(1).footer().to$().addClass("hide");
            }
            else {
                datatable.columns(1).header().to$().removeClass("hide");
                datatable.columns(1).footer().to$().removeClass("hide");
            }

            if (groupby.indexOf("model") == -1 && groupby.indexOf("bank") == -1) {
                var column = datatable.column(0);
                $(column.footer()).html("Total:");
            }
            else {
                var column = datatable.column(0);
                $(column.footer()).html("");
            }
        });
    });

    // Call for table values
    function getVehicleData(date, group_by, ViewTypeOf) {
        if (datatable != null) { datatable.destroy(); }
        $('#salesTable').html('');
        $('#salesTable').append($('#' + ViewTypeOf).tmpl()[0]);
        datatable = $('#data-table').DataTable(getDataTableObject(date, group_by, ViewTypeOf));
    }

    function getDataTableObject(date, group_by, ViewTypeOf) {
        if (ViewTypeOf == "salesTemplate") {
            return {
                "ajax": baseUrl + 'api/getvehiclesales?date=' + date + '&groupby=' + group_by + '&dealer=' + dealerGroupFilter,
                "retrieve": true,
                "autoWidth": false,
                "responsive": true,
                "columnDefs": [
                    {
                        "targets": [0]
                        //className: (group_by.indexOf('model') == -1 ? "hide" : "")
                    },
                    {
                        "targets": [1]
                    },
                    {
                        "targets": [2]
                    },
                    {
                        "targets": [3]
                        // className: (group_by.indexOf('bank') == -1 ? "hide" : "vehicleSales")
                    },
                    {
                        "render": function (data, type, row) {
                            if (data) {
                                data = data.toFixed(2)
                                data = data.toString();
                                data = "$ " + data;
                                data = data.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                            }
                            return data;
                        },
                        "targets": [5, 6]
                    }
                ],
                "footerCallback": function (row, data, start, end, display) {
                    var api = this.api(), data;

                    // Remove the formatting to get integer data for summation
                    var intVal = function (i) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '') * 1 :
                            typeof i === 'number' ?
                            i : 0;
                    };

                    for (i = 4; i <= 6; i++) {
                        // Total over all pages
                        var total = api
                            .column(i)
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);

                        var htmlText = "";
                        if (i != 4) {
                            htmlText = '$' + total.toFixed(2);
                        }
                        else {
                            htmlText = total.toFixed(2);
                        }
                        // Update footer
                        $(api.column(i).footer()).html(
                          htmlText
                            //'$' + pageTotal + ' ( $' + total + ' total)'
                        );
                    }

                }
            }
        }
        else {
            return {
                "ajax": baseUrl + 'api/getvehicleservice?date=' + date + '&groupby=' + group_by + '&dealer=' + dealerGroupFilter,
                "retrieve": true,
                "autoWidth": false,
                "responsive": true,
                "columnDefs": [
                    {
                        "targets": [0]
                        //className: (group_by.indexOf('model') == -1 ? "hide" : "")
                    },
                    {
                        "targets": [1]
                    },
                    {
                        "targets": [2]
                    },
                    {
                        "render": function (data, type, row) {
                            if (data) {
                                data = data.toFixed(2)
                                data = data.toString();
                                data = "$ " + data;
                                data = data.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                            }
                            return data;
                        },
                        "targets": [3, 4, 5]
                    }
                ],
                "footerCallback": function (row, data, start, end, display) {
                    var api = this.api(), data;

                    // Remove the formatting to get integer data for summation
                    var intVal = function (i) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '') * 1 :
                            typeof i === 'number' ?
                            i : 0;
                    };

                    for (i = 3; i <= 5; i++) {
                        // Total over all pages
                        var total = api
                            .column(i)
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);

                        var htmlText = "";
                        //if (i != 4) {
                        htmlText = '$' + total.toFixed(2);
                        //}
                        //else {
                        //  htmlText = total.toFixed(2);
                        //}
                        // Update footer
                        $(api.column(i).footer()).html(
                          htmlText
                            //'$' + pageTotal + ' ( $' + total + ' total)'
                        );
                    }

                }
            }
        }

    }

    function getDrillDownSalesPersonData(year, vehicleId, bankId) {

        $('#drill-data-table').DataTable({
            "ajax": baseUrl + 'api/Sales/SalesDrillDown?year=' + year + '&vehicleId=' + vehicleId + '&bankId=' + bankId,
            "createdRow": function (row, data, index) {
                //$(row).addClass('vehicleSales');
            },
            retrieve: true,
            responsive: true,
            "columnDefs": [
                {
                    "render": function (data, type, row) {
                        if (data) {
                            data = data.toFixed(2)
                            data = data.toString();
                            data = "$ " + data;
                            data = data.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                        }
                        return data;
                    },
                    "targets": [2, 3, 4]
                }
            ]
        });
    }


    if (!window.dw) {
        window.dw = {};
    }

    ui.initialize();

})(window.jQuery, window));
