﻿import tabReducer from './tab-reducer';
import { combineReducers } from '../../thirdparty/redux.min';

export const RootReducer = combineReducers({
    tabs: tabReducer
});

