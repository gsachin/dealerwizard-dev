﻿import { DW } from '../constants/app';
 
const initialState = [];

export default function tabReducer(state = initialState, action) {
    switch (action.type){
        case DW.REQUEST_TAB_LOAD:
            return initialState;
        case DW.TAB_LOADED:
            let obj = Object.assign([], state, action.payload);
            return obj;
        default:
            return state;
    }
};