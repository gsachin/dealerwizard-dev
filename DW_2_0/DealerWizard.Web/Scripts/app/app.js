﻿import envVariable from '../env';
import { RootReducer } from './reducers/index';
import ngRedux from '../thirdparty/ng-redux.min';
import promiseMiddleware  from 'redux-promise';
import thunkMiddleware   from 'redux-thunk';
import * as treeControl from 'angular-tree-control'


angular.module('dwServiceModule', []);

angular.module('dealerWizard', ['ngRoute', 'ngBootstrap', 'rzModule', 'dwServiceModule', 'ngRedux','treeControl'])
    .config(config)
        //.run()
        .run(['$rootScope', '$location', 'authorizationService', '$route', 'userActivityService', function ($rootScope, $location, authorizationService, $route,userActivityService) {
            //$rootScope.$on('$routeChangeStart', function (event) {
                //if (!Auth.isLoggedIn()) {
                //    console.log('DENY');
                //    event.preventDefault();
                //    $location.path('/login');
                //}
                //else {
                //    console.log('ALLOW');
                //    $location.path('/home');
                //}
            //});
            $rootScope.$on('$routeChangeStart', function (evt, next, current) {
                userActivityService.routeTimeLogger(next, current);//.then(function (result) { console.log(result); });
               
            });
    }]);

// Register environment in AngularJS as constant
angular.module('dealerWizard').constant('__dw_env', envVariable);
config.$inject = ['$routeProvider', '$ngReduxProvider'];

function config($routeProvider, $ngReduxProvider) {

    $ngReduxProvider.createStoreWith(RootReducer,[thunkMiddleware]);

    $routeProvider
        .when("/newcar", {
            template: "<operation type='newcar' department-Code='NEWCAR'></operation>",
            resolve: {
                permission: function (authorizationService, $route) {
                    return authorizationService.permissionCheck($route);
                }
            }
        })
        .when("/usedcar", {
            template: "<operation type='usedcar' department-Code='USEDCAR'></operation>",
            resolve: {
                permission: function (authorizationService, $route) {
                    return authorizationService.permissionCheck($route);
                }
            }
        })
        .when("/variable", {
            template: "<operation type='variable' department-Code='VAR'></operation>",
            resolve: {
                permission: function (authorizationService, $route) {
                    return authorizationService.permissionCheck($route);
                }
            }
        })
        .when("/fi", {
            template: "<operation type='fi' department-Code='FI'></operation>",
            resolve: {
                permission: function (authorizationService, $route) {
                    return authorizationService.permissionCheck($route);
                }
            }
        })
        .when("/inventory", {
            template: "<operation type='inventory' department-Code='INV'></operation>",
            resolve: {
                permission: function (authorizationService, $route) {
                    return authorizationService.permissionCheck($route);
                }
            }
        })
        .when("/servicedepartment", {
            template: "<operation type='service' department-Code='SERVICE'></operation>",
            resolve: {
                permission: function (authorizationService, $route) {
                    return authorizationService.permissionCheck($route);
                }
            }
        })
        .when("/overview", {
            template: "<operation type='dealership' department-Code='DO'></operation>",
            resolve: {
                permission: function (authorizationService, $route) {
                    return authorizationService.permissionCheck($route);
                }
            }
        })
        .when("/salesOpportunities", {
            template: "<externalpage path='$resolve.path' ></externalpage>",
            resolve: {
                permission: function (authorizationService, $route) {
                    return authorizationService.permissionCheck($route);
                },
                'path': function () { return 'DW_2_0/ASP_Pages/MonthlySalesOpps.aspx' }
            }
        })
        .when("/serviceOpportunities", {
            template: "<externalpage path='$resolve.path' ></externalpage>",
            resolve: {
                permission: function (authorizationService, $route) {
                    return authorizationService.permissionCheck($route);
                },
                'path': function () { return 'DW_2_0/ASP_Pages/MonthlyServiceOpps.aspx' }
            }
        })
        .when("/serviceDriveOpps", {
            template: "<externalpage path='$resolve.path' ></externalpage>",
            resolve: {
                permission: function (authorizationService, $route) {
                    return authorizationService.permissionCheck($route);
                },
                'path': function () { return 'DW_2_0/ASP_Pages/ServiceDriveOpps.aspx' }
            }
        })
        .when("/databaseMiningSales", {
            template: "<externalpage path='$resolve.path' ></externalpage>",
            resolve: {
                permission: function (authorizationService, $route) {
                    return authorizationService.permissionCheck($route);
                },
                'path': function () { return 'DW_2_0/ASP_Pages/DatabaseMiningSales.aspx' }
            }
        })
        .when("/databaseMiningService", {
            template: "<externalpage path='$resolve.path' ></externalpage>",
            resolve: {
                permission: function (authorizationService, $route) {
                    return authorizationService.permissionCheck($route);
                },
                'path': function () { return 'DW_2_0/ASP_Pages/DatabaseMiningService.aspx' }
            }
        })
        .when("/conquestMarketing", {
            template: "<externalpage path='$resolve.path' ></externalpage>",
            resolve: {
                permission: function (authorizationService, $route) {
                    return authorizationService.permissionCheck($route);
                },
                'path': function () { return 'DW_2_0/ASP_Pages/ConquestMarketing.aspx' }
            }
        })
        .when("/customerSearch", {
            template: "<externalpage path='$resolve.path' ></externalpage>",
            resolve: {
                permission: function (authorizationService, $route) {
                    return authorizationService.permissionCheck($route);
                },
                'path': function () { return 'DW_2_0/ASP_Pages/CustomerSearch.aspx' }
            }
        })
        .when("/appraisalPro", {
            template: "<externalpage path='$resolve.path' ></externalpage>",
            resolve: {
                permission: function (authorizationService, $route) {
                    return authorizationService.permissionCheck($route);
                },
                'path': function () { return 'popups/AppraisalPro.aspx' }
            }
        })
        .when("/tradeAppraisal", {
            template: "<externalpage path='$resolve.path' ></externalpage>",
            resolve: {
                permission: function (authorizationService, $route) {
                    return authorizationService.permissionCheck($route);
                },
                'path': function () { return 'DW_2_0/ASP_Pages/TradeAppraisal.aspx' }
            }
        })
        .when("/updateAppraisal", {
            template: "<externalpage path='$resolve.path' ></externalpage>",
            resolve: {
                permission: function (authorizationService, $route) {
                    return authorizationService.permissionCheck($route);
                },
                'path': function () { return 'DW_2_0/ASP_Pages/UpdateAppraisal.aspx' }
            }
        })
        .when("/dealerSettings", {
            template: "<externalpage path='$resolve.path' ></externalpage>",
            resolve: {
                permission: function (authorizationService, $route) {
                    return authorizationService.permissionCheck($route);
                },
                'path': function () { return 'admin/dealerships.aspx' }
            }
        })
        .when("/dealerAdmin", {
            template: "<externalpage path='$resolve.path' ></externalpage>",
            resolve: {
                permission: function (authorizationService, $route) {
                    return authorizationService.permissionCheck($route);
                },
                'path': function () { return 'admin/dealership.aspx?DB=true' }
            }
        })
        .when("/DW1.0", {
            template: "<externalpage path='$resolve.path' ></externalpage>",
            resolve: {
                permission: function (authorizationService, $route) {
                    return authorizationService.permissionCheck($route);
                },
                'path': function () { return '/DealershipOverview.aspx?DB=true' }
            }
        })
        .when("/vehicleforecasting", {
            templateUrl: "Scripts/app/forecasting/vehiclesales.html",
            controller: "vehiclesalesforecastCtrl",
            resolve: {
                permission: function (authorizationService, $route) {
                    return authorizationService.permissionCheck($route);
                },
            }
        })
        .when("/fiforecasting", {
            templateUrl: "Scripts/app/forecasting/fiforecasting.html",
            controller: "fiforecastCtrl",
            resolve: {
                permission: function (authorizationService, $route) {
                    return authorizationService.permissionCheck($route);
                },
            }
        })
        .when("/serviceforecasting", {
            templateUrl: "Scripts/app/forecasting/serviceforecasting.html",
            controller: "serviceforecastCtrl",
            resolve: {
                permission: function (authorizationService, $route) {
                    return authorizationService.permissionCheck($route);
                },
            }
        })
        .when("/reportconfigurator", {
            templateUrl: "Scripts/app/report/reportconfigurator.html",
            controller: "reportconfiguratorCtrl",
            resolve: {
                permission: function (authorizationService, $route) {
                    return authorizationService.permissionCheck($route);
                },
            }
        })
        .when("/reportconfigurator/:id", {
            templateUrl: "Scripts/app/report/reportconfigurator.html",
            controller: "reportconfiguratorCtrl",
            resolve: {
                permission: function (authorizationService, $route) {
                    return authorizationService.permissionCheck($route);
                },
            }
        })
		.when("/reportconfiguratorlist", {
			templateUrl: "Scripts/app/report/reportconfiguratorlist.html",
			controller: "reportconfiguratorlistCtrl",
			resolve: {
			    permission: function (authorizationService, $route) {
			        return authorizationService.permissionCheck($route);
			    },
			}
		})
            .when("/useractivitylogs/:reportname", {
                template: "<activitytable></activitytable>",
                resolve: {
                    permission: function (authorizationService, $route) {
                        return authorizationService.permissionCheck($route);
                    }
                }
            })
            .when("/useractivitylog", {
                template: "<useractivitylog></useractivitylog>",
                resolve: {
                    permission: function (authorizationService, $route) {
                        return authorizationService.permissionCheck($route);
                    }
                }
            })
            .when("/managedashboard/:name", {
                template: "<dashboardinvoker></dashboardinvoker>"
            })
            .when("/usertemplate", {
                template: "<createusertemplate></createusertemplate>",
                resolve: {
                    permission: function (authorizationService, $route) {
                        return authorizationService.permissionCheck($route);
                    }
                }
            })
            .when("/managedashboard",{
                template: "<managedashboard></managedashboard>"
            })
            .when("/reportbuilder",{
                template: "<report-Builder-Component></report-Builder-Component>"
            })
            .when("/inventoryoptimization",{
                template: "<inventory-Optimization></inventory-Optimization>"
            })
            .otherwise({ redirectTo: "/overview" });
}
