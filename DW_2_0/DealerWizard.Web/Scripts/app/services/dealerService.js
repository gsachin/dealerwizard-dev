(function (app) {
    'use strict';

    app.factory('dealerService', dealerServiceFunc);

    dealerServiceFunc.$inject = ['apiService', 'userService', 'dealerWizardWebService', '$q', '$rootScope'];

    function dealerServiceFunc(apiService, userService, dealerWizardWebService, $q, $rootScope) {
        var dealerService = {
            getUserAssociatedDealers: getUserAssociatedDealersFunc,
            getDealerSettings: getDealerSettingsFunc,
            getDealerBookValueItemList:getDealerBookValueItemListFunc,
            associatedDealers: [],
            currentDealer: null,
            getCurrentDealer:getcurrentDealer,
            setCurrentDealer: function (dealer) {
                angular.copy(dealer, dealerService.currentDealer);
                updateCurrentDealer(dealer);
                return dealerWizardWebService.updateDealerSession(dealerService.currentDealer.ReferenceDealerId);
            }
        };

        function getDealerSettingsFunc(dealerId) {          
           var deferred = $q.defer();

            apiService.get('api/dealersetting?dealer=' + dealerId, null, function (data) {
                deferred.resolve(data.data);
                return deferred.promise;
            }, function (errorMsg) {               
                deferred.reject("error occured while fetching records from the server.");
                return deferred.promise;
            });
            return deferred.promise;
        }

        function getDealerBookValueItemListFunc(dealerId) {
            var deferred = $q.defer();

            apiService.get('api/dealerbookitemlist?dealerkey=' + dealerId, null, function (data) {
                deferred.resolve(data.data);
                return deferred.promise;
            }, function (errorMsg) {
                deferred.reject("error occured while fetching records from the server.");
                return deferred.promise;
            });
            return deferred.promise;
        }

        function getUserAssociatedDealersFunc(defaultDealerRefId) {
            var deferred = $q.defer();
            if (sessionStorage.associatedDealers != null) {
                var associatedDealers = JSON.parse(sessionStorage.associatedDealers);
                angular.copy(associatedDealers, dealerService.associatedDealers);
                var _dealer = null;
                if (sessionStorage.currentDealer !== null && sessionStorage.currentDealer !== undefined) {
                    _dealer = JSON.parse(sessionStorage.currentDealer);
                }
                else {
                    _dealer = _.find(associatedDealers, function (arg) {
                        return (arg.IsDefaultDealer);
                    });
                    _dealer = (_dealer == undefined) ? _.find(associatedDealers, function (arg) {
                        return arg.ReferenceDealerId == defaultDealerRefId;
                    }) : _dealer;
                }
                if (_dealer == undefined || _dealer == null) {
                    _dealer = associatedDealers[0];
                }
                updateCurrentDealer(_dealer);
                deferred.resolve(associatedDealers);
                return deferred.promise;
            }

            apiService.get('api/getdealers', null, function (data) {
                dealerService.currentDealer = {};
                angular.copy(data.data, dealerService.associatedDealers);

                var defaultDealer = _.find(dealerService.associatedDealers, function (arg) {
                    return (arg.IsDefaultDealer);
                });
                defaultDealer = (defaultDealer == undefined) ? _.find(dealerService.associatedDealers, function (arg) {
                    return arg.ReferenceDealerId == defaultDealerRefId;
                }) : defaultDealer;
                if (defaultDealer == undefined) {
                    defaultDealer = dealerService.associatedDealers[0];
                }
                updateCurrentDealer(defaultDealer);
                sessionStorage.associatedDealers = JSON.stringify(dealerService.associatedDealers);
                deferred.resolve(dealerService.associatedDealers);
            });

            return deferred.promise;
        }

        function getcurrentDealer()
        {
            var deferred = $q.defer();
            if (sessionStorage.currentDealer != null) {
                dealerService.currentDealer = JSON.parse(sessionStorage.currentDealer);
                deferred.resolve(dealerService.currentDealer);
            }
            else
            {
                userService.getCurrentUser()
					.then(function (userPref) {
					    var defaultDealerId = userPref.DefaultDealer.ReferenceDealerId;
					    getUserAssociatedDealersFunc(userPref.DefaultDealer.ReferenceDealerId)
						.then(function () {
						      deferred.resolve(dealerService.currentDealer);
						      return deferred.promise;
						});
					    return deferred.promise;
					});
            }
           return deferred.promise;

        }

        function updateCurrentDealer(newDealer) {
            angular.copy(newDealer, dealerService.currentDealer);
            sessionStorage.currentDealer = JSON.stringify(newDealer);
        }

        if (sessionStorage.currentDealer != null) {
           
            dealerService.currentDealer = JSON.parse(sessionStorage.currentDealer);
           
        }

        return dealerService;
    }
})(angular.module('dwServiceModule'));;