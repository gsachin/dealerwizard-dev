﻿/// <reference path="dealerWizardWebService.js" />
(function (app) {
    'use strict';

    app.factory('dealerWizardWebService', dealerWizardWebServiceFunc);

    dealerWizardWebServiceFunc.$inject = ['$http', '$location', '$rootScope', '$q', '$window'];

    function dealerWizardWebServiceFunc($http, $location, $rootScope, $q, $window) {
        $rootScope.webBaseUrl = $location.protocol() + "://" + $location.host() + ":" + $location.port();

        var service = {
            updateDealerSession: function (dealerId, success, failure) {
                var url = '/action/UpdateSelectedDealer';
                return $http.post($rootScope.webBaseUrl + url, { "dealerId": dealerId }, null);
            }
        };
        return service;
    }

})(angular.module('dwServiceModule'));