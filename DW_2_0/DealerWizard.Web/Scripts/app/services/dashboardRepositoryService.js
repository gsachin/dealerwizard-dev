﻿import uuidV4 from 'uuid/v4';

//widgetFactoryFunc.$inject = ['']

const dashboardRepositoryFunc = () => {

    let dashboardList = [];
    console.log(moment());

    dashboardList.push({
        key: "102829",
        value: {
            name: "New Car dashboard",
            id: "102829",
            template: "template30x70.html",
            widgets: [
                        {
                            name: "",
                            type: "kpi",
                            container: "section1"
                        },
                        {
                            name: "",
                            type: "kpi",
                            container: "section2"
                        },
                        {
                            name: "",
                            type: "kpi",
                            container: "section2"
                        },
                        {
                            name: "",
                            type: "gauge",
                            container: "section1"
                        },
                        {
                            name: "",
                            type: "gauge",
                            container: "section2"
                        },
                        {
                            name: "",
                            type: "table",
                            container: "section1"
                        }
            ],
            createdOn: moment().format("YYYY-MM-DD HH:mm:ss")
        }});

    let dashboardRepo = {
        save: (dashboard) => {
            let keyDashboard = dashboardList.find((d) => d.name == dashboard.name);
            if (keyDashboard){
                console.log(`dashboard with name {dashboard.value.name} already exist`);
                return;
            }
            dashboard.id = uuidV4();
            dashboardList.push({ key:dashboard.id, value:  dashboard});
            return angular.copy(dashboard);
        },
        get: (key) => {
            let keyDashboard = dashboardList.find((d) => d.key == key);
            return angular.copy(((keyDashboard) ? keyDashboard.value : undefined));
        },
        getAll: () => {
            return _.map(dashboardList,(d) => {
                return {
                        name:d.value.name,
                        id:d.value.id,
                        createdOn:d.value.createdOn
                       };
            });
        }

    }
    
    return dashboardRepo;
};

angular.module('dwServiceModule')
.factory('dashboardRepository', dashboardRepositoryFunc);