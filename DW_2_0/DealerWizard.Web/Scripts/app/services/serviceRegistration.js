﻿import apiService from './apiService';

let serviceModule = angular.module('dwServiceModule');

serviceModule.factory('apiService', apiService);