(function (app) {
    'use strict';

    app.factory('userService', userServiceFunc);

    userServiceFunc.$inject = ['apiService', '$q'];

    function userServiceFunc(apiService, $q) {
        var userPref = null, userMenuItms = null, currentUserdefered;

        var Obj= {
            getCurrentUser: function () {
                if (currentUserdefered != undefined && currentUserdefered != null) {
                    return currentUserdefered.promise;
                }
                else {
                    currentUserdefered = $q.defer();

                    apiService.get('api/currentuser', null, function (data) {
                        userPref = {};
                        angular.copy(data.data, userPref);
                        currentUserdefered.resolve(userPref);
                        return currentUserdefered.promise;
                        //window.location.href = data.data.Preference.Landing_Page;
                    }, function (result) {
                        currentUserdefered.reject(result);
                        return currentUserdefered.promise;
                    })
                }

                return currentUserdefered.promise;
            },
            getMenuItems: function () {
                var defered = $q.defer();

                if (userMenuItms == null) {
                    apiService.get('/api/getMenuItems', null, function (data) {
                        userMenuItms = [{}];
                        angular.copy(data.data, userMenuItms);
                        defered.resolve(userMenuItms);
                    })
                }
                else {
                    defered.resolve(userMenuItms);
                }
                return defered.promise;
            },
            getUserLandingPage: function () {
                var deferred = $q.defer();                
                Obj.getCurrentUser()
					.then(function (data) {
					  deferred.resolve(data.Preference.Landing_Page.replace("analytics#", ""));
					  return deferred.promise;
					}, function (result) {
					    deferred.reject(result);
					    return deferred.promise;
					});
                return deferred.promise;
           }
        }
        return Obj;
    }
})(angular.module('dwServiceModule'));