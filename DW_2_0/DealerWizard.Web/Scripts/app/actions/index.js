﻿import { DW } from '../constants/app';
import fetch from 'isomorphic-fetch';
import envVariable from '../../env';

function loadTab(ctx){
    return {
        type: DW.TAB_LOADED,
        payload: ctx
    }
}

function requestTabLoad(ctx){
    return {
        type: DW.REQUEST_TAB_LOAD,
        payload: ctx
    }
}

function fetchTabConfig(deptCode){
    return (dispatch) => {

        dispatch(requestTabLoad(deptCode))

        return fetchApi('api/getreportobject?dealerid=5&departmentcode=' + deptCode)
      .then(
        response => response.json(),
        // Do not use catch, because that will also catch
        // any errors in the dispatch and resulting render,
        // causing an loop of 'Unexpected batch number' errors.
        // https://github.com/facebook/react/issues/6895
        error => console.log('An error occured.', error)
      )
      .then(json =>
          // We can dispatch many times!
          // Here, we update the app state with the results of the API call.

          dispatch(loadTab(json))
      );
    }
}

function fetchApi(url, options = {})
{   
    let currentUser = sessionStorage.user;
    let apiUrl = envVariable.apiUrl + url;
    if (currentUser != null) {
        options.headers = options.headers || {};
        options.headers['Authorization'] = 'bearer' + ' ' + currentUser;
    }
    return fetch(apiUrl, options);
}

export default { loadTab, fetchTabConfig, requestTabLoad, fetchApi }; 