﻿(function (app) {
    'use strict';

    app.controller('fiforecastCtrl', fiforecastCtrl);

    fiforecastCtrl.$inject = ['$scope', 'apiService', 'valueFormatter', 'applicationObject', 'dealerService'];

    function fiforecastCtrl($scope, apiService, valueFormatter, applicationObject, dealerService) {
        $scope.listOfMakes = [];
        $scope.listOfModels = [];
        $scope.modelList = [];
        $scope.selectedMake = {
            MakeId: -10,
            MakeName: "ALL Makes"
        };
        $scope.selectedModel = {
            MakeId: -10,
            ModelId: -10,
            ModelName: "All Models"
        };
        $scope.Dealer = dealerService.currentDealer;
        //$scope.globalFilter = applicationObject.GetApplicationObject();
        $scope.selectedNewOrUsed = 2;

        $scope.filterResultForType = "both";

        $scope.ShowLoader = true;

        $scope.TotalOriginal = 0;
        $scope.TotalUserEdit = 0;
        $scope.TotalDiff = 0;

        $scope.dates = {
            startDate: moment('2016-08-01').add(1, "M").startOf('month'),
            endDate: moment('2016-08-01').add(3, "M").startOf('month')
        };

        $scope.selectedStartdate = moment($scope.dates.startDate).format("MMM-YYYY");
        $scope.selectedEnddate = moment($scope.dates.endDate).format("MMM-YYYY");

        $scope.ActualSales = [],
        $scope.forecastedSales = [];
        $scope.inventroyData = [];

        $scope.AcDataset = [];
        $scope.FrDataset = [];

        var metricChartObject = {
            fi: [{
                dataSetId: "data2",
                chart: {
                    type: "area",
                    x: "FullDate",
                    y1: "Lower80_Forecast_Financed_Perc",
                    y2: "Upper80_Forecast_Financed_Perc",
                    classes: ["steelbluefill"]
                },
                fnTooltip: function (d, i) {
                    return d;
                }
            }, {
                dataSetId: "data1",
                chart: {
                    type: "line",
                    x: "Actual_Date",
                    y: "financed_Perc",
                    classes: ["line"]
                },
                fnTooltip: function (d, i) {
                    return d;
                }
            },
                {
                    dataSetId: "data1",
                    chart: {
                        type: "circle",
                        cx: "Actual_Date",
                        cy: "financed_Perc",
                        r: 4,
                        classes: ["actualCircle", "dot"],
                        opacity: 1e-6
                    },
                    fnTooltip: function (d, i) {
                        var TooltipHtml = "% : " + d.financed_Perc + "<br />";
                        TooltipHtml += "Period : " + moment(d.Actual_Date).format("MMMM-YYYY");
                        return TooltipHtml;
                    }
                },
                {
                    dataSetId: "data2",
                    chart: {
                        type: "line",
                        x: "FullDate",
                        y: "Forecast_Financed_Edit_Perc",
                        classes: ["line", "frLine"]
                    },
                    fnTooltip: function (d, i) {
                        return d;
                    }
                },
                {
                    dataSetId: "data2",
                    chart: {
                        type: "circle",
                        cx: "FullDate",
                        cy: "Forecast_Financed_Edit_Perc",
                        r: 4,
                        classes: ["forecastCircle", "dot"],
                        opacity: 1
                    },
                    fnTooltip: function (d, i) {
                        var TooltipHtml = "% : " + d.Forecast_Financed_Edit_Perc + "<br />";
                        TooltipHtml += "period : " + moment(d.FullDate).format("MMMM-YYYY") + "<br />";
                        TooltipHtml += "Lower threshold % : " + d.Lower80_Forecast_Financed_Perc + "<br />";
                        TooltipHtml += "Upper threshold %: " + d.Upper80_Forecast_Financed_Perc;
                        return TooltipHtml;
                    }
                }
            ],
            gap: [{
                dataSetId: "data2",
                chart: {
                    type: "area",
                    x: "FullDate",
                    y1: "Lower80_Forecast_GAP_Perc",
                    y2: "Upper80_Forecast_GAP_Perc",
                    classes: ["steelbluefill"]
                },
                fnTooltip: function (d, i) {
                    return d;
                }
            }, {
                dataSetId: "data1",
                chart: {
                    type: "line",
                    x: "Actual_Date",
                    y: "GAP_Perc",
                    classes: ["line"]
                },
                fnTooltip: function (d, i) {
                    return d;
                }
            },
                {
                    dataSetId: "data1",
                    chart: {
                        type: "circle",
                        cx: "Actual_Date",
                        cy: "GAP_Perc",
                        r: 4,
                        classes: ["actualCircle", "dot"],
                        opacity: 1e-6
                    },
                    fnTooltip: function (d, i) {
                        var TooltipHtml = "% : " + d.GAP_Perc + "<br />";
                        TooltipHtml += "Period : " + moment(d.Actual_Date).format("MMMM-YYYY");
                        return TooltipHtml;
                    }
                },
                {
                    dataSetId: "data2",
                    chart: {
                        type: "line",
                        x: "FullDate",
                        y: "Forecast_GAP_Edit_Perc",
                        classes: ["line", "frLine"]
                    },
                    fnTooltip: function (d, i) {
                        return d;
                    }
                },
                {
                    dataSetId: "data2",
                    chart: {
                        type: "circle",
                        cx: "FullDate",
                        cy: "Forecast_GAP_Edit_Perc",
                        r: 4,
                        classes: ["forecastCircle", "dot"],
                        opacity: 1
                    },
                    fnTooltip: function (d, i) {
                        var TooltipHtml = "% : " + d.Forecast_Financed_Edit_Perc + "<br />";
                        TooltipHtml += "period : " + moment(d.FullDate).format("MMMM-YYYY") + "<br />";
                        TooltipHtml += "Lower threshold % : " + d.Lower80_Forecast_GAP_Perc + "<br />";
                        TooltipHtml += "Upper threshold %: " + d.Upper80_Forecast_GAP_Perc;
                        return TooltipHtml;
                    }
                }
            ],
            mnt: [{
                dataSetId: "data2",
                chart: {
                    type: "area",
                    x: "FullDate",
                    y1: "Lower80_Forecast_Maint_Perc",
                    y2: "Upper80_Forecast_Maint_Perc",
                    classes: ["steelbluefill"]
                },
                fnTooltip: function (d, i) {
                    return d;
                }
            }, {
                dataSetId: "data1",
                chart: {
                    type: "line",
                    x: "Actual_Date",
                    y: "Maint_Perc",
                    classes: ["line"]
                },
                fnTooltip: function (d, i) {
                    return d;
                }
            },
                {
                    dataSetId: "data1",
                    chart: {
                        type: "circle",
                        cx: "Actual_Date",
                        cy: "Maint_Perc",
                        r: 4,
                        classes: ["actualCircle", "dot"],
                        opacity: 1e-6
                    },
                    fnTooltip: function (d, i) {
                        var TooltipHtml = "% : " + d.Maint_Perc + "<br />";
                        TooltipHtml += "Period : " + moment(d.Actual_Date).format("MMMM-YYYY");
                        return TooltipHtml;
                    }
                },
                {
                    dataSetId: "data2",
                    chart: {
                        type: "line",
                        x: "FullDate",
                        y: "Forecast_Maint_Edit_Perc",
                        classes: ["line", "frLine"]
                    },
                    fnTooltip: function (d, i) {
                        return d;
                    }
                },
                {
                    dataSetId: "data2",
                    chart: {
                        type: "circle",
                        cx: "FullDate",
                        cy: "Forecast_Maint_Edit_Perc",
                        r: 4,
                        classes: ["forecastCircle", "dot"],
                        opacity: 1
                    },
                    fnTooltip: function (d, i) {
                        var TooltipHtml = "% : " + d.Forecast_Maint_Edit_Perc + "<br />";
                        TooltipHtml += "period : " + moment(d.FullDate).format("MMMM-YYYY") + "<br />";
                        TooltipHtml += "Lower threshold % : " + d.Lower80_Forecast_Maint_Perc + "<br />";
                        TooltipHtml += "Upper threshold %: " + d.Upper80_Forecast_Maint_Perc;
                        return TooltipHtml;
                    }
                }
            ],
            srv: [{
                dataSetId: "data2",
                chart: {
                    type: "area",
                    x: "FullDate",
                    y1: "Lower80_Forecast_SVC_Perc",
                    y2: "Upper80_Forecast_SVC_Perc",
                    classes: ["steelbluefill"]
                },
                fnTooltip: function (d, i) {
                    return d;
                }
            }, {
                dataSetId: "data1",
                chart: {
                    type: "line",
                    x: "Actual_Date",
                    y: "SVC_Perc",
                    classes: ["line"]
                },
                fnTooltip: function (d, i) {
                    return d;
                }
            },
                {
                    dataSetId: "data1",
                    chart: {
                        type: "circle",
                        cx: "Actual_Date",
                        cy: "SVC_Perc",
                        r: 4,
                        classes: ["actualCircle", "dot"],
                        opacity: 1e-6
                    },
                    fnTooltip: function (d, i) {
                        var TooltipHtml = "% : " + d.SVC_Perc + "<br />";
                        TooltipHtml += "Period : " + moment(d.Actual_Date).format("MMMM-YYYY");
                        return TooltipHtml;
                    }
                },
                {
                    dataSetId: "data2",
                    chart: {
                        type: "line",
                        x: "FullDate",
                        y: "Forecast_SVC_Edit_Perc",
                        classes: ["line", "frLine"]
                    },
                    fnTooltip: function (d, i) {
                        return d;
                    }
                },
                {
                    dataSetId: "data2",
                    chart: {
                        type: "circle",
                        cx: "FullDate",
                        cy: "Forecast_SVC_Edit_Perc",
                        r: 4,
                        classes: ["forecastCircle", "dot"],
                        opacity: 1
                    },
                    fnTooltip: function (d, i) {
                        var TooltipHtml = "% : " + d.Forecast_SVC_Edit_Perc + "<br />";
                        TooltipHtml += "period : " + moment(d.FullDate).format("MMMM-YYYY") + "<br />";
                        TooltipHtml += "Lower threshold % : " + d.Lower80_Forecast_SVC_Perc + "<br />";
                        TooltipHtml += "Upper threshold %: " + d.Upper80_Forecast_SVC_Perc;
                        return TooltipHtml;
                    }
                }
            ]
        };

        var unModifiedForecastedSales = [];
        var clearFrDatasetWatch;

        $scope.sliderFinance = { //requires angular-bootstrap to display tooltips
            value: 50,
            options: {
                floor: 0,
                ceil: 100,
                step: 0.5,
                precision: 1,
                showTicksValues: 10,
                showSelectionBar: true,
                onEnd: function () {
                    $scope.tableFiDataStart = $scope.tableFiDataChange;
                    $scope.tableFiDataChange = $scope.sliderFinance.value;
                    $scope.recalculateTableFi();
                },
                translate: function (value) {
                    return value;
                },
                ticksTooltip: function (v) {
                    return 'Tooltip for ' + v;
                }
            }
        };

        $scope.sliderGap = { //requires angular-bootstrap to display tooltips
            value: 50,
            options: {
                floor: 0,
                ceil: 100,
                step: 0.5,
                precision: 1,
                showTicksValues: 10,
                showSelectionBar: true,
                onEnd: function () {
                    $scope.tableGAPDataStart = $scope.tableGAPDataChange;
                    $scope.tableGAPDataChange = $scope.sliderGap.value;
                    $scope.recalculateTableGap();
                },
                translate: function (value) {
                    return value;
                },
                ticksTooltip: function (v) {
                    return 'Tooltip for ' + v;
                }
            }
        };

        $scope.sliderService = { //requires angular-bootstrap to display tooltips
            value: 50,
            options: {
                floor: 0,
                ceil: 100,
                step: 0.5,
                precision: 1,
                showTicksValues: 10,
                showSelectionBar: true,
                onEnd: function () {
                    $scope.tableSrvDataStart = $scope.tableSrvDataChange;
                    $scope.tableSrvDataChange = $scope.sliderService.value;
                    $scope.recalculateTableSvc();
                },
                translate: function (value) {
                    return value;
                },
                ticksTooltip: function (v) {
                    return 'Tooltip for ' + v;
                }
            }
        };

        $scope.sliderMaintenance = { //requires angular-bootstrap to display tooltips
            value: 50,
            options: {
                floor: 0,
                ceil: 100,
                step: 0.5,
                precision: 1,
                showTicksValues: 10,
                showSelectionBar: true,
                onEnd: function () {
                    $scope.tableMntDataStart = $scope.tableMntDataChange;
                    $scope.tableMntDataChange = $scope.sliderMaintenance.value;
                    $scope.recalculateTableMnt();
                },
                translate: function (value) {
                    return value;
                },
                ticksTooltip: function (v) {
                    return 'Tooltip for ' + v;
                }
            }
        };

        function helperMethodForVerifyingValue(val) {
            if (isNaN(val))
                return 0;
            if (!isFinite(val))
                return 0;
            else
                return parseFloat(val.toFixed(2));
        }

        /*Section for fetching date from API start*/
        function getVehicleMakeModel() {            
            apiService.get('api/getvehiclemakemodel?dealerkey=36&dealtype=B', null, function (data) {
                $scope.listOfMakes = data.data.Makes;

                $scope.listOfMakes = _.sortBy($scope.listOfMakes, function (d) {
                    return d.MakeName;
                });

                $scope.listOfMakeModel = data.data.Models;
                $scope.listOfMakeModel = _.sortBy($scope.listOfMakeModel, function (d) {
                    return d.ModelName;
                });
            });
        }

        function GetActualSales() {
            apiService.get('api/getactualsale?dealerid=36&makeid=0&modelid=0', null, function (data) {
                $scope.ActualSales = data.data;
                $scope.updateChart();

            });
        }

        function GetForecastSales() {
            apiService.get('api/getforecastedsale?dealerid=36&startdate=' + moment($scope.dates.startDate).format('DD-MM-YYYY') + '&enddate=' + moment($scope.dates.endDate).format('DD-MM-YYYY'), null, function (data) {
                unModifiedForecastedSales = data.data;

                unModifiedForecastedSales = _.map(unModifiedForecastedSales, function (element) {
                    return _.extend({}, element, {
                        Forecast_Financed_Edit: element.Forecast_Financed,
                        Forecast_GAP_Edit: element.Forecast_GAP,
                        Forecast_Maint_Edit: element.Forecast_Maint,
                        Forecast_SVC_Edit: element.Forecast_SVC
                    });
                });

                $scope.forecastedSales = unModifiedForecastedSales;                

                $scope.setupSliders();
                $scope.updateChart();

            });
        }

        /*Section for fetching date from API end*/

        $scope.GetAllMakeModels = function () {
            $scope.selectedMake = {
                MakeId: -10,
                MakeName: "ALL Makes"
            };
            $scope.selectedModel = {
                MakeId: -10,
                ModelId: -10,
                ModelName: "All Models"
            };
            $scope.filterResultForType = "both";
            $scope.modelList = [];
            $scope.updateChart();
        }

        $scope.GetListedModels = function (SelectedMake) {
            $scope.selectedMake = SelectedMake;
            $scope.selectedModel = {
                MakeId: -10,
                ModelId: -10,
                ModelName: "All Models"
            };
            $scope.modelList = $scope.listOfMakeModel.filter(function (val) {
                return val.MakeId == SelectedMake.MakeId
            })
            $scope.updateChart();
        }

        $scope.metricList = [{
            metricName: '% Financed Deals',
            metricValue: 'fi'
        },
            {
                metricName: '% GAP Coverage',
                metricValue: 'gap'
            },
            {
                metricName: '% Service Plans',
                metricValue: 'srv'
            },
            {
                metricName: '% Maintenance Plans',
                metricValue: 'mnt'
            }
        ];

        $scope.currentMetric = {
            metricName: '% Financed Deals',
            metricValue: 'fi'
        };

        $scope.chartObject = metricChartObject[$scope.currentMetric.metricValue];

        $scope.SetNewMetric = function (val) {
            $scope.currentMetric = val;
            $scope.chartObject = metricChartObject[$scope.currentMetric.metricValue];
        }

        $scope.setupSliders = function () {
            $scope.sliderPositions = {};
        }

        $scope.ResetChart = function () {
            $scope.selectedMake = {
                MakeId: -10,
                MakeName: "ALL Makes"
            };
            $scope.selectedModel = {
                MakeId: -10,
                ModelId: -10,
                ModelName: "All Models"
            };
            $scope.modelList = [];
            $scope.filterResultForType = "both";
            $scope.dates = {
                startDate: moment('2016-08-01').add(1, "M").startOf('month'),
                endDate: moment('2016-08-01').add(3, "M").startOf('month')
            };
            $('input[type = "daterange"]').val(moment($scope.dates.startDate).format('MMM/YYYY') + ' - ' + moment($scope.dates.endDate).format('MMM/YYYY'));
            $('input[type="daterange"]').trigger('apply.daterangepicker');
            //$scope.updateChart();
        }

        $scope.invokeSelectedModel = function (SelectedModel) {
            $scope.selectedModel = SelectedModel;
            $scope.updateChart();
        }

        $scope.recalculateTableFi = function () {
            $scope.ShowLoader = true;

            for (var i = 0; i < $scope.FrDataset.length; i++) {
                var diffInValue = 0;

                $scope.FrDataset[i].Forecast_Financed_Edit_Perc = parseFloat(((parseFloat($scope.FrDataset[i].Forecast_Financed_Edit_Perc) / $scope.tableFiDataStart) * $scope.tableFiDataChange).toFixed(2));
                var diff = parseFloat((parseFloat($scope.FrDataset[i].Forecast_Financed_Edit_Perc) - parseFloat($scope.FrDataset[i].Forecast_Financed_Perc)).toFixed(2));
                diffInValue = (diff / 100) * $scope.FrDataset[i].unitsDelivered;
                $scope.FrDataset[i].Forecast_Financed_Edit = $scope.FrDataset[i].Forecast_Financed_Edit + diffInValue;

                ManipulateForecastedData($scope.FrDataset[i].FullDate, $scope.selectedMake, $scope.selectedModel, diffInValue, 'Forecast_Financed_Edit');

            }
            createTableData();
            $scope.ShowLoader = false;
        };
        $scope.recalculateTableGap = function () {
            $scope.ShowLoader = true;

            for (var i = 0; i < $scope.FrDataset.length; i++) {
                var diffInValue = 0;

                $scope.FrDataset[i].Forecast_GAP_Edit_Perc = parseFloat(((parseFloat($scope.FrDataset[i].Forecast_GAP_Edit_Perc) / $scope.tableGAPDataStart) * $scope.tableGAPDataChange).toFixed(2));
                var diff = parseFloat((parseFloat($scope.FrDataset[i].Forecast_GAP_Edit_Perc) - parseFloat($scope.FrDataset[i].Forecast_GAP_Perc)).toFixed(2));
                diffInValue = (diff / 100) * $scope.FrDataset[i].unitsDelivered;
                $scope.FrDataset[i].Forecast_GAP_Edit = $scope.FrDataset[i].Forecast_GAP_Edit + diffInValue;

                ManipulateForecastedData($scope.FrDataset[i].FullDate, $scope.selectedMake, $scope.selectedModel, diffInValue, 'Forecast_GAP_Edit');

            }
            createTableData();
            $scope.ShowLoader = false;
        };
        $scope.recalculateTableSvc = function () {
            $scope.ShowLoader = true;

            for (var i = 0; i < $scope.FrDataset.length; i++) {
                var diffInValue = 0;

                $scope.FrDataset[i].Forecast_SVC_Edit_Perc = parseFloat(((parseFloat($scope.FrDataset[i].Forecast_SVC_Edit_Perc) / $scope.tableSrvDataStart) * $scope.tableSrvDataChange).toFixed(2));
                var diff = parseFloat((parseFloat($scope.FrDataset[i].Forecast_SVC_Edit_Perc) - parseFloat($scope.FrDataset[i].Forecast_Financed_Perc)).toFixed(2));
                diffInValue = (diff / 100) * $scope.FrDataset[i].unitsDelivered;
                $scope.FrDataset[i].Forecast_SVC_Edit = $scope.FrDataset[i].Forecast_SVC_Edit + diffInValue;

                ManipulateForecastedData($scope.FrDataset[i].FullDate, $scope.selectedMake, $scope.selectedModel, diffInValue, 'Forecast_SVC_Edit');

            }
            createTableData();
            $scope.ShowLoader = false;
        };
        $scope.recalculateTableMnt = function () {
            $scope.ShowLoader = true;

            for (var i = 0; i < $scope.FrDataset.length; i++) {
                var diffInValue = 0;

                $scope.FrDataset[i].Forecast_Maint_Edit_Perc = parseFloat(((parseFloat($scope.FrDataset[i].Forecast_Maint_Edit_Perc) / $scope.tableMntDataStart) * $scope.tableMntDataChange).toFixed(2));
                var diff = parseFloat((parseFloat($scope.FrDataset[i].Forecast_Maint_Edit_Perc) - parseFloat($scope.FrDataset[i].Forecast_Financed_Perc)).toFixed(2));
                diffInValue = (diff / 100) * $scope.FrDataset[i].unitsDelivered;
                $scope.FrDataset[i].Forecast_Maint_Edit = $scope.FrDataset[i].Forecast_Maint_Edit + diffInValue;

                ManipulateForecastedData($scope.FrDataset[i].FullDate, $scope.selectedMake, $scope.selectedModel, diffInValue, 'Forecast_Maint_Edit');


            }
            createTableData();
            $scope.ShowLoader = false;
        };

        function ManipulateForecastedData(period, make, model, ChangedValue, columnEdit) {

            if (make.MakeId == -10 && model.ModelId == -10) {
                ChangedValue = ChangedValue / $scope.listOfMakes.length;
                ChangedValue = ChangedValue / $scope.listOfMakeModel.length;

                $scope.forecastedSales = _.map($scope.forecastedSales, function (e) {
                    if (moment(e.FullDate).isSame(moment(period))) {
                        e[columnEdit] = e[columnEdit] + ChangedValue;
                        return e;
                    } else {
                        return e;
                    }
                });

            } else if (make.MakeId != -10 && model.ModelId == -10) {
                var SelectedModel = _.filter($scope.listOfMakeModel, function (d) {
                    return d.MakeId == make.MakeId
                });
                ChangedValue = ChangedValue / SelectedModel.length;
                $scope.forecastedSales = _.map($scope.forecastedSales, function (e) {
                    if (moment(e.FullDate).isSame(moment(period)) && make.MakeId == e.Make_Key) {
                        e[columnEdit] = e[columnEdit] + ChangedValue;
                        return e;
                    } else {
                        return e;
                    }
                });
            } else if (make.MakeId != -10 && model.ModelId != -10) {
                $scope.forecastedSales = _.map($scope.forecastedSales, function (e) {
                    if (moment(e.FullDate).isSame(moment(period)) && make.MakeId == e.Make_Key && model.ModelId == e.Model__Key) {
                        e[columnEdit] = e[columnEdit] + ChangedValue;
                        return e;
                    } else {
                        return e;
                    }
                });
            }
        }

        function FilterActualData(ActualData) {
            var filtereddata = ActualData
            if ($scope.selectedMake.MakeId != -10) {
                filtereddata = _.filter(filtereddata, function (d) {
                    return d.Make_Key == $scope.selectedMake.MakeId
                });
                if ($scope.selectedModel.ModelId != -10) {
                    filtereddata = _.filter(filtereddata, function (d) {
                        return d.Model_Key == $scope.selectedModel.ModelId
                    });
                }
            }

            if ($scope.filterResultForType == "new") {
                filtereddata = _.filter(filtereddata, function (d) {
                    return d.New_Used == 1
                });
            }
            if ($scope.filterResultForType == "used") {
                filtereddata = _.filter(filtereddata, function (d) {
                    return d.New_Used == 0
                });
            }



            filtereddata = _.sortBy(filtereddata, function (d) {
                return d.Actual_Date;
            });

            return filtereddata;
        }

        function FilterForecastedData(ForecastData) {
            var filtereddata = ForecastData;
            if ($scope.selectedMake.MakeId != -10) {
                filtereddata = _.filter(filtereddata, function (d) {
                    return d.Make_Key == $scope.selectedMake.MakeId
                });
                if ($scope.selectedModel.ModelId != -10) {
                    filtereddata = _.filter(filtereddata, function (d) {
                        return d.Model__Key == $scope.selectedModel.ModelId
                    });
                }
            }

            if ($scope.filterResultForType == "new") {
                filtereddata = _.filter(filtereddata, function (d) {
                    return d.New_Used == 1
                });
            }
            if ($scope.filterResultForType == "used") {
                filtereddata = _.filter(filtereddata, function (d) {
                    return d.New_Used == 0
                });
            }

            return filtereddata;
        }

        $('input[type=daterange]').on('apply.daterangepicker', function (ev, picker) {
            $scope.ShowLoader = true;
            $scope.forecastedSales = [];
            GetForecastSales();
            $scope.ShowLoader = false;
        });

        $scope.filterDataForType = function (val) {
            $scope.ShowLoader = true;
            $scope.filterResultForType = val;
            $scope.forecastedSales = [];
            GetForecastSales();
            //$scope.forecastedSales = unModifiedForecastedSales;
            //var _ForecastDataSet = FilterForecastedData(unModifiedForecastedSales);
            //var grpForecast = _.groupBy(_ForecastDataSet, function (d) { return d.FullDate });           
            //$scope.updateChart();             
        }

        $scope.updateChart = function () {

            if ($scope.ActualSales.length == 0 || $scope.forecastedSales.length == 0) {
                return;
            }

            if (clearFrDatasetWatch !== undefined)
                clearFrDatasetWatch();

            var parseTime = d3.timeParse("%d-%m-%Y");
            var _ActualDataSet = FilterActualData($scope.ActualSales);
            var _ForecastDataSet = FilterForecastedData($scope.forecastedSales);

            var mappedActualData = _.map(_.groupBy(_ActualDataSet, function (d) {
                return d.Actual_Date
            }), function (value, key) {
                return [parseTime(moment(key).format('DD-MM-YYYY')), _.reduce(value, function (result, currentObject) {
                    result = result + currentObject.Units_Delivered;
                    return result;
                }, 0),
                    _.reduce(value, function (result, currentObject) {
                        result = result + currentObject.Financed;
                        return result;
                    }, 0),
                    _.reduce(value, function (result, currentObject) {
                        result = result + currentObject.GAP;
                        return result;
                    }, 0),
                    _.reduce(value, function (result, currentObject) {
                        result = result + currentObject.Maint;
                        return result;
                    }, 0),
                    _.reduce(value, function (result, currentObject) {
                        result = result + currentObject.SVC;
                        return result;
                    }, 0)
                ];
            });


            var mappedForecastedData = _.map(_.groupBy(_ForecastDataSet, function (d) {
                return d.FullDate
            }), function (value, key) {
                return [parseTime(moment(key).format('DD-MM-YYYY')), _.reduce(value, function (result, currentObject) {
                    result = result + currentObject.Forecast_Units_Delivered;
                    return result;
                }, 0),
                    _.reduce(value, function (result, currentObject) {
                        result = result + currentObject.Forecast_Financed;
                        return result;
                    }, 0),
                    _.reduce(value, function (result, currentObject) {
                        result = result + currentObject.Forecast_Financed_Edit;
                        return result;
                    }, 0),
                    _.reduce(value, function (result, currentObject) {
                        result = result + currentObject.Forecast_GAP;
                        return result;
                    }, 0),
                    _.reduce(value, function (result, currentObject) {
                        result = result + currentObject.Forecast_GAP_Edit;
                        return result;
                    }, 0),
                    _.reduce(value, function (result, currentObject) {
                        result = result + currentObject.Forecast_Maint;
                        return result;
                    }, 0),
                    _.reduce(value, function (result, currentObject) {
                        result = result + currentObject.Forecast_Maint_Edit;
                        return result;
                    }, 0),
                    _.reduce(value, function (result, currentObject) {
                        result = result + currentObject.Forecast_SVC;
                        return result;
                    }, 0),
                    _.reduce(value, function (result, currentObject) {
                        result = result + currentObject.Forecast_SVC_Edit;
                        return result;
                    }, 0),
                    _.reduce(value, function (result, currentObject) {
                        result = result + currentObject.Lower80_Financed;
                        return result;
                    }, 0),
                    _.reduce(value, function (result, currentObject) {
                        result = result + currentObject.Lower80_GAP;
                        return result;
                    }, 0),
                    _.reduce(value, function (result, currentObject) {
                        result = result + currentObject.Lower80_Maint;
                        return result;
                    }, 0),
                    _.reduce(value, function (result, currentObject) {
                        result = result + currentObject.Lower80_SVC;
                        return result;
                    }, 0),
                    _.reduce(value, function (result, currentObject) {
                        result = result + currentObject.Upper80_Financed;
                        return result;
                    }, 0),
                    _.reduce(value, function (result, currentObject) {
                        result = result + currentObject.Upper80_GAP;
                        return result;
                    }, 0),
                    _.reduce(value, function (result, currentObject) {
                        result = result + currentObject.Upper80_Maint;
                        return result;
                    }, 0),
                    _.reduce(value, function (result, currentObject) {
                        result = result + currentObject.Upper80_SVC;
                        return result;
                    }, 0),
                    _.reduce(value, function (result, currentObject) {
                        result = result + currentObject.Lower80_Units_Delivered;
                        return result;
                    }, 0),
                    _.reduce(value, function (result, currentObject) {
                        result = result + currentObject.Upper80_Units_Delivered;
                        return result;
                    }, 0)
                ];
            });

            $scope.AcDataset = [], $scope.FrDataset = [];
           
            mappedActualData.forEach(function (item, index) {
                $scope.AcDataset.push({
                    Actual_Date: item[0],
                    unitsDelivered: item[1],
                    financed: item[2],
                    financed_Perc: helperMethodForVerifyingValue(item[2] / item[1] * 100),
                    GAP: item[3],
                    GAP_Perc: helperMethodForVerifyingValue(item[3] / item[1] * 100),
                    Maint: item[4],
                    Maint_Perc: helperMethodForVerifyingValue(item[4] / item[1] * 100),
                    SVC: item[5],
                    SVC_Perc: helperMethodForVerifyingValue(item[5] / item[1] * 100)
                });
            });
            //TableDate: moment(item[0]).format('MMMM-YYYY'),
            mappedForecastedData.forEach(function (item, index) {
                $scope.FrDataset.push({
                    FullDate: item[0],
                    unitsDelivered: item[1],
                    Forecast_Financed: item[2],
                    Forecast_Financed_Perc: helperMethodForVerifyingValue(item[2] / item[1] * 100),
                    Forecast_Financed_Edit: item[3],
                    Forecast_Financed_Edit_Perc: helperMethodForVerifyingValue(item[3] / item[1] * 100),
                    Forecast_GAP: item[4],
                    Forecast_GAP_Perc: helperMethodForVerifyingValue(item[4] / item[1] * 100),
                    Forecast_GAP_Edit: item[5],
                    Forecast_GAP_Edit_Perc: helperMethodForVerifyingValue(item[5] / item[1] * 100),
                    Forecast_Maint: item[6],
                    Forecast_Maint_Perc: helperMethodForVerifyingValue(item[6] / item[1] * 100),
                    Forecast_Maint_Edit: item[7],
                    Forecast_Maint_Edit_Perc: helperMethodForVerifyingValue(item[7] / item[1] * 100),
                    Forecast_SVC: item[8],
                    Forecast_SVC_Perc: helperMethodForVerifyingValue(item[8] / item[1] * 100),
                    Forecast_SVC_Edit: item[9],
                    Forecast_SVC_Edit_Perc: helperMethodForVerifyingValue(item[9] / item[1] * 100),
                    Lower80_Forecast_Financed_Perc: helperMethodForVerifyingValue(item[10] / item[18] * 100),
                    Lower80_Forecast_GAP_Perc: helperMethodForVerifyingValue(item[11] / item[18] * 100),
                    Lower80_Forecast_Maint_Perc: helperMethodForVerifyingValue(item[12] / item[18] * 100),
                    Lower80_Forecast_SVC_Perc: helperMethodForVerifyingValue(item[13] / item[18] * 100),
                    Upper80_Forecast_Financed_Perc: helperMethodForVerifyingValue(item[14] / item[19] * 100),
                    Upper80_Forecast_GAP_Perc: helperMethodForVerifyingValue(item[15] / item[19] * 100),
                    Upper80_Forecast_Maint_Perc: helperMethodForVerifyingValue(item[16] / item[19] * 100),
                    Upper80_Forecast_SVC_Perc: helperMethodForVerifyingValue(item[17] / item[19] * 100)
                });
            });

            $scope.AcDataset = _.sortBy($scope.AcDataset, function (d) {
                return d.Actual_Date
            });
            $scope.FrDataset = _.sortBy($scope.FrDataset, function (d) {
                return d.FullDate
            });

            createTableData();
            $scope.avgForecast = Math.round(_.reduce($scope.FrDataset, function (res, c) {
                res = res + c.unitsDelivered;
                return res
            }, 0) / $scope.FrDataset.length);


            var avgFinancedPerc = Math.round(_.reduce($scope.FrDataset, function (res, c) {
                res = res + c.Forecast_Financed_Edit_Perc;
                return res
            }, 0) / $scope.FrDataset.length);
            var avgGAPPerc = Math.round(_.reduce($scope.FrDataset, function (res, c) {
                res = res + c.Forecast_GAP_Edit_Perc;
                return res
            }, 0) / $scope.FrDataset.length);
            var avgMaintPerc = Math.round(_.reduce($scope.FrDataset, function (res, c) {
                res = res + c.Forecast_Maint_Edit_Perc;
                return res
            }, 0) / $scope.FrDataset.length);
            var avgSVCPerc = Math.round(_.reduce($scope.FrDataset, function (res, c) {
                res = res + c.Forecast_SVC_Edit_Perc;
                return res
            }, 0) / $scope.FrDataset.length);


            $scope.sliderFinance.value = helperMethodForVerifyingValue(avgFinancedPerc);
            $scope.sliderGap.value = helperMethodForVerifyingValue(avgGAPPerc);
            $scope.sliderService.value = helperMethodForVerifyingValue(avgSVCPerc);
            $scope.sliderMaintenance.value = helperMethodForVerifyingValue(avgMaintPerc);

            $scope.tableFiDataStart = $scope.sliderFinance.value;
            $scope.tableFiDataChange = $scope.sliderFinance.value;

            $scope.tableGAPDataStart = $scope.sliderGap.value;
            $scope.tableGAPDataChange = $scope.sliderGap.value;

            $scope.tableSrvDataStart = $scope.sliderService.value;
            $scope.tableSrvDataChange = $scope.sliderService.value;

            $scope.tableMntDataStart = $scope.sliderMaintenance.value;
            $scope.tableMntDataChange = $scope.sliderMaintenance.value;

            $scope.ShowLoader = false;
        }

        function createTableData() {
            $scope.tableData = [];

            // $scope.metricList

            var temp = [],
                temp2 = [];
            _.map($scope.FrDataset, function (d) {
                var _obj = {};
                _obj.Month = moment(d.FullDate).format("MMM-YYYY");
                _obj.Metric = 'FI';
                _obj.Orignal = helperMethodForVerifyingValue(d.Forecast_Financed_Perc);
                _obj.Revised = helperMethodForVerifyingValue(d.Forecast_Financed_Edit_Perc);
                _obj.diff = helperMethodForVerifyingValue(d.Forecast_Financed_Edit_Perc - d.Forecast_Financed_Perc);
                temp.push(_obj);
            });
            var orignal = 0,
                revised = 0,
                diff = 0;
            _.each(temp, function (d) {
                orignal += d.Orignal;
                revised += d.Revised;
                diff += d.diff;
                $scope.tableData.push(d);
            });
            $scope.tableData.push({
                Month: "Avg/Month",
                Metric: 'FI',
                Orignal: helperMethodForVerifyingValue(orignal / temp.length),
                Revised: helperMethodForVerifyingValue(revised / temp.length),
                diff: helperMethodForVerifyingValue(diff / temp.length)
            });

            var temp = [],
                temp2 = [];
            _.map($scope.FrDataset, function (d) {
                var _obj = {};
                _obj.Month = moment(d.FullDate).format("MMM-YYYY");
                _obj.Metric = 'GAP';
                _obj.Orignal = helperMethodForVerifyingValue(d.Forecast_GAP_Perc);
                _obj.Revised = helperMethodForVerifyingValue(d.Forecast_GAP_Edit_Perc);
                _obj.diff = helperMethodForVerifyingValue(d.Forecast_GAP_Edit_Perc - d.Forecast_GAP_Perc);
                temp.push(_obj);
            });
            var orignal = 0,
                revised = 0,
                diff = 0;
            _.each(temp, function (d) {
                orignal += d.Orignal;
                revised += d.Revised;
                diff += d.diff;
                $scope.tableData.push(d);
            });
            $scope.tableData.push({
                Month: "Avg/Month",
                Metric: 'GAP',
                Orignal: helperMethodForVerifyingValue(orignal / temp.length),
                Revised: helperMethodForVerifyingValue(revised / temp.length),
                diff: helperMethodForVerifyingValue(diff / temp.length)
            });

            var temp = [],
                temp2 = [];
            _.map($scope.FrDataset, function (d) {
                var _obj = {};
                _obj.Month = moment(d.FullDate).format("MMM-YYYY");
                _obj.Metric = 'Maint';
                _obj.Orignal = helperMethodForVerifyingValue(d.Forecast_Maint_Perc);
                _obj.Revised = helperMethodForVerifyingValue(d.Forecast_Maint_Edit_Perc);
                _obj.diff = helperMethodForVerifyingValue(d.Forecast_Maint_Edit_Perc - d.Forecast_Maint_Perc);
                temp.push(_obj);
            });
            var orignal = 0,
                revised = 0,
                diff = 0;
            _.each(temp, function (d) {
                orignal += d.Orignal;
                revised += d.Revised;
                diff += d.diff;
                $scope.tableData.push(d);
            });
            $scope.tableData.push({
                Month: "Avg/Month",
                Metric: 'Maint',
                Orignal: helperMethodForVerifyingValue(orignal / temp.length),
                Revised: helperMethodForVerifyingValue(revised / temp.length),
                diff: helperMethodForVerifyingValue(diff / temp.length)
            });

            var temp = [],
                temp2 = [];
            _.map($scope.FrDataset, function (d) {
                var _obj = {};
                _obj.Month = moment(d.FullDate).format("MMM-YYYY");
                _obj.Metric = 'SVC';
                _obj.Orignal = helperMethodForVerifyingValue(d.Forecast_SVC_Perc);
                _obj.Revised = helperMethodForVerifyingValue(d.Forecast_SVC_Edit_Perc);
                _obj.diff = helperMethodForVerifyingValue(d.Forecast_SVC_Edit_Perc - d.Forecast_SVC_Perc);
                temp.push(_obj);
            });
            var orignal = 0,
                revised = 0,
                diff = 0;
            _.each(temp, function (d) {
                orignal += d.Orignal;
                revised += d.Revised;
                diff += d.diff;
                $scope.tableData.push(d);
            });
            $scope.tableData.push({
                Month: "Avg/Month",
                Metric: 'SVC',
                Orignal: helperMethodForVerifyingValue(orignal / temp.length),
                Revised: helperMethodForVerifyingValue(revised / temp.length),
                diff: helperMethodForVerifyingValue(diff / temp.length)
            });

        }

        getVehicleMakeModel();
        GetActualSales();
        GetForecastSales();

    }

})(angular.module('dealerWizard'));