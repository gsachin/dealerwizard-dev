﻿(function (app) {
    'use strict';

    app.controller('vehiclesalesforecastCtrl', vehiclesalesforecastCtrl);

    vehiclesalesforecastCtrl.$inject = ['$scope', 'apiService', 'valueFormatter', 'applicationObject', 'dealerService', '$log', 'notificationService', '$rootScope'];

    function vehiclesalesforecastCtrl($scope, apiService, valueFormatter, applicationObject, dealerService, $log, notificationService, $rootScope) {
        var moneyFormatter = valueFormatter.getformatter("money"),
		numberFormatter = valueFormatter.getformatter("number");

        $scope.listOfMakes = [];
        $scope.listOfModels = [];

        dealerService.getCurrentDealer().then(function (dealer) {
            $scope.Dealer = dealer;
            Init();
        });

        $scope.ShowLoader = true;

        $scope.TotalOriginal = 0;
        $scope.TotalUserEdit = 0;
        $scope.TotalDiff = 0;

        $scope.currentInventoryData = [];
        $scope.ActualSales = [],
        $scope.forecastedSales = [];
        $scope.inventroyData = [];

        $scope.AcDataset = [];
        $scope.ActualDataset = [];
        $scope.FrDataset = [];
        $scope.invDataset = [];

        $scope.kpiVehicleProfit = { legendFor: "Sales Gross", valueOfLegend: '$0', legendType: "Monthly" };
        $scope.kpiFiProfit = { legendFor: "FI Gross", valueOfLegend: '$0', legendType: "Monthly" };
        $scope.kpicarryingCost = { legendFor: "Carrying Cost", valueOfLegend: '$0', legendType: "Monthly" };
        $scope.kpiOpportunityCost = { legendFor: "Opportunity Cost", valueOfLegend: '$0', legendType: "Monthly" };
        $scope.kpiTotalInefficiencyCost = { legendFor: "Total Inefficiency Cost", valueOfLegend: '$0', legendType: "Monthly" };
        $scope.kpiCurrentInventory = { legendFor: "Current Inventory", valueOfLegend: '0', legendType: "Vehicles" };

        $scope.currnetInventoryLegends = [
                  { LegendName: "Current Inventory", currentValue: 0, recommendedValue: 0, dataForType: "Vehicles", profitLossType: 'fa fa-arrow-up fa-2', profitLossColor: 'green', diffValue: 0, diffLegendName: 'Inventory' },
                  { LegendName: "Days To Stock", currentValue: 0, recommendedValue: 0, dataForType: "Days", profitLossType: 'fa fa-arrow-up fa-2', profitLossColor: 'green', diffValue: 0, diffLegendName: 'Days' }
        ];

        $scope.kpiPeriod = "A";

        $scope.getCurrentFilterQuery = function () {
            var filterQuery = {};
            if ($scope.selectedMake.MakeId != -10) {
                filterQuery.Make_Key = $scope.selectedMake.MakeId;

                if ($scope.selectedModel.ModelId != -10) {
                    filterQuery.Model_Key = $scope.selectedModel.ModelId;
                }
            }

            if ($scope.filterResultForType === "used") {
                filterQuery.New_Used = 0;
            }
            if ($scope.filterResultForType === "new") {
                filterQuery.New_Used = 1;
            }

            return filterQuery;
        }

        function getSelectedFilterForInventory() {
            var filterQuery = {};
            if ($scope.selectedMake.MakeId != -10) {
                filterQuery.MakeKey = $scope.selectedMake.MakeId;

                if ($scope.selectedModel.ModelId != -10) {
                    filterQuery.ModelKey = $scope.selectedModel.ModelId;
                }
            }

            if ($scope.filterResultForType === "used") {
                filterQuery.NewOrUsed = 0;
            }
            if ($scope.filterResultForType === "new") {
                filterQuery.NewOrUsed = 1;
            }

            return filterQuery;
        }

        $scope.slider = { //requires angular-bootstrap to display tooltips
            value: 100,
            options: {
                floor: 70,
                ceil: 130,
                step: 1,
                showTicksValues: 10,
                showSelectionBar: true,
                onEnd: function () {
                    if ($scope.slider.value !== $scope.tableDataChange) {
                        $scope.tableDataStart = $scope.tableDataChange;
                        $scope.tableDataChange = $scope.slider.value;
                        $scope.recalculateTable();
                    }
                },
                translate: function (value) {
                    return value;
                },
                ticksTooltip: function (v) {
                    return 'Tooltip for ' + v;
                }
            }
        };

        $scope.sliderInterest = { //requires angular-bootstrap to display tooltips
            value: 2.5,
            minValue: 0.5,
            options: {
                floor: 0.5,
                ceil: 7.5,
                step: 0.5,
                precision: 1,
                showTicksValues: .5,
                showSelectionBar: true,
                enforceStep: true,
                onEnd: function () {
                    $scope.updateChart();
                },
                translate: function (value) {
                    return value;
                },
                ticksTooltip: function (v) {
                    return 'Tooltip for ' + v;
                }
            }
        };

        $scope.chartObject = [{
            dataSetId: "data2",
            chart: {
                type: "area",
                x: "FullDate",
                y1: "lowerThresholdUnitDel",
                y2: "upperThresholdUnitDel",
                classes: ["steelbluefill"]
            },
            fnTooltip: function (d, i) { return d; }
        },
            {
                dataSetId: "data1",
                chart: {
                    type: "line",
                    x: "Actual_Date",
                    y: "actualValue",
                    classes: ["line"]
                },
                fnTooltip: function (d, i) { return d; }
            },
        {
            dataSetId: "data1",
            chart: {
                type: "circle",
                cx: "Actual_Date",
                cy: "actualValue",
                r: 3,
                classes: ["actualCircle", "dot"],
                opacity: 1e-6
            },
            fnTooltip: function (d, i) {
                var TooltipHtml = "Deals : " + d.actualValue + "<br />";
                TooltipHtml += "Period : " + moment(d.Actual_Date).format("MMMM-YYYY");
                return TooltipHtml;
            }
        },
        {
            dataSetId: "data1",
            chart: {
                type: "line",
                x: "Actual_Date",
                y: "inventoryGoal",
                classes: ["DashedSteelBlueLine"]
            },
            fnTooltip: function (d, i) { return d; }
        },
        {
            dataSetId: "data1",
            chart: {
                type: "circle",
                cx: "Actual_Date",
                cy: "inventoryGoal",
                r: 3,
                classes: ["inventoryCircle", "dot"],
                opacity: 1e-6
            },
            fnTooltip: function (d, i) {
                var TooltipHtml = "Inv Rec : " + d.inventoryGoal + "<br />";
                TooltipHtml += "Period : " + moment(d.Actual_Date).format("MMMM-YYYY");
                return TooltipHtml;
            }
        },
        {
            dataSetId: "data3",
            chart: {
                type: "line",
                x: "Actual_Date",
                y: "inventoryStock",
                classes: ["SolidBlackLine"]
            },
            fnTooltip: function (d, i) { return d; }
        },
        {
            dataSetId: "data3",
            chart: {
                type: "circle",
                cx: "Actual_Date",
                cy: "inventoryStock",
                r: 3,
                classes: ["inventoryStockCircle", "dot"],
                opacity: 1e-6
            },
            fnTooltip: function (d, i) {
                var TooltipHtml = "Inv Cnt : " + d.inventoryStock + "<br />";
                TooltipHtml += "Period : " + moment(d.Actual_Date).format("MMMM-YYYY");
                return TooltipHtml;
            }
        },
        {
            dataSetId: "data2",
            chart: {
                type: "line",
                x: "FullDate",
                y: "userDefinedValue",
                classes: ["line", "frLine"]
            },
            fnTooltip: function (d, i) { return d; }
        },
        {
            dataSetId: "data2",
            chart: {
                type: "circle",
                cx: "FullDate",
                cy: "userDefinedValue",
                r: 3,
                classes: ["forecastCircle", "dot"],
                opacity: 1
            },
            fnTooltip: function (d, i) {
                var TooltipHtml = "Deals Fcst : " + d.userDefinedValue + "<br />";
                TooltipHtml += "period : " + moment(d.FullDate).format("MMMM-YYYY") + "<br />";
                TooltipHtml += "Lower threshold value : " + d.lowerThresholdUnitDel + "<br />";
                TooltipHtml += "Upper threshold value: " + d.upperThresholdUnitDel;
                return TooltipHtml;
            }
        }, {
            dataSetId: "data2",
            chart: {
                type: "line",
                x: "FullDate",
                y: "inventoryForecasted",
                classes: ["line", "DashedSteelBlueLine"]
            },
            fnTooltip: function (d, i) { return d; }
        },
        {
            dataSetId: "data2",
            chart: {
                type: "circle",
                cx: "FullDate",
                cy: "inventoryForecasted",
                r: 3,
                classes: ["forecastCircle2", "dot"],
                opacity: 1
            },
            fnTooltip: function (d, i) {
                var TooltipHtml = "Inv Rec : " + d.inventoryForecasted + "<br />";
                TooltipHtml += "period : " + moment(d.FullDate).format("MMMM-YYYY") + "<br />";
                TooltipHtml += "Lower threshold value : " + d.lowerThresholdUnitDel + "<br />";
                TooltipHtml += "Upper threshold value: " + d.upperThresholdUnitDel;
                return TooltipHtml;
            }
        }
        ];

        $scope.legends = [
                              { text: 'Actual Units Delivered', color: 'steelblue', dashedLine: '0,0', circleVisible: 0, width: 120 },
                              { text: 'Inventory Rec', color: 'steelblue', dashedLine: '3,3', circleVisible: 0, width: 120 },
                              { text: 'Actual Inventory', color: 'black', dashedLine: '0,0', circleVisible: 0, width: 130 },
                              { text: 'Forecast Units Delivered', color: 'steelblue', dashedLine: '0,0', circleVisible: 1, width: 170 }
        ];

        $scope.GetListedModels = function (SelectedMake) {
            $scope.selectedMake = SelectedMake;
            SetModelForType();
            $scope.selectedModel = { MakeId: -10, ModelId: -10, ModelName: "All Models" };
            $scope.modelList = ((SelectedMake.MakeId == -10) ? [] : $scope.listOfMakeModel.filter(function (val) { return val.MakeId == SelectedMake.MakeId }));
            $scope.updateChart();
        }

        $scope.setupSliders = function () {
            $scope.sliderPositions = {};
        }

        $scope.ResetChart = function () {
            initialiseFilters();
            $('input[type = "daterange"]').val(moment($scope.dates.startDate).format('MMM/YYYY') + ' - ' + moment($scope.dates.endDate).format('MMM/YYYY'));
            $('input[type="daterange"]').trigger('apply.daterangepicker');
        }

        $scope.invokeSelectedModel = function (SelectedModel) {
            $scope.selectedModel = SelectedModel;
            $scope.updateChart();
        }

        $scope.recalculateTable = function () {
            $scope.ShowLoader = true;
            $scope.TotalUserEdit = 0;
            $scope.TotalDiff = 0;

            var localForecastedData = angular.copy($scope.FrDataset);

            _.each(localForecastedData, function (o) {
                var currentForecastValue = o.userDefinedValue;
                if ($scope.tableDataStart == 0) {
                    o.userDefinedValueWoRounding = $scope.tableDataChange;
                } else {
                    o.userDefinedValueWoRounding = parseFloat((parseFloat(o.userDefinedValueWoRounding) / $scope.tableDataStart) * $scope.tableDataChange);
                }

                o.userDefinedValue = Math.round(o.userDefinedValueWoRounding);
                o.diff = (o.userDefinedValue - currentForecastValue);
                manipulateForecastedData(o.FullDate, $scope.selectedMake, $scope.selectedModel, o.diff);
            });

            $scope.updateChart();
            $scope.ShowLoader = false;
        };

        function manipulateForecastedData(period, make, model, ChangedValue) {

            var filteredModels = _.filter(_.where($scope.forecastedSales, $scope.getCurrentFilterQuery()), function (val) {
                return moment(val.FullDate).isSame(moment(period));
            });

            var perUnitRevisedDeals = ChangedValue / filteredModels.length;

            _.each(filteredModels, function (e, i) {
                e.unitsDeliveredUserEdit += perUnitRevisedDeals;
                e.Forecast_Total_GrossEdit += (perUnitRevisedDeals * e.Forecast_Total_Gross);
                e.Forecast_Sales_GrossEdit += (perUnitRevisedDeals * e.Forecast_Sales_Gross);
                e.Forecast_FI_GrossEdit += (perUnitRevisedDeals * e.Forecast_FI_Gross);
            });
        }

        function FilterActualData(ActualData) {
            var filtereddata = ActualData
            if ($scope.selectedMake.MakeId != -10) {
                filtereddata = _.filter(filtereddata, function (d) { return d.Make_Key == $scope.selectedMake.MakeId });
                if ($scope.selectedModel.ModelId != -10) {
                    filtereddata = _.filter(filtereddata, function (d) { return d.Model_Key == $scope.selectedModel.ModelId });
                }
            }

            if ($scope.filterResultForType == "new") {
                filtereddata = _.filter(filtereddata, function (d) { return d.New_Used == 1 });
            }
            if ($scope.filterResultForType == "used") {
                filtereddata = _.filter(filtereddata, function (d) { return d.New_Used == 0 });
            }



            filtereddata = _.sortBy(filtereddata, function (d) { return d.Actual_Date; });

            return filtereddata;
        }

        function FilterInventoryData(data) {
            var filtereddata = data;
            if ($scope.selectedMake.MakeId != -10) {
                filtereddata = _.filter(filtereddata, function (d) { return d.MakeKey == $scope.selectedMake.MakeId });
                if ($scope.selectedModel.ModelId != -10) {
                    filtereddata = _.filter(filtereddata, function (d) { return d.ModelKey == $scope.selectedModel.ModelId });
                }
            }

            if ($scope.filterResultForType == "new") {
                filtereddata = _.filter(filtereddata, function (d) { return d.NewOrUsed == 1 });
            }
            if ($scope.filterResultForType == "used") {
                filtereddata = _.filter(filtereddata, function (d) { return d.NewOrUsed == 0 });
            }

            filtereddata = _.sortBy(filtereddata, function (d) { return d.AsOnDate; });

            return filtereddata;
        }

        function FilterForecastedData(ForecastData) {
            var filteredCurrentInventory = _.filter($scope.currentInventoryData, getSelectedFilterForInventory()),
			_startDate = moment($scope.dates.startDate).format('M/D/YYYY hh:mm:ss A');
            // put empty forecasted data for missing vehicle
            _.each($scope.listOfMakeModelAll, function (mm) {
                var vehicleType = (mm.VehicleType == "New" ? 1 : 0);
                var _frMake = _.filter(ForecastData, function (o) { return (o.Make_Key == mm.MakeId && o.Model_Key == mm.ModelId && o.New_Used == vehicleType) });

                if (_frMake.length == 0) {
                    var newFrData = {
                        Dealer_Key: $scope.Dealer.Id,
                        Dlr_Name: $scope.Dealer.Name,
                        Forecast_FI_Gross: 0,
                        Forecast_Sales_Gross: 0,
                        Forecast_Total_Gross: 0,
                        Forecast_Units_Delivered: 0,
                        FullDate: _startDate,
                        Lower80_Units_Delivered: 0,
                        Make_Desc: _.find($scope.listOfMakesALL, function (m) { return m.MakeId == mm.MakeId }).MakeName,
                        Make_Key: mm.MakeId,
                        Model_Desc: mm.ModelName,
                        Model_Key: mm.ModelId,
                        New_Used: vehicleType,
                        Upper80_Units_Delivered: 0,
                        unitsDeliveredUserEdit: 0
                    };
                    ForecastData.push(newFrData);
                }
            });

            return _.map(_.where(ForecastData, $scope.getCurrentFilterQuery()), function (element) {

                if (element.New_Used == 1) {
                    var inventory = _.find(filteredCurrentInventory, function (o) { return (o.ModelKey == element.Model_Key && o.NewOrUsed == 1) });

                    if (inventory == undefined) {
                        inventory = { Count: 0, Value: 0 };
                    }
                    return _.extend({}, element, {
                        inventoryForecasted: element.unitsDeliveredUserEdit * 2,
                        Forecast_Total_GrossEdit: (element.unitsDeliveredUserEdit * element.Forecast_Total_Gross),
                        Forecast_Sales_GrossEdit: (element.unitsDeliveredUserEdit * element.Forecast_Sales_Gross),
                        Forecast_FI_GrossEdit: (element.unitsDeliveredUserEdit * element.Forecast_FI_Gross),
                        Inventory: inventory.Count,
                        InventoryValue: inventory.Value
                    });
                }
                else {
                    var inventory = _.find(filteredCurrentInventory, function (o) { return (o.ModelKey == element.Model_Key && o.NewOrUsed == 0) });
                    if (inventory == undefined) {
                        inventory = { Count: 0, Value: 0 };
                    }
                    return _.extend({}, element, {
                        inventoryForecasted: element.unitsDeliveredUserEdit * 1.5,
                        Forecast_Total_GrossEdit: (element.unitsDeliveredUserEdit * element.Forecast_Total_Gross),
                        Forecast_Sales_GrossEdit: (element.unitsDeliveredUserEdit * element.Forecast_Sales_Gross),
                        Forecast_FI_GrossEdit: (element.unitsDeliveredUserEdit * element.Forecast_FI_Gross),
                        Inventory: inventory.Count,
                        InventoryValue: inventory.Value
                    });
                }
            });
        }

        $('input[type=daterange]').on('apply.daterangepicker', function (ev, picker) {
            $scope.ShowLoader = true;
            $scope.dates.getFormatedDate = function (val) { return moment(val).startOf('Month').format('DD-MM-YYYY'); }
            $scope.forecastedSales = [];
            GetForecastSales();
        });

        $scope.filterDataForType = function (val) {
            $scope.ShowLoader = true;
            $scope.filterResultForType = val;
            SetMakeForType();
            $scope.selectedModel = { MakeId: -10, ModelId: -10, ModelName: "All Models" };
            $scope.updateChart();
        }

        $scope.ChangeKpiPeriod = function (val) {
            $scope.kpiPeriod = val;
            var parseTime = d3.timeParse("%d-%m-%Y"), avgForecast = 0;
            var filteredCurrentInventory = _.filter($scope.currentInventoryData, getSelectedFilterForInventory());
            var _ForecastDataSet = FilterForecastedData($scope.forecastedSales, filteredCurrentInventory);
            $scope.FrDataset = CustomizeForecastedDataForChart(_ForecastDataSet, parseTime);
            forecastDataForTiles(_ForecastDataSet);
        }

        function CustomizeActualDataForChart(data, dateTimeFormatter) {
            var customizedChartData = [], parseTime = dateTimeFormatter;

            var mappedActualData = _.map(_.groupBy(data, function (d) { return d.Actual_Date }), function (value, key) {
                return [parseTime(moment(key).format('DD-MM-YYYY')), _.reduce(value, function (result, currentObject) {
                    result = result + currentObject.Units_Delivered;
                    return result;
                }, 0),
                 _.reduce(value, function (result, currentObject) {
                     result = result + (currentObject.New_Used == 0 ? (currentObject.Units_Delivered * 1.5) : (currentObject.Units_Delivered * 2));
                     return result;
                 }, 0)];
            });

            mappedActualData.forEach(function (item, index) {
                customizedChartData.push({
                    Actual_Date: item[0],
                    actualValue: Math.round(item[1]),
                    inventoryGoal: Math.round(item[2])
                });
            });

            return _.sortBy(customizedChartData, function (d) { return d.Actual_Date });
        }

        function CustomizeInventoryDataForChart(data, dateTimeFormatter) {
            var customizedChartData = [], parseTime = dateTimeFormatter;

            var mappedInventoryData = _.map(_.groupBy(data, function (d) { return d.AsOnDate }), function (value, key) {
                return [parseTime(moment(key).format('DD-MM-YYYY')), _.reduce(value, function (result, currentObject) {
                    result = result + currentObject.Count;
                    return result;
                }, 0), _.reduce(value, function (result, currentObject) {
                    result = result + currentObject.Value;
                    return result;
                }, 0)];
            });

            mappedInventoryData.forEach(function (item, index) {
                customizedChartData.push({
                    Actual_Date: item[0],
                    inventoryStock: Math.round(item[1]),
                    inventoryStockValue: item[2]
                });
            });

            return _.sortBy(customizedChartData, function (d) { return d.Actual_Date });
        }

        function CustomizeForecastedDataForChart(data, dateTimeFormatter) {
            var customizedChartData = [], parseTime = dateTimeFormatter;

            var mappedForecastedData = _.map(_.groupBy(data, function (d) { return d.FullDate }), function (value, key) {
                return [parseTime(moment(key).format('DD-MM-YYYY')), _.reduce(value, function (result, currentObject) {
                    result = result + currentObject.unitsDeliveredUserEdit;
                    return result;
                }, 0), _.reduce(value, function (result, currentObject) {
                    result = result + currentObject.Lower80_Units_Delivered;
                    return result;
                }, 0), _.reduce(value, function (result, currentObject) {
                    result = result + currentObject.Upper80_Units_Delivered;
                    return result;
                }, 0), _.reduce(value, function (result, currentObject) {
                    result = result + currentObject.Forecast_Total_GrossEdit;
                    return result;
                }, 0)
                , _.reduce(value, function (result, currentObject) {
                    result = result + currentObject.Forecast_Sales_GrossEdit;
                    return result;
                }, 0)
                , _.reduce(value, function (result, currentObject) {
                    result = result + currentObject.Forecast_FI_GrossEdit;
                    return result;
                }, 0), _.reduce(value, function (result, currentObject) {
                    result = result + currentObject.Forecast_Total_Gross;
                    return result;
                }, 0)
                , _.reduce(value, function (result, currentObject) {
                    result = result + currentObject.Forecast_Sales_Gross;
                    return result;
                }, 0)
                , _.reduce(value, function (result, currentObject) {
                    result = result + currentObject.Forecast_FI_Gross;
                    return result;
                }, 0)
                , _.reduce(value, function (result, currentObject) {
                    result = result + currentObject.inventoryForecasted;
                    return result;
                }, 0),
				_.reduce(value, function (result, currentObject) {
				    result = result + currentObject.Forecast_Units_Delivered;
				    return result;
				}, 0)
                ];
            });

            mappedForecastedData.forEach(function (item, index) {
                customizedChartData.push({
                    FullDate: item[0],
                    TableDate: moment(item[0]).format('MMMM-YYYY'),
                    actualForecastedValue: Math.round(item[11]),
                    userDefinedValueWoRounding: item[1],
                    userDefinedValue: Math.round(item[1]),
                    lowerThresholdUnitDel: Math.round(item[2]),
                    upperThresholdUnitDel: Math.round(item[3]),
                    diff: Math.round(item[1]) - Math.round(item[11]),
                    inventoryForecasted: Math.round(item[10]),
                    inventoryForecastedWoRounding: item[10],
                    frTotalGrossEdit: item[4],
                    frSalesGrossEdit: item[5],
                    frFiGrossEdit: item[6],
                    frTotalGross: item[6],
                    frSalesGross: item[8],
                    frFiGross: item[9],
                    frTotalGrossDiff: 0,
                    frSalesGrossDiff: 0,
                    frFiGrossDiff: 0
                });
            });

            return _.sortBy(customizedChartData, function (d) { return d.FullDate });;
        }

        $scope.updateChart = function () {
            $scope.TotalOriginal = $scope.TotalUserEdit = $scope.TotalDiff = 0;
            if ($scope.inventroyData.length == 0) {
                return;
            }

            if ($scope.forecastedSales.length == 0 || $scope.currentInventoryData.length == 0) {
                return;
            }

            var _ActualDataSet = FilterActualData(angular.copy($scope.ActualDataset));
            $scope.AcDataset = CustomizeActualDataForChart(_ActualDataSet, d3.timeParse("%d-%m-%Y"));

            var _inventoryDataSet = FilterInventoryData(angular.copy($scope.inventroyData));
            $scope.invDataset = CustomizeInventoryDataForChart(_inventoryDataSet, d3.timeParse("%d-%m-%Y"));

            d3.select('#inventory-chart').html("");
            d3.select("#forecast-chart").html("");

            var avgForecast = 0;
            var _ForecastDataSet = FilterForecastedData($scope.forecastedSales);
            $scope.FrDataset = CustomizeForecastedDataForChart(_ForecastDataSet, d3.timeParse("%d-%m-%Y"));

            if ($scope.FrDataset.length > 0) {
                $scope.TotalOriginal = Math.round(_.reduce($scope.FrDataset, function (res, c) { res = res + Math.round(c.actualForecastedValue); return res }, 0) / $scope.FrDataset.length);
                $scope.TotalUserEdit = Math.round(_.reduce($scope.FrDataset, function (res, c) { res = res + Math.round(c.userDefinedValue); return res }, 0) / $scope.FrDataset.length);
                $scope.TotalDiff = $scope.TotalUserEdit - $scope.TotalOriginal;
                avgForecast = Math.round(_.reduce($scope.FrDataset, function (res, c) { res = res + c.userDefinedValue; return res }, 0) / $scope.FrDataset.length);
            }

            forecastDataForTiles(_ForecastDataSet);

            $scope.slider.options.floor = Math.round(avgForecast * 0.50);
            $scope.slider.options.ceil = (Math.round(avgForecast * 1.50) < 10 ? 10 : Math.round(avgForecast * 1.50));
            $scope.slider.options.showTicksValues = Math.round($scope.slider.options.ceil * 0.1);
            $scope.slider.value = avgForecast;

            $scope.tableDataStart = $scope.slider.value;
            $scope.tableDataChange = $scope.slider.value;

            $scope.ShowLoader = false;
        }

        function forecastDataForTiles(ForecastDataSet) {

            var frSalesGross = 0, frFiGross = 0, currentInventoryDollars = 0,
			currentInventory = 0, inventoryForecasted = 0, inventoryVariance = 0,
			avgInventoryValue = 0, avgGrossProfitForecast = 0, avgFIProfitForecast = 0, avgProfitForecast = 0,
			inventoryVarianceOpp = 0, newMonthlyForecast = 0, shortageInventory = 0, opportunityCost = 0,
			surplussInventory = 0, carryingCost = 0, _recommendedInv = 0, tempInventoryCount = 0;

            var inventoryCostRate = ($scope.sliderInterest.value / 100);

            var modelWiseRecommendedInventory = _.map(
			_.groupBy(ForecastDataSet, function (v) { return [v.Model_Key, v.New_Used]; }), function (val, key) {
			    return {
			        val: val,
			        Model_Key: key.split(',')[0],
			        New_Used: key.split(',')[1],
			        AvgFrInventory: (_.reduce(val, function (res, co) { res = res + co.unitsDeliveredUserEdit; return res; }, 0) / val.length),
			        RecommendedInventory: Math.round(_.reduce(val, function (res, co) { res = res + ((key.split(',')[1] == "1" ? 2 : 1.5) * Math.round(co.unitsDeliveredUserEdit)); return res; }, 0) / val.length),
			        ActualInventory: val[0].Inventory,
			        InventoryValue: val[0].InventoryValue,
			        TotalGross: Math.round(_.reduce(val, function (res, co) { res = (res + co.Forecast_Sales_GrossEdit + co.Forecast_FI_GrossEdit); return res; }, 0) / val.length)  //val.length is 6
			    };
			});

            _.each(modelWiseRecommendedInventory, function (d) {
                _recommendedInv += d.RecommendedInventory;
                tempInventoryCount += d.ActualInventory;
                if (d.ActualInventory < d.RecommendedInventory) {
                    shortageInventory += (d.RecommendedInventory - d.ActualInventory);
                    opportunityCost += Math.round((d.TotalGross) * (d.RecommendedInventory - d.ActualInventory));
                }
                else if (d.ActualInventory > d.RecommendedInventory) {
                    surplussInventory += (d.ActualInventory - d.RecommendedInventory);
                    carryingCost += Math.round((d.ActualInventory - d.RecommendedInventory) * inventoryCostRate * (d.InventoryValue));
                }
            });

            //when there is no inventory for given filter
            var filteredCurrentInventory = _.filter($scope.currentInventoryData, getSelectedFilterForInventory());

            if (filteredCurrentInventory.length > 0) {
                currentInventoryDollars = _.reduce(filteredCurrentInventory, function (res, c) { res = res + (c.Count * c.Value); return res }, 0);
                currentInventory = _.reduce(filteredCurrentInventory, function (res, c) { res = res + c.Count; return res }, 0);
                avgInventoryValue = currentInventoryDollars / currentInventory;
            }
            //when there is no forcasted data 
            if ($scope.FrDataset.length > 0) {
                frSalesGross = (_.reduce($scope.FrDataset, function (res, c) { res = res + Math.round(c.frSalesGrossEdit); return res }, 0) / $scope.FrDataset.length);
                frFiGross = (_.reduce($scope.FrDataset, function (res, c) { res = res + Math.round(c.frFiGrossEdit); return res }, 0) / $scope.FrDataset.length);
                inventoryForecasted = Math.round(_.reduce($scope.FrDataset, function (res, c) { res = res + c.inventoryForecasted; return res }, 0) / $scope.FrDataset.length);
                inventoryVariance = currentInventory - inventoryForecasted;
                avgGrossProfitForecast = frSalesGross / $scope.slider.value;
                avgFIProfitForecast = frFiGross / $scope.slider.value;
                avgProfitForecast = avgGrossProfitForecast + avgFIProfitForecast;
            }

            var inventoryVarianceCost = ((carryingCost + opportunityCost)).toFixed(2);

            var kpiPeriodValue = ($scope.kpiPeriod == "A") ? 12 : 1;

            if ($scope.kpiPeriod == "A") {
                kpiPeriodValue = 12;
            }
            else {
                carryingCost = carryingCost / 12;
                opportunityCost = opportunityCost / 12;
                inventoryVarianceCost = ((carryingCost + opportunityCost)).toFixed(2);
            }

            $scope.kpiVehicleProfit = { legendFor: "Sales Gross", valueOfLegend: moneyFormatter(frSalesGross * kpiPeriodValue), legendType: "Annually", title: "Forecasted Sales Gross (Annualized)" };
            $scope.kpiFiProfit = { legendFor: "FI Gross", valueOfLegend: moneyFormatter(frFiGross * kpiPeriodValue), legendType: "Annually", title: "Forecasted FI Gross (Annualized)" };
            $scope.kpicarryingCost = { legendFor: "Carrying Cost", valueOfLegend: moneyFormatter(carryingCost), legendType: "Annually", title: "Inventory Interest Rate * { Sum of (Excess Inventory * Inventory Value) Across All Models} (Annualized)" };
            $scope.kpiOpportunityCost = { legendFor: "Opportunity Cost", valueOfLegend: moneyFormatter(opportunityCost), legendType: "Annually", title: "{ Sum of (Shortage Inventory * Forecast Total Gross per Vehicle) Across All Models} (Annualized)" };
            $scope.kpiTotalInefficiencyCost = { legendFor: "Total Inefficiency Cost", valueOfLegend: moneyFormatter(inventoryVarianceCost), legendType: "Annually", title: "(Carrying Cost + Opportunity Cost)" };
            $scope.kpiCurrentInventory = { legendFor: "Current Inventory", valueOfLegend: numberFormatter(currentInventory), legendType: "Vehicles", title: "Current Inventory On-Hand at Dealership" };

            var recommendedDaysSupply = ($scope.filterResultForType == "new" ? 60 : (($scope.filterResultForType == "used") ? 45 : 55)),
			recommendedInventoryValue = 0, currentInventoryDaysSupply = 0;

            if ($scope.FrDataset.length > 0) {
                newMonthlyForecast = Math.round(_.reduce($scope.FrDataset, function (res, c) { res = res + c.userDefinedValue; return res }, 0) / $scope.FrDataset.length);

                if ($scope.filterResultForType == "new") {
                    recommendedInventoryValue = newMonthlyForecast * 2;
                }
                else if ($scope.filterResultForType == "used") {
                    recommendedInventoryValue = Math.round(newMonthlyForecast * 1.5);
                }
                else {
                    recommendedInventoryValue = Math.round(_.reduce(_.where($scope.forecastedSales, $scope.getCurrentFilterQuery()),
					function (res, c) {
					    res = res + (Math.round(c.unitsDeliveredUserEdit) * ((c.New_Used == 1) ? 2 : 1.5));
					    return res;
					}, 0) / $scope.FrDataset.length);
                }

                if (newMonthlyForecast == 0) {
                    recommendedDaysSupply = 0;
                } else {
                    recommendedDaysSupply = Math.round(30 * (recommendedInventoryValue / newMonthlyForecast));
                }

            }

            if (recommendedInventoryValue !== 0) {
                currentInventoryDaysSupply = Math.round((currentInventory / newMonthlyForecast) * 30);
            }

            var vehicleDiffColor = "",
                vehicleSalesMargin = "",
                vehicleDiff = 0,
                vehicleDiffLegendName = "",
                daysSalesMargin = "",
                daysDiffColor = "",
                daysDiff = 0,
                daysDiffLegendName = "";

            var vechicleMarginHighRed = Math.round(recommendedInventoryValue + (recommendedInventoryValue * 0.1));
            var vechicleMarginLowRed = Math.round(recommendedInventoryValue - (recommendedInventoryValue * 0.1));
            var vechicleMarginHighEmber = Math.round(recommendedInventoryValue + (recommendedInventoryValue * 0.05));
            var vechicleMarginLowEmber = Math.round(recommendedInventoryValue - (recommendedInventoryValue * 0.05));

            var daysMarginHighRed = Math.round(recommendedDaysSupply + (recommendedDaysSupply * 0.1));
            var daysMarginLowRed = Math.round(recommendedDaysSupply - (recommendedDaysSupply * 0.1));
            var daysMarginHighEmber = Math.round(recommendedDaysSupply + (recommendedDaysSupply * 0.05));
            var daysMarginLowEmber = Math.round(recommendedDaysSupply - (recommendedDaysSupply * 0.05));



            // for current inventory diff cal
            if (recommendedInventoryValue == currentInventory) {
                vehicleSalesMargin = "";
                vehicleDiffColor = "";
            }
            else if (vechicleMarginHighRed <= currentInventory) {
                vehicleSalesMargin = "fa fa-arrow-down fa-2";
                vehicleDiffColor = "red";
            }
            else if (vechicleMarginLowRed >= currentInventory) {
                vehicleSalesMargin = "fa fa-arrow-up fa-2";
                vehicleDiffColor = "red";
            }
            else if (vechicleMarginHighRed > currentInventory && vechicleMarginHighEmber <= currentInventory) {
                vehicleSalesMargin = "fa fa-arrow-down fa-2";
                vehicleDiffColor = "yellow";
            }
            else if (vechicleMarginLowRed < currentInventory && vechicleMarginLowEmber >= currentInventory) {
                vehicleSalesMargin = "fa fa-arrow-up fa-2";
                vehicleDiffColor = "yellow";
            }
            else if (vechicleMarginHighEmber > currentInventory && vechicleMarginLowEmber < currentInventory) {
                vehicleSalesMargin = "";
                vehicleDiffColor = "green";
            }

            //For days supply cal
            if (recommendedDaysSupply == currentInventoryDaysSupply) {
                daysSalesMargin = "";
                daysDiffColor = "";
            } else if (daysMarginHighRed <= currentInventoryDaysSupply) {
                daysSalesMargin = "fa fa-arrow-down fa-2";
                daysDiffColor = "red";
            }
            else if (daysMarginLowRed >= currentInventoryDaysSupply) {
                daysSalesMargin = "fa fa-arrow-up fa-2";
                daysDiffColor = "red";
            }
            else if (daysMarginHighRed > currentInventoryDaysSupply && daysMarginHighEmber <= currentInventoryDaysSupply) {
                daysSalesMargin = "fa fa-arrow-down fa-2";
                daysDiffColor = "yellow";
            }
            else if (daysMarginLowRed < currentInventoryDaysSupply && daysMarginLowEmber >= currentInventoryDaysSupply) {
                daysSalesMargin = "fa fa-arrow-up fa-2";
                daysDiffColor = "yellow";
            }
            else if (daysMarginHighEmber > currentInventoryDaysSupply && daysMarginLowEmber < currentInventoryDaysSupply) {
                daysSalesMargin = "";
                daysDiffColor = "green";
            }

            if ((currentInventory - recommendedInventoryValue) < 0) {
                vehicleDiffLegendName = "Inventory Shortage";
            }
            else {
                vehicleDiffLegendName = "Excess Inventory";
            }

            if (recommendedDaysSupply - currentInventoryDaysSupply < 0) {
                daysDiffLegendName = "Excess Days Supply";
            }
            else {
                daysDiffLegendName = "Shortage Days Supply";
            }

            vehicleDiff = Math.abs(currentInventory - recommendedInventoryValue);
            daysDiff = Math.abs(currentInventoryDaysSupply - recommendedDaysSupply);

            $scope.currnetInventoryLegends = [
                    { LegendName: "Current Inventory", currentValue: currentInventory, recommendedValue: recommendedInventoryValue, dataForType: "Vehicles", profitLossType: vehicleSalesMargin, profitLossColor: vehicleDiffColor, diffValue: vehicleDiff, diffLegendName: vehicleDiffLegendName, title: "Absolute Value of (CurrentInventory - RecommendedInventoryValue)" },
                    { LegendName: "Days Supply", currentValue: currentInventoryDaysSupply, recommendedValue: recommendedDaysSupply, dataForType: "Days", profitLossType: daysSalesMargin, profitLossColor: daysDiffColor, diffValue: daysDiff, diffLegendName: daysDiffLegendName, title: "Absolute Value of (CurrentInventoryDaysSupply - RecommendedDaysSupply)" }
            ];
        }

        function initialiseFilters() {
            var currentDate = moment();
            var _startDate = moment().startOf('Month'),
			_endDate = moment(currentDate).add(3, "M").endOf('month');

            $scope.kpiPeriod = "A";
            $scope.selectedMake = { MakeId: -10, MakeName: "ALL Makes" };
            $scope.selectedModel = { MakeId: -10, ModelId: -10, ModelName: "All Models" };
            $scope.modelList = [];
            $scope.filterResultForType = "both";
            $scope.dates = { startDate: _startDate, endDate: _endDate, getFormatedDate: function (val) { return moment(val).startOf('Month').format('DD-MM-YYYY'); } };

        }

        function SetMakeForType() {
            $scope.listOfMakeModel = [];
            $scope.modelList = [];
            if ($scope.filterResultForType == "both") {
                $scope.listOfMakes = _.uniq(_.sortBy($scope.listOfMakesALL, function (d) { return d.MakeName; }), function (o) { return o.MakeName; });
            }
            else {
                var filterQuery = {};

                if ($scope.filterResultForType == "new") {
                    filterQuery.VehicleType = "New";
                }
                else {
                    filterQuery.VehicleType = "Used";
                }

                $scope.listOfMakes = _.sortBy(_.where($scope.listOfMakesALL, filterQuery), function (d) { return d.MakeName; });
            }

            $scope.selectedMake = { MakeId: -10, MakeName: "ALL Makes" };
        }

        function SetModelForType() {
            if ($scope.filterResultForType == "both") {
                $scope.listOfMakeModel = _.uniq(_.sortBy($scope.listOfMakeModelAll, function (d) { return d.ModelName; }), function (o) { return o.ModelName; });
            }
            else {
                var filterQuery = {};

                if ($scope.filterResultForType == "new") {
                    filterQuery.VehicleType = "New";
                }
                else {
                    filterQuery.VehicleType = "Used";
                }
                $scope.listOfMakeModel = _.sortBy(_.where($scope.listOfMakeModelAll, filterQuery), function (d) { return d.ModelName; });
            }
        }
        /*Section for fetching date from API start*/
        function getVehicleMakeModel() {
            return apiService.get('api/getvehiclemakemodel?dealerkey=' + $scope.Dealer.Id, null, function (data) {
                $scope.listOfMakesALL = _.sortBy(data.data.Makes, function (d) { return d.MakeName; });
                $scope.listOfMakeModelAll = _.sortBy(data.data.Models, function (d) { return d.ModelName; });

                SetMakeForType();
                GetCurrentInventory();
            });
        }

        function GetInventoryData() {
            apiService.get('api/vehicleinventory?dealerId=' + $scope.Dealer.Id, null, function (data) {
                $scope.inventroyData = angular.copy(data.data);
                $scope.updateChart();
            });
        }

        function GetActualSales() {
            apiService.get('api/getactualsale?dealerid=' + $scope.Dealer.Id + '&makeid=0&modelid=0', null, function (data) {
                $scope.ActualDataset = angular.copy(data.data);
                $scope.updateChart();
                //var _ActualDataSet = FilterActualData(angular.copy(data.data));
                //$scope.AcDataset = CustomizeActualDataForChart(_ActualDataSet, d3.timeParse("%d-%m-%Y"));
            }, function (errorMsg) {
                notificationService.error("error occured while fetching historical sales from the server.");
            });
        }

        function GetForecastSales() {
            return apiService.get('api/getforecastedsale?dealerid=' + $scope.Dealer.Id + '&startdate=' + $scope.dates.getFormatedDate($scope.dates.startDate) + '&enddate=' + $scope.dates.getFormatedDate($scope.dates.endDate), null, function (data) {

                var modifiedForecastedData = _.map(data.data, function (element) {
                    return _.extend({}, element, {
                        unitsDeliveredUserEdit: element.Forecast_Units_Delivered
                    });
                });

                $scope.forecastedSales = _.sortBy(modifiedForecastedData, function (d) { return d.FullDate; });
                $scope.setupSliders();
                $scope.updateChart();
            });
        }

        function GetCurrentInventory() {
            return apiService.get('api/currentinventory?&dealerid=' + $scope.Dealer.Id, null, function (data) {
                $scope.currentInventoryData = _.filter(data.data, function (a) {
                    return _.find($scope.listOfMakeModelAll, function (b) {
                        return b.ModelId === a.ModelKey;
                    });
                });
                $scope.updateChart();
            });
        }

        function Init() {
            initialiseFilters();
            getVehicleMakeModel();
            GetInventoryData();
            GetActualSales();
            GetForecastSales();
        }
        
        function prepareDownloadRequestObject() {
            return {
                Dealer: $scope.Dealer.Id,
                Startdate: $scope.dates.getFormatedDate($scope.dates.startDate),
                Enddate: $scope.dates.getFormatedDate($scope.dates.endDate),
                SelectedType: $scope.filterResultForType == "new" ? 1 : $scope.filterResultForType == "used" ? 0 : 2,//new=1/used=0/both=2
                MakeId: $scope.selectedMake.MakeId,
                ModelId: $scope.selectedModel.ModelId
            };
        }

        $scope.downloadFile = function () {
            var reportParam = prepareDownloadRequestObject(), tempInLoop = {}, inventoryCostRate = ($scope.sliderInterest.value / 100);
            reportParam.ForecastedData = [];
            var filteredCurrentInventory = _.filter($scope.currentInventoryData, getSelectedFilterForInventory());
            var _ForecastDataSet = FilterForecastedData($scope.forecastedSales, filteredCurrentInventory);
            var _filteredCurrentInventory = _.filter($scope.currentInventoryData, getSelectedFilterForInventory());

            let temp = [];

            //Current Inventory, Excess Inventory, Carrying Cost, Shortage Inventory, Opportunity Cost
            _.each(_ForecastDataSet, function (d) {
                temp.push({
                    FullDate: d.FullDate,
                    Make_Key: d.Make_Key,
                    Make_Desc: d.Make_Desc,
                    Model_Key: d.Model_Key,
                    Model_Desc: d.Model_Desc,
                    New_Used: d.New_Used,
                    Forecast_Units_Delivered: d.Forecast_Units_Delivered,
                    UnitsDeliveredUserEdit: d.unitsDeliveredUserEdit,
                    Inventory: d.Inventory,
                    InventoryValue: d.InventoryValue,
                    RecommendedInventory: d.inventoryForecasted,
                    TotalGross: Math.round(d.Forecast_Sales_GrossEdit + d.Forecast_FI_GrossEdit),
                    InventoryCostRate: inventoryCostRate
                });
            });



            var groups = _.groupBy(temp, function (value) {
                return value.Make_Key + '#' + value.Model_Key + '#' + value.New_Used;
            });

            function groupSum(data, Col) {
                return _.reduce(_.pluck(data, Col), function (memo, num) { return memo + Math.round(num); }, 0);
            }

            function groupAvg(data, Col) {
                return Math.round(_.reduce(_.pluck(data, Col), function (memo, num) { return memo + num; }, 0) / data.length);
            }
            function groupAvgWoRound(data, Col) {
                return (_.reduce(_.pluck(data, Col), function (memo, num) { return memo + num; }, 0) / data.length);
            }

            function fetchRecomended(group) {
                return Math.round(_.reduce(_.pluck(group, 'UnitsDeliveredUserEdit'), function (memo, num) { var val = memo + (Math.round(num) * ((group[0].New_Used == 1) ? 2 : 1.5)); return val; }, 0) / group.length)
            }

            let divideBy = ($scope.kpiPeriod == 'M') ? 12 : 1;

            var _data = _.map(groups, function (group) {
                return {
                    Date: moment($scope.dates.startDate).format('MMM-YYYY') + " - " + moment($scope.dates.endDate).format('MMM-YYYY'),
                    "Veh Make Id": group[0].Make_Key,
                    "Veh Make": _.pluck(group, 'Make_Desc')[0],
                    "Veh Model Id": group[0].Model_Key,
                    "Veh Model": _.pluck(group, 'Model_Desc')[0],
                    "NewUsed": (group[0].New_Used == 1) ? "New" : "Used",
                    "NewOrUsed": group[0].New_Used,
                    "Recommended Inventory": fetchRecomended(group),
                    "Current Inventory": groupAvg(group, 'Inventory'),
                    "Original Forecast": Math.round(_.reduce(_.pluck(group, 'Forecast_Units_Delivered'), function (memo, num) { return memo + Math.round(num); }, 0) / group.length),
                    "Revised Forecast": Math.round(_.reduce(_.pluck(group, 'UnitsDeliveredUserEdit'), function (memo, num) { return memo + Math.round(num); }, 0) / group.length),
                    "Total Gross": Math.round(_.reduce(_.pluck(group, 'TotalGross'), function (memo, num) { return memo + num; }, 0) / group.length),
                    "Inventory Value": groupAvgWoRound(group, 'InventoryValue'),
                    "Excess Inventory": groupAvg(group, 'Inventory') > fetchRecomended(group) ? groupAvg(group, 'Inventory') - fetchRecomended(group) : 0,
                    "Annual Carrying Cost": Math.round((groupAvg(group, 'Inventory') > fetchRecomended(group) ? groupAvgWoRound(group, 'InventoryCostRate') * groupAvgWoRound(group, 'InventoryValue') * (groupAvgWoRound(group, 'Inventory') - fetchRecomended(group)) : 0)/divideBy),
                    "Shortage Inventory": groupAvg(group, 'Inventory') < fetchRecomended(group) ? fetchRecomended(group) - groupAvg(group, 'Inventory') : 0,
                    "Annual Opportunity Cost": Math.round((groupAvg(group, 'Inventory') < fetchRecomended(group) ? (groupAvg(group, 'TotalGross') * (fetchRecomended(group) - groupAvg(group, 'Inventory'))) : 0)/divideBy),
                    "InvCostRate": groupAvgWoRound(group, 'InventoryCostRate')
                }
            });

            _.each(_filteredCurrentInventory, function (d) {
                if (!_.find(_data, function (o) { return o["Veh Model Id"] == d.ModelKey && o["NewOrUsed"] == d.NewOrUsed; })) {
                    _data.push({
                        Date: moment($scope.dates.startDate).format('MMM-YYYY') + " - " + moment($scope.dates.endDate).format('MMM-YYYY'),
                        "Veh Make Id": d.MakeKey,
                        "Veh Make": d.Make,
                        "Veh Model Id": d.ModelKey,
                        "Veh Model": d.Model,
                        "NewUsed": (d.NewOrUsed == 1) ? "New" : "Used",
                        "NewOrUsed": d.NewOrUsed,
                        "Recommended Inventory": 0,
                        "Current Inventory": d.Count,
                        "Original Forecast": 0,
                        "Revised Forecast": 0,
                        "Total Gross": 0,
                        "Inventory Value": d.Value,
                        "Excess Inventory": 0,
                        "Annual Carrying Cost": 0,
                        "Shortage Inventory": 0,
                        "Annual Opportunity Cost": 0,
                        "InvCostRate": 0
                    });
                }
            });
            //var result = _.filter(a, function (obj) { return diff.indexOf(obj.id) >= 0; });


            reportParam.ForecastedData = _data;

            //Create Generic URL Path for all parts
            var httpPath = 'api/vehiclesalesforecastcsv';
            //Send Request
            var downloadedData = apiService.post(httpPath, JSON.stringify(reportParam), null, function (result) {
                var file = new Blob([result.data], {
                    type: 'application/csv;base64'
                });
                var url = window.URL || window.webkitURL;
                var fileURL = url.createObjectURL(file);
                var seconds = new Date().getTime() / 1000;
                var fileName = "cert" + parseInt(seconds) + ".csv";

                if (window.navigator.msSaveOrOpenBlob) // For IE
                    window.navigator.msSaveOrOpenBlob(new Blob([result.data], { type: "application/csv;base64" }), fileName);
                else {
                    var a = document.createElement("a");
                    document.body.appendChild(a);
                    a.href = fileURL;
                    a.download = fileName;
                    a.click();
                    document.body.removeChild(a);
                }
            });
        };

        $scope.print = function () {
            window.print();
        };
    }

})(angular.module('dealerWizard'));
