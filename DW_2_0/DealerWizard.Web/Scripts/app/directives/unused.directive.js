﻿(function (app) {
    'use strict';

    app.component('smallMultipleBars',
        {
            templateUrl: "/Scripts/app/directives/smallMultipleBars.html",
            controller: ['$scope', '$element', '$attrs', smallMultipleBarsCtrl],
            bindings: {
                data: "<",
                onSelectBar: "&"
            }
        });


    function smallMultipleBarsCtrl($scope, $element, $attrs) {

        var ctrl = this;

        ctrl.select = function (d) {
            ctrl.onSelectBar({ bar: { selectedDate: d.Date } });
        }

        var ui = { showPriorPeriod: false, periodFor: 'YTD' };

        var margin = { top: 35, right: 20, bottom: 10, left: 50 },
              width = 400 - margin.left - margin.right,
              height = 130 - margin.top - margin.bottom;

        // Parse the date / time
        var parseDate = d3.time.format("%Y-%m-%d").parse;
        var parseDateInDays = d3.time.format("%Y-%m-%d").parse;

        ctrl.$onChanges = function (changedObj) {

            if (changedObj.data.currentValue !== "$") {
                renderMultipleBulletChart("divSmallMultiple", changedObj.data.currentValue.data);
            }
        };



        function renderMultipleBulletChart(idfordisp, chartJson) {

            if (ui.showPriorPeriod) {
                chartJson.previous.forEach(function (d, i) {
                    d.measureDate = (ui.periodFor == "YTD") ? parseDate(chartJson.Current[i].Date) : parseDateInDays(chartJson.Current[i].Date);
                    d.Date = (ui.periodFor == "YTD") ? parseDate(d.Date) : parseDateInDays(d.Date);
                    d.Value = +d.Value;
                    d.Name = d.Name;
                    d.select = function () {
                        ctrl.select(this);
                    };
                });
            }

            angular.forEach(chartJson.Current, function (d, i) {

                d.measureDate = (ui.periodFor == "YTD") ? parseDate(chartJson.Current[i].Date) : parseDateInDays(chartJson.Current[i].Date);
                d.Date = (ui.periodFor == "YTD") ? parseDate(d.Date) : parseDateInDays(d.Date);
                d.Value = +d.Value;
                d.Name = d.Name;
                d.select = function () {
                    ctrl.select(this);
                };

            });

            var config = { data: chartJson, dateFormat: (ui.periodFor == "YTD") ? d3.time.format("%b %y") : d3.time.format("%d %b") };

            d3.select('#' + idfordisp).selectAll("svg").remove();
            d3.select(".divSvgAxis").remove();

            drawSmallMultiple(config);

            function drawSmallMultiple(config) {
                var svg = [], x = [], y = [], xAxis = [], yAxis = [], data = config.data.Current, PYearData = config.data.previous;
                var line = [], Linedata = [];

                var ActualData = d3.nest()
                          .key(function (d) { return d.Name; })
                          .entries(data);

                var PYearActualData = [];

                if (ui.showPriorPeriod) {
                    PYearActualData = d3.nest()
                              .key(function (d) { return d.Name; })
                              .entries(PYearData);
                }

                function FormatTicks(d) {
                    if (d >= 100000) {
                        return (d / 100000) + 'L';
                    }
                    else if (d >= 1000) {
                        return (d / 1000) + 'T';
                    }
                    else if (d <= -100000) {
                        return (d / 100000) + 'L';
                    }
                    else if (d <= -1000) {
                        return (d / 1000) + 'T';
                    }
                    else {
                        return d;
                    }
                }

                for (var SvgCount = 0; SvgCount < ActualData.length; SvgCount++) {
                    svg[SvgCount] = {};
                    x[SvgCount] = d3.scale.ordinal().rangeRoundBands([0, width], .05);

                    y[SvgCount] = d3.scale.linear().range([height, 0]);

                    xAxis[SvgCount] = d3.svg.axis()
                         .scale(x[SvgCount])
                         .orient("bottom")
                         .tickFormat(config.dateFormat);

                    yAxis[SvgCount] = d3.svg.axis()
                         .scale(y[SvgCount])
                         .orient("left")
                         .ticks(5)
                         .tickFormat(FormatTicks);

                    var tip = d3.tip()
                   .attr('class', 'd3-tip')
                   .offset([-10, 0])
                   .html(function (d) {
                       return "<strong>" + MonthName(d.Date.getMonth() + 1) + " " + ((ui.periodFor == "YTD") ? d.Date.getFullYear() : d.Date.getDate()) + "</strong> <br/> <strong>" + d.Name + ":</strong> <span style='color:white'>" + d.Value + "</span>";
                   })

                    var MaxBounValue = Math.max.apply(Math, ActualData[SvgCount].values.map(function (o) { return o.Value; }));
                    var MinBounValue = Math.min.apply(Math, ActualData[SvgCount].values.map(function (o) { return o.Value; }));
                    var PYMaxBounValue = 0, PYMinBounValue = 0;
                    if (ui.showPriorPeriod) {
                        PYMaxBounValue = Math.max.apply(Math, PYearActualData[SvgCount].values.map(function (o) { return o.Value; }));
                        PYMinBounValue = Math.min.apply(Math, PYearActualData[SvgCount].values.map(function (o) { return o.Value; }));
                    }

                    MaxBounValue = (MaxBounValue > PYMaxBounValue) ? MaxBounValue : PYMaxBounValue;
                    MinBounValue = (MinBounValue < PYMinBounValue) ? MinBounValue : PYMinBounValue;

                    x[SvgCount].domain(data.map(function (d) { return d.measureDate; }));
                    y[SvgCount].domain([MinBounValue, MaxBounValue]);


                    svg[SvgCount] = d3.select('#' + idfordisp)
                        .append("svg").attr("width", width + margin.left + margin.right)
                    .attr("height", height + margin.top + margin.bottom)
                    .append("g")
                    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

                    svg[SvgCount].call(tip);

                    svg[SvgCount].append("g")
                    .selectAll("text")
                        .style("text-anchor", "end")
                        .attr("dx", "-.8em")
                        .attr("dy", "-.55em");

                    svg[SvgCount].append("text")
                    .attr("x", (width / 2))
                    .attr("y", 0 - (margin.top / 2))
                    .attr("text-anchor", "middle")
                        .style("font-family", '"Helvetica Neue",Helvetica,Arial,sans-serif')
                    .style("font-size", "14px")
                    .text(ActualData[SvgCount].key);

                    //svg[SvgCount].append("g")
                    //    .attr("class", "x axis")
                    //    .attr("transform", "translate(0," + height + ")")
                    //    .call(xAxis[SvgCount])
                    //  .selectAll("text")
                    //    .style("text-anchor", "end")
                    //    .attr("dx", "-.8em")
                    //    .attr("dy", "-.55em")
                    //    .attr("transform", "rotate(-90)");

                    d3.select('body').append('div').attr('id', 'tooltip' + [SvgCount]).attr('class', 'hdn').append('span').attr('id', 'value');

                    svg[SvgCount].append("g")
                        .attr("class", "y axis")
                        .call(yAxis[SvgCount]);


                    svg[SvgCount].selectAll(".bar")
                                 .data(function (d) { return ActualData[SvgCount].values; })
                                 .enter()
                                 .append("rect").attr("class", "bar").on("click", function (d, i) {
                                     d.select();
                                 })
                        .attr("x", function (d) { return x[SvgCount](d.measureDate); })
                        .attr("width", function () {
                            return x[SvgCount].rangeBand();
                        })
                        .attr("y", function (d, i) {
                            return (d.Value < 0) ? y[SvgCount](0) : y[SvgCount](d.Value);
                        })
                        .attr("height", function (d, i) {
                            if (MinBounValue < 0) {
                                return Math.abs(y[SvgCount](d.Value) - y[SvgCount](0));
                            }
                            else {
                                return Math.abs(height - y[SvgCount](d.Value));
                            }


                        })
                        .attr("fill", "lightsteelblue")
                    .on("mouseover", function (d, i) {
                        var bars = d3.select('#' + idfordisp).selectAll('svg').selectAll('.bar');

                        for (var a = 0; a < ActualData.length; a++) {

                            d3.select(bars[a][i]).classed("barhover", true);

                            var pst = $(bars[a][i]).position();
                            var _data = ActualData[a].values[i];


                            //"<strong>" + MonthName(_data.Date.getMonth() + 1) + " " + ((PeriodFor == "YTD") ? _data.Date.getFullYear() : _data.Date.getDate()) + "</strong> <br/> <strong>" + _data.Name + ":</strong> <span>" + _data.Value + "</span>";
                            var TooltipHtml = "<strong>" + MonthName(_data.Date.getMonth() + 1) + " " + ((ui.periodFor == "YTD") ? _data.Date.getFullYear() : _data.Date.getDate()) + "</strong> : <span>" + _data.Value + "</span>";

                            if (ui.showPriorPeriod) {
                                var _Pdata = PYearActualData[a].values[i];
                                TooltipHtml += "<br/><strong>" + MonthName(_Pdata.Date.getMonth() + 1) + " " + ((ui.periodFor == "YTD") ? _Pdata.Date.getFullYear() : _Pdata.Date.getDate()) + "</strong> : <span>" + _Pdata.Value + "</span>";
                                var _Difference = Math.round(_data.Value - _Pdata.Value, 2);
                                var _Percent = Math.round((_Difference / _Pdata.Value) * 100, 2);
                                var _color = (_Difference < 0) ? "red" : "green";
                                TooltipHtml += '<br/> Diff: <span style="color:' + _color + '">' + _Difference + ' (' + _Percent + '% ) </span';
                            }

                            d3.select('#tooltip' + a)
                                .attr('class', 'ttip')
                            .style('left', (pst.left + 10) + 'px')
                            .style('top', (pst.top - 20) + 'px')
                                .select('#value')
                            .html(TooltipHtml);

                            d3.select('#tooltip' + a).classed('hdn', false);
                        }
                        //tip.show(d,i);
                        //d3.select(this).classed("barhover", true);
                    })
                    .on("mouseout", function (d, i) {
                        var bars = d3.select('#' + idfordisp).selectAll('svg').selectAll('.bar');
                        for (var a = 0; a < ActualData.length; a++) {
                            d3.select(bars[a][i]).classed("barhover", false);
                            d3.select('#tooltip' + a).classed('hdn', true);
                        }
                        $('.ttip').addClass('hdn');
                        //tip.hide(d,i);
                        // d3.select(this).classed("barhover", false);
                    });
                    if (ui.showPriorPeriod) {
                        line[SvgCount] = d3.svg.line()
                                     .x(function (d) { return x[SvgCount](d.measureDate); })
                                     .y(function (d) { return y[SvgCount](d.Value); })
                                     .interpolate('linear');

                        svg[SvgCount].append("path")
                                      .datum(PYearActualData[SvgCount].values)
                                      .attr("class", "line")
                                      .attr("d", line[SvgCount]);

                        svg[SvgCount].selectAll("dot")
                                    .data(function (d) { return PYearActualData[SvgCount].values; })
                                    .enter().append("circle")
                            .attr('class', 'CircularDot')
                                    .attr("r", 3)
                                    .attr("cx", function (d) { return x[SvgCount](d.measureDate); })
                                    .attr("cy", function (d) { return y[SvgCount](d.Value); })
                                    .attr('fill', 'steelblue')
                                    .on("mouseover", function (d, i) {
                                        var Circles = d3.select('#' + idfordisp).selectAll('svg').selectAll('.CircularDot');

                                        for (var a = 0; a < PYearActualData.length; a++) {

                                            d3.select(Circles[a][i]).attr("r", 5);

                                            var pst = $(Circles[a][i]).position();
                                            var _data = PYearActualData[a].values[i];

                                            //"<strong>" + MonthName(_data.Date.getMonth() + 1) + " " + ((PeriodFor == "YTD") ? _data.Date.getFullYear() : _data.Date.getDate()) + "</strong> <br/> <strong>" + _data.Name + ":</strong> <span>" + _data.Value + "</span>"
                                            var TooltipHtml = "<strong>" + MonthName(_data.Date.getMonth() + 1) + " " + ((ui.periodFor == "YTD") ? _data.Date.getFullYear() : _data.Date.getDate()) + "</strong> : <span>" + _data.Value + "</span>";

                                            if (ui.showPriorPeriod) {
                                                var _Cdata = ActualData[a].values[i];
                                                TooltipHtml += "<br/><strong>" + MonthName(_Cdata.Date.getMonth() + 1) + " " + ((ui.periodFor == "YTD") ? _Cdata.Date.getFullYear() : _Cdata.Date.getDate()) + "</strong> : <span>" + _Cdata.Value + "</span>";
                                                var _Difference = Math.round(_Cdata.Value - _data.Value, 2);
                                                var _Percent = Math.round((_Difference / _data.Value) * 100, 2);
                                                var _color = (_Difference < 0) ? "red" : "green";
                                                TooltipHtml += '<br/> Diff: <span style="color:' + _color + '">' + _Difference + ' (' + _Percent + '% ) </span';
                                            }

                                            d3.select('#tooltip' + a)
                                                .attr('class', 'ttip')
                                            .style('left', (pst.left + 10) + 'px')
                                            .style('top', (pst.top - 20) + 'px')
                                                .select('#value')
                                            .html(TooltipHtml);

                                            d3.select('#tooltip' + a).classed('hdn', false);
                                        }
                                    })
                                    .on("mouseout", function (d, i) {
                                        var Circles = d3.select('#' + idfordisp).selectAll('svg').selectAll('.CircularDot');
                                        for (var a = 0; a < PYearActualData.length; a++) {
                                            d3.select(Circles[a][i]).attr("r", 3);
                                            d3.select('#tooltip' + a).classed('hdn', true);
                                        }
                                        $('.ttip').addClass('hdn');
                                    });
                    }
                }

                var x1, y1, xAxis1, yAxis1, svg2 = {};

                x1 = d3.scale.ordinal().rangeRoundBands([0, width], .05);

                xAxis1 = d3.svg.axis()
                         .scale(x1)
                         .orient("bottom")
                         .tickFormat(config.dateFormat);

                x1.domain(data.map(function (d) { return d.measureDate; }));

                svg2 = d3.select('#' + idfordisp)
                    .append('div').attr('class', 'divSvgAxis')
                    .selectAll("svg")
                 .data(ActualData)
                 .enter()
                 .append("svg:svg").attr("width", width + margin.left + margin.right)
                 .attr("height", 50)
               .append("g")
                 .attr("transform", "translate(0,0)");

                svg2
                    .append("g")
                    .attr("class", "x axis")
               .attr("transform", "translate(" + margin.left + "," + 0 + ")")
               .call(xAxis1)
             .selectAll("text")
               .style("text-anchor", "end")
               .attr("dx", "-.8em")
               .attr("dy", "-.55em")
               .attr("transform", "rotate(-45)");


                var resizeTimmer;
                d3.select(window).on("resize.one", function (e) {
                    clearTimeout(resizeTimmer);
                    resizeTimmer = setTimeout(function () {
                        resized();
                    }, 350)
                });

                function resized() {

                    //Get the width of the window
                    var w = Math.ceil((d3.select('#' + idfordisp).node().clientWidth) / 2.04) - margin.left - margin.right;

                    var ss = d3.select('#' + idfordisp).selectAll('svg');

                    //svg = [], x = [], y = [], xAxis = [], yAxis
                    if (w < 220) {
                        w = Math.ceil((d3.select('#' + idfordisp).node().clientWidth)) - margin.left - margin.right;
                    }

                    var line = d3.svg.line()
                  .x(function (d) { return x(d.measureDate); })
                  .y(function (d) { return y(d.Value); });


                    ss.forEach(function (item, index) {
                        item.forEach(function (item2, index1) {
                            if (index1 < ActualData.length) {
                                x[index1] = x[index1].rangeRoundBands([0, w], .25);
                                xAxis[index1] = xAxis[index1].scale(x[index1]);
                                var svg = d3.select(item2);
                                svg.attr('width', w + margin.left + margin.right);
                                var Bars = svg.selectAll(".bar");
                                Bars[0].forEach(function (bar, index2) {
                                    d3.select(bar)
                                        .attr("x", x[index1](ActualData[0].values[index2].measureDate))
                                        .attr("width", x[index1].rangeBand());

                                });

                                if (ui.showPriorPeriod) {
                                    var Paths = svg.selectAll("path.line");

                                    Paths.forEach(function (path, index2) {

                                        line[index1] = d3.svg.line()
                                      .x(function (d) { return x[index1](d.measureDate); })
                                      .y(function (d) { return y[index1](d.Value); });


                                        Linedata[index1] = PYearActualData[index1].values.map(function (d) {
                                            return {
                                                measureDate: d.measureDate,
                                                Value: d.Value
                                            };
                                        });

                                        svg.select("path.line")
                                      .datum(Linedata[index1])
                                      .attr("class", "line")
                                      .attr("d", line[index1]);
                                    });

                                    var lineDots = svg.selectAll("circle.CircularDot");

                                    lineDots.data(function (d) { return PYearActualData[index1].values; })
                                            .attr("r", 3)
                                            .attr("cx", function (d) { return x[index1](d.measureDate); })
                                            .attr("cy", function (d) { return y[index1](d.Value); });
                                }


                                var txt = svg.selectAll("text");
                                txt.forEach(function (t, index2) {
                                    d3.select(t[0])
                                        .attr("x", (w / 2))

                                });

                            }
                            else {

                                x1 = x1.rangeRoundBands([0, w], .05);
                                xAxis1 = xAxis1.scale(x1);

                                var svg = d3.select(item2);
                                svg.attr('width', w + margin.left + margin.right);
                                svg.call(xAxis1);
                            }

                        });
                    });


                }
                resized();
            }

        }

        function MonthName(index) {
            var month = new Array();
            month[1] = "Jan";
            month[2] = "Feb";
            month[3] = "Mar";
            month[4] = "Apr";
            month[5] = "May";
            month[6] = "Jun";
            month[7] = "Jul";
            month[8] = "Aug";
            month[9] = "Sep";
            month[10] = "Oct";
            month[11] = "Nov";
            month[12] = "Dec";

            return month[index];
        }


    }

})(angular.module('dealerWizard'));