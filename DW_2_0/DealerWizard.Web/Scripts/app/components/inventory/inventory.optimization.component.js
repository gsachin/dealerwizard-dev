﻿function inventoryOptimizationCtrl($scope) {
    let ctrl = this;

    ctrl.modelData = [["Ram", 60], ["Jeep", 50]];

    ctrl.inventory = { //requires angular-bootstrap to display tooltips
        value: 0,
        options: {
            floor: 0,
            ceil: 120,
            step: 1,
            showTicksValues: 10, 
            showSelectionBar: true,
            //onEnd: function () {
            //    if ($scope.slider.value !== $scope.tableDataChange) {
            //        $scope.tableDataStart = $scope.tableDataChange;
            //        $scope.tableDataChange = $scope.slider.value;
            //        $scope.recalculateTable();
            //    }
            //},
            translate: function (value) {
                return value;
            },
            ticksTooltip: function (v) {
                return 'Tooltip for ' + v;
            }
        }
    };

    ctrl.dbData = [
                            { Name: "RAM", Val1: 401, Val2: 450 },
                            { Name: "JEEP", Val1: 399, Val2: 440 },
                            { Name: "DODGE", Val1: 339, Val2: 345 },
                            { Name: "CHRYSLER", Val1: 211, Val2: 234 },
                            { Name: "ACURA", Val1: 412, Val2: 449 },
                            { Name: "AUDI", Val1: 359, Val2: 431 },
                            { Name: "BMW", Val1: 190, Val2: 214 },
                            { Name: "FORD", Val1: 214, Val2: 239 },
                            { Name: "RAM1", Val1: 201, Val2: 222 },
                            { Name: "JEEP1", Val1: 167, Val2: 201 },
                            { Name: "DODGE1", Val1: 99, Val2: 114 },
                            { Name: "CHRYSLER1", Val1: 65, Val2: 78 },
                            { Name: "ACURA1", Val1: 110, Val2: 119 },
                            { Name: "AUDI1", Val1: 75, Val2: 89 },
                            { Name: "BMW1", Val1: 41, Val2: 54 },
                            { Name: "FORD1", Val1: 0, Val2: 0 }
    ];


}

inventoryOptimizationCtrl.$inject = ['$scope'];

angular.module('dealerWizard').component('inventoryOptimization', {
    templateUrl:'./Scripts/app/components/inventory/inventoryoptimization.html',
    bindings: {

    },
    controller:inventoryOptimizationCtrl
})