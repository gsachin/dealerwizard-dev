﻿(function (app) {

    app.component("widgetPopup", {
        templateUrl: "/Scripts/app/components/popup/widgetpopup.html",
        controller: ['$scope', '$element', '$attrs', 'reportService', widgetPopupCtrl],
        bindings: {
            showpopup: "<",
            title: "<",
            selectedElem: "<",
            widgetName:"<",
            onCloseClick: "&",
            onWidgetSelection: "&"
        }
    });

    function widgetPopupCtrl($scope, $element, $attrs, reportService) {
        var ctrl = this;
        ctrl.widgetName = "Widget";
        ctrl.selectedElem = { type: "bar" };
        ctrl.gaugesList = [1];
        ctrl.addItemToGaugeList = () => {
            ctrl.gaugesList.push(ctrl.gaugesList.length + 1);
        };
        ctrl.customReportList = [];
        ctrl.selectedTableReport = "0";

        ctrl.changeWidgetName = (name) => { ctrl.widgetName = name };

        ctrl.widgetList = [{
            widgetClass:"fa fa-bar-chart",
            type: "bar",
            selected:true
        },
        {
            widgetClass: "fa fa-table",
            type: "table",
            selected: false,
            reportKey: ctrl.selectedTableReport
        },
        {
            widgetClass: "fa fa-pie-chart",
            type: "kpi",
            selected: false
        },
        {
            widgetClass: "fa fa-line-chart",
            type: "timeseries",
            selected: false
        },
        {
            widgetClass: "fa fa-bar-chart",
            type: "gauge",
            selected: false,
            multiple: false
        },
        {
            widgetClass: "fa fa-pie-chart",
            type: "buble",
            selected: false
        }]

        ctrl.$onInit = function () {
            ctrl.showpopup = ctrl.showpopup;            
            ctrl.title = ctrl.title;
            reportService.getAll().then((reportList) => {
                ctrl.customReportList.push({ id: 0, name: 'Add a Report' });
                ctrl.customReportList.push({ id: 1, name: 'Average time spent by user' });
                ctrl.customReportList = [...ctrl.customReportList, ...reportList];
            });
        }

        ctrl.$onChanges = function (ChangeObject) {
            console.log(ChangeObject);
            if (ChangeObject.showpopup) {
                ctrl.widgetName = "Widget";
                $scope.showpopup = ctrl.showpopup;
            }

            if (ChangeObject.widgetName) {
                if (ChangeObject.widgetName.currentValue != undefined && ChangeObject.widgetName.currentValue != "") {
                    ctrl.widgetName = ChangeObject.widgetName.currentValue;
                }
                else {
                    ctrl.widgetName = "Widget";
                }
            }

            if (ChangeObject.title) {
                $scope.title = ctrl.title;
            }

            if(ChangeObject.selectedElem)
            {
                console.log(ChangeObject.selectedElem,ctrl.selectedElem)
                if (ChangeObject.selectedElem.currentValue != undefined && ChangeObject.selectedElem.currentValue != "") {
                    ctrl.selectedElem = ChangeObject.selectedElem.currentValue;
                    console.log(ctrl.selectedElem);
                    selectElem(ctrl.selectedElem.type);
                }
                else
                {
                    ctrl.selectedElem = { type: "bar" };
                    selectElem(ctrl.selectedElem.type);
                }
            }
        }

        let selectElem = (type) => {
            let oldSelectedElem = _.find(ctrl.widgetList, function (d) { return d.selected == true });
            oldSelectedElem.selected = false;

            let newSelectedElem = _.find(ctrl.widgetList, function (d) { return d.type == type });
            newSelectedElem.selected = true;
            
            return newSelectedElem.type;
        }

        $scope.selectWidget = function (data) {
            console.log(data);
            var type = selectElem(data.type);
            ctrl.selectedElem = { type: type };
        }

        $scope.addWidgetToTemplate = () => {
            let selectedWidget = _.find(ctrl.widgetList, function (d) { return d.selected == true });
            let widgetData = {
                widgetName: ctrl.widgetName
            }

            if (selectedWidget.type == 'table') {
                selectedWidget.reportKey = ctrl.selectedTableReport;
            }

            angular.extend(widgetData, selectedWidget);

            ctrl.onWidgetSelection({ ctx: widgetData });
            ctrl.onCloseClick();
            ctrl.showpopup = false;
            $scope.showpopup = false;
        };
        $scope.popUpCloseClick = function () {
            ctrl.showpopup = false;
            $scope.showpopup = false;
            ctrl.onCloseClick();
        }
    }
})(angular.module("dealerWizard"));