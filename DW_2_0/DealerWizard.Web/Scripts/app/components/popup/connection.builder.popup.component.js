﻿(function (app) {
    "use strict"
    app.component("connectionBuilderPopup", {
        templateUrl: "/Scripts/app/components/popup/connectionbuilder.html",
        controller: ['$scope', '$element', '$attrs', connectionBuilderPopupCtrl],
        bindings: {
            showpopup: "<",
            title: "<",
            onCloseClick: "&" ,
            onSetConnection: "&"
        }
    });

    function connectionBuilderPopupCtrl($scope, $element, $attrs) {
        var ctrl = this;
        ctrl.connectionSource = "Connection1";

        ctrl.connectionType = [
        {
            name: "Cube",             
            typeSelected: true,            
            config:"",
            icon:"fa fa-cube"
        },
        {
            name: "MS Sql Database",            
            typeSelected: false,
            config:"",
            icon:"fa fa-database"
        } ];


        ctrl.$onInit = function () {
            ctrl.showpopup = ctrl.showpopup;
            ctrl.title = ctrl.title;
        }

        ctrl.$onChanges = function (ChangeObject) {
            if (ChangeObject.showpopup) {
                $scope.showpopup = ctrl.showpopup;
            }

            if (ChangeObject.title) {
                $scope.title = ctrl.title;
            }
        }

        $scope.setTemplate = function (data) {
            console.log(data);
            let oldSelectedElem = _.find(ctrl.connectionType, function (d) { return d.typeSelected == true });
            oldSelectedElem.typeSelected = false;

            let newSelectedElem = _.find(ctrl.connectionType, function (d) { return d.name == data.name });
            newSelectedElem.typeSelected = true;

        }

        $scope.loadTemplate = () => {
            let selectedTemplate = _.find(ctrl.connectionType, function (d) { return d.typeSelected == true });
            selectedTemplate.config = ctrl.connectionSource;
            ctrl.onSetConnection({ ctx: selectedTemplate });
            ctrl.onCloseClick();
            ctrl.showpopup = false;
            $scope.showpopup = false;
        };

        $scope.popUpCloseClick = function () {
            ctrl.showpopup = false;
            $scope.showpopup = false;
            ctrl.onCloseClick();
        }
    }
})(angular.module("dealerWizard"));