﻿(function (app) {

    app.component("datatablePopup", {
        templateUrl: "/Scripts/app/components/datatable/datatablepopup.html",
        controller: ['$scope', '$element', '$attrs', datatableCtrl],
        bindings: {
            showpopup: "<",
            path: "<",
            title: "<",
            onCloseClick: "&"
        }
    });

    function datatableCtrl($scope, $element, $attrs) {
        var ctrl = this;

        ctrl.$onInit = function () {
            ctrl.showpopup = ctrl.showpopup;
            ctrl.path = ctrl.path;
            ctrl.title = ctrl.title;
        }

        ctrl.$onChanges = function (ChangeObject) {
            if (ChangeObject.showpopup) {
                $scope.showpopup = ctrl.showpopup;

                //Required to Refresh the popup
                if (ctrl.showpopup == true) {
                    var element = $element[0].children;
                    var frame = $(element[0]).find("iframe");
                    //if (frame) frame[0].src = ('src', ($(frame[0]).attr('src')));
                    if (frame) frame[0].src = ('src', ctrl.path);
                }
            }

            if (ChangeObject.path) {
                $scope.path = ctrl.path;
            }

            if (ChangeObject.title) {
                $scope.title = ctrl.title;
            }
        }

        $scope.ReportItemClick = function (data) {
            ctrl.onListItemClick({ ctx: data });
        }

        $scope.ReportPopUpCloseClick = function () {
            ctrl.onCloseClick();
            var element = $element[0].children;
            var frame = $(element[0]).find("iframe");
            if (frame) frame[0].src = ('src', 'about:blank');
        }
    }
})(angular.module("dealerWizard"));