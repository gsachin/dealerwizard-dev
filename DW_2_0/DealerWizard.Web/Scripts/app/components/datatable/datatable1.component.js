﻿function datatable1Ctrl($scope,$element){
    let ctrl = this;
    let isTranscluded = false;
    let dataTable = {};
    ctrl.dtObject = {
        "responsive": true,
        "iDisplayLength": 50,
        "oLanguage": {
            "sLengthMenu": 'Show <select data-toggle="dropdown">' +
                           '<option value="10">10</option>' +
                           '<option value="20">20</option>' +
                           '<option value="50">50</option>' +
                           '<option value="100">100</option>' +
                           '</select> Records'
        },
        "autoWidth": false,
        "orderCellsTop": true,
        "deferRender": true,
        "bSortCellsTop": true,
        "order": [],
        "oLanguage": { "sEmptyTable": "No records available" }
    }

    ctrl.$postLink = () => {
        console.log("post link called");
        console.log($($element[0].querySelector('#datatable')).find('thead').length);
        isTranscluded = $('#datatable thead').length > 0 ? true : false;
        prepareObject();
        ctrl.renderTable();
    }
    
    ctrl.setColumns = (columnsObj) => {
        ctrl.columns = columnsObj;
    }

    ctrl.getColumns = () => {
        if (ctrl.columns == undefined && isTranscluded == false) {
            console.log(_.map(Object.keys(ctrl.data[0]), (d) => {
                return { 'title': d };
            }));
            return _.map(Object.keys(ctrl.data[0]), (d) => {
                return { 'title': d };
            });
        }
        else {
            return ctrl.columns;
        }
    }

    ctrl.setData = (data) => {
        ctrl.data = data;
    }

    ctrl.getData = () => {
        return ctrl.data;
    }

    let prepareObject = () => {
        if (!isTranscluded) {
            angular.extend(ctrl.dtObject, { columns: ctrl.getColumns() });
        }
        
        angular.extend(ctrl.dtObject, { data: ctrl.getData() });
    }

    ctrl.postRender;
    ctrl.PreRender;

    ctrl.renderTable = () => {
        if (typeof (ctrl.preRender) == "function") {
            ctrl.preRender(ctrl.dtObject);
        }

        dataTable = $($element[0].querySelector('#datatable')).DataTable(ctrl.dtObject);

        if (typeof (ctrl.postRender) == "function") {
            ctrl.postRender(dataTable);
        }
    }

}

datatable1Ctrl.$inject = ['$scope', '$element'];

angular.module('dealerWizard').component('datatable1', {
    templateUrl: './Scripts/app/Components/datatable/datatable1.html',
    transclude: {
        'header': '?tableHeader'
    },
    bindings: {
        data: "<",
        columns: "<"
    },
    controller: datatable1Ctrl
})