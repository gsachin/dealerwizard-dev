﻿(function (app) {

    app.component("externalpage", {
        templateUrl: "/Scripts/app/components/externalpage/externalpage.html",
        controller: ['$scope', '$element', '$attrs','$rootScope','dealerService', externalpageCtrl],
        bindings: {
            path: "<"
        }
    });

    function externalpageCtrl($scope, $element, $attrs, $rootScope, dealerService) {
        var ctrl = this;
        ctrl.path = ctrl.path;
        
        ctrl.$onInit = function () {
            ctrl.path = ctrl.path;
        }

        ctrl.$onChanges = function (ChangeObject) {
            if (ChangeObject.path) {
                $scope.path = ctrl.path;
            }
        }

        ctrl.$postLink = function () {
            $scope.pageHeight = (window.innerHeight - 70) + "px";           
            dealerService.getCurrentDealer().then(function (dealer) {
                var element = $element[0].children;
                $(element[0]).attr('src', ($(element[0]).attr('src')));
            });
        }

    }
})(angular.module("dealerWizard"));