﻿angular.module('dealerWizard')
.component("dashboardinvoker", {
    templateUrl: "/Scripts/app/components/dashboard/container1.html",
    controller: dashboardInvokerController
});

dashboardInvokerController.$inject = ['$ngRedux', '$scope', '$routeParams', '$compile','widgetFactory', 'dashboardRepository', '$templateCache', '$element'];

function dashboardInvokerController($ngRedux, $scope, $routeParams, $compile, widgetFactory, dashboardRepository, $templateCache, $element) {
    var ctrl = this;
    //constructor() {
        //console.log("dashboard init");
       // alert(1);
       // let unsubscribe = $ngRedux.connect(this.mapStateToThis, {})(this);
        //$scope.$on('$destroy', unsubscribe);
    //}

    var _curDate = moment().subtract(1, 'd'),
            _startDate = moment(_curDate).subtract(1, 'y').add(1, 'd');

    ctrl.globalFilter = { startDate: _startDate, endDate: _curDate };
    
    ctrl.$onInit = () => {
        let dashboard = dashboardRepository.get($routeParams.name);
        ctrl.dashbordName = dashboard.name;
        //ctrl.templatePath = dashboard.template.templatePath;
        let templateHtml = $templateCache.get(dashboard.template.templatePath);
        var containerElement = $($element[0].querySelector('#container'));
        containerElement.append(templateHtml);

        dashboard.widgets.forEach(o => {
            var newScope = $scope.$new(true, $scope);
            var element = $($element[0].querySelector('#' + o.container));
            let widget = widgetFactory.getWidget({ type: o.type, context: o });
            newScope = angular.merge(newScope, widget.data);
            element.append($compile(widget.markUp)(newScope));
        });
    }

    ctrl.$postLink = () => {
         //let dashboard = dashboardRepository.get($routeParams.name);
        
       
    }

    // Which part of the Redux global state does our component want to receive?
   /* mapStateToThis(state) {
        return {
            value: state.counter
        };
    }*/
}
