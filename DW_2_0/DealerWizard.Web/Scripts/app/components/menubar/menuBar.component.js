﻿(function (app) {

    app.component('menuBar', {
        templateUrl: "/Scripts/app/components/menubar/menuBar.html",
        controller: ['$scope', '$location', '$element', '$attrs', 'apiService', '$compile', 'userService', menuBarCtrl],
        bindings: {

        }
    });
    app.directive('linkHighlight', function () {
        return function ($scope, $element, $attrs) {
            $scope.$watch('linkId', function (linkId) {
                $element.find('li').removeClass('childActive');
                $element.find('li[data-linkid="' + linkId + '"]').addClass('childActive');
            })
        };
    });
    function menuBarCtrl($scope, $location, $element, $attrs, apiService, $compile,userService) {
        var ctrl = this;

        //var menu =
        //    [
        //        {
        //            ID: "1",
        //            displayName: "Home",
        //            URL: "/home",
        //            order: 1,
        //            classname: "fa fa-home",
        //            ParentID: ""
        //        },
        //        {
        //            ID: "2",
        //            displayName: "Dealer Overview",
        //            URL: "/overview",
        //            order: 2,
        //            classname: "fa fa-tachometer",
        //            ParentID: ""
        //        },
        //        {
        //            ID: "3",
        //            displayName: "Variable Operations",
        //            URL: "",
        //            order: 3,
        //            classname: "fa fa-briefcase",
        //            ParentID: ""
        //        },
        //        {
        //            ID: "4",
        //            displayName: "Variable",
        //            URL: "/variable",
        //            order: 1,
        //            classname: "fa fa-caret-right",
        //            ParentID: "3"
        //        }
        //    ];
        //Create Dynamic Link

        //Send Request
        //var downloadedData = apiService.get('/api/getMenuItemsByUserId?', null, function (result) {
        userService.getMenuItems().then(function (userMenuItms) {
        var menu = userMenuItms.menuItems;
            var links = $($element).find('ul').last();
            if (links) {
                var html = ""; var parentMenus = [];
                _.find(menu, function (itm, i)
                { if (itm.ParentMenuId == undefined && itm.Active == true) parentMenus[parentMenus.length] = itm; });

                //Sorting
                parentMenus = _.sortBy(parentMenus, function (d) { return d.Order; });

                angular.forEach(parentMenus, function (pItem) {

                    var childMenus = [];
                    _.find(menu, function (itm, i)
                    { if (itm.ParentMenuId != undefined && itm.ParentMenuId == pItem.MenuId && itm.Active == true) childMenus[childMenus.length] = itm; });

                    //Sorting
                    childMenus = _.sortBy(childMenus, function (d) { return d.Order; });

                    //Build HTML
                    var plink = '<li id="parentMenu' + pItem.Name.replace(/ /g, '') + '">';

                    if (childMenus.length > 0) plink += "<a title='" + pItem.Name + "' class='cursor-pointer'>";
                    else {
                        var url = ""; if (pItem.URL != undefined) url = pItem.URL;
                        plink += "<a title='" + pItem.Name + "' href='#" + url + "'>";
                    }

                    plink += "<i class='" + pItem.IconClassName + "'></i>" +
                    "<span class='title'>" + pItem.Name + "</span>";

                    if (childMenus.length > 0) plink += "<span class='selected'></span><span class='arrow'></span>";
                    plink += "</a>";

                    if (childMenus.length > 0) plink += "<ul class='sub-menu'>";

                    angular.forEach(childMenus, function (cItem, index) {
                        var url = ""; if (cItem.URL != undefined) url = cItem.URL;
                        plink += '<li id="subMenu' + cItem.Name.replace(/ /g, '') + index + '">' +
                        //plink += "<li ng-class='{childActive: isActiveMenu(\"" + url + "\",\"" + index + "\")}'>" +
                        "<a title='" + cItem.Name + "' href='#" + url + "'>" +
                        "<i class='" + cItem.IconClassName + "'></i>" +
                        "<span class='title'>" + cItem.Name + "</span></a>" +
                        "</li>";
                    });
                    // 

                    if (childMenus.length > 0) plink += "</ul>";

                    plink += "</li>";

                    html += plink;
                });
                links.html(html);
                $compile(html)($scope);

                //close side menu
                $('[id*="subMenu"]').on('click', function (e) {
                    $(".sub-menu li").removeClass("childActive");
                    $(this).addClass('childActive');
                    if ($('.sub-menu').is(':visible') && $("#main-menu").hasClass("mini")) {
                        $('.sub-menu').slideUp(200);
                    } else if (!$('#main-menu').hasClass("mini")) {
                        $('#layout-condensed-toggle').trigger('click');
                    }
                    //var custom = $(this);
                    //setTimeout(function () {
                    //    if (custom.attr('id').indexOf('FISales') >= 0) {
                    //        $("div.cmp-button-group").removeClass('cmp-button-group').addClass('cmp-button-group-fi');
                    //    } else {
                    //        $("div.cmp-button-group").removeClass('cmp-button-group-fi');
                    //    }
                    //}, 1000);
                    e.stopImmediatePropagation();
                });

                //close parent side menu
                $('[id*="parentMenu"]').on('click', function () {
                    $(".parent-menu-wrapper li").removeClass("parentActive");
                    if ($(this).children().length < 2) {
                        $(".sub-menu li").removeClass("childActive open");
                    }
                    $(this).addClass('parentActive');
                    if ($('.sub-menu').is(':visible') && $("#main-menu").hasClass("mini") && $(this).children().length < 2) {
                        $('.sub-menu').slideUp(200);
                    } else if (!$('#main-menu').hasClass("mini") && $(this).children().length < 2) {
                        $('#layout-condensed-toggle').trigger('click');
                    }
                    //var custom = $(this);
                    //setTimeout(function () {
                    //    if (custom.attr('id').indexOf('ServiceDepartment') >= 0) {
                    //        $("div.cmp-button-group").removeClass('cmp-button-group').addClass('cmp-button-group-sd');
                    //    } else {
                    //        $("div.cmp-button-group").removeClass('cmp-button-group-sd');
                    //    }
                    //}, 1000);

                });

                //close dealer list
                $('#groups-list').on('click', 'li', function () {
                    if (!$(this).is(':first-child')) {
                        if ($('body').hasClass('open-menu-right')) {
                            $('body').removeClass('open-menu-right');
                            setTimeout(function () {
                                $('.chat-window-wrapper').removeClass('show').addClass("hide");                               
                            }, 200);
                        }
                    }
                });

                function attachEvents() {
                    $('#layout-condensed-toggle').click(function () {
                        if ($('#main-menu').attr('data-inner-menu') == '1') {
                            //Do nothing
                            //console.log("Menu is already condensed");
                        } else {
                            if ($('#main-menu').hasClass('mini')) {
                                //$('body').removeClass('grey');
                                $('body').removeClass('condense-menu');
                                $('#main-menu').removeClass('mini');
                                //$('.page-content').removeClass('condensed');
                                $('#page-wrapper').removeClass('condensed');
                                $('.scrollup').removeClass('to-edge');
                                $('.header-seperation').show();
                                //Bug fix - In high resolution screen it leaves a white margin
                                $('.header-seperation').css('height', '61px');
                                $('.navbar-custom').addClass('resp-sidemenu-ml');
                                $('#page-wrapper').addClass('resp-pagewrap-ml');
                                $('.quicklinks').addClass('link-selected');
                                $(window).trigger('resize');
                                //$('.footer-widget').show();
                                if ($('.sub-menu li').hasClass('childActive')) {
                                    if (!$('.childActive').parent().parent().hasClass('open'))
                                        $('.childActive').parent().parent().children().first().trigger('click');
                                }
                                if ($(window).width() < 1023) {
                                    if ($("#move-top").hasClass('resp-inline-block')) {
                                        $("#move-top").removeClass('resp-inline-block').addClass('resp-hide');
                                    }
                                }
                            } else {
                                //$('body').addClass('grey');
                                $('#main-menu').addClass('mini');
                                //$('.page-content').addClass('condensed');
                                $('#page-wrapper').addClass('condensed');
                                $('.scrollup').addClass('to-edge');
                                $('.header-seperation').hide();
                                $('.navbar-custom').removeClass('resp-sidemenu-ml');
                                $('#page-wrapper').removeClass('resp-pagewrap-ml');
                                $('.quicklinks').removeClass('link-selected');
                                //$('.footer-widget').hide();
                                $('.main-menu-wrapper').scrollbar('destroy');
                                if ($('#page-wrapper').hasClass('condensed')) {
                                    var elem = jQuery('.page-sidebar ul');
                                    elem.children('li.open').children('a').children('.arrow').removeClass('open');
                                    elem.children('li.open').children('a').children('.arrow').removeClass('active');
                                    elem.children('li.open').children('.sub-menu').slideUp(200);
                                    elem.children('li').removeClass('open');
                                }
                                $(window).trigger('resize');
                                if ($(window).width() < 1023) {
                                    if ($("#move-top").hasClass('resp-hide')) {
                                        $("#move-top").removeClass("resp-hide").addClass('resp-inline-block');
                                    }
                                }
                            }
                        }
                    });

                    //**********************************BEGIN MAIN MENU********************************
                    jQuery('.page-sidebar li a').on('click', function (e) {
                        if ($(this).next().hasClass('sub-menu') === false) {
                            return;
                        }
                        var parent = $(this).parent().parent();


                        parent.children('li.open').children('a').children('.arrow').removeClass('open');
                        parent.children('li.open').children('a').children('.arrow').removeClass('active');
                        parent.children('li.open').children('.sub-menu').slideUp(200);
                        parent.children('li').removeClass('open');
                        //  parent.children('li').removeClass('active');

                        var sub = jQuery(this).next();
                        if (sub.is(":visible")) {
                            jQuery('.arrow', jQuery(this)).removeClass("open");
                            jQuery(this).parent().removeClass("active");
                            sub.slideUp(200, function () {
                                handleSidenarAndContentHeight();
                            });
                        } else {
                            jQuery('.arrow', jQuery(this)).addClass("open");
                            jQuery(this).parent().addClass("open");
                            sub.slideDown(200, function () {
                                handleSidenarAndContentHeight();
                            });
                        }

                        e.preventDefault();
                    });
                    //Auto close open menus in Condensed menu
                    if ($('#page-wrapper').hasClass('condensed')) {
                        var elem = jQuery('.page-sidebar ul');
                        elem.children('li.open').children('a').children('.arrow').removeClass('open');
                        elem.children('li.open').children('a').children('.arrow').removeClass('active');
                        elem.children('li.open').children('.sub-menu').slideUp(200);
                        elem.children('li').removeClass('open');
                    }
                    //**********************************END MAIN MENU********************************
                    var handleSidenarAndContentHeight = function () {
                        var content = $('.page-content');
                        var sidebar = $('.page-sidebar');

                        if (!content.attr("data-height")) {
                            content.attr("data-height", content.height());
                        }

                        if (sidebar.height() > content.height()) {
                            content.css("min-height", sidebar.height() + 120);
                        } else {
                            content.css("min-height", content.attr("data-height"));
                        }
                    };
                    $('.panel-group').on('hidden.bs.collapse', function (e) {
                        $(this).find('.panel-heading').not($(e.target)).addClass('collapsed');
                    });

                    $('.panel-group').on('shown.bs.collapse', function (e) {
                        // $(e.target).prev('.accordion-heading').find('.accordion-toggle').removeClass('collapsed');
                    });

                    //***********************************BEGIN Function calls *****************************
                    //function closeAndRestSider() {
                    //    if ($('#main-menu').attr('data-inner-menu') == '1') {
                    //        $('#main-menu').addClass("mini");
                    //        $('#main-menu').removeClass("left");
                    //    } else {
                    //        $('#main-menu').removeClass("left");
                    //    }

                    //}
                    $('#main-menu-toggle').on('touchstart click', function (e) {
                        e.preventDefault();
                        toggleMainMenu();
                    });
                    $('#chat-menu-toggle, .chat-menu-toggle').on('touchstart click', function (e) {
                        e.preventDefault();
                        toggleChat();
                        if ($('.DealerSliderMenu ul li').length >= 13) {
                            $('.DealerSliderMenu ul li:last').css('padding-bottom', '38px');
                        }
                        $('[data-toggle="tooltip"]').tooltip('hide');
                    });

                    function rebuildSider() {

                    }

                    //function toggleMainMenu() {
                    //    var timer;
                    //    if ($('body').hasClass('open-menu-left')) {
                    //        $('body').removeClass('open-menu-left');
                    //        timer = setTimeout(function () {
                    //            $('.page-sidebar').removeClass('show').addClass("hide");
                    //        }, 50);

                    //    }
                    //    else {
                    //        clearTimeout(timer);
                    //        $('.page-sidebar').removeClass("hide").addClass('show');
                    //        setTimeout(function () {
                    //            $('body').addClass('open-menu-left');
                    //        }, 50);

                    //    }
                    //}
                    function toggleChat() {
                        var timer;
                        if ($('body').hasClass('open-menu-right')) {
                            $('body').removeClass('open-menu-right');
                            timer = setTimeout(function () {
                                $('.chat-window-wrapper').removeClass('show').addClass("hide");
                            }, 300);

                        }
                        else {
                            clearTimeout(timer);                          
                            $('.chat-window-wrapper').removeClass("hide").addClass('show');
                            //$('.chat-window-wrapper').show(100);
                            $('body').addClass('open-menu-right');
                        }
                    }
                }

                attachEvents();
            }
        });



    }
})(angular.module("dealerWizard"));