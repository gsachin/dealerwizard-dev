﻿    function dumbBellCtrl($scope, $element) {
        var ctrl = this;
        $scope.ShowLoader = false;
        //var ageingGroup = [];

        ctrl.$postLink = () => {            
            ctrl.renderChart();           
        }

        ctrl.$onDestroy = function () {
            $(window).off('resize');
        }

        ctrl.renderChart = () => {
            renderDumbBellChart(ctrl.data, $element[0].querySelector('#divDumbBellChart'));
        }

    }

    function renderDumbBellChart(mdata, container) {
        if (mdata == undefined) return;
        $(container).html('');
        console.log("called");
        // set the dimensions and margins of the graph
        var margin = { top: 30, right: 70, bottom: 80, left: 90 };
        var width = $(container).width() - margin.left - margin.right;
        var height = 300 - margin.top - margin.bottom;

        var data = mdata;

        var svg = d3.select(container).append("svg")
                    .attr('class', 'svgclass')
                .attr("width", width + margin.left + margin.right)
                .attr("height", height + margin.top + margin.bottom)
                .append("g")
                .attr("transform", "translate(" + margin.left + ", " + margin.top + ")");

        var yScale = d3.scaleBand()
                       .range([0, height])
                       .paddingInner([1])
                       .paddingOuter([0.3])
                       .domain(data.map((d) => { return d.Name; }));
        var yAxis = d3.axisLeft(yScale);


        var xScale = d3.scaleLinear().range([0, width]);
        var xAxis = d3.axisBottom(xScale);

        var maxVal1 = d3.max(data, (d) => { return d.Val1; })
        var minVal1 = d3.min(data, (d) => { return d.Val1; })
        var maxVal2 = d3.max(data, (d) => { return d.Val2; })
        var minVal2 = d3.min(data, (d) => { return d.Val2; })

        var minMaxArray = [maxVal1, minVal1, maxVal2, minVal2];

        xScale.domain(d3.extent(minMaxArray));


        //append xAxis
        svg.append('g')
        .attr('class', 'axis grid')
           .attr("transform", "translate(0," + height + ")")
           .call(xAxis.ticks(9).tickSize(-height));

        // text chart-label for the x axis
        svg.append("text")
          .attr('class', 'xlabel chart-label')
          .attr("transform",
                "translate(" + (width / 2) + " ," +
                               (height + margin.top + 20) + ")")
          .style("text-anchor", "middle")
          .text("Inventory");



        //append yAxis
        svg.append('g')
        .attr('class', 'axis-y')
            .call(yAxis);

        // text chart-label for the y axis
        svg.append("text")
          .attr("transform", "rotate(-90)")
          .attr("y", 0 - margin.left)
          .attr("x", 0 - (height / 2))
          .attr("dy", "1em")
          .attr("class", "ylabel chart-label")
          .style("text-anchor", "middle")
          .text("Model");

        //groupings for the visualiztion objects
        var dumbBell = svg.selectAll('dumbBell')
                        .data(data)
                        .enter()
                        .append('g')
                        .attr('class', 'dumbBell');

        dumbBell.append('line')
            .attr('class', 'dashedline')
            .attr('x1', (d) => { return xScale(0); })
            .attr('x2', (d) => { return xScale(d.Val1); })
            .attr('y1', (d) => { return yScale(d.Name); })
            .attr('y2', (d) => { return yScale(d.Name); })
            .attr('stroke', "#CCCCFF")
            .attr('stroke-width', 2)
            .attr('stroke-dasharray', 1);

        dumbBell.append('line')
            .attr('class', 'bridgeline')
            .attr('x1', (d) => { return xScale(d.Val1); })
            .attr('x2', (d) => { return xScale(d.Val2); })
            .attr('y1', (d) => { return yScale(d.Name); })
            .attr('y2', (d) => { return yScale(d.Name); })
            .attr('stroke', "#8DB6CD")
            .style('opacity', '0.9')
            .attr('stroke-width', 4);

        dumbBell.append('circle')
            .attr('class', 'forecastcircle')
            .attr('r', 6)
            .attr('cx', (d) => { return xScale(d.Val1); })
            .attr('cy', (d) => { return yScale(d.Name); })
            .attr('fill', "steelblue")

        dumbBell.append('text') 
            .attr('x', (d) => { return (xScale(d.Val1) < 50) ? xScale(d.Val1) : xScale(d.Val1) - 35; })
            .attr('y', (d) => { return (xScale(d.Val2) < 50) ? yScale(d.Name) - 15 : yScale(d.Name); })
            .attr('dy', '.35em')
            .attr('class', 'forecasttext chart-label')
            .style('text-anchor', 'middle')
            .style('font-size', '10px')
            .style('font-weight', 'bold')
            .text((d) => { return d.Val1 });

        dumbBell.append('text')            
            .attr('x', (d) => { return (xScale(d.Val2) < 50) ? xScale(d.Val2) + 15 : xScale(d.Val2) + 35; })
            .attr('y', (d) => { return yScale(d.Name); })
            .attr('dy', '.35em')
            .attr('class', 'currenttext chart-label')
            .style('text-anchor', 'middle')
            .style('font-size', '10px')
            .style('opacity', '0.7')
            .text((d) => { return d.Val2 });

        dumbBell.append('circle')
            .attr('class', 'currentcircle')
            .attr('r', 6)
            .attr('cx', (d) => { return xScale(d.Val2); })
            .attr('cy', (d) => { return yScale(d.Name); })
            .attr('fill', "#8DB6CD")
            .style('opacity', '0.9');


        var resizeTimmer;
        $(window).on("resize", function (e) {
            clearTimeout(resizeTimmer);
            resizeTimmer = setTimeout(function () {
                resized();
            }, 350)
        });

        function resized() {
            //Get the width of the window
            //var margin = { top: 20, right: 20, bottom: 30, left: 60 };
            var w = Math.ceil((d3.select('#divDumbBellChart').node().clientWidth)) - margin.left - margin.right;
            if (w <= 0) w = Math.ceil((d3.select($('#divDumbBellChart').closest('div').prop('id')).node().clientWidth)) - margin.left - margin.right - 30;
            console.log(w);
            var ss = d3.select('#divDumbBellChart').selectAll('.svgclass');
            console.log(ss._groups[0]);

            xScale = xScale.range([0, w]);
            xAxis = d3.axisBottom(xScale);

            d3.select(ss._groups[0][0]).attr("width", w + margin.left + margin.right);

            ss._groups.forEach(function (item, index) {
                angular.forEach(item, function (item2, index1) {
                    svg.attr('width', w + margin.left + margin.right);
                    var dashedlines = svg.selectAll(".dashedline");
                    angular.forEach(dashedlines._groups[0], function (dashedline, index2) {
                        d3.select(dashedline)
                            .attr('x1', (d) => { return xScale(0); })
                            .attr('x2', (d) => { return xScale(d.Val1); })
                    });

                    var bridgelines = svg.selectAll(".bridgeline");
                    angular.forEach(bridgelines._groups[0], function (bridgeline, index2) {
                        d3.select(bridgeline)
                            .attr('x1', (d) => { return xScale(d.Val1); })
                            .attr('x2', (d) => { return xScale(d.Val2); })
                    });

                    var forecastcircles = svg.selectAll(".forecastcircle");
                    angular.forEach(forecastcircles._groups[0], function (forecastcircle, index2) {
                        d3.select(forecastcircle)
                           .attr('cx', (d) => { return xScale(d.Val1); })
                    });

                    var forecasttexts = svg.selectAll(".forecasttext");
                    angular.forEach(forecasttexts._groups[0], function (forecasttext, index2) {
                        d3.select(forecasttext)
                            .attr('x', (d) => { return (xScale(d.Val1) < 50) ? xScale(d.Val1) : xScale(d.Val1) - 35; })
                            .attr('y', (d) => { return (xScale(d.Val2) < 50) ? yScale(d.Name) - 15 : yScale(d.Name); })
                    });

                    var currentcircles = svg.selectAll(".currentcircle");
                    angular.forEach(currentcircles._groups[0], function (currentcircle, index2) {
                        d3.select(currentcircle)
                           .attr('cx', (d) => { return xScale(d.Val2); })
                    });

                    var currenttexts = svg.selectAll(".currenttext");
                    angular.forEach(currenttexts._groups[0], function (crt, index2) {
                        console.log(crt);
                        d3.select(crt)
                            .attr('x', (d) => { return (xScale(d.Val2) < 50) ? xScale(d.Val2) + 15 : xScale(d.Val2) + 35; })
                            .attr('y', (d) => { return yScale(d.Name); })
                    });

                    svg.select(".xlabel")
                        .attr("transform", "translate(" + (w / 2) + " ," + (height + margin.top + 20) + ")");

                    svg.select("g")
                        .attr('class', 'axis grid')
                        .attr("transform", "translate(0," + height + ")")
                        .call(xAxis.ticks(9).tickSize(-height));

                });
            });


        }
        resized();

    }

    angular.module('dealerWizard').component("dumbbell", {
        templateUrl: "/Scripts/app/components/dumbbell/dumbbell.html",
        controller: ['$scope', '$element', dumbBellCtrl],
        bindings: {
            data: "<"
        }
    });
