﻿
function gaugeCtrl($scope, $element, $attrs) {
    var ctrl = this;
    $scope.ShowLoader = false;

    ctrl.$onInit = function () {
        $scope.ShowLoader = (ctrl.wConfig) ? ctrl.wConfig.ShowLoader : false;
    }

    $scope.ReportChange = function (d) {
        ctrl.onKpiClick({ ctx: d });
    }

    ctrl.$postLink = () => {
        $scope.ShowLoader = false;
    }
}

angular.module("dealerWizard")
.component("gaugeList", {
    templateUrl: "/Scripts/app/components/gauge/gaugelist.html",
    controller: ['$scope', '$element', '$attrs', gaugeCtrl],
    transclude: true,
    bindings: {
        data: "<",
        wConfig: "<",
        onKpiClick: "&"
    }
});
