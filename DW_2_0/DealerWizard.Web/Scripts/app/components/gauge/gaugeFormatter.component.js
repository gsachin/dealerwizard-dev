﻿
angular.module("dealerWizard")
.component('gaugeFormatter', {
    transclude: true,
    require: {
        gaugeCtrl: '^gauge'
    },
    bindings: {
        format: "="
    },
    controller: ['$scope', 'valueFormatter', function ($scope, valueFormatter) {
        let ctrl = this;

        ctrl.$onInit = () => {

            let formatter = valueFormatter.getformatter(ctrl.format);

            if (ctrl.gaugeCtrl.setDataFormatter && (typeof (ctrl.gaugeCtrl.setDataFormatter) == "function")) {
                ctrl.gaugeCtrl.setDataFormatter(formatter);
            }

        }

    }]
});