﻿function gaugeCtrl($scope, $element, $attrs, businessNameServices) {
    var ctrl = this;

    ctrl.$onInit = function () {
    }

    ctrl.setDataFormatter = (fn) => {
        if (typeof (fn) !== "function") {
            return;
        }
        ctrl.formatter = fn;
    }

    ctrl.getDataFormatter = () => {
        return (ctrl.formatter == undefined) ? (val) => val : ctrl.formatter;
    }

    ctrl.$onChanges = function (ChangeObject) {
        
    }

    ctrl.$onDestroy = () => {
        
    }

    ctrl.$postLink = () => {
        let thisElement = $element[0].children[0];
        $(thisElement).html('');
        RenderChartTiles(thisElement, ctrl.key);
    }

    function RenderChartTiles(container, Elementkey) {

        function MinValueRange(ActualValue, RangeMax, RangeMin, ReverseOrder) {
            if (ReverseOrder) {
                return (ActualValue < RangeMax) ? ((ActualValue < 0) ? ActualValue + (ActualValue * .1) : 0) : ((RangeMax < 0) ? RangeMax + (RangeMax * .1) : (RangeMax == 0) ? ((RangeMin == 0) ? -40 : (RangeMax - RangeMin) * .1) : 0);

            }
            return (ActualValue < RangeMin) ? ((ActualValue < 0) ? ActualValue + (ActualValue * .1) : 0) : ((RangeMin < 0) ? RangeMin + (RangeMin * .1) : (RangeMin == 0) ? ((RangeMax == 0) ? -40 : (RangeMin - RangeMax) * .1) : 0);
        }

        function MaxValueRange(ActualValue, RangeMax, RangeMin, ReverseOrder) {
            if (ReverseOrder) {
                return (ActualValue > RangeMin) ? (ActualValue < 0) ? ActualValue - (ActualValue * .1) : ActualValue + (ActualValue * .1) : (RangeMin < 0) ? RangeMin - (RangeMin * .1) : RangeMin + (RangeMin * .1);
            }
            return (ActualValue > RangeMax) ? (ActualValue < 0) ? ActualValue - (ActualValue * .1) : ActualValue + (ActualValue * .1) : (RangeMax < 0) ? RangeMax - (RangeMax * .1) : RangeMax + (RangeMax * .1);
        }

        var gauge = function (container, configuration, data) {

            var that = {};
            var config = {
                size: 100,
                clipWidth: 100,
                clipHeight: 55,
                ringInset: 20,
                ringWidth: 20,

                pointerWidth: 6,
                pointerTailLength: 3,
                pointerHeadLengthPercent: 0.6,

                minValue: MinValueRange(data.ActualValue, data.RangeMax, data.RangeMin, data.ReverseOrder),
                maxValue: MaxValueRange(data.ActualValue, data.RangeMax, data.RangeMin, data.ReverseOrder),

                minAngle: -120,
                maxAngle: 120,

                transitionMs: 750,

                majorTicks: 3,
                labelTicks: 6,
                labelFormat: d3.format(","),
                labelInset: 10,

                arcColorFn: d3.interpolateHsl(d3.rgb('#e8e2ca'), d3.rgb('#3e6c0a')),
                ColorScheme: ["#d50b1f", "#d9a01c", "#2C5532", "steelblue"],
                lightColorScheme: ["#f53d50", "#F4C549", "#4c9457", "lightsteelblue"]
            };


            var range = undefined;
            var r = undefined;
            var pointerHeadLength = undefined;
            var value = 0;


            var svg = undefined;
            var arc = undefined;
            var scale = undefined;
            var ticks = undefined;
            var tickData = undefined;
            var pointer = undefined;

            var rangesum = Math.abs(config.minValue) + Math.abs(config.maxValue);
            var _a = 0, _b = 0;

            _a = data.RangeMin / rangesum;
            _b = data.RangeMax / rangesum;
                
            if (config.minValue < 0 && config.maxValue < 0) {
                var temp = Math.abs(config.minValue - config.maxValue);
                _a = ((Math.abs(config.minValue - data.RangeMin) / temp));                   
                _b = (1 - (Math.abs(config.maxValue - data.RangeMax) / temp));
                if (data.ReverseOrder) {
                    _a = (1 - (Math.abs(config.minValue - data.RangeMin) / temp));
                    _b = ((Math.abs(config.maxValue - data.RangeMax) / temp));
                }
            }
            else if (config.minValue < 0) {
                _a = (data.RangeMin - config.minValue) / rangesum;
                _b = (data.RangeMax - config.minValue) / rangesum
            }

            var donut = d3.pie();
            var RangesBand = [0, _a, _b];
            var RangesBandEnd = [_a, _b, 1];
            if (data.ReverseOrder) {
                RangesBand = [1, _a, _b];
                RangesBandEnd = [_a, _b, 0];
            }

            function deg2rad(deg) {
                return deg * Math.PI / 180;
            }

            function newAngle(d) {
                var ratio = scale(d);
                var newAngle = config.minAngle + (ratio * range);
                return newAngle;
            }

            function configure(configuration) {

                var prop = undefined;
                for (prop in configuration) {
                    config[prop] = configuration[prop];
                }

                range = config.maxAngle - config.minAngle;
                r = config.size / 2;
                pointerHeadLength = Math.round(r * config.pointerHeadLengthPercent);

                // a linear scale that maps domain values to a percent from 0..1
                scale = d3.scaleLinear()
                    .range([0, 1])
                    .domain([config.minValue, config.maxValue]);

                ticks = scale.ticks(config.majorTicks);
                // tickData = d3.range(config.majorTicks).map(function () { return 1 / config.majorTicks; });
                tickData = RangesBand.map(function (d) { return d; });
                //var  tickData1 = d3.range(3).map(function (d) { return d / 100; });                    
                arc = d3.arc()
                    .innerRadius(r - config.ringWidth - config.ringInset)
                    .outerRadius(r - config.ringInset)
                    .startAngle(function (d, i) {
                        var ratio = d;

                        return deg2rad(config.minAngle + (ratio * range));
                        //return config.minAngle * Math.PI / 180;

                    })
                    .endAngle(function (d, i) {
                        var ratio = RangesBandEnd[i];

                        return deg2rad(config.minAngle + (ratio * range));
                        //return config.minAngle * Math.PI / 180 + 2 * Math.PI
                    });
            }
            that.configure = configure;

            function centerTranslation() {
                return 'translate(' + r + ',' + r + ')';
            }

            function isRendered() {
                return (svg !== undefined);
            }
            that.isRendered = isRendered;

            function render(newValue) {
                svg = d3.select(container)
                    .append('svg:svg')
                        .attr('class', 'gauge')
                        .attr('width', config.clipWidth)
                        .attr('height', config.clipHeight);

                var centerTx = centerTranslation();

                var arcs = svg.append('g')
                        .attr('class', 'arc')
                        .attr('transform', centerTx);

                arcs.selectAll('path')
                        .data(tickData)
                    .enter().append('path')
                        .attr('fill', function (d, i) {
                            return config.lightColorScheme[i];
                        })
                        .attr('d', arc);

                var lg = svg.append('g')
                        .attr('class', 'label')
                        .attr('transform', centerTx);
                lg.selectAll('text')
                        .data(scale.ticks(config.labelTicks))
                    .enter().append('text')
					.attr('style', 'text-anchor: middle')
                        .attr('transform', function (d) {
                            var ratio = scale(d);

                            var newAngle = config.minAngle + (ratio * range);
                            return 'rotate(' + newAngle + ') translate(0,' + (config.labelInset - r) + ')';
                        })
                        .text(config.labelFormat);

                var lb_text = svg.append("text")
                .attr("x", (config.clipWidth / 2) - 3)
                .attr("y", (config.clipHeight / 1.2) + 20)
                .attr("class", "btn btn-link")
                .style("font-family", "\"Helvetica Neue\",Helvetica,Arial,sans-serif")
                .style("text-anchor", "middle")
                .style("font-size", "12px")
                .style("opacity", 1)
                .append("tspan")
                .text(function (d, i) {
                    return data.text;
                })
                .on("click", function (d, i) {
                    ctrl.onKpiSelect({ ctx: ctrl.data });
                });

                businessNameServices.getUserFriendlyName(data.title).then(function (result) {
                    lb_text.append("title").text(result);
                });

                var lb_text2 = svg.append("text")
                .attr("x", (config.clipWidth / 2) - 2)
                .attr("y", (config.clipHeight / 1.2) + 5)
                .style("font-family", "\"Helvetica Neue\",Helvetica,Arial,sans-serif")
                .style("text-anchor", "middle")
                .style("font-size", "16px")
                .style("opacity", 1)
                .append("tspan")
                .style("font-weight", 700)
                .text(function (d, i) {
                    return data.formattedValue;
                });

                var lineData = [[config.pointerWidth / 2, 0],
                                [0, -pointerHeadLength],
                                [-(config.pointerWidth / 2), 0],
                                [0, config.pointerTailLength],
                                [config.pointerWidth / 2, 0]];
                var pointerLine = d3.line().curve(d3.curveMonotoneX);
                var pg = svg.append('g').data([lineData])
                        .attr('class', 'pointer')
                        .attr('transform', centerTx);

                pointer = pg.append('path')
                    .attr('d', pointerLine/*function(d) { return pointerLine(d) +'Z';}*/)
                    .attr('transform', 'rotate(' + config.minAngle + ')');
                update(data.ActualValue);
            }
            that.render = render;

            function update(newValue, newConfiguration) {
                if (newConfiguration !== undefined) {
                    configure(newConfiguration);
                }



                var ratio = scale(newValue);


                var newAngle = config.minAngle + (ratio * range);


                pointer.transition()
                    .duration(config.transitionMs)
                    .ease(d3.easeElastic)
                    .attr('transform', 'rotate(' + newAngle + ')');
            }
            that.update = update;

            configure(configuration);

            return that;
        };

        var powerGauge = gauge(container, {
            size: 150,
            clipWidth: 150,
            clipHeight: 136,
            ringWidth: 20,
            transitionMs: 4000,
            labelTicks: 6
        }, { ActualValue: ctrl.value, formattedValue: ctrl.getDataFormatter()(ctrl.value), RangeMin: ctrl.minRange, RangeMax: ctrl.maxRange, text: ctrl.text, title: ctrl.textTitle });
        powerGauge.render();
    }

}

angular.module("dealerWizard")
.component("gauge", {
    templateUrl: "/Scripts/app/components/gauge/gauge.html",
    transclude: true,
    controller: ['$scope', '$element', '$attrs', 'businessNameServices', gaugeCtrl],
    bindings: {
        key: "@",
        text: "<",
        textTitle: "<",
        value: "<",
        minRange: "<",
        maxRange: "<",
        reversePlot: "<",
        data: "<",
        wConfig: "<",
        onKpiSelect: "&"
    }
});


