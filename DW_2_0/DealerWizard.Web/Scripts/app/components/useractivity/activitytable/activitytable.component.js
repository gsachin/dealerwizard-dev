﻿(function (app) {

    app.component("activitytable", {
        templateUrl: "/Scripts/app/components/useractivity/activitytable/activitytable.html",
        controller: ['$scope', '$element', '$attrs', 'valueFormatter', '$routeParams', '$timeout', 'userService', 'reportService', 'apiService', activitytableCtrl],
        bindings: {
            reportKey: '<'
        }
    });

    function activitytableCtrl($scope, $element, $attrs, valueFormatter, $routeParams, $timeout, userService, reportService, apiService) {
        var ctrl = this;
        $scope.ShowLoader = false;

        let currentUser = "";

        $scope.reportConfig = { ShowLoader: true };
        $scope.reportDataList = { columns: [], data: [] };
        $scope.reportList = [];

        ctrl.$onInit = function () {
            if ($routeParams.reportname != undefined) {
                ctrl.Dept = $routeParams.reportname;
                userService.getCurrentUser().then(function (res) {
                    currentUser = res.User.Name
                });
                $timeout(createData, 30);
                return;
            }

            userService.getCurrentUser().then(function (res) {
                currentUser = res.User.Name
            });

            console.log($attrs);
            console.log(ctrl);

            if ($attrs.reportkey == undefined || $attrs.reportkey == 1 || $attrs.reportkey == 0) {
                $timeout(createData, 30);
            } else {
                reportService.get({ id: $attrs.reportkey }).then((obj) => {
                    apiService.post("api/getqueryresult", JSON.stringify(obj.report), null, function (response) {
                        $scope.reportDataList = response.data;
                    });
                });
            }
        }

        //ctrl.$onChanges = function (ChangedObject) {

        //};

        function createData() {
            $scope.reportDataList = {
                columns: [{
                    Columndatatype: "String", DataType: "String", IsLink: false, NextReportName: null, ReportFilters: "",
                    ReportRefKey: 1, ReportType: null, UrlName: null, UrlPath: null, UrlRefKey: 0, data: "Date", ismeasure: false, reportname: "User Activity Log", title: "Date"
                },
                {
                    Columndatatype: "String", DataType: "String", IsLink: false, NextReportName: null, ReportFilters: "",
                    ReportRefKey: -1, ReportType: null, UrlName: null, UrlPath: null, UrlRefKey: 0, data: "UserName", ismeasure: false, reportname: "User Activity Log", title: "UserName"
                },
                {
                    Columndatatype: "Int", DataType: "Number", IsLink: false, NextReportName: null, ReportFilters: "",
                    ReportRefKey: -1, ReportType: null, UrlName: null, UrlPath: null, UrlRefKey: 0, data: "LoggedTime", ismeasure: false, reportname: "User Activity Log", title: "Total Logged Time(In Min)"
                }
                ],
                data: [{ Date: "2017-05-19", UserName: currentUser, LoggedTime: 21 },
                        { Date: "2017-05-20", UserName: currentUser, LoggedTime: 22 },
                        { Date: "2017-05-21", UserName: currentUser, LoggedTime: 34 },
                        { Date: "2017-05-22", UserName: currentUser, LoggedTime: 33 },
                        { Date: "2017-05-23", UserName: currentUser, LoggedTime: 17 },
                        { Date: "2017-05-24", UserName: currentUser, LoggedTime: 19 },
                        { Date: "2017-05-25", UserName: currentUser, LoggedTime: 55 },
                        { Date: "2017-05-26", UserName: currentUser, LoggedTime: 14 },
                        { Date: "2017-05-27", UserName: currentUser, LoggedTime: 42 }]
            };
        }

    }


})(angular.module('dealerWizard'));