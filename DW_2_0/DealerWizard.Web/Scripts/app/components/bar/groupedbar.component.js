﻿(function (app) {

    app.component("groupedBar", {
        templateUrl: "/Scripts/app/components/bar/groupedbar.html",
        controller: ['$scope', '$element', 'valueFormatter', groupedbarCtrl],
        bindings: {
            data: "<",
            wConfig: "<",
            reportList: "<",
            renderElement: "<",
            onListItemClick: "&"
        }
    });

    function groupedbarCtrl($scope, $element, valueFormatter) {
           var ctrl = this;
        ctrl.ShowLoader = false;
        var ageingGroup = [];
		
        ctrl.$onInit = function () {
            ctrl.data = ctrl.data;
            ctrl.renderElement = ctrl.renderElement;
            if (ctrl.renderElement) {
                ageingGroup = ctrl.reportList[ctrl.reportList.length - 1].ChartObject;
                RenderBarChart(ctrl.data.data, $element[0].querySelector('#divGroupBarChart'), {}, ctrl.data.columns, ageingGroup, valueFormatter, ctrl);
            }
        }

        ctrl.$onChanges = function (ChnagedObject) {
            if (ChnagedObject.data && !ChnagedObject.data != undefined) {
                if (ctrl.renderElement) {
                    $('.d3-tip').remove();
                    ageingGroup = ctrl.reportList[ctrl.reportList.length - 1].ChartObject;
                    RenderBarChart(ctrl.data.data, $element[0].querySelector('#divGroupBarChart'), {}, ctrl.data.columns, ageingGroup, valueFormatter, ctrl);
                }
            }

            if (ChnagedObject.renderElement != undefined) {
                if (ctrl.renderElement) {
                    $('.d3-tip').remove();
                    ageingGroup = ctrl.reportList[ctrl.reportList.length - 1].ChartObject;
                    RenderBarChart(ctrl.data.data, $element[0].querySelector('#divGroupBarChart'), {}, ctrl.data.columns, ageingGroup, valueFormatter, ctrl);
                }
            }
        };

        ctrl.$onDestroy = function () {
            $(window).off('resize');
            $('.d3-tip').remove();
        }

    }

    function getValueFormatter(Type, Data, valueFormatter) {
        try {
            if (Type === 'Money') {
                return valueFormatter.money(Data);
            }
            else if (Type === 'Number') {
                return valueFormatter.number(Data);
            }
            else if (Type === 'Int') {
                return valueFormatter.int(Data);
            }
            else if (Type === 'Percent') {
                return valueFormatter.percent(Data);
            }
            else if (Type === 'Decimal1') {
                return valueFormatter.decimal1(Data);
            }
            else if (Type === 'Decimal2') {
                return valueFormatter.decimal2(Data);
            }
            else if (Type === 'Decimal3') {
                return valueFormatter.decimal3(Data);
            }
            else if (Type === 'Date') {
                return valueFormatter.date(Data, 'MM/dd/yyyy');
            }
            else {
                return Data;
            }
        }
        catch (ex) {
            return Data;
        }
    }

    function RenderBarChart(mdata, container, widgetConfig, colMetadata, ageingGroup, valueFormatter,ctrl) {
        $(container).html('');
        //var ageingGroup = ["1-30", "31-60", "61-90", "91-120", "121+"];
        // set the dimensions and margins of the graph
        var margin = { top: 20, right: 30, bottom: 70, left: 100 },
            width = 900 - margin.left - margin.right,
            height = 330 - margin.top - margin.bottom;
        if (mdata.length == 0) return;
        var _keys = Object.keys(mdata[0]);
        //var _yKeys = colMetadata[1].data;

        var data = []
        if (ageingGroup) {
            angular.forEach(ageingGroup, function (val, index) {
                var filterResult = mdata.filter(function (v) { return (v[_keys[0]] == val); });

                var _obj = {};

                for (var i = 0; i < _keys.length; i++) {
                    if (i == 0) {
                        _obj[_keys[i]] = val;
                    }
                    else if (filterResult.length != 0 && (i == 1 || i == 2)) {
                        _obj[_keys[i]] = parseFloat(filterResult[0][_keys[i]]);
                    }
                    else if (filterResult.length != 0) {
                        _obj[_keys[i]] = filterResult[0][_keys[i]];
                    }
                    else if (filterResult.length == 0) {
                        _obj[_keys[i]] = 0;
                    }

                }

                data.push(_obj);

            });
        }
        else
        {
            data = mdata;
        }
        var Colkeys = Object.keys(data[0]);

        var svg = d3.select(container).append("svg")
             .attr("width", width + margin.left + margin.right)
             .attr("height", height + margin.top + margin.bottom)
           .append("g")
             .attr("transform",
                   "translate(" + margin.left + "," + margin.top + ")");
 

        var x0 = d3.scaleBand()
                    .rangeRound([0, width])
                    .paddingInner(0.1);

        var x1 = d3.scaleBand()
            .padding(0.05);

        var y = d3.scaleLinear()
            .rangeRound([height, 0]);

        var z = d3.scaleOrdinal()
            .range(["blue", "#ff6a00", "#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56", "#d0743c", "#ff8c00"]);


        var xValue = Colkeys[0]; //use it where state is written
        var y0Value = Colkeys[1];
        var y1Value = Colkeys[2];
        var xValueDataType = colMetadata[0].DataType;
        var xAxisTitle = colMetadata[0].title;
        var yValueDataType = colMetadata[1].DataType;
        var yAxisTitle = colMetadata[2].title.substr(0, colMetadata[2].title.lastIndexOf(" "));
        var xMetaData = colMetadata[0];
 
        var keys = [y0Value, y1Value];

        x0.domain(data.map(function (d) { return d[xValue]; }));
        x1.domain([y0Value, y1Value]).rangeRound([0, x0.bandwidth()]);

        var minValue0 = d3.min(data, function (d) { return d[y0Value]; });
        var minValue1 = d3.min(data, function (d) { return d[y1Value]; });
       
        var minValue = (minValue0 < minValue1) ? minValue0 : minValue1;

        minValue = (minValue < 0) ? minValue + (minValue * .1) : 0;

        var maxValue0 = d3.max(data, function (d) { return parseInt(d[y0Value]); });
        var maxValue1 = d3.max(data, function (d) { return parseInt(d[y1Value]); });
        var maxValue = (maxValue0 > maxValue1) ? maxValue0 : maxValue1;
        maxValue = maxValue + (maxValue * .05);

        y.domain([minValue, maxValue]).nice();
 
        var xAxis = d3.axisBottom(x0);
        var yAxis = d3.axisLeft(y).ticks(null, "s");

        var tip = d3.tip()
                .attr('class', 'd3-tip ttip2')
                .offset([-10, 0])
                .html(function (d, i) {
                    var TooltipHtml = "";
                    colMetadata.forEach(function (item, index) {
                        var filterData = _.filter(data, function (f) { return f[xValue] == d.dataFor });                        
                        var val = getValueFormatter(item.DataType, filterData[0][item.data], valueFormatter);
                        if (val == "" || !val) val = filterData[0][item.data];
                        TooltipHtml += item.title + ": " + val + "<br />";
                    });
                    return TooltipHtml;
                });

        d3.select('body').append('div').attr('id', 'barTooltip').append('span').attr('id', 'value');
        
        svg.call(tip);

        var resizeTimmer;
        $(window).on("resize", function (e) {
            clearTimeout(resizeTimmer);
            resizeTimmer = setTimeout(function () {
                resized();
            }, 350)
        });

        function resized() {           
            //Get the width of the window
            //var margin = { top: 20, right: 20, bottom: 30, left: 60 };
            var w = Math.ceil((d3.select('#divGroupBarChart').node().clientWidth)) - margin.left - margin.right;
            if (w <= 0) {
                w = Math.ceil((d3.select('#panel3').node().clientWidth)) - margin.left - margin.right - 30
            };
            //var ss = d3.select('#divGroupBarChart').selectAll('svg');
           
            d3.select(d3.select('#divGroupBarChart').select('svg')._groups[0][0]).attr("width", w + margin.left + margin.right)
            //svg.attr('width', w + margin.left + margin.right);

            x0 = x0.range([0, w]);
            x1 = x1.rangeRound([0, x0.bandwidth()]);
            xAxis.scale(x0);
            
            if (d3.select('.barGroup')._groups[0][0] != null)
                d3.select('.barGroup')._groups[0][0].remove();

            svg.append("g")
           .attr('class', 'barGroup')
         .selectAll("g")
         .data(data)
         .enter().append("g")
           .attr("transform", function (d) { return "translate(" + x0(d[xValue]) + ",0)"; })
         .selectAll("rect")
         .data(function (d) { return keys.map(function (key) { return { key: key, value: d[key], dataFor: d[xValue] }; }); })
         .enter().append("rect")
           .attr("x", function (d) { return x1(d.key); })
           .attr("y", function (d) { return y(d.value); })
           .attr("width", x1.bandwidth())
           .attr("height", function (d) { return height - y(d.value); })
           .attr("fill", function (d) { return z(d.key); })
            .on("mouseover", function (d, i) {
                tip.show(d, i);
            })
            .on("mouseout", function (d, i) {
                tip.hide(d, i);
            });

            if (d3.select('.xaxis')._groups[0][0] != null)
                d3.select('.xaxis')._groups[0][0].remove();
            if (d3.select('.yaxis')._groups[0][0])
                d3.select('.yaxis')._groups[0][0].remove();

            svg.append("g")
            .attr("class", "xaxis")
            .attr("transform", "translate(0," + height + ")")
            .call(xAxis)
            .selectAll("text")
           .style("text-anchor", "end")
           .attr("dx", "-.8em")
           .attr("dy", "-.55em")
           .attr("transform", "rotate(-45)");

            svg.append("g")
                .attr("class", "yaxis")
                .call(yAxis);

            if (d3.select('.legendDiv')._groups[0][0])
                d3.select('.legendDiv')._groups[0][0].remove();


            var legend = svg.append("g")
                         .attr("class","legendDiv")
                         .attr("font-size", 10)
                         .attr("text-anchor", "end")
                         .selectAll("g")
                         .data(keys.slice().reverse())
                         .enter().append("g")
                         .attr("transform", function (d, i) { return "translate(0," + i * 20 + ")"; });

            legend.append("rect")
                .attr("x", w - 19)
                .attr("width", 19)
                .attr("height", 19)
                .attr("fill", z);

            legend.append("text")
                .attr("x", w - 24)
                .attr("y", 9.5)
                .attr("dy", "0.32em")
                .text(function (d) {
                    var colName = _.find(colMetadata, function (o) { return o.data == d; });                    
                    return colName.title;
                });

            if (d3.select('.xAxisText')._groups[0][0])
                d3.select('.xAxisText')._groups[0][0].remove();

            var xaxistext = svg.append("text").attr("class","xAxisText")
            .attr("text-anchor", "middle")  // this makes it easy to centre the text as the transform is applied to the anchor
            .attr("transform", "translate(" + (w / 2) + "," + (height + (margin.bottom - 5)) + ")")  // centre below axis
            .text(xAxisTitle);
         
            if (d3.select('.yAxisText')._groups[0][0])
                d3.select('.yAxisText')._groups[0][0].remove();

            var yaxistext = svg.append("text").attr("class", "yAxisText")
                 .attr("text-anchor", "middle")  // this makes it easy to centre the text as the transform is applied to the anchor
                 .attr("transform", "translate(" + ((-margin.left - 20) / 2) + "," + (height / 2) + ")rotate(-90)")  // text is drawn off the screen top left, move down and out and rotate
                 .text(yAxisTitle);

        }
        resized();
    }

})(angular.module('dealerWizard'));