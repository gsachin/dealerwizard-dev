(function (app) {
    'use strict';

    app.component('sidebarDealerList',
        {
            templateUrl: "/Scripts/app/components/sidebar-dealerlist/sidebar-dealerlist.html",
            controller: ['$scope', '$element', '$attrs', 'dealerService', sidebarDealerListCtrl],
            bindings: {
                data: "<",
                defaultDealerRefId: "<",
                onDealerChange: "&"
            }
        });

    function sidebarDealerListCtrl($scope, $element, $attrs, dealerService) {
        var ctrl = this;
        $scope.dealerList = [];
        $scope.selectedDealerId = "";

        $scope.selectDealer = function (dealer) {
            ctrl.onDealerChange({ ctx: dealer });
        }

        ctrl.$onInit = function () {
            $scope.dealerList = [];
        }

        ctrl.$onChanges = function (ChangeObject) {
            if (ChangeObject.data != undefined && !ChangeObject.data.isFirstChange()) {
                angular.copy(ctrl.data, $scope.dealerList);
            }

            if (ChangeObject.defaultDealerRefId != undefined && !ChangeObject.defaultDealerRefId.isFirstChange()) {
                $scope.selectedDealerId = ctrl.defaultDealerRefId;
            }
        }

        ctrl.$postLink = function () {
            $('.scrollbar-inner').scrollbar();
            $('.DealerSliderMenu').css('height', $(window).height() - $('.navbar').height() - 20);
        };
    }

})(angular.module('dealerWizard'));