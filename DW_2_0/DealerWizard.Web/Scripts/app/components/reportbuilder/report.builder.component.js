﻿(function (app) {
    "use strict"
     
    app.component("reportBuilderComponent", {
        templateUrl: "/Scripts/app/components/reportbuilder/reportbuilder.html",
        controller: ['$scope', '$element', '$attrs', 'apiService', 'notificationService', 'reportService', builderConnectionPopupCtrl],
        bindings: {
             
        }
    });

    function builderConnectionPopupCtrl($scope, $element, $attrs, apiService, notificationService, reportService) {
        var ctrl = this;
        ctrl.reportName = "";
        $scope.selectedCube = "ProductWiseDeals";

        let reportObject = {
            cubeName: $scope.selectedCube,
            measures: [],
            dimensions: []
        };
        $scope.showReportSave = false;
        var sourceElement;
        const handleDragStart = (event, ui) => {
            $(this).css('zIndex', '999');
            $(ui.helper).addClass("ui-draggable-helper");
            sourceElement = $(ui.helper[0]).parents('div.ui-droppable').attr('id');
            console.log(sourceElement);
        }

        const handleDropEvent = (event, ui) => {
            if (ui.draggable.lastDropObject !== undefined) {
                ui.draggable.lastDropObject.droppable('enable');
            }

            $('.ui-draggable-helper').removeClass('ui-draggable-helper');
             
            console.log($(ui.draggable[0]).attr('data-value'));
            let value=$(ui.draggable[0]).attr('data-value');
            let contentType=$(ui.draggable[0]).attr('data-contentType');
            if (contentType == "Measure") {
               var elem= _.find(reportObject.measures, (o) => {
                    return o == value
                });
               if (elem == undefined) {
                   reportObject.measures.push(value);
                   getData(reportObject);
                   $scope.showReportSave = true;
               }

            } else {
                var elem = _.find(reportObject.dimensions, (o) => {
                    return o == value
                });
                if (elem == undefined) {
                    reportObject.dimensions.push(value);
                    getData(reportObject);
                    $scope.showReportSave = true;
                }
            }

        }
        const getDroppableObject = () => {
            return {
                drop: handleDropEvent,
                tolerance: "touch",
            }
        }

        ctrl.saveReport = () => {
            reportService.save(ctrl.reportName, reportObject)
                .then((success) => {
                    notificationService.success(`report saved successfully.`);
                });
        }

        $scope.ShowLoader = false;

        $scope.reportConfig = { ShowLoader: false };
        $scope.reportDataList = { };
        $scope.reportList = [];
        
        $scope.treeOptions = {
            nodeChildren: "children",
            dirSelectable: false,
            injectClasses: {
                ul: "a1",
                li: "a2",
                liSelected: "a7",
                iExpanded: "a3",
                iCollapsed: "a4",
                iLeaf: "a5",
                label: "a6",
                labelSelected: "a8"
            } 
        }
        $scope.dataForTheTree = [
            {
                "Name": "Measures", "dragable": true, contentType: "Measure", value: "[Measures]", "children": [
                  {
                      "Name": "Tbl Deals Fact", "dragable": false, contentType: "Measure", value: "", "children": [
                          { "Name": "Cash Down", "dragable": true, contentType: "Measure", value: "Cash Down", "children": [] },
                          { "Name": "Cash Price", "dragable": true, contentType: "Measure", value: "Cash Price", "children": [] },
                          { "Name": "Days To Sale", "dragable": true, contentType: "Measure", value: "Days To Sale", "children": [] },
                          { "Name": "Finance Reserve", "dragable": true, contentType: "Measure", value: "Finance Reserve", "children": [] },
                          { "Name": "PVR", "dragable": true, contentType: "Measure", value: "PVR", "children": [] },
                          { "Name": "Total FI Gross", "dragable": true, contentType: "Measure", value: "Total FI Gross", "children": [] },
                          { "Name": "Total Sales Gross", "dragable": true, contentType: "Measure", value: "Total Sales Gross", "children": [] },
                          { "Name": "TotalUnitsSold", "dragable": true, contentType: "Measure", value: "TotalUnitsSold", "children": [] },
                          { "Name": "Units Sold - Tbl Deals Fact", "dragable": true, contentType: "Measure", value: "Units Sold - Tbl Deals Fact", "children": [] },
                          { "Name": "Vehicle Inventory Value", "dragable": true, contentType: "Measure", value: "Vehicle Inventory Value", "children": [] }
                      ]
                  } 
                ]
            },
            {
                "Name": "Kpi", "dragable": false,value:"", "children": [{
                }]
            },
            {
                "Name": "Dealer", "dragable": true, contentType: "Dimension",value:"[Dealer]", "children": [
                    {
                        "Name": "Name", "dragable": true, contentType: "Dimension", value: "[Dealer].[Name]", "children": [
                        { "Name": "Name", "dragable": true, contentType: "Dimension", value: "[Dealer].[Name].[Name]", "children": [] }
                        ]
                    },
                    {
                        "Name": "Dealer Key", "dragable": true, contentType: "Dimension", value: "[Dealer].[Dealer Key]", "children": [
                        {
                            "Name": "Dealer Key", "dragable": true, contentType: "Dimension",value:"[Dealer].[Dealer Key].[Dealer Key]", "children":[]
                        }]
                    }
                ]
            }
        ];

        const getData = (reportObj) => {
            var obj = JSON.stringify(reportObj)
            //return apiService.post("api/getqueryresult",reportObj, null, function (response) {
            //    $scope.reportDataList = response.data;
            //});

            return apiService.post("api/getqueryresult", obj, null, function (response) {
                $scope.reportDataList = response.data;
            });
        }
        
        ctrl.filterConfig = { ShowLoader :true};
        //$scope.showToggle = function (node, expanded, $parentNode, $index, $first, $middle, $last, $odd, $even, $path, $parent) {
             
            $("treecontrol").on("mousemove", "span.dragable", function (event) {
                $(this).draggable({
                        start: handleDragStart,
                        cursor: 'move',
                        revert: "invalid",
                        containment: "window",
                        scroll: false,
                        appendTo: "body",
                        drag: function (event, ui) {
                            var offset = $(this).offset();
                            var xPos = offset.left;
                            var yPos = offset.top;
                            $(this).css('zIndex', '10000');
                            $('.ui-draggable-helper').css('z-index', '10000');
                            //  $(this).text('x: ' + xPos + 'y: ' + yPos);
                            var shw_xy = 'x: ' + xPos + 'y: ' + yPos;
                            console.log(shw_xy);
                        },
                        helper: function (e) {
                            console.log($(e.target).hasClass("ui-draggable"),$(e.target));
                            var original = $(e.target).hasClass("ui-draggable") ? $(e.target) : $(e.target).closest(".ui-draggable");
                            return original.clone().css({
                                width: original.width()<100?"100px": original.width(),
                                backgroundColor:"white",
                                padding:"5px",
                                border:"3px solid #ddd"
                            });

                        }
                    });
            });

        //};


        $("#reportViewer").droppable(getDroppableObject());

        ctrl.$postLink = () => {
           
        }
    }
})(angular.module("dealerWizard"));