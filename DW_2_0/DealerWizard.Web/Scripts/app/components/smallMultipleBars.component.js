﻿(function (app) {
    'use strict';

    app.component('smallMultipleBars',
        {
            templateUrl: "/Scripts/app/components/smallMultipleBars.html",
            controller: ['$scope', '$element', '$attrs', 'valueFormatter','businessNameServices',smallMultipleBarsCtrl],
            bindings: {
                data: "<",
                wConfig: "<",
                aggregatePeriod: "<",
                showPriorPeriod: "<",
                onSelectBar: "&",
                onSelect: "&",
                onAggregatedPeriodChange: "&",
                onPriorPeriodClick: "&"
            }
        });


    function smallMultipleBarsCtrl($scope, $element, $attrs, valueFormatter,businessNameServices) {

        var ctrl = this, dataCopy = ctrl.data, enableDaysSelection=false;
        $scope.ShowLoader = true;
        $scope.aggregatedPeriodClass = { ytd: "btn-primary", mtd: "btndisabled" };

        ctrl.select = function (d) {
            enableDaysSelection = true;        
            $scope.ShowLoader = true;
            dataCopy = "$"
            ctrl.onSelectBar({ ctx: { selectedDate: d.Date } });
        }

        var margin = { top: 32, right: 20, bottom: 8, left: 50 },
              width = 400 - margin.left - margin.right,
              height = 130 - margin.top - margin.bottom;

        // Parse the date / time
        var parseDate = d3.timeParse("%Y-%m-%d");

        ctrl.$onInit = function () {
            $scope.ShowLoader = ctrl.wConfig.ShowLoader;
            $element[0].querySelector("#chkPyear").checked = ctrl.showPriorPeriod;
        }

        ctrl.$onChanges = function (changedObj) {
            if (changedObj.data) {
                dataCopy = changedObj.data.currentValue;
            }

            if (changedObj.aggregatePeriod != undefined) {
                var aggPeriod = $scope.aggregatedPeriodClass;
                if (changedObj.aggregatePeriod.currentValue === "YTD") {
                    aggPeriod.ytd = "btn-primary";
                    aggPeriod.mtd = (enableDaysSelection == false) ? "Blocked" : "btndisabled";
                }
                else {
                    aggPeriod.ytd = "btndisabled";
                    aggPeriod.mtd = "btn-primary";
                }
            }

            if (changedObj.showPriorPeriod != undefined) {
                $element[0].querySelector("#chkPyear").checked = changedObj.showPriorPeriod.currentValue;

                if ($scope.aggregatedPeriodClass.ytd == "btn-primary" && changedObj.showPriorPeriod.currentValue) {
                    $scope.aggregatedPeriodClass.mtd = "Blocked";
                    enableDaysSelection = false;
                }
            }

            if (dataCopy !== "$") {//Fix this for better null check 
                var dataLocalCopy = angular.copy(dataCopy);

                var config = {                   
                    periodFor: ctrl.aggregatePeriod
                };

                renderMultipleBulletChart("divSmallMultiple", dataLocalCopy, config, ctrl.wConfig);

                //ctrl.wConfig.ShowLoader = false;
                $scope.ShowLoader = false;
            }

            if (changedObj.wConfig != undefined) {
                $scope.ShowLoader = ctrl.wConfig.ShowLoader;                 
            }
        };


        ctrl.$onDestroy = function () {        
            $(window).off('resize');
            $('.hdn').remove();
        }

        $scope.ChangeAggregatedPeriod = function (NewAggregateValue) {
            if (NewAggregateValue === "MTD" && !enableDaysSelection)
                return false;
 
            if (ctrl.aggregatePeriod !== NewAggregateValue) {
                $scope.ShowLoader = true;                 
                ctrl.onAggregatedPeriodChange({ ctx: NewAggregateValue });
            }

        }
         
        $scope.InvokePriorYear = function () {                        
            if ($element[0].querySelector("#chkPyear").checked === false) {
                enableDaysSelection = true;
                $scope.ShowLoader = true;                 
                if ($scope.aggregatedPeriodClass.mtd == "Blocked") {
                    $scope.aggregatedPeriodClass.mtd = "btndisabled";
                }
                ctrl.onPriorPeriodClick({ ctx: $element[0].querySelector("#chkPyear").checked });
            }
            else if ($element[0].querySelector("#chkPyear").checked === true && $scope.aggregatedPeriodClass.mtd === "btn-primary") {              
                $element[0].querySelector("#chkPyear").checked = false;
                enableDaysSelection = true;
            }
            else {
                enableDaysSelection = false;
                $scope.ShowLoader = true;
                if ($scope.aggregatedPeriodClass.mtd == "btndisabled" && !enableDaysSelection) {
                    $scope.aggregatedPeriodClass.mtd = "Blocked";
                }
                ctrl.onPriorPeriodClick({ ctx: $element[0].querySelector("#chkPyear").checked });
            }
        }

        function renderMultipleBulletChart(idfordisp, chartJson, ui, customConfig) {           
            chartJson.Current = _.sortBy(chartJson.Current, function (d) { return d.Date });

            if ($element[0].querySelector("#chkPyear").checked == true && chartJson.previous != null) {
                chartJson.previous = _.sortBy(chartJson.previous, function (d) { return d.Date });
                angular.forEach(chartJson.previous, function (d, i) {
                    d.measureDate = parseDate(chartJson.Current[i].Date);
                    d.Date = parseDate(d.Date);
                    d.Value = +d.Value;
                    d.Name = d.Name;
                    d.select = function () {                        
                        ctrl.select(this);
                    };
                });
            }

            angular.forEach(chartJson.Current, function (d, i) {
                d.measureDate = parseDate(chartJson.Current[i].Date);
                d.Date = parseDate(d.Date);
                d.Value = +d.Value;
                d.Name = d.Name;
                d.select = function () {
                    ctrl.select(this);
                };

            });
            var config = { data: chartJson, dateFormat: (ui.periodFor == "YTD") ? d3.timeFormat("%b %y") : d3.timeFormat("%d %b") };


            var config1 = {
                Series: [
                    {
                        data: chartJson.Current,
                        dateFormat: (ui.periodFor == "YTD") ? d3.timeFormat("%b %y") : d3.timeFormat("%d %b"),
                        type: "bar"
                    },
                    {
                        data: chartJson.previous,
                        dateFormat: (ui.periodFor == "YTD") ? d3.timeFormat("%b %y") : d3.timeFormat("%d %b"),
                        type: "bar"
                    }
                ]
            };

            d3.select('#' + idfordisp).selectAll("svg").remove();
            d3.select(".divSvgAxis").remove();

            drawSmallMultiple(config);

            function getCustomAttributes(key) {
                return customConfig.bars.filter(function (val) { return val.key == key; })[0];
            }

            function getValueFormatter(key) {
                return valueFormatter.getformatter(getCustomAttributes(key).format);
            }

            function drawSmallMultiple(config) {
                var svg = [], x = [], y = [], xAxis = [], yAxis = [], data = config.data.Current, aggregatedSum = config.data.AggregatedSum, lblText=[];
                var line = [], Linedata = [];

                var ActualData = d3.nest()
                          .key(function (d) { return d.Name; })
                          .entries(data);

                var PYearActualData = [];

                if ($element[0].querySelector("#chkPyear").checked == true && config.data.previous != null) {
                    PYearActualData = d3.nest()
                              .key(function (d) { return d.Name; })
                              .entries(config.data.previous);
                }

                function FormatTicks(d) {
                    if (d >= 100000000) {
                        return (d / 100000000) + 'B';
                    }
                    else if (d >= 1000000) {
                        return (d / 1000000) + 'M';
                    }
                    else if (d >= 1000) {
                        return (d / 1000) + 'K';
                    }
                    else if (d <= -100000000) {
                        return (d / 100000000) + 'B';
                    }
                    else if (d <= -1000000) {
                        return (d / 1000000) + 'M';
                    }
                    else if (d <= -1000) {
                        return (d / 1000) + 'K';
                    }
                    else {
                        return parseInt(d) === d ? d : d.toFixed(2);
                    }
                }

                for (var SvgCount = 0; SvgCount < ActualData.length; SvgCount++) {
                    svg[SvgCount] = {},lblText[SvgCount]={};
                    x[SvgCount] = d3.scaleBand()
                                    .range([0, width])
                                    .padding(.05);

                    y[SvgCount] = d3.scaleLinear()
                                    .range([height, 0]);

                    var valueFormatter = getValueFormatter(ActualData[SvgCount].key);

                    var xAxisTickCount = (ActualData[SvgCount].values.length > 30) ? 31 : ActualData[SvgCount].values.length;

                    xAxis[SvgCount] = d3.axisBottom(x[SvgCount])
                        .ticks(ActualData[SvgCount])
                         .tickFormat(config.dateFormat);

                    yAxis[SvgCount] = d3.axisLeft(y[SvgCount])
                         .ticks(5)
                        .tickFormat(FormatTicks);

                    var MaxBounValue = Math.max.apply(Math, ActualData[SvgCount].values.map(function (o) { return o.Value; }));
                    var MinBounValue = Math.min.apply(Math, ActualData[SvgCount].values.map(function (o) { return o.Value; }));
                    var PYMaxBounValue = 0, PYMinBounValue = 0;
                    if ($element[0].querySelector("#chkPyear").checked == true && chartJson.previous != null) {
                        PYMaxBounValue = Math.max.apply(Math, PYearActualData[SvgCount].values.map(function (o) { return o.Value; }));
                        PYMinBounValue = Math.min.apply(Math, PYearActualData[SvgCount].values.map(function (o) { return o.Value; }));
                    }

                    MaxBounValue = (MaxBounValue > PYMaxBounValue) ? MaxBounValue : PYMaxBounValue;
                    MinBounValue = (MinBounValue < PYMinBounValue) ? MinBounValue : PYMinBounValue;

                    if (MinBounValue == MaxBounValue && MinBounValue > 0) {
                        MinBounValue = 0;
                    }
                    else {
                        MinBounValue = (MinBounValue < 0) ? MinBounValue + (MinBounValue * .2) : MinBounValue - (MinBounValue * .2);
                    }

                    if (MaxBounValue < 0) {
                        MaxBounValue = 0;
                    }

                    x[SvgCount].domain(data.map(function (d) { return d.measureDate; }));
                    y[SvgCount].domain([MinBounValue, MaxBounValue]);

                    svg[SvgCount] = d3.select('#' + idfordisp)
                        .append("svg").attr("width", width + margin.left + margin.right)
                    .attr("height", height + margin.top + margin.bottom)
                    .append("g")
                    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

                    //svg[SvgCount].call(tip);

                    svg[SvgCount].append("g")
                    .selectAll("text")
                        .style("text-anchor", "end")
                        .attr("dx", "-.8em")
                        .attr("dy", "-.55em");

                    lblText[SvgCount] = svg[SvgCount].append("text")
					.data([ActualData[SvgCount]])
                    .attr("x", (width / 2))
                    .attr("y", 0 - (margin.top / 2))
                    .attr("text-anchor", "middle")
                    .style("font-family", '"Helvetica Neue",Helvetica,Arial,sans-serif')
                    .style("font-size", "14px");

                    lblText[SvgCount].append("tspan")
                     .attr("class", "btn btn-link titleTxt")
                    .text(function (d, i) {
                        return d.key + " : ";
                    })                    
                    .on("click", function (d, i) {
                        var key = _.find(customConfig.bars, function (f) { return f.key == d.key });
                        ctrl.onSelect({ ctx: key });
                    })
                        .append("tspan")
					.style("font-weight", "bold")
                    .text(function (d, i) {
                        var _aggSum = 0;

                        aggregatedSum.forEach(function (_d, _i) {
                            if (_d.Name == d.key) {
                                _aggSum = _d.Value;
                            }
                        });

                        return valueFormatter(_aggSum);
                    });
                     
                   d3.select('body').append('div').attr('id', 'tooltip' + [SvgCount]).attr('class', 'hdn').append('span').attr('id', 'value');
                   var elem = lblText[SvgCount].append("title");
                    
                   function setFriendlyName(elem, key) {
                       businessNameServices.getUserFriendlyName(key).then(function (result) {
                           elem.text(result);
                       });
                   }

                   setFriendlyName(elem, ActualData[SvgCount].key);
                   
                    svg[SvgCount].append("g")
                        .attr("class", "y axis")
                        .call(yAxis[SvgCount]);

                    CreateBars(idfordisp, svg[SvgCount], 'bar', x[SvgCount], y[SvgCount], MinBounValue, ActualData, SvgCount, PYearActualData, { xAddOn: 0, WidthAddon: 0 }, chartJson, ui, 'blue', "0.8", customConfig);
                    
                    if ($element[0].querySelector("#chkPyear").checked == true && chartJson.previous != null) {
                        CreateBars(idfordisp, svg[SvgCount], 'bar2', x[SvgCount], y[SvgCount], MinBounValue, PYearActualData, SvgCount, ActualData, { xAddOn: 5, WidthAddon: 10 }, chartJson, ui, 'lightsteelblue', "0.5", customConfig);
                    }
                    
                }

                var x1, y1, xAxis1, yAxis1, svg2 = {};

                x1 = d3.scaleBand().range([0, width]).padding(.05);

                var xAxisTickCount = (ActualData[0].values.length > 30) ? 31 : ActualData[0].values.length;
                xAxis1 = d3.axisBottom(x1)
                    .ticks(xAxisTickCount)
                         .tickFormat(config.dateFormat);

                x1.domain(data.map(function (d) { return d.measureDate; }));

                if (ActualData[0].values.length > 31) {
                    var modValue = Math.floor(ActualData[0].values.length / 12);

                    xAxis1 = d3.axisBottom(x1)
                        .tickFormat(config.dateFormat)
                    .tickValues(x1.domain().filter(function (d, i) { return !(i % modValue) }));
                }
                CreateAxis(width, margin, idfordisp, svg2, ActualData, xAxis1);

                var resizeTimmer;
                $(window).on("resize", function (e) {
                    clearTimeout(resizeTimmer);
                    resizeTimmer = setTimeout(function () {
                        resized();
                    }, 350)
                });

                function resized() {
                    if (d3.select('#' + idfordisp).node() == null) {
                        return;
                    }
                    //Get the width of the window
                    var w = Math.ceil((d3.select('#' + idfordisp).node().clientWidth) / 3.04) - margin.left - margin.right;

                    var ss = d3.select('#' + idfordisp).selectAll('svg');

                    //svg = [], x = [], y = [], xAxis = [], yAxis
                    if (w < 220) {
                        w = Math.ceil((d3.select('#' + idfordisp).node().clientWidth)) - margin.left - margin.right;
                    }

                    var line = d3.line()
                  .x(function (d) { return x(d.measureDate); })
                  .y(function (d) { return y(d.Value); });

                    ss._groups.forEach(function (item, index) {
                        angular.forEach(item, function (item2, index1) {
                            if (index1 < ActualData.length) {
                                x[index1] = x[index1].range([0, w]).padding(.25);
                                xAxis[index1] = xAxis[index1].scale(x[index1]);
                                var svg = d3.select(item2);
                                svg.attr('width', w + margin.left + margin.right);
                                var Bars = svg.selectAll(".bar");
                                angular.forEach(Bars._groups[0], function (bar, index2) {
                                    d3.select(bar)
                                        .attr("x", x[index1](ActualData[0].values[index2].measureDate) + (x[index1].bandwidth() * .2))
                                        .attr("width", x[index1].bandwidth() - (x[index1].bandwidth() * .4));

                                });

                                if ($element[0].querySelector("#chkPyear").checked == true && chartJson.previous != null) {

                                    var Bars = svg.selectAll(".bar2");
                                    angular.forEach(Bars._groups[0], function (bar, index2) {
                                        d3.select(bar)
                                            .attr("x", x[index1](PYearActualData[0].values[index2].measureDate))
                                            .attr("width", x[index1].bandwidth());

                                    });


                                    var Paths = svg.selectAll("path.line");

                                    angular.forEach(Paths._groups[0], function (path, index2) {

                                        line[index1] = d3.line()
                                      .x(function (d) { return x[index1](d.measureDate); })
                                      .y(function (d) { return y[index1](d.Value); });


                                        Linedata[index1] = PYearActualData[index1].values.map(function (d) {
                                            return {
                                                measureDate: d.measureDate,
                                                Value: d.Value
                                            };
                                        });

                                        svg.select("path.line")
                                      .datum(Linedata[index1])
                                      .attr("class", "line")
                                      .attr("d", line[index1]);
                                    });

                                    var lineDots = svg.selectAll("circle.CircularDot");

                                    lineDots.data(function (d) { return PYearActualData[index1].values; })
                                            .attr("r", 3)
                                            .attr("cx", function (d) { return x[index1](d.measureDate); })
                                            .attr("cy", function (d) { return y[index1](d.Value); });
                                }


                                var txt = svg.selectAll(".titleTxt");
                                angular.forEach(txt._groups[0], function (t, index2) {
                                    d3.select(t)
                                        .attr("x", (w / 2))

                                });

                            }
                            else {

                                x1 = x1.range([0, w]).padding(.05);
                                xAxis1 = xAxis1.scale(x1);

                                var svg = d3.select(item2);
                                svg.attr('width', w + margin.left + margin.right);
                                svg.call(xAxis1);
                            }

                        });
                    });
                }
                resized();
            }
        }

        function getValueFormatter(customConfig, key) {
            return valueFormatter.getformatter(customConfig.bars.filter(function (val) { return val.key == key; })[0].format);
        }

        function CreateDots(DotClass, idfordisp, svg, data, data2, dataIndex, x, y, ui, chartJson) {
            svg.selectAll("dot")
                                   .data(data[dataIndex].values)
                                   .enter().append("circle")
                           .attr('class', DotClass)
                                   .attr("r", 3)
                                   .attr("cx", function (d) { return x(d.measureDate); })
                                   .attr("cy", function (d) { return y(d.Value); })
                                   .attr('fill', 'steelblue')
                                   .on("mouseover", function (d, i) {
                                       var Circles = d3.select('#' + idfordisp).selectAll('svg').selectAll("." + DotClass);

                                       for (var a = 0; a < data.length; a++) {

                                           d3.select(Circles[a][i]).attr("r", 5);

                                           var pst = $(Circles[a][i]).position();
                                           var _data = data[a].values[i];

                                           //"<strong>" + MonthName(_data.Date.getMonth() + 1) + " " + ((PeriodFor == "YTD") ? _data.Date.getFullYear() : _data.Date.getDate()) + "</strong> <br/> <strong>" + _data.Name + ":</strong> <span>" + _data.Value + "</span>"
                                           var TooltipHtml = "<strong>" + MonthName(_data.Date.getMonth() + 1) + " " + ((ui.periodFor == "YTD") ? _data.Date.getFullYear() : _data.Date.getDate()) + "</strong> : <span>" + _data.Value + "</span>";

                                           if ($element[0].querySelector("#chkPyear").checked == true && chartJson.previous != null) {
                                               var _Cdata = data2[a].values[i];
                                               TooltipHtml += "<br/><strong>" + MonthName(_Cdata.Date.getMonth() + 1) + " " + ((ui.periodFor == "YTD") ? _Cdata.Date.getFullYear() : _Cdata.Date.getDate()) + "</strong> : <span>" + _Cdata.Value + "</span>";
                                               var _Difference = Math.round(_Cdata.Value - _data.Value, 2);
                                               var _Percent = Math.round((_Difference / _data.Value) * 100, 2);
                                               var _color = (_Difference < 0) ? "red" : "green";
                                               TooltipHtml += '<br/> Diff: <span style="color:' + _color + '">' + _Difference + ' (' + _Percent + '% ) </span';
                                           }

                                           d3.select('#tooltip' + a)
                                               .attr('class', 'ttip')
                                           .style('left', (pst.left + 10) + 'px')
                                           .style('top', (pst.top - 20) + 'px')
                                               .select('#value')
                                           .html(TooltipHtml);

                                           d3.select('#tooltip' + a).classed('hdn', false);
                                       }
                                   })
                                   .on("mouseout", function (d, i) {
                                       var Circles = d3.select('#' + idfordisp).selectAll('svg').selectAll("." + DotClass);
                                       for (var a = 0; a < data.length; a++) {
                                           d3.select(Circles[a][i]).attr("r", 3);
                                           d3.select('#tooltip' + a).classed('hdn', true);
                                       }
                                       $('.ttip').addClass('hdn');
                                   });
        }

        function CreateLine(svg, line, data, dataIndex) {
            line[dataIndex] = d3.line()
                         .x(function (d) { return x[dataIndex](d.measureDate); })
                         .y(function (d) { return y[dataIndex](d.Value); })
                         .interpolate('linear');

            svg.append("path")
                           .datum(data[dataIndex].values)
                           .attr("class", "line")
                           .attr("d", line[dataIndex]);
        }

        function CreateBars(idfordisp, svg, barClass, x, y, MinBounValue, data, dataIndex, data2, barSeparators, chartJson, ui, barColor, opacity, customConfig) {
            svg.selectAll("." + barClass)
                                 .data(data[dataIndex].values)
                                 .enter()
                                 .append("rect").attr("class", barClass).on("click", function (d, i) {
                                     if (ui.periodFor == "YTD") {
                                         d.select();
                                     }
                                 })
                        .attr("x", function (d) { return x(d.measureDate) + barSeparators.xAddOn })
                        .attr("width", function () {
                            return x.bandwidth() - barSeparators.WidthAddon;
                        })
                        .attr("y", function (d, i) {
                            return (d.Value < 0) ? y(0) : y(d.Value);
                        })
                        .attr("height", function (d, i) {
                            if (MinBounValue < 0) {
                                return Math.abs(y(d.Value) - y(0));
                            }
                            else {
                                return Math.abs(height - y(d.Value));
                            }
                        })
                        .attr("fill", barColor)
                        .style("opacity", opacity)
                    .on("mouseover", function (d, i) {
                        var bars = d3.select('#' + idfordisp).selectAll('svg').selectAll("." + barClass);
                        if (barClass == "bar2") {
                            for (var a = 0; a < data2.length; a++) {
                                var _valFormatter = getValueFormatter(customConfig, data2[a].key);
                                d3.select(this).classed("cursor-pointer", true);
                                d3.select(bars._groups[a][i]).classed("barhover", true);

                                var pst = $(bars._groups[a][i])[0].getBoundingClientRect();
                                var _data = data2[a].values[i];

                                var TooltipHtml = "<strong>" + MonthName(_data.Date.getMonth() + 1) + " " + ((ui.periodFor == "YTD") ? _data.Date.getFullYear() : _data.Date.getDate()) + "</strong> : <span>" + _valFormatter(_data.Value) + "</span>";

                                if ($element[0].querySelector("#chkPyear").checked == true && chartJson.previous != null) {
                                    var _Pdata = data[a].values[i];
                                    TooltipHtml += "<br/><strong>" + MonthName(_Pdata.Date.getMonth() + 1) + " " + ((ui.periodFor == "YTD") ? _Pdata.Date.getFullYear() : _Pdata.Date.getDate()) + "</strong> : <span>" + _valFormatter(_Pdata.Value) + "</span>";
                                    var _Difference = Math.round(_data.Value - _Pdata.Value, 2);
                                    var _Percent = Math.round((_Difference / _Pdata.Value) * 100, 2);
                                    var _color = (_Difference < 0) ? "red" : "green";
                                    TooltipHtml += '<br/> Diff: <span style="color:' + _color + '">' + _valFormatter(_Difference) + ' (' + _Percent + '% ) </span';
                                }

                                if (navigator.userAgent.match(/msie/i) || navigator.userAgent.match(/trident/i)) {
                                    pst = $($(bars._groups[a][i])[0]).offset();
                                    d3.select('#tooltip' + a)
                                  .attr('class', 'ttip')
                                  .style('left', (pst.left) + 'px')
                                  .style('top', (pst.top - 40) + 'px')
                                      .select('#value')
                                  .html(TooltipHtml);
                                }
                                else {
                                    d3.select('#tooltip' + a)
                                  .attr('class', 'ttip')
                                  .style('left', (pst.left + window.scrollX) + 'px')
                                  .style('top', (pst.top + window.scrollY - 40) + 'px')
                                      .select('#value')
                                  .html(TooltipHtml);
                                }
                              

                                d3.select('#tooltip' + a).classed('hdn', false);
                            }
                        } else {
                            for (var a = 0; a < data.length; a++) {
                                var _valFormatter = getValueFormatter(customConfig, data[a].key);
                                d3.select(this).classed("cursor-pointer", true);
                                d3.select(bars._groups[a][i]).classed("barhover", true);

                                var pst = $(bars._groups[a][i])[0].getBoundingClientRect();
                                var _data = data[a].values[i];

                                var TooltipHtml = "<strong>" + MonthName(_data.Date.getMonth() + 1) + " " + ((ui.periodFor == "YTD") ? _data.Date.getFullYear() : _data.Date.getDate()) + "</strong> : <span>" + _valFormatter(_data.Value) + "</span>";

                                if ($element[0].querySelector("#chkPyear").checked == true && chartJson.previous != null) {
                                    var _Pdata = data2[a].values[i];
                                    TooltipHtml += "<br/><strong>" + MonthName(_Pdata.Date.getMonth() + 1) + " " + ((ui.periodFor == "YTD") ? _Pdata.Date.getFullYear() : _Pdata.Date.getDate()) + "</strong> : <span>" + _valFormatter(_Pdata.Value) + "</span>";
                                    var _Difference = Math.round(_data.Value - _Pdata.Value, 2);
                                    var _Percent = Math.round((_Difference / _Pdata.Value) * 100, 2);
                                    var _color = (_Difference < 0) ? "red" : "green";
                                    TooltipHtml += '<br/> Diff: <span style="color:' + _color + '">' + _valFormatter(_Difference) + ' (' + _Percent + '% ) </span';
                                }

                                d3.select('#tooltip' + a)
									.attr('class', 'ttip')
								.style('left', (pst.left + window.scrollX) + 'px')
								.style('top', (pst.top + window.scrollY - 40) + 'px')
									.select('#value')
								.html(TooltipHtml);

                                d3.select('#tooltip' + a).classed('hdn', false);
                            }
                        }
                    })
                    .on("mouseout", function (d, i) {
                        var bars = d3.select('#' + idfordisp).selectAll('svg').selectAll("." + barClass);
                        if (barClass == "bar2") {
                            for (var a = 0; a < data2.length; a++) {
                                d3.select(bars._groups[a][i]).classed("barhover", false);
                                d3.select(this).classed("cursor-pointer", false);
                                d3.select('#tooltip' + a).classed('hdn', true);
                            }
                        } else {
                            for (var a = 0; a < data.length; a++) {
                                d3.select(bars._groups[a][i]).classed("barhover", false);
                                d3.select(this).classed("cursor-pointer", false);
                                d3.select('#tooltip' + a).classed('hdn', true);
                            }
                        }

                        $('.ttip').addClass('hdn');
                    });
        }

        function CreateAxis(width, margin, idfordisp, svg, data, xAxis1) {
            svg = d3.select('#' + idfordisp)
                .append('div').attr('class', 'divSvgAxis')
                .selectAll("svg")
             .data(data)
             .enter()
             .append("svg:svg").attr("width", width + margin.left + margin.right)
             .attr("height", 50)
           .append("g")
             .attr("transform", "translate(0,0)");

            svg
                .append("g")
                .attr("class", "x axis")
           .attr("transform", "translate(" + margin.left + "," + 0 + ")")
           .call(xAxis1)
         .selectAll("text")
           .style("text-anchor", "end")
           .attr("dx", "-.8em")
           .attr("dy", "-.55em")
           .attr("transform", "rotate(-45)");
        }

        function MonthName(index) {
            var month = new Array();
            month[1] = "Jan";
            month[2] = "Feb";
            month[3] = "Mar";
            month[4] = "Apr";
            month[5] = "May";
            month[6] = "Jun";
            month[7] = "Jul";
            month[8] = "Aug";
            month[9] = "Sep";
            month[10] = "Oct";
            month[11] = "Nov";
            month[12] = "Dec";

            return month[index];
        }


    }

})(angular.module('dealerWizard'));