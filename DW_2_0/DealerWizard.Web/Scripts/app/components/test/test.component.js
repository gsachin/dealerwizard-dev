﻿const testCtrl=($scope)=>{
    $scope.data = [[1,"abc" ],
                   [2,"abc1"],
                   [3,"abc2"],
                   [4,"abc3"],
                   [5,"abc4"]];

    console.log('test called');

}

testCtrl.$inject = ['$scope'];

angular.module('dealerWizard')
       .component('test', {
           templateUrl: "./Scripts/app/components/test/test.html",
           controller: testCtrl
       })