﻿(function (app) {

    app.component("createusertemplate", {
        templateUrl: "/Scripts/app/components/userdefinedtemplating/createusertemplate/createusertemplate.html",
        controller: ['$scope', '$element', '$attrs', 'notificationService', 'dashboardRepository','$location', createUserTemplateCtrl]
    });

    function createUserTemplateCtrl($scope, $element, $attrs,notificationService,dashboardRepository, $location) {
        var ctrl = this;
        ctrl.widgetContainer;
        ctrl.showWidgetPopup = false;
        ctrl.showTemplatePopup = true;
        ctrl.widgetName = "";
        ctrl.selectedElem = "";
        $scope.dashboardName;

        ctrl.$onInit = function () {
            ctrl.template = {
                name: "template1",
                templateInfo: [{
                    templateSize: "100%",
                    size: 12
                }],
                templateSelected: true,
                templatePath: "/Scripts/app/components/userdefinedtemplating/usertemplateloader/template1.html"
            };
            ctrl.widgetContainer = [{ ["section1"]: [] }];
        }

        $scope.loadTemplate = (template) => {
            ctrl.template = template;
            let setCount = template.templateInfo.length;
            let i = 1;
            ctrl.widgetContainer = [];
            while (i <= setCount) {
                ctrl.widgetContainer.push({ ["section"+i]: [] });
                i++;
            }
            //ctrl.widgetContainer = angular.copy(ctrl.widgetContainer)
            ctrl.showWidgetPopup = true;
        };

        $scope.selectWidget = (widgetObject) => {
            ctrl.widgetContainer[0].section1.push(widgetObject);
            ctrl.widgetContainer = angular.copy(ctrl.widgetContainer);
        }

        $scope.elementDrag = (dragWidgetDetail) => {
            console.log(dragWidgetDetail);
            var sourceIndex = dragWidgetDetail.sourceElement.substr(-1) - 1,
            destinationIndex = dragWidgetDetail.destinationElement.substr(-1) - 1;

            var obj = _.find(ctrl.widgetContainer[sourceIndex][dragWidgetDetail.sourceElement], (o) => {
                console.log(o, dragWidgetDetail.draggedObjectId);
                return o.id == dragWidgetDetail.draggedObjectId
            });

            if (obj !== undefined) {
                ctrl.widgetContainer[sourceIndex][dragWidgetDetail.sourceElement] = _.filter(ctrl.widgetContainer[sourceIndex][dragWidgetDetail.sourceElement], (o) => {
                    return o.id != dragWidgetDetail.draggedObjectId
                });

                console.log(ctrl.widgetContainer, obj);

                ctrl.widgetContainer[destinationIndex][dragWidgetDetail.destinationElement].push(obj);
                ctrl.widgetContainer = angular.copy(ctrl.widgetContainer);
                $scope.$apply();
            }
        }

        $scope.removeElement = (obj) => {
            var sourceIndex = obj.section.substr(-1) - 1;
            ctrl.widgetContainer[sourceIndex][obj.section] = _.filter(ctrl.widgetContainer[sourceIndex][obj.section], (o) => {
                return o.id != obj.id
            });
            ctrl.widgetContainer = angular.copy(ctrl.widgetContainer);
            //$scope.$apply();
        }

        $scope.editWidget = (obj) => {
            ctrl.showWidgetPopup = angular.copy(true);

            ctrl.widgetName = angular.copy(obj.name);
            var sourceIndex = obj.section.substr(-1) - 1;
            let type = _.find(ctrl.widgetContainer[sourceIndex][obj.section], (o) => {
                return o.id == obj.id;
            }).type;

            ctrl.selectedElem = angular.copy({ type: type });
        }

        $scope.openWidgetPopup = () => {
            ctrl.showWidgetPopup = angular.copy(true);
        };

        $scope.openTemplatePopup = () => {
            ctrl.showTemplatePopup = angular.copy(true);
        };

        $scope.closeTemplatePopup = () => {
            ctrl.showTemplatePopup = false;
        };

        $scope.closeWidgetPopup = () => {
            ctrl.showWidgetPopup = false;
        };

        $scope.saveDashboard = () => {

            //{
            //    name: "New Car dashboard",
            //    template: "template30x70.html",
            //    widgets: [
            //                {
            //                    name: "",
            //                    type: "kpi",
            //                    container: "section1"
            //                }
            //    ]}

            if ($scope.dashboardName == "" || $scope.dashboardName == undefined) {
                notificationService.error("Please enter dashboard name!");
                return;
            }

            var widgetObj = [];
            _.map(ctrl.widgetContainer, (o) => {
                var key = Object.keys(o)[0];
                _.each(o[key], (d) => {
                    widgetObj.push({
                        name: d.widgetName,
                        type: d.type,
                        container: key,
                        reportKey: d.reportKey
                    })
                });
            });

            var dashboarbConfig = {
                name: $scope.dashboardName,
                template: ctrl.template,
                widgets: widgetObj,
                createdOn: moment().format('YYYY-MM-DD HH:mm:ss')
            };

            var id = dashboardRepository.save(dashboarbConfig);
            notificationService.success("saved");
            $location.path('/managedashboard');
        };;
    }
})(angular.module("dealerWizard"));