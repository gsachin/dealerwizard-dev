﻿(function (app) {
    'use strict'
    app.component("usertemplateloader", {
        template: '<div class="col-lg-12" ng-include="$ctrl.template.templatePath">',
        controller: ['$scope', '$element', '$attrs', '$routeParams','$compile','widgetFactory', usertemplateloaderCtrl],
        bindings: {
            template: "<",
            widgetContainer: "<",
            onElementDrag: "&",
            removeElement: "&",
            editWidget: "&"
        }
    });

    function usertemplateloaderCtrl($scope, $element, $attrs, $routeParams, $compile, widgetFactory) {
        var ctrl = this;
        var sourceElement;
        let keyArr = [];
        let handleDragStart= (event, ui) => {
            //$(this).css('zIndex', '999');
            $(ui.helper).addClass("ui-draggable-helper");
            sourceElement = $(ui.helper[0]).parents('div.ui-droppable').attr('id');
            console.log(sourceElement);
        }

        function handleDropEvent(event, ui) {
            if (ui.draggable.lastDropObject !== undefined) {
                ui.draggable.lastDropObject.droppable('enable');
            }
            
            $('.ui-draggable-helper').removeClass('ui-draggable-helper');

            //$(this).append(ui.draggable);
            //console.log($(this).attr('id'), ui.draggable[0].id, ui);
            var destinationElement = $(this).attr('id');
            let obj = {
                sourceElement: sourceElement,
                destinationElement: destinationElement,
                draggedObjectId:ui.draggable[0].id
            };
            ctrl.onElementDrag({ ctx: obj });
            
        }

        $scope.removeWidget = (id, element) => {
            console.log(id);
            var obj = {
                id: id,
                section: element
            };
            ctrl.removeElement({ ctx: obj });

        }

        $scope.editWidget = (id , section, name) => {
            var obj = {
                id: id,
                section: section,
                name: name
            };
            ctrl.editWidget({ ctx: obj });
        }

        const getDroppableObject = () => {
            return {
                drop: handleDropEvent,
                tolerance: "touch",
            }
        }


        ctrl.$onInit = function () {
            ctrl.template = angular.copy(ctrl.template);
            ctrl.widgetContainer = ctrl.widgetContainer;
        }
        
        ctrl.$onChanges = function (ChangeObject) {            
            if (ChangeObject.template != undefined) {
                if (!ChangeObject.template.isFirstChange()) {
                    ctrl.template = angular.copy(ctrl.template);
                }
            }

            if (ChangeObject.widgetContainer != undefined) {
                if (!ChangeObject.widgetContainer.isFirstChange()) {
                    _.each(ctrl.widgetContainer, (o) => {                        
                        var key = Object.keys(o);
                        keyArr.push(key);
                        $($element[0].querySelector('div#' + key[0])).html('');                        
                        _.each(o[key[0]], (d, i) => {
                            console.log($scope);
                            var newScope = $scope.$new(true, $scope);
                            //console.log(newScope);
                            let widget = widgetFactory.getWidget({ type: d.type, context: d });
                            newScope = angular.merge(newScope, widget.data);                            
                            let html = widgetWrapper(widget.id, d, key);
                            //
                            var $elem = $($element[0].querySelector('div#' + key)).append($compile(html)($scope));
                            $elem.find('#' + widget.id + '_body').append($compile(widget.markUp)(newScope));
                            angular.extend(d, { id: widget.id });
                        });
                    });

                    $(".dragdrop").draggable({
                        start: handleDragStart,
                        cursor: 'move',
                        revert: "invalid",
                        containment: "window",
                        scroll: false,
                        drag: function (event, ui) {
                            var offset = $(this).offset();
                            var xPos = offset.left;
                            var yPos = offset.top;
                            $(this).css('zIndex', '1000');
                            $('.ui-draggable-helper').css('z-index', '1000');
                            //  $(this).text('x: ' + xPos + 'y: ' + yPos);
                            var shw_xy = 'x: ' + xPos + 'y: ' + yPos;
                            console.log(shw_xy);
                        },
                        helper: function(e) {
                            var original = $(e.target).hasClass("ui-draggable") ? $(e.target) : $(e.target).closest(".ui-draggable");
                            return original.clone().css({
                                width: original.width()
                            });
                
                        }
                    });
                    $(".dragdrop").sortable();
                    _.each(keyArr, (d) => {
                        $("#" + d).droppable(getDroppableObject());
                    });
                }
                
            }
        }

       
        function widgetWrapper(id, widgetObject,containerElemnet) {            
            var _html = `<div class="dragdrop panel panel-default" id='`+id+`'>
                    <div class ="panel-heading resp-fs-18">
                    <div style="display:inline-block" class="fw-bold"> `+ widgetObject.widgetName +` </div>
                    <div style="text-align:right;display:inline-block; float:right;">
                       <div class ="btn-group">
                        <i class ="fa fa-pencil-square-o hovicon effect-1 sub-a" title="Edit Widget" ng-click= "editWidget('`+ id +`','`+containerElemnet+`','`+ widgetObject.widgetName +`')" ></i>
                        <i class ="fa fa-times hovicon effect-1 sub-a" title="Edit Widget" ng-click="removeWidget('`+ id +`','`+containerElemnet+`')" ></i>
                        </div></div>
                    </div><div class="text-center" id="`+id+`_body"> </div><div style="clear:both;"></div></div>`;

            return _html;
        }
    }
})(angular.module("dealerWizard"));