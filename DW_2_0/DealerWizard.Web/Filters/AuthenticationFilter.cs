﻿using System.Web.Mvc.Filters;

namespace DealerWizard.Web.Filters
{
    public class AuthenticationFilter : IAuthenticationFilter
    {
        public void OnAuthentication(AuthenticationContext filterContext)
        {

            var currSession = filterContext.HttpContext.Session;
            if (currSession.IsNewSession || currSession["clsDW_DLR"] == null)
            {
                filterContext.HttpContext.Response.Redirect("~/Membership/Login.aspx", true);
            }
            else
            {
                var oDlr = (DMS.clsDW_DLR)currSession["clsDW_DLR"];
                if (!oDlr.Ready)
                {
                    filterContext.HttpContext.Response.Redirect("~/Membership/Login.aspx?err=" + filterContext.HttpContext.Server.UrlEncode("No Dlr ID Set."));
                }
            }
        }

        public void OnAuthenticationChallenge(AuthenticationChallengeContext filterContext)
        {
            //throw new NotImplementedException();
        }
    }
}