﻿using System.Text;
using System.Web.Optimization;
namespace DealerWizard.Web
{
    public class AngularTemplatesTransform : IBundleTransform
    {
        private readonly string _moduleName;

        public AngularTemplatesTransform(string moduleName)
        {
            _moduleName = moduleName;
        }

        public void Process(
            BundleContext context, BundleResponse response)
        {
            var strBundleResponse = new StringBuilder();
            strBundleResponse.AppendFormat(
                "angular.module('{0}')", _moduleName);
            strBundleResponse.Append(
                ".run(['$templateCache', function(t) {");

            foreach (var file in response.Files)
            {
                var content = file.ApplyTransforms();
                content = content
                    .Replace("'", "\\'")
                    .Replace("\r", "")
                    .Replace("\n", "");
                var path = file.IncludedVirtualPath
                    .Replace("~", "")
                    .Replace("\\", "/");
                strBundleResponse.AppendFormat(
                    "t.put('{0}','{1}');", path, content);
            }

            strBundleResponse.Append("}]);");

            response.Files = new BundleFile[] { };
            response.Content = strBundleResponse.ToString();
            response.ContentType = "text/javascript";
        }
    }
}