﻿using System.Web.Optimization;

namespace DealerWizard.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/bundles/style/thirdparty").Include(
                        "~/Content/thirdparty/bootstrap.min.css",
                        "~/Content/thirdparty/JqueryUI/JqueryUI.css",
                        "~/Content/thirdparty/JqueryUI/JQueryUITheme.css",
                        "~/Content/thirdparty/jquery.scrollbar.css",
                        "~/Content/thirdparty/date.range.picker.css",
                        "~/Content/thirdparty/rzslider.css",
                        "~/Content/thirdparty/responsive.datatables.min.css",
                        "~/Content/thirdparty/toastr.min.css"));

            bundles.Add(new StyleBundle("~/bundles/style/app").Include(
                        "~/Content/app/sb-admin-2.css",
                        "~/Content/app/dashboard.css",
                        "~/Content/app/responsive.css",
                        "~/Content/app/dealerwizard.custom.css",
                        "~/Content/app/extensions/responsive.dataTables.custom.css"));
            
            // Add the templates for the app
            var templates = new AngularTemplatesBundle("dealerWizard", "~/bundles/templates")
                .IncludeDirectory("~/Scripts/app/", "*.html", true);

            bundles.Add(templates);

            //bundles.Add(new ScriptBundle("~/bundles/d3").Include(
            //    "~/Scripts/thirdparty/d3.v3.min.js",
            //    "~/Scripts/thirdparty/d3.tip.js"));

            //bundles.Add(new ScriptBundle("~/bundles/metisMenu").Include(
            //    "~/Scripts/thirdparty/metisMenu.min.js"));

            //BundleTable.EnableOptimizations = false;

        }
    }
}
