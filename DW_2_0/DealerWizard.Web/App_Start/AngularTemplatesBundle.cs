﻿using System.Web.Optimization;
namespace DealerWizard.Web
{
    public class AngularTemplatesBundle : Bundle
    {
        public AngularTemplatesBundle(
            string moduleName, string virtualPath)
        : base(virtualPath,
            new AngularTemplatesTransform(moduleName))
        { }
    }
}