﻿using System.Web.Mvc;
using System.Web.UI;

namespace DealerWizard.Web.Controllers
{
    [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {           
            return View();
        }
    }
}