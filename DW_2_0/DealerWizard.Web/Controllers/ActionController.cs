﻿using System.Web.Mvc;

namespace DealerWizard.Web.Controllers
{
    public class ActionController : Controller
    {
        // POST: Update Selected Dealer
        [HttpPost]
        public JsonResult UpdateSelectedDealer(string dealerId)
        {
            var dealerWizardDealerInstance = new DMS.clsDW_DLR(dealerId);
            Session["clsDW_DLR"] = dealerWizardDealerInstance;
            Session["DealerName"] = dealerWizardDealerInstance.DLR_Name;
            Session["etcId"] = "";
            return Json(new { Result = "success" });
        }
    }
}